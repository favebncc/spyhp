(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[3],{

/***/ "./resources/js/installGuide.js":
/*!**************************************!*\
  !*** ./resources/js/installGuide.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

removeDarkText = function removeDarkText(e) {
  e.target.classList.remove('text-dark');
};

addDarkText = function addDarkText(e) {
  if (e.target.classList.contains('button')) {
    return;
  } else {
    e.target.classList.add('text-dark');
  }
};

var styleActiveButton = function styleActiveButton(button) {
  button.classList.remove("button-alt");
  button.classList.remove("text-dark");
  button.classList.add("button");
};

var Req = ["Android OS 4.4 atau lebih tinggi", "Mac OS X 10.13 ke atas", "Windows 8 ke atas", "Paket data yang tidak terbatas direkomendasikan"];
var Req_en = ["Android OS 4.4 or above", "Mac OS X 10.13 or above", "Windows 8 or above", "Unlimited data plan recommended."];

requirement_content = function requirement_content(idx_content_req) {
  var language = window.location.pathname.substr(1).split("/")[0];
  var full_requirement = "\n    <span>".concat(Req[idx_content_req], "</span>\n    <span>").concat(Req[3], "</span>\n    ");
  var full_requirement_en = "\n    <span>".concat(Req_en[idx_content_req], "</span>\n    <span>").concat(Req_en[3], "</span>\n    ");
  return language === "id" ? full_requirement : full_requirement_en;
};

var requirement = document.getElementById("requirement");

fill_Requirement = function fill_Requirement(device) {
  var angka = {
    android: 0,
    mac: 1,
    computer: 2
  };
  button = document.getElementById("".concat(device, "-install-button"));
  styleActiveButton(button);
  requirement.innerHTML = requirement_content(angka[device]);
};

if (document.title.match("Install Guide for Android")) {
  device = "android";
  fill_Requirement(device);
} else if (document.title.match("Install Guide for Mac")) {
  device = "mac";
  fill_Requirement(device);
} else if (document.title.match("Install Guide for Computer")) {
  device = "computer";
  fill_Requirement(device);
}

/***/ })

}]);