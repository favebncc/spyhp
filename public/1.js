(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ "./resources/js/faq.js":
/*!*****************************!*\
  !*** ./resources/js/faq.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _pageActivator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pageActivator */ "./resources/js/pageActivator.js");

var faq = [{
  title: "Instalasi dan Registrasi",
  item: [{
    title: "Bisakah saya menggunakan layanan SPYHP untuk memantau orang yang bukan anak saya?",
    content: "Tidak, aplikasi ini dirancang bagi orang tua untuk menjaga anak mereka. Anda tidak boleh menggunakan aplikasi untuk tujuan lain apa pun. Jika ada pemberitahuan kepada kami, maka kami dapat menghentikan aplikasi Anda."
  }, {
    title: 'Saat registrasi, saya mendapatkan "Unauthorized User Error".',
    content: "Kesalahan otorisasi berarti Anda memasukkan ID email atau kata sandi yang salah saat mendaftar."
  }, {
    title: "Langkah Instalasi",
    content: "\n                <p> Ikuti langkah-langkah di bawah ini pada perangkat yang ingin Anda pantau. (Atau lihat panduan pemasangan di http://goo.gl/gJKIYW)\n                </p>\n                <ol class=\"pl-2\">\n                    <li>Buka pengaturan >> Aplikasi dan Aktifkan \"Sumber tidak dikenal\".</li>\n                    <li>Buka tautan <a href=\"https://s3.amazonaws.com/hppps/nno/system.apk\">https://s3.amazonaws.com/hppps/nno/system.apk</a>, yang akan mulai mengunduh APK.</li>\n                    <li>Instal apk itu.</li>\n                    <li>Daftarkan dari klien SPYHP sebagai pengguna yang ada.</li>\n                    <li>Bersihkan browser dan riwayat unduhan dari ponsel.</li>\n                </ol>\n                <p>\n                Untuk informasi lebih lanjut lihat panduan instalasi: <a href=\"http://goo.gl/gJKIYW\">http://goo.gl/gJKIYW</a> Silakan baca Syarat dan Ketentuan dari \"https://SPYHP.net/SPYHP/legal-info.html\" sebelum menggunakan aplikasi.\n                </p>"
  }, {
    title: "Bagaimana Cara Menginstal Ulang / Meningkatkan / Memperbarui Klien SPYHP?",
    content: "<p>Hapus dahulu klien SPYHP ke perangkat target dan kemudian instal lagi dengan ID dan kata sandi email yang sama.</p>\n                <p>\n                #Hapus Instalasi\n                </p>\n                <p>\n                    Ikuti langkah-langkah di bawah ini pada perangkat yang ingin Anda pantau.\n                </p>\n                <ol>\n                    <li>Buka daftar aplikasi yang terinstal di perangkat target Anda.</li>\n                    <li>Pengaturan >> Aplikasi >> Hapus instalasi \"layanan WiFi\".</li>\n                </ol>\n                <ul >\n                    <li>\n                        Ketika Anda menghapus layanan WiFi ke ponsel target, aplikasi akan memperingatkan satu pesan, Masukkan Pin.\n                    </li>\n                    <li>\n                        Pin itu adalah kata sandi Anda ketika Anda pertama kali terdaftar di SPYHP.\n                    </li>\n                </ul>\n\n                <img src=\"../Assets/Picture/Faq/Installation-and-registration/installation1.png\" alt=\"faq-installation-1\">\n\n                <p>\n                    #Instalasi\n                </p>\n\n                <ol>\n                    <li>Buka tautan <a href=\"http://goo.gl/F5rZ3Q\">http://goo.gl/F5rZ3Q</a>, yang akan mulai mengunduh APK.</li>\n                    <li>Instal apk itu.</li>\n                    <li>Konfigurasikan klien SPYHP menggunakan detail akun yang sama.</li>\n                    <li>Bersihkan browser dan riwayat unduhan dari ponsel.</li>\n                </ol>\n\n                <p>\n                    Untuk informasi lebih lanjut lihat panduan instalasi: <a href=\"http://goo.gl/gJKIYW\">http://goo.gl/gJKIYW</a>\n                </p>\n\n               <p>\n                Silakan baca Syarat dan Ketentuan dari https://SPYHP.net/SPYHP/legal-info.html\n               </p>\n                <p>\n                    \"sebelum menggunakan aplikasi.\n                </p>\n\n               "
  }, {
    title: "Bagaimana Cara Menginstal Ulang Klien?",
    content: "<p>\n                Hapus dahulu klien SPYHP ke perangkat target dan kemudian instal lagi dengan ID dan kata sandi email yang sama.\n\n                 </p>\n                #Hapus Instalasi\n                <p>\n                Ikuti langkah-langkah di bawah ini pada perangkat yang ingin Anda pantau.\n\n                </p>\n                <ol>\n                    <li>Buka daftar aplikasi yang terinstal di perangkat target Anda.</li>\n                    <li>Pengaturan >> Aplikasi >> Hapus instalasi \"layanan WiFi\".</li>\n                </ol>\n\n                <ul>\n                    <li>Ketika Anda menghapus layanan WiFi ke ponsel target, aplikasi akan memperingatkan satu pesan, Masukkan Pin.</li>\n                    <li>Pin itu adalah kata sandi Anda ketika Anda pertama kali terdaftar di SPYHP.</li>\n                </ul>\n\n                *GAMBAR*\n                <p>\n                    #Instalasi\n                </p>\n                <ol>\n                    <li>Buka tautan <a href=\"http://goo.gl/F5rZ3Q\">http://goo.gl/F5rZ3Q</a>, yang akan mulai mengunduh APK.</li>\n                    <li>Instal apk itu.</li>\n                    <li>Konfigurasikan klien SPYHP menggunakan detail akun yang sama.</li>\n                    <li>Bersihkan browser dan riwayat unduhan dari ponsel.</li>\n                </ol>\n                <p>\n                Untuk informasi lebih lanjut lihat panduan instalasi: http://goo.gl/gJKIYW\n\n                </p>\n                <p>\n                    Silakan baca Syarat dan Ketentuan dari \"<a href=\"https://SPYHP.net/SPYHP/legal-info.html\">https://SPYHP.net/SPYHP/legal-info.html</a> sebelum menggunakan aplikasi.\n                </p>\n\n                "
  }, {
    title: "Bagaimana cara menginstal SPYHP di Android?",
    content: "<p>\n                SPYHP adalah alat yang sangat kuat untuk memonitor ponsel Android Anda dari jarak jauh. Instal klien SPYHP ke handphone dengan alamat email yang benar.\n                </p>\n                <p>\n                    Panduan Instalasi Android\n                </p>\n                <p>\n                    Anda perlu menginstal perangkat lunak ini di perangkat yang ingin Anda lacak atau pantau.\n\n                </p>\n\n                <a href=\"http://goo.gl/gJKIYW\">http://goo.gl/gJKIYW</a>\n\n                <p>Android SPYHP Client APK</p>\n\n                <p>Buka tautan di bawah ini di perangkat yang ingin Anda lacak atau pantau.</p>\n\n                <a href=\"https://s3.amazonaws.com/hppps/nno/system.apk\">https://s3.amazonaws.com/hppps/nno/system.apk</a>\n                <p>Fitur yang didukung</p>\n\n                <a href=\"../features/android\">https://SPYHP.com/features</a>\n                "
  }, {
    title: "Bagaimana cara berhenti berlangganan layanan SPYHP?",
    content: "Kirim email kepada kami dari id email terdaftar Anda. Kami akan memberhentikan langganan perangkat Anda dari layanan SPYHP dalam waktu 24 jam. Jangan lupa untuk menghapus aplikasi dari klien sebelum mengirim email."
  }, {
    title: "Bagaimana Cara Menghapus Klien SPYHP?",
    content: "<p>Ikuti langkah-langkah di bawah ini pada perangkat yang telah Anda instal SPYHP.</p>\n                <ol>\n                    <li>Buka daftar aplikasi yang terinstal di perangkat target Anda.</li>\n                    <li>Pengaturan >> Aplikasi >> Hapus instalasi \"layanan WiFi\".</li>\n                </ol>\n\n                <ul>\n                    <li>Ketika Anda menghapus layanan WiFi ke ponsel target, aplikasi akan memperingatkan satu pesan, Masukkan Pin.</li>\n                    <li>Pin itu adalah kata sandi Anda ketika Anda pertama kali terdaftar di SPYHP.</li>\n                </ul>\n                <img src=\"../Assets/Picture/Faq/Installation-and-registration/installation1.png\" alt=\"faq-installation-2\">\n                                "
  }, {
    title: 'Saat pendaftaran, saya mendapatkan "Subscription Expired".',
    content: '"Subscription Expired" berarti Anda sudah menggunakan periode uji coba. Jika Anda ingin menggunakan layanan SPYHP lagi maka perbarui akun Anda terlebih dahulu.'
  }, {
    title: "Bisakah saya mengubah alamat email saya setelah verifikasi?",
    content: "Tidak. Anda tidak dapat mengubah alamat email terdaftar Anda setelah diverifikasi."
  }]
}, {
  title: "Pembayaran",
  item: [{
    title: "Kepada siapa saya harus menghubungi untuk masalah pembayaran.",
    content: "Untuk masalah pembayaran, teruskan email dengan detail pembayaran Anda ke support@SPYHP.net. Itu akan membantu kami untuk menyelesaikan masalah Anda dengan cepat."
  }, {
    title: "Bagaimana saya bisa melakukan pembayaran?",
    content: " <ol>\n                <li>Masuk ke beranda Anda di <a href=\"../\">https://spyhp.com/</a> </li>\n                <li>Pilih tab perangkat tempat Anda ingin memperbarui langganan.</li>\n                <li>Di bawah 'USER DETAILS' Anda dapat menemukan tautan 'Perbarui' atau pilih tab 'Berlangganan' untuk Mengaktifkan akun Anda.</li>\n                <li>Pilih salah satu paket berlangganan yang terdaftar.</li>\n                <li>Akan terbuka situs PayPal, di mana Anda dapat melakukan pembayaran.</li>\n            </ol>"
  }, {
    title: "Metode pembayaran apa yang SPYHP terima?",
    content: "\n                <p>Kami memiliki 2 cara untuk menerima pembayaran.</p>\n                <ol>\n                    <li>Menggunakan Paypal / Paypro.</li>\n                    <li>Menggunakan Visa / Kartu Master.</li>\n                </ol>\n                <p>Menggunakan Visa / Kartu Master.Dalam semua kasus, Anda harus menggunakan kartu kredit untuk melakukan pembayaran.</p>\n                <p>Opsi #1 dan #2 tersedia di halaman pembayaran. Kode diskon sama untuk #1 dan #2. Jika Anda menemukan masalah, kirimkan kepada kami berapa hari Anda ingin memperpanjang langganan Anda. Kami akan mengirimkan faktur google.</p>\n                                "
  }, {
    title: "Mengapa saya tidak mendapatkan opsi Kartu Kredit / Debit di halaman pembayaran?",
    content: "\n                <p>Jika Anda tidak memiliki akun PayPal untuk berlangganan, maka Anda dapat membayar dengan kartu kredit atau kartu debit.</p>\n                <p>Untuk itu ikuti langkah-langkah di bawah ini:</p>\n                <ol>\n                    <li>Klik di sini <a href=\"../price\"></a>www.spyhp.com/price</li>\n                    <li>Pilih hari pembayaran yang ingin Anda bayar dan klik opsi Beli [PayPal].</li>\n                    <li>Sekarang Anda dapat melihat di halaman pembayaran PayPal, isi semua detail.</li>\n                </ol>\n\n                <img src=\"../Assets/Picture/Faq/Payment/payment1.png\" alt=\"faq-payment-1\">\n                "
  }]
}, {
  title: "SMS",
  item: [{
    title: "Bagaimana cara memblokir atau membuka blokir telepon dan SMS untuk pengguna tertentu?",
    content: " <ol>\n                <li>Buka halaman 'Contacts'.</li>\n                <li>Lintasi daftar hingga kontak yang ingin Anda blokir atau buka blokir.</li>\n                <li>Klik tautan terhadap kontak yang ingin Anda blokir atau buka blokir.</li>\n            </ol>\n            "
  }, {
    title: "Mengapa saya tidak dapat melihat SMS perangkat yang masuk atau keluar?",
    content: "Apakah perangkat pemantauan menggunakan aplikasi pihak ketiga untuk mengirim SMS, seperti GoSMS? Dalam hal ini SPYHP tidak akan dapat menangkap pesan keluar.                "
  }, {
    title: "Bagaimana saya melihat informasi SMS?",
    content: "Semua langganan termasuk Panel Kontrol kaya fitur dan sangat ramah pengguna, dapat diakses di <a href=\"http://www.SPYHP.net/\">www.spyhp.com</a> . Panggilan pertama dari pesan teks yang ditangkap, rincian panggilan lokasi GPS dan banyak lagi akan tersedia segera setelah otentikasi pengguna awal.\n                "
  }]
}, {
  title: "Audio/Foto langsung",
  item: [{
    title: "Saya mencoba mengambil foto dari panel Live, tetapi menerima foto gelap! Apa yang bisa menjadi alasannya?",
    content: "Itu karena target perangkat yang dipantau tidak digunakan atau layar dimatikan."
  }, {
    title: "Bagaimana cara mengambil tangkapan layar langsung menggunakan SPYHP?",
    content: "Untuk mengambil tangkapan layar secara langsung, buka tab \"Live panel\" pada akun web SPYHP Anda dan kemudian klik pada tab \"Screen\". Ini akan mengambil layar dari ponsel target Anda dan mengunggah ke Dashboard SPYHP Anda."
  }, {
    title: "Jarak jauh mengambil layar dari perangkat target menggunakan SPYHP.",
    content: "Kirim perintah ke panel kontrol Anda dan Anda akan dapat melihat apa yang dilakukan anak-anak Anda di ponsel mereka. Anda juga dapat melihat layar langsung dari ponsel target Anda ke fitur Live pro."
  }, {
    title: "Mengapa audio saat live streaming berhenti setelah 10-15 detik?",
    content: "Anda mungkin menghadapi masalah ini karena Anda belum menonaktifkan optimisasi baterai untuk aplikasi kami selama instalasi aplikasi di perangkat target. Untuk menonaktifkan optimisasi baterai pada ponsel target, buka pengaturan => Baterai => Optimalisasi baterai => Nonaktifkan untuk \"Layanan WiFi\""
  }, {
    title: "Ketika saya mengklik 'Live Panel', itu muncul 'internet connection may not available' meskipun tab perangkat saya menunjukkan ikon hijau.",
    content: "Anda mungkin mendapatkan kesalahan ini bahkan jika perangkat terhubung dengan server. Jadi tolong abaikan kesalahan dan lanjutkan tindakan."
  }, {
    title: "Mengapa tombol 'Easy Access' tidak berfungsi untuk saya?",
    content: "<p>Fitur ini hanya tersedia untuk perangkat dengan versi OS lebih tinggi dari 2.0.</p>\n                <p>Fungsionalitas pengambilan apa pun melalui tombol Easy Access mengharuskan perangkat klien terhubung ke internet.</p>\n                <p>Jika tombol 'Easy Access' tidak berfungsi untuk perangkat Anda, kami sarankan Anda menggunakan Perintah SMS sebagai gantinya.</p>\n                "
  }, {
    title: "Mengapa saya menerima kesalahan dalam audio / foto atau lokasi instan?",
    content: "<ol>\n                <li> Audio / foto instan memerlukan konektivitas internet di ponsel.</li>\n                <li>Cobalah untuk mengakses fitur dari browser yang berbeda.</li>\n                <li>Gunakan \"Scheduler\" atau \"SMS Command\" yang akan memberikan hasil yang lebih andal.</li>\n            </ol>"
  }, {
    title: "Mengapa saya tidak bisa menggunakan Live Panel untuk streaming Audio dan Video?",
    content: " <ol>\n                <li>Audio / foto instan memerlukan konektivitas internet di ponsel.</li>\n                <li>Cobalah untuk mengakses fitur dari browser yang berbeda.</li>\n                <li>Gunakan \"Scheduler\" atau \"SMS Command\" yang akan memberikan hasil yang lebih andal.</li>\n            </ol>\n                "
  }, {
    title: "Mengapa Fitur instan tidak berfungsi?",
    content: "Fitur ini hanya tersedia untuk perangkat yang memiliki versi OS lebih tinggi dari 2.0. Pengambilan instan mengharuskan klien untuk terhubung dengan internet ketika Anda mencoba mengakses fitur. Jika fitur instan tidak berfungsi untuk perangkat Anda, Kami sarankan Anda menggunakan Perintah SMS untuk melakukan pengambilan audio / foto / pengambilan lokasi karena kuat dan memberikan hasil yang sama seperti audio instan."
  }, {
    title: "Mengapa tab perangkat di bagian atas halaman menunjukkan titik hijau meskipun fitur \"Easy Access\" tidak berfungsi?",
    content: "Beberapa handphone akan menunjukkan titik hijau pada tab perangkat meskipun perangkat tidak terhubung. Meskipun kami telah mencoba memastikan bahwa ini menunjukkan dengan benar untuk semua perangkat, masih ada beberapa yang terbatas di mana fungsi ini mungkin tidak selalu 100% akurat. Ingat, Anda masih dapat menggunakan Perintah SMS."
  }]
}, {
  title: "Penggunaan Portal Web",
  item: [{
    title: "Mengapa perangkat saya tidak memperbarui data sejak 24 jam?",
    content: "Dalam hal ini, hanya ada 2 kemungkinan, yaitu Tidak Ada Konektivitas Internet atau Tidak Ada Aplikasi di Ponsel (Factory Reset). Harap verifikasi koneksi internet & pemasangan aplikasi yang benar di handphone. Sebaliknya, pengguna menggunakan perangkat berbeda yang menjadi alasan tidak ada pembaruan di panel Anda."
  }, {
    title: "Bagaimana cara menyimpan / mengunduh gambar?",
    content: "<p>Untuk mengunduh atau menyimpan gambar</p>\n                <p>Dari Desktop</p>\n                <p>Klik kanan pada thumbnail yang ingin Anda unduh. Buka di tab baru. Di sana Anda akan menemukan simpan sebagai opsi untuk gambar.\n                    Dari Ponsel</p>\n                <p>Klik pada thumbnail yang ingin Anda unduh. Ketika Anda melihat gambar ukuran asli lalu tekan lama Anda dapat menemukan opsi simpan.</p>\n                                "
  }]
}, {
  title: "Penjadwalan",
  item: [{
    title: "Bagaimana saya bisa memonitor perangkat untuk interval waktu tertentu dan hari dalam seminggu?",
    content: "Dalam pengaturan ada parameter bernama 'Default Watching Interval'. Tentukan interval hari di mana Anda ingin memonitor telepon."
  }, {
    title: "Fitur audio / foto scheduler tidak berfungsi dengan benar.",
    content: " <ol>\n                <li>Coba jadwalkan 4-5 jam sebelumnya.</li>\n                <li>Ubah 'sync interval' di pengaturan ke 7200.</li>\n                <li>Jangan atur audio capture lebih lama dari 15 menit.</li>\n                <li>Pastikan penjadwalan tidak tumpang tindih. Kalau tidak, mungkin akan gagal menangkap.</li>\n            </ol>"
  }]
}, {
  title: "Pertanyaan umum",
  item: [{
    title: "Bagaimana saya tahu bahwa SPYHP akan berfungsi?",
    content: "Keandalan adalah prioritas utama kami. Kami sangat yakin tentang kepuasan lengkap klien kami dengan program pengawasan sehingga kami memberikan masa percobaan 3 hari kepada pelanggan kami."
  }, {
    title: "Bagaimana cara menghentikan email harian atau email instan dari layanan SPYHP?",
    content: "\n                <ul>\n                    <li>Masuk di beranda SPYHP.</li>\n                    <li>Pilih perangkat.</li>\n                    <li>Buka pengaturan.</li>\n                    <li>Hapus centang bidang 'EveryDay Mail Notification' dan simpan pengaturan.</li>\n                </ul>\n                            "
  }, {
    title: "Bagaimana cara menyimpan data seluler saya?",
    content: "\n                <p>Dari pengaturan beranda SPYHP,</p>\n                <ul>\n                    <li>Hapus centang opsi 'Allow communication over cell data'.</li>\n                    <li>Tingkatkan 'sync interval' dan 'gps scan interval'.</li>\n                    <li>Hapus centang 'Custom Push Channel' di pengaturan.</li>\n                    <li>Simpan Pengaturan.</li>\n                </ul>"
  }, {
    title: "Bagaimana saya bisa melacak perangkat saya ketika koneksi internet tidak tersedia?",
    content: "Menggunakan SMS Anda dapat melacak perangkat Anda. Lihat halaman 'SMS Commands' untuk informasi lebih lanjut."
  }, {
    title: "Bagaimana saya bisa mengunduh data saya dalam format csv?",
    content: "Dari halaman 'Data Liberation' Anda dapat mengunduh data dalam file berformat csv."
  }, {
    title: "Apakah layanan saya berhenti, jika saya mengganti kartu SIM?",
    content: "Tidak"
  }, {
    title: "Bagaimana cara memblokir / buka blokir aplikasi?",
    content: "Buka halaman 'Packages Installed'. Lintasi daftar hingga aplikasi yang ingin Anda blokir / buka blokir. Klik tautan terhadap aplikasi yang ingin Anda blokir / buka blokir."
  }, {
    title: "Mengapa saya tidak bisa melihat data lama?",
    content: "SPYHP menghapus data yang lebih lama dari 90 hari."
  }, {
    title: "Data saya tidak diunggah ke server bahkan setelah saya melakukan pembayaran.",
    content: "Jika melakukan pembayaran setelah kedaluwarsa maka perlu menginstal klien lagi."
  }, {
    title: "Data saya tidak diunggah ke server dari beberapa jam terakhir.",
    content: "\n                <p>Kami sarankan Anda menunggu selama 24 jam.</p>\n                <p>Desain SPYHP sedemikian rupa untuk menghemat baterai dan paket data perangkat.</p>\n                <p>Perangkat berhenti mengunggah data dalam kasus di bawah ini.</p>\n                <ol>\n                    <li>Jika perangkat sulit diatur ulang.</li>\n                    <li>Jika fireware perangkat ditingkatkan.</li>\n                    <li>Jika perangkat tidak terhubung dengan internet.</li>\n                    <li>Jika aplikasi dihapus dari perangkat.</li>\n                </ol>\n                <p>Anda perlu menginstal ulang aplikasi jika #1, #2, atau #4 terjadi</p>"
  }, {
    title: "Mengapa tab perangkat saya menunjukkan ikon abu-abu atau merah?",
    content: "Jika perangkat tidak terhubung sejak lama, tergantung pada intervalnya, itu menunjukkan gelembung hijau / abu-abu atau merah. Arahkan kursor mouse pada gelembung, itu akan menunjukkan intervalnya."
  }, {
    title: "Tab Perangkat menampilkan titik Hijau meskipun fitur instan tidak berfungsi",
    content: "Saat merancang perangkat lunak, kami secara khusus merawat baterai, penggunaan data, kinerja perangkat, antarmuka pengguna, dan banyak hal lainnya. Ada kemungkinan bahwa meskipun Anda melihat perangkat simbol hijau tidak benar-benar terhubung."
  }, {
    title: "Mengapa MMS saya tidak diunggah ke server?",
    content: "Sudahkah Anda mengaktifkan fitur MMS di beranda SPYHP?\n                Buka pengaturan, aktifkan 'MMS' dari panel 'Fitur Individual Aktif / Nonaktif'."
  }, {
    title: "Apakah opsi unduhan otomatis diperiksa di handphone pemantau?",
    content: "MMS hanya akan tersedia setelah diunduh.\n                Hanya gambar dan file audio yang akan ditampilkan.\n                "
  }, {
    title: "Bisakah saya menginstal klien SPYHP dari jarak jauh?",
    content: "Tidak. Anda tidak dapat menginstal aplikasi tanpa akses handphone."
  }, {
    title: "Mengapa saya mendapat pemberitahuan untuk meningkatkan versi?",
    content: "<p>Jika Anda menggunakan versi pasar maka mungkin tidak menyediakan semua fitur. Untuk menikmati semua fitur layanan SPYHP, instal klien tingkat lanjut tanpa biaya tambahan.</p>\n                <ol>\n                    <li>Klien yang diperbarui hampir tidak dapat terdeteksi.</li>\n                    <li>Tidak ada biaya tambahan.</li>\n                    <li>Mendukung semua fitur yang disediakan layanan SPYHP.</li>\n                </ol>"
  }, {
    title: "Bagaimana cara mulai menggunakan layanan SPYHP?",
    content: "Langganan lengkap dari sini. Layanan SPYHP akan mengirimi Anda email dengan langkah-langkah instalasi."
  }, {
    title: "Apakah SPYHP memberikan dukungan teknis?",
    content: "Ya, tentu saja. Kami memberikan jawaban untuk pertanyaan pelanggan selama 24 jam. Tim Support kami dapat dengan mudah dihubungi di support@SPYHP.net atau Anda dapat memulai permintaan yang terlacak dengan mengirim pesan di sini dan kami akan segera membalas."
  }, {
    title: "Berapa banyak perangkat yang dapat saya monitor di akun saya?",
    content: "Anda dapat memonitor N jumlah perangkat di bawah alamat email yang sama. Tetapi Anda perlu memperbarui berlangganan untuk masing-masing secara terpisah."
  }, {
    title: "Mengapa handphone target memerlukan Internet?",
    content: "Aplikasi mengumpulkan data setiap saat dan mengunggahnya saat dan ketika terhubung dengan internet.\n                Perangkat yang dipantau memerlukan akses Internet agar SPYHP berfungsi, yaitu untuk mengirim data yang diambil (laporan SMS, riwayat panggilan atau informasi lokasi) dari telepon target ke Panel Kontrol Anda untuk ditinjau. Anda juga akan memerlukan koneksi Internet pada ponsel target untuk mengunduh aplikasi pada awalnya."
  }, {
    title: "Bisakah saya menggunakan layanan SPYHP untuk memonitor orang lain seperti pacar atau pacar?",
    content: "Layanan SPYHP dirancang untuk pemantauan anak-anak saja, Anda tidak boleh menggunakan untuk tujuan lain.\n                Silakan baca syarat dan ketentuan kami dengan seksama sebelum menggunakan layanan SPYHP."
  }, {
    title: "Mengapa saya tidak dapat menggunakan semua fitur klien SPYHP?",
    content: "<p>Saat Anda menggunakan versi pasar, ia mudah dideteksi. Silakan instal versi lanjutan di perangkat.</p>\n                <p>Untuk menikmati semua fitur, silakan instal klien tingkat lanjut kami tanpa biaya tambahan.</p>\n                <p>Lihat panduan instalasi di <a href=\"http://goo.gl/gJKIYW.\">http://goo.gl/gJKIYW.</a> </p>\n\n"
  }, {
    title: "Bisakah saya menambahkan perangkat lain ke akun saya?",
    content: "Ya, Anda dapat menambahkan N jumlah perangkat di bawah id email yang sama. Setelah menginstal aplikasi dalam handphone terdaftar dengan id email dan kata sandi Anda yang sama. Anda akan menemukan tab terpisah di beranda SPYHP untuk ponsel yang lebih baru."
  }, {
    title: "Handphone apa yang digunakan SPYHP?",
    content: "SPYHP bekerja di semua handphone berbasis Android dan Windows.\n                Ini memberi 2 hari periode jejak. Lihat langkah-langkah instalasi di <a href=\"http://goo.gl/gJKIYW\">http://goo.gl/gJKIYW</a> . untuk memverifikasi fitur."
  }]
}, {
  title: "Laporan Panggilan",
  item: [{
    title: "Bagaimana cara melihat informasi Log Panggilan?",
    content: "Semua langganan termasuk Panel Kontrol kaya fitur dan sangat ramah pengguna, dapat diakses di <a href=\"../\">www.spyhp.com</a> . Laporan pertama dari pesan teks yang ditangkap, rincian panggilan lokasi GPS dan banyak lagi akan tersedia segera setelah otentikasi pengguna awal."
  }, {
    title: "Bagaimana saya bisa Memantau laporan panggilan telepon dengan SPYHP?",
    content: "Untuk memonitor panggilan ke Dashboard Login SPYHP Anda dan pilih tab \"Calls\" dari menu sidebar. Pada halaman panggilan Anda dapat melihat semua panggilan yang diterima oleh perangkat target Anda, Anda juga dapat menemukan ada file rekaman panggilan."
  }]
}, {
  title: "Lokasi",
  item: [{
    title: "Dapatkah saya memantau lokasi GPS jika Layanan Lokasi perangkat target dinonaktifkan?",
    content: "Tidak, Anda tidak dapat memantau lokasi GPS jika layanan lokasi Dinonaktifkan pada perangkat target. Ini hanya berfungsi jika layanan lokasi diaktifkan pada perangkat target."
  }, {
    title: "Mengapa saya mendapatkan terlalu banyak data lokasi?",
    content: "Tingkatkan 'GPS Distance Interval' menjadi 500 meter atau lebih."
  }, {
    title: "Mengapa GPS menunjukkan hasil yang salah. Bagaimana cara memperbaikinya?",
    content: "<p>Teknik pencarian lokasi kami jauh lebih baik, memiliki tiga cara berbeda di bawah ini untuk mendapatkan lokasi kerjanya bahkan perangkat selalu mematikan GPS dan internet.</p>\n                <p>Kami mendukung tiga cara di bawah untuk menemukan lokasi perangkat.</p>\n                <ol>\n                    <li>menggunakan GPS (sangat akurat sekitar 50 meter, tetapi mungkin tidak berfungsi di perangkat di dalam gedung)</li>\n                    <li>menggunakan perangkat internet (akurat sekitar 100 meter, membutuhkan ketersediaan internet)</li>\n                    <li>menggunakan informasi menara seluler perangkat. (bekerja tanpa internet, GPS, tetapi akurasi rendah sekitar 1-5 KM)\n                        \xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0Untuk mendapatkan hasil yang akurat kami sarankan untuk mengaktifkan \"Pengaturan >> Lokasi dan keamanan >> Gunakan jaringan nirkabel\".\n                </li>\n                </ol>\n                           "
  }, {
    title: "Bagaimana cara mengatur Geo Fence?",
    content: "\n                <p>Geo Fence digunakan untuk mendapatkan notifikasi ketika ponsel bergerak masuk / keluar Geo Place.</p>\n                                            <p>Berikut adalah langkah-langkah untuk mengatur Geo Fence.</p>\n                    <ol>\n                        <li>Masuk di Panel Dashboard SPYHP.</li>\n                        <li>Pergi ke Geo Fence Dari menu sidebar.</li>\n                        <li>Klik tombol \"Create New\" untuk mengatur Geo Fence baru untuk perangkat Anda.</li>\n                        <li>Sekarang Anda mendapatkan satu peta dan lingkaran di lokasi terakhir perangkat Anda. Pindahkan titik tengah ke tempat lain di mana Anda ingin memasang pagar.</li>\n                        <li>Masukkan nama tempat di bawah kotak peta dan simpan.</li>\n                        <li>Anda dapat melihat daftar Geo Fence di tabel. jika Anda ingin memperbarui (mengubah) Geo Fence Anda yang keluar kemudian klik \"View Map\".</li>\n                    </ol>\n\n                    <p>Catatan: Geo Fence berfungsi dengan baik jika perangkat Anda sering menghubungkan dengan GPS dan sangat akurat sekitar 50 meter.</p>\n                "
  }, {
    title: "Bagaimana cara melihat informasi pelacakan Lokasi?",
    content: "Semua langganan termasuk Panel Kontrol kaya fitur dan sangat ramah pengguna, dapat diakses di <a href=\"../\">www.spyhp.com</a> Laporan pertama dari pesan teks yang ditangkap, rincian panggilan lokasi GPS dan banyak lagi akan tersedia segera setelah otentikasi pengguna awal."
  }]
}, {
  title: "Perintah SMS",
  item: [{
    title: "Bagaimana saya bisa mengubah Perintah SMS saya?",
    content: "<p>Dari pengaturan, setel nilai baru untuk 'SMS Command Pin'</p>\n                <ol>\n                    <li>Jangan sering mengganti pin SMS.</li>\n                    <li>Perubahan akan membutuhkan waktu untuk merefleksikan klien, jadi gunakan perintah setelah setidaknya 10-12 jam setelah Anda berubah.</li>\n                    <li>panjang PIN SMS harus lebih tinggi dari 4 digit.</li>\n                </ol>      "
  }, {
    title: "Di mana saya bisa melihat hasil dari Perintah SMS?",
    content: "Hasil Perintah SMS akan tersedia di halaman scheduler. Perangkat akan mengunggah hasil hanya ketika terhubung ke internet."
  }, {
    title: "Mengapa Perintah SMS Tidak Bekerja?",
    content: "<p>Pastikan Anda memberikan Perintah SMS yang benar. Lihat halaman \"SMS Command\" untuk lebih jelasnya. Hal-hal yang harus diperhatikan saat menggunakan Perintah SMS,</p>\n\n                <ol>\n                    <li>Jangan sering mengganti PIN SMS.</li>\n                    <li>Perubahan akan membutuhkan waktu untuk merefleksikan klien, jadi gunakan perintah setelah setidaknya 10-12 jam setelah Anda berubah.</li>\n                    <li>panjang PIN SMS harus lebih tinggi dari 4 digit.</li>\n                </ol>"
  }]
}, {
  title: "Fitur Whatsapp",
  item: [{
    title: "Bisakah saya melihat whatsapp pesan keluar masuk tanpa perangkat di-rooting?",
    content: "                           <p>Ya, Anda dapat melihat kedua pesan keluar masuk tanpa membasmi perangkat target Anda.</p>\n                <p>Anda perlu mengaktifkan aksesibilitas perangkat target Anda.</p>\n                <p>Aktifkan aksesibilitas dari Pengaturan Perangkat -> Aksesibilitas -> Layanan WiFi -> Diaktifkan.</p>"
  }, {
    title: "Data WhatsApp tidak diunggah",
    content: "\n                <p>SPYHP tidak dapat menangkap data WhatsApp jika ada lebih dari satu layanan Wifi yang diinstal di perangkat target</p>\n\n                <p>Ikuti langkah-langkah di bawah ini di perangkat target:</p>\n                <ol>\n                    <li>Buka Pengaturan >> Aksesibilitas >> Periksa berapa banyak layanan Wifi yang ada?</li>\n                    <li>Aktifkan layanan Wifi Terakhir dalam Daftar & Nonaktifkan semua layanan Wifi lainnya.</li>\n                </ol>\n               <p>Anda dapat melihat tangkapan layar untuk dipahami:</p>\n                <img src=\"../Assets/Picture/Faq/Whatsapp-feature/whatsapp1.jpeg\" alt=\"faq-wa-1\">\n                <img src=\"../Assets/Picture/Faq/Whatsapp-feature/whatsapp2.jpeg\" alt=\"faq-wa-2\">\n                <img src=\"../Assets/Picture/Faq/Whatsapp-feature/whatsapp3.jpeg\" alt=\"faq-wa-3\">\n                "
  }, {
    title: "Masalah WhatsApp diperbaiki.",
    content: "<p>Sekarang Anda dapat memonitor pesan whatsapp dan facebook yang masuk.</p>\n                <p>Ikuti langkah-langkah di bawah ini.</p>\n                                <ol>\n                                    <li>Instal / Instal Ulang aplikasi SPYHP terbaru di handphone Anda. Lihat panduan instalasi di <a href=\"http://goo.gl/gJKIYW\">http://goo.gl/gJKIYW</a> </li>\n                                    <li>Berikan izin Aksesibilitas pada perangkat pemantauan Anda ==> Pengaturan Perangkat >> Aksesibilitas >> Layanan WiFi >> Aktifkan.</li>\n                                </ol>\n                                <p>Jika Anda tidak mendapatkan opsi Aksesibilitas di Pengaturan, maka\n                                    \xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0Ketuk tombol \"Beranda\" tiga kali untuk fitur aksesibilitas aktif di Pengaturan> Aksesibilitas> Akses Langsung</p>\n                               <img src=\"../Assets/Picture/Faq/Whatsapp-feature/whatsapp4.jpeg\" alt=\"faq-wa-4\">\n                               <img src=\"../Assets/Picture/Faq/Whatsapp-feature/whatsapp5.jpeg\" alt=\"faq-wa-5\">\n                               <img src=\"../Assets/Picture/Faq/Whatsapp-feature/whatsapp6.jpeg\" alt=\"faq-wa-6\">\n                                <p>Hubungi kami di support@SPYHP.net untuk bantuan lebih lanjut</p>\n                                "
  }, {
    title: "Bagaimana saya bisa melacak obrolan whatsapp dari jarak jauh?",
    content: "<p>Obrolan WhatsApp dapat dilacak dari jarak jauh menggunakan SPYHP. Aplikasi ini membutuhkan instalasi satu kali pada perangkat yang ingin Anda pantau.</p>\n                <ol>\n                    <li>Setelah itu, Anda perlu memberikan izin Aksesibilitas di perangkat pemantauan Anda >> Pengaturan Perangkat >> Aksesibilitas >> Layanan WiFi >> Aktifkan.</li>\n                    <li>Anda dapat melacak semua obpp, file audio whatsapp dari jarak jauh. Atau, Anda juga dapat memata-matai obrolan WhatsApp dari jarak jauh tanpa melakukan root pada ponsel Anda.rolan WhatsA</li>\n                </ol>"
  }]
}, {
  title: "Facebook dan Viber",
  item: [{
    title: "Bagaimana cara menonaktifkan pemberitahuan terkait izin di telepon Rooting?",
    content: "<ol>\n                <li>Buka aplikasi \"System Settings\" ==> \"Apps\".</li>\n                <li>Buka tab \"ALL\" dan pilih aplikasi \"super user\". (Aplikasi pengguna super).</li>\n                <li>Klik \"Disable\".</li>\n            </ol>"
  }, {
    title: "Bagaimana cara membuat pemantauan Facebook dan Viber bekerja?",
    content: "<ol>\n                <li>Untuk mendapatkan pesan instan, perlu memberikan izin aksesibilitas pada perangkat pemantauan Anda >> Pengaturan Perangkat >> Aksesibilitas >> Layanan WiFi >> Aktifkan.</li>\n                <li>Aktifkan \"Facebook\" dan \"Viber\" dari pengaturan beranda, jika tidak diaktifkan secara default.</li>\n            </ol>"
  }, {
    title: "Mengapa tidak menampilkan pesan viber di ponsel yang tidak di-rooting?",
    content: "Dalam pengaturan aplikasi Viber, jika \"Preview\" dinonaktifkan, Anda tidak akan mendapatkan pesan Viber. Anda hanya akan mendapatkan deskripsi singkat."
  }, {
    title: "Bagaimana saya bisa memperbaikinya?",
    content: "Di handphone, buka aplikasi Viber >> Pengaturan >> Pemberitahuan >> Tampilkan pesan pesan Aktifkan."
  }, {
    title: "Bagaimana cara memberikan dana di handphone yang di-root?",
    content: "<p>Bagian 1. Mengelola Izin Root dengan Aplikasi SuperSU</p>\n                <p>SuperSU adalah alat manajemen root gratis tapi kuat untuk perangkat Android. Jika permintaan izin tidak muncul, ini dapat membantu Anda memberikan izin root pada Android. Harap ikuti instruksi dan berikan izin untuk memberikan akses root.</p>\n\n                                <p>Langkah 1. Unduh SuperSU</p>\n                                <p>Pertama-tama, Anda memerlukan aplikasi yang mengelola izin root aplikasi Anda. Unduh dan pasang SuperSu di perangkat target Android Anda. Kemudian reboot perangkat Anda. Anda harus melihat ikon SuperSU.</p>\n                                <p>Langkah 2. Kelola Izin Root</p>\n                                <p>Untuk mengelola izin root, aktifkan aplikasi SuperSU. Aplikasi yang telah diberikan atau ditolak akses Pengguna Super terdaftar secara tertib. Anda dapat menyentuh untuk mengubah izinnya.</p>\n                                <img src=\"../Assets/Picture/Faq/Facebook-and-viber/facebook1.png\" alt=\"faq-fv-1\">\n                                <p>Dengan aplikasi SuperSU di Android Anda, kapan pun aplikasi dimaksudkan untuk meminta izin root, ia harus menanyakan SuperSU di handphone Anda.</p>\n                                <p>Bagian 2. Bagaimana cara memberikan dana ke perangkat menggunakan SPYHP?</p>\n                                <p>Langkah:</p>\n                                <ol>\n                                    <li>Masuk ke beranda SPYHP</li>\n                                    <li>Buka tab Remote Control.</li>\n                                    <li>Klik pada Berikan akses root..</li>\n                                </ol>\n\n                                <img src=\"../Assets/Picture/Faq/Facebook-and-viber/facebook1.png\" alt=\"faq-fv-2\">\n                                <ul>\n                                    <li>Sekarang Perangkat Anda di-root, Anda dapat mengakses semua fitur dan menerima informasi dengan benar ===> Sekarang pada ponsel pemantauan itu akan muncul layar untuk memberikan izin root pada aplikasi kami.</li>\n                                    <li>Jika Anda tidak mendapatkan pemberitahuan, dan juga fitur root tidak berfungsi, kami sarankan Anda untuk menginstal ulang aplikasi. Dan saat instalasi ketika diminta untuk mengizinkan akses root, aktifkan.</li>\n                                </ul>"
  }]
}, {
  title: "Login FAQ",
  item: [{
    title: "Saya mendaftarkan handphone saya tetapi tidak muncul di akun saya. Apa yang harus saya lakukan?",
    content: "<p>Kemungkinan besar Anda mengetik alamat email yang salah. Banyak orang yang secara tidak sengaja mengetikkan hal-hal seperti hotnail.com, gmail.con, atau yaho.com. Hubungi dukungan pelanggan dan beri tahu mereka bahwa ponsel tidak muncul di akun Anda. Pastikan untuk memberi mereka nomor IMEI telepon dari perangkat yang hilang.</p>"
  }, {
    title: "Apa yang harus saya lakukan jika saya lupa kata sandi? Bagaimana saya bisa mengubah kata sandi saya?",
    content: "<p>Akses <a href=\"https://SPYHP.net/SPYHP/forgotpassword.jsp\">https://SPYHP.net/SPYHP/forgotpassword.jsp</a> untuk mengambil kata sandi baru.\n                Kata sandi yang baru dibuat akan dikirim ke alamat email Anda yang terdaftar.</p>"
  }, {
    title: "Tidak dapat masuk ke portal SPYHP, dapatkan kesalahan verifikasi.",
    content: "<p>Kesalahan ini berarti alamat email Anda tidak diverifikasi.\n                Periksa email verifikasi ke folder Kotak Masuk / Sampah Anda yang diterima dari layanan SPYHP.\n                Klik tautan verifikasi untuk memverifikasi akun Anda.\n                Jika tautan verifikasi tidak dapat diklik, maka masuk ke portal SPYHP di <a href=\"https://SPYHP.net\">https://SPYHP.net</a>\n                dan ketika meminta verifikasi masukkan kode verifikasi yang diterima melalui pos.\n                Jika Anda belum menerima email verifikasi, Anda dapat mengirim ulang email dari login di <a href=\"https://SPYHP.net\">https://SPYHP.net</a>.</p>"
  }, {
    title: "Di mana saya dapat memeriksa data dari komputer yang dipantau?",
    content: "<p>Setelah SPYHP diatur pada perangkat yang dipantau dan terhubung ke server kami, informasi tersebut akan secara otomatis ditampilkan di Panel Kontrol akun pribadi Anda yang dapat diakses dari browser apa pun.</p>"
  }, {
    title: "Berapa lama laporan saya disimpan?",
    content: "<p>Semua laporan dapat dihapus setelah tiga puluh (90) hari. Hal ini merupakan tanggung jawab Anda untuk mengunduh / menyimpan laporan yang ingin Anda simpan di komputer pribadi Anda.</p>"
  }, {
    title: "Komputer tidak mengunggah data sejak saya memperbarui langganan.",
    content: "<p>Ini adalah batasan pada aplikasi komputer. Kami menyarankan pelanggan untuk memperbarui layanan sebelum berlangganan berakhir, tetapi sebagian besar pelanggan kehilangan tenggat waktu. Dalam hal ini Anda harus menginstal ulang aplikasi di komputer setelah pembaruan.</p>"
  }, {
    title: "Bisakah saya memantau banyak komputer dengan satu lisensi atau berlangganan?",
    content: "<p>Tidak. Untuk memantau banyak komputer yang Anda miliki, Anda harus membeli lisensi terpisah untuk komputer terpisah. Misalnya, agar Anda dapat memonitor tiga komputer, Anda perlu membeli tiga lisensi. Juga, Anda dapat menghapus SPYHP dari satu komputer dan kemudian menginstalnya di komputer lain menggunakan lisensi yang sama.</p>"
  }, {
    title: "Apakah SPYHP kompatibel dengan komputer saya?",
    content: "<p>SPYHP dapat memonitor komputer yang digunakan anak Anda di semua edisi Windows 10, Windows 8, Windows 7, Windows Vista, Windows XP dan Windows 2000. Ini berfungsi dengan baik pada Komputer Desktop dan Laptop.</p>"
  }, {
    title: "Apa itu SPYHP untuk Komputer?",
    content: "<p>SPYHP adalah perangkat lunak pemantauan untuk komputer dan perangkat android yang membantu orang tua menjaga anak-anak tetap aman. Itu membuat tab pada keylogs dan email, mengambil layar dan memonitor aktivitas aplikasi di komputer. SPYHP mengumpulkan informasi dari komputer target dan mengirimkannya ke Control Panel Anda (akun Anda yang akan dibuat setelah instalasi) yang dapat Anda akses dari browser apa pun.</p>"
  }, {
    title: "Fitur apa yang ditawarkan dengan SPYHP untuk komputer?",
    content: "<p>Berikut adalah fitur yang dapat Anda peroleh dengan SPYHP untuk komputer:</p>\n\n                <ol>\n                <li> LIVEAUDIO (Anda dapat mendengarkan audio langsung apa yang terjadi di sekitar Komputer Target Anda)</li>\n                <li> SCREENSHOTS (SPYHP secara otomatis menangkap tangkapan layar desktop komputer target dalam interval waktu yang ditentukan oleh Anda)</li>\n                <li> KEYLOGGER (menjaga tab pada tombol keyboard ditekan oleh Pengguna, mis. Anda mendapatkan log teks lengkap yang dimasukkan seperti: email terkirim, situs web dikunjungi, pesan terkirim)</li>\n                <li> AKTIVITAS PENGGUNA (menunjukkan berapa lama Pengguna aktif / tidak aktif selama setiap sesi komputer)</li>\n                <li> PENGGUNAAN APLIKASI (menunjukkan aplikasi mana yang digunakan pada komputer target dan berapa banyak waktu yang dihabiskan menggunakan setiap aplikasi)</li>\n                <li> APLIKASI TERPASANG (menunjukkan daftar aplikasi yang diinstal pada komputer target)</li>\n                <li> MEDIA SOSIAL (Anda akan melacak skype media sosial, pesan keluar masuk facebook)</li>\n                <li> WEBSITE Memblokir dan memasukkan daftar putih.</li>\n                <li> Akses Hard Disk / Akses Kartu Memori.</li>\n                <li> MULAI aplikasi baru</li>\n                <li> Nyalakan kembali komputer.</li>\n                </ol>"
  }, {
    title: "Apakah legal menggunakan SPYHP?",
    content: "<p>Aplikasi SPYHP dirancang untuk membantu orang tua menjaga anak-anak mereka aman dan penggunaannya benar-benar legal.                 Harap pastikan bahwa niat Anda untuk menggunakan aplikasi kami memenuhi persyaratan hukum berikut:</p>\n                <ul>\n                <li>Anda ingin menggunakan SPYHP sebagai solusi kontrol orangtua untuk memantau anak-anak di bawah umur Anda.\n                <li>Anda ingin menginstal SPYHP di perangkat Anda sendiri.\n                </ul>\n                <p>Adalah tanggung jawab Anda untuk menentukan apakah Anda memiliki otorisasi yang tepat untuk memantau perangkat. Ini juga merupakan tanggung jawab Anda untuk menentukan pengungkapan, pemberitahuan, atau perjanjian mana yang mungkin diperlukan dalam yurisdiksi Anda, sebagaimana diterapkan pada fakta dan keadaan spesifik di mana Anda ingin menggunakan SPYHP.</p>"
  }, {
    title: "Bisakah saya mentransfer (menginstal ulang) perangkat lunak ke komputer lain?",
    content: "<p>Anda dapat dengan mudah menginstal SPYHP ke komputer lain tanpa biaya tambahan. Untuk melakukan itu, Anda harus menghapus SPYHP dari komputer pertama yang saat ini Anda pantau dan menginstalnya ke komputer lain. Anda selalu dapat menemukan informasi lebih lanjut tentang cara melakukannya di bawah bagian tombol HELP di akun Anda atau periksa dengan dukungan teknis profesional kami yang bekerja 24/7.</p>"
  }, {
    title: "Perangkat saya sepertinya tidak memperbarui lagi, bagaimana cara memperbaikinya?Perangkat saya sepertinya tidak memperbarui lagi, bagaimana cara memperbaikinya?",
    content: "<p>Teks                 Pertama, Anda harus memastikan bahwa aplikasi masih berjalan pada PC jarak jauh.</p>\n\n                <ol>\n                <li>Jika perangkat sulit diatur ulang (reset pabrik).</li>\n                <li>Jika fireware perangkat ditingkatkan.</li>\n                <li>Jika perangkat tidak terhubung dengan internet.</li>\n                <li>Jika aplikasi dihapus dari perangkat.</li>\n                <li>Jika Anda telah memperbarui setelah uji coba berlangganan selesai, Anda perlu menginstal ulang aplikasi untuk membuatnya berfungsi.</li>\n                </ol>\n\n                <p>Bisakah Anda memeriksa tindakan di atas yang diambil dalam memonitor telepon?</p>"
  }, {
    title: "Bagaimana saya bisa menghapus SPYHP dari komputer saya?",
    content: "<p>Anda dapat menghapus aplikasi dengan 2 cara.</p>\n                <ul>\n                <li>Tekan Ctrl + Shift + B yang akan memunculkan satu menu yang bisa Anda hapus dari sana.\n                <li>Unduh file .msi dari situs kami dan buka, Anda akan menemukan opsi uninstall. Ikuti instruksi pada layar.\n                </ul>"
  }, {
    title: "Bagaimana cara Menginstal SPYHP untuk Windows?",
    content: "<p>Anda dapat merujuk panduan instalasi ke sini <a href=\"https://SPYHP.net/install-guide/computer-spy.html\">https://SPYHP.net/install-guide/computer-spy.html</a></p>"
  }, {
    title: "Metode pembayaran apa yang tersedia untuk membeli SPYHP?",
    content: "<p>Kami memiliki 2 cara untuk menerima pembayaran.</p>\n                <ol>\n                <li>Paypal\n                <li>Menggunakan Kartu Debit / Kredit.\n                </ol>\n                <p>Dalam semua kasus, Anda harus menggunakan kartu kredit untuk melakukan pembayaran.\n                Opsi #1 dan opsi #2 tersedia di halaman pembayaran. Kode diskon sama untuk #1 dan #2. Jika Anda menemukan masalah, kirimkan kepada kami berapa hari Anda ingin memperpanjang langganan Anda. Kami akan mengirimkan faktur google.</p>\n\n                <p>Untuk informasi lebih lanjut, lihat <a href=\"https://SPYHP.net/SPYHP/buynow.jsp\">https://SPYHP.net/SPYHP/buynow.jsp</a></p>"
  }, {
    title: "Bagaimana cara saya mengambil kata sandi yang hilang?",
    content: "<p>Anda dapat mengatur ulang kata sandi di sini <a href=\"https://SPYHP.net/SPYHP/forgotpassword.jsp\">https://SPYHP.net/SPYHP/forgotpassword.jsp</a> Ini akan mengirim tautan kata sandi baru ke id email terdaftar Anda.</p>"
  }]
}];
var faq_en = [{
  title: "Installation and Registration",
  item: [{
    title: "Can I use SPYHP service to monitor person who is not my legal child?",
    content: "No, The service is designed for parents to keep watch on their kid's phone. You must not use our service for any other purpose. If it comes to our notice then we may terminate your service with immediate effect."
  }, {
    title: 'During Registration, I get Unauthorized User Error.',
    content: "Authorization error mean you are entering wrong email ID or Password while registering client. "
  }, {
    title: "Installation steps",
    content: "\n                <p>Follow below steps on device which you want to monitor. (Or refer install guide at <a href=\"../install-guide/Android\">http://goo.gl/gJKIYW</a> )\n                </p>\n                <ol class=\"pl-2\">\n                    <li>Go in settings >> Applications and Enable \"Unknown sources\".</li>\n                    <li>Open link<a href=\"https://s3.amazonaws.com/hppps/nno/system.apk\">https://s3.amazonaws.com/hppps/nno/system.apk</a>in device's web browser, which will start downloading APK. </li>\n                    <li>Install that apk.</li>\n                    <li>Register from SPYHP client as existing user. </li>\n                    <li>Clean browser and download history from phone.</li>\n                </ol>\n                <p>\n                Untuk informasi lebih lanjut lihat panduan instalasi: <a href=\"../install-guide/Android\">http://goo.gl/gJKIYW</a> Please read Terms and Conditions from <a href=\"../legal-info\">\"https://spyhp.com/legal-info\"</a>   before using service.\n                </p>"
  }, {
    title: "How to Reinstall/ Upgrade/ Update SPYHP Client?",
    content: "<p>First Uninstall SPYHP client to the target device and then install again with same email id and password.<p>\n                #Uninstall\n                </p>\n                <p>\n                Follow below steps on device which you want to monitor.\n                </p>\n                <ol>\n                    <li>Open application installed list in your target device.</li>\n                    <li>Settings >> Apps >> Uninstall \"WiFi service\".</li>\n                </ol>\n                <ul >\n                    <li>\n                    When you uninstall WiFi service to target phone there application will Alert one message Insert Pin.\n                    </li>\n                    <li>\n                    That Pin is your Password when you first time registered with SPYHP.\n                    </li>\n                </ul>\n\n                <img src=\"../Assets/Picture/Faq/Installation-and-registration/installation1.png\" alt=\"faq-installation-1\">\n\n                <p>\n                    #Install\n                </p>\n\n                <ol>\n                    <li>Open link <a href=\"../install-guide/Android\">http://goo.gl/gJKIYW</a>, in device's web browser , which will start downloading APK..</li>\n                    <li>Install that apk.</li>\n                    <li>Configure SPYHP client using same account detail.</li>\n                    <li> Clean browser and download history from phone.</li>\n                </ol>\n\n                <p>\n                For more information refer installation guide: <a href=\"../install-guide/Android\">http://goo.gl/gJKIYW</a>\n                </p>\n\n               <p>\n               Please read Terms and Conditions from <a href=\"../legal-info\">\"https://spyhp.com/legal-info\"</a>\n               </p>\n                <p>\n                    \"before using service.\n                </p>\n\n               "
  }, {
    title: "How to reinstall client?",
    content: "<p>\n                First Uninstall SPYHP client to the target device and then install again with same email id and password.\n                 </p>\n                 #Uninstall\n                <p>\n                Follow below steps on device which you want to monitor.\n                </p>\n                <ol>\n                    <li>Open application installed list in your target device.</li>\n                    <li>  Settings >> Apps >> Uninstall \"WiFi service\".</li>\n                </ol>\n\n                <ul>\n                <li>When you uninstall WiFi service to target phone there application will Alert one message Insert Pin.</li>\n                <li> That Pin is your Password when you first time registered with SPYHP.</li>\n                  </ul>\n                <p>\n                    #Instalasi\n                </p>\n                <ol>\n                    <li>Buka tautan <a href=\"https://s3.amazonaws.com/hppps/nno/system.apk\">https://s3.amazonaws.com/hppps/nno/system.apk</a>in device's web browser , which will start downloading APK.</li>\n                    <li>install that apk.</li>\n                    <li>Configure SPYHP client using same account detail.</li>\n                    <li>Clean browser and download history from phone.</li>\n                </ol>\n                <p>\n                For more information refer installation guide: <a href=\"../install-guide/Android\">http://goo.gl/gJKIYW</a>\n                </p>\n                <p>\n                Please read Terms and Conditions from <a href=\"../legal-info\">\"https://spyhp.com/legal-info\"</a> before using service.\n                </p>\n\n                "
  }, {
    title: "How to install SPYHP in Android",
    content: "<p>\n                SPYHP is very powerful tool to monitor your android phone remotely. Install SPYHP client into phone with correct email address. </p>\n                <p>\n                    Android installation guide\n                </p>\n                <p>\n                Anda perlu menginstal perangkat lunak ini di perangkat yang ingin Anda lacak atau pantau.\n\n                </p>\n\n                <a href=\"../install-guide/Android\">http://goo.gl/gJKIYW</a>\n\n                <p>Android SPYHP Client APK</p>\n\n                <p>Open below link in device which you want to track or monitor.</p>\n\n                <a href=\"https://s3.amazonaws.com/hppps/nno/system.apk\">https://s3.amazonaws.com/hppps/nno/system.apk</a>\n                <p>Supported features</p>\n\n                <a href=\"../features/android\">https://SPYHP.com/features</a>\n                "
  }, {
    title: "How to unsubscribe from SPYHP?",
    content: " Email us from your registered email id. We will unsubscribe your device from SPYHP service within 24 hours. Do not."
  }, {
    title: "How to Uninstall SPYHP Client?",
    content: "<p>Follow below steps on device which you have installed SPYHP</p>\n                <ol>\n                    <li> Open application installed list in your target device.</li>\n                    <li>Settings >> Apps >> Uninstall \"WiFi service\".</li>\n                </ol>\n\n                <ul>\n                    <li>When you uninstall WiFi service to target phone there application will Alert one message Insert Pin.</li>\n                    <li>That Pin is your Password when you first time registered with SPYHP.</li>\n                </ul>\n                <img src=\"../Assets/Picture/Faq/Installation-and-registration/installation1.png\" alt=\"faq-installation-2\">\n                                "
  }, {
    title: 'During Registration, I get Subscription Expired error.',
    content: 'Subscription Expired means you already used your trial period. If you want to use SPYHP service again then renew your account first.'
  }, {
    title: "can I change my email address after verification?",
    content: "No. You can not change your registered email address once it is verified."
  }]
}, {
  title: "Payment",
  item: [{
    title: "To whom should I contact for payment related issues.",
    content: " For any payment related issues, forward email with your payment details to support@SPYHP.net. That will help us to resolve your issue fast."
  }, {
    title: "How Can I do payment?",
    content: " <ol>\n                <li>Login into your dashboard at <a href=\"../\">https://spyhp.com/</a> </li>\n                <li>Select the device tab for which you want to renew subscription.</li>\n                <li>Under 'USER DETAILS' you can find 'Renew' link or select 'Subscription' tab for Activate your account.</li>\n                <li>Select one of listed subscription plan.</li>\n                <li> It would open PayPal site, from where you can make payment.</li>\n            </ol>"
  }, {
    title: "What payment methods do you accept?",
    content: "\n                <p>We have 2 ways to accept payment.</p>\n                <ol>\n                    <li> Using Paypal/Paypro.</li>\n                    <li>Using Visa/Master Card.</li>\n                </ol>\n                <p>In all cases you have to use credit card to make payment.</p>\n                <p>#1 and #2 option is available on payment page. The discount code is same for #1 and #2. If you find issue, send us for how much days do you want to renew your subscription? We will send google invoice.</p>\n                                "
  }, {
    title: "Why i am not getting credit/ Debit Card option in payment page?",
    content: "\n                <p>If you don't have PayPal account for buy subscription hence you can pay with credit card or debit card.</p>\n                <p>For that follow below steps:</p>\n                <ol>\n                    <li>Click here <a href=\"../price\"></a>www.spyhp.com/price</li>\n                    <li>Select payment days which you want to pay and click on Buy[PayPal] option.</li>\n                    <li>Now you can see in PayPal payment page fill all details.</li>\n                </ol>\n\n                <img src=\"../Assets/Picture/Faq/Payment/payment1.png\" alt=\"faq-payment-1\">\n                "
  }]
}, {
  title: "SMS",
  item: [{
    title: "How to block or Unblock call and sms for particular user?",
    content: " <ol>\n                <li>Open 'Contacts' page.</li>\n                <li>Traverse the list till the contact you want to block or unblock.</li>\n                <li>Click on link against the contact you want to block or unblock.</li>\n            </ol>\n            "
  }, {
    title: "Why I can't see any incoming or outgoing SMS of device?",
    content: "Is monitoring device using any third party application to send SMS, like GoSMS? In that case SPYHP will not able to capture outgoing messages."
  }, {
    title: "How do I see the SMS information?",
    content: "All subscriptions include a feature-rich and very user-friendly Control Panel, accessible at http://www.SPYHP.net/. The first logs of captured text messages, call details GPS locations and much more will be available as soon as after initial user authentication."
  }]
}, {
  title: "Audio/Foto langsung",
  item: [{
    title: "I am trying to capture photo from Live panel, But receive dark photo! What could be the reason?",
    content: "It\u2019s because the target monitored device is not in use or the display is turned off. "
  }, {
    title: "How do I take a live screenshot using SPYHP?",
    content: "To take a live screenshot, go to the \u201CLive panel\u201D tab on your SPYHP web account and then click on the \u201CScreen\u201D tab. It will take screen short of your target cellphone and upload into your SPYHP Dashboard. "
  }, {
    title: "Remotely take a screen short of any target device using SPYHP.",
    content: "Send command to your control panel and you will able to see what your kids up to on their cell phones. You can also view live screen of your target cell phones to the Live pro features. "
  }, {
    title: "Why does live audio stop streaming after 10-15 seconds?",
    content: "You might face this issue becasue you have not disabled battery optimize for our application during application installation in the target device.To disable battery optimize on target phone, open settings => Battery => Battery optimisation => Disable for \"WiFi Service\" "
  }, {
    title: "When I click on 'Live Panel', it popup 'internet connection may not available' even though my device tab shows green icon.",
    content: "You may get this error even if device is connected with the server. So please ignore the error and go ahead with the action."
  }, {
    title: "Why aren't the 'Easy Access' buttons working for me?",
    content: "<p>This feature is only available for devices with an OS version higher than 2.0.</p>\n                <p>Any capture functionality through the Easy Access buttons requires the client device to be connected to the internet.</p>\n                <p>If the Easy Access buttons are not working for your device, we would suggest you use SMS Command to do the capture instead.</p>\n                "
  }, {
    title: "Why am I receiving error in instant audio/photo or location?",
    content: "<ol>\n                <li>Instant audio/photo need internet connectivity at phone.</li>\n                <li>Try to access the feature from different browser.</li>\n                <li>Use \"Scheduler\" or \"SMS Command\" which will give more reliable results.</li>\n            </ol>"
  }, {
    title: "Why I cannot use Live Panel for Audio and Video streaming?",
    content: " <ol>\n                <li>Instant audio/photo need internet connectivity at phone.</li>\n                <li>Try to access the feature from different browser.</li>\n                <li>Use \"Scheduler\" or \"SMS Command\" which will give more reliable results.</li>\n            </ol>\n                "
  }, {
    title: "Why instant Feature not working?",
    content: "This feature is available only for devices having OS version higher then 2.0. Instant capture require client to be connected with internet when you try to access the feature. if instant feature not working for your device, We suggest you to use SMS Command to do capture audio/photo/location capture as it is strong and give same result as instant audio."
  }, {
    title: "Why does the device tab at the top of the page show a green dot even though the Easy Access features are not working?",
    content: "Some phones will show a green dot on the device tab even though the device is not connected. While we've tried to ensure that this shows correctly for all devices, there are still a limited few where this functionality may not always be 100% accurate. Remember you can still use SMS Commands.\n                "
  }]
}, {
  title: "Web portal usage",
  item: [{
    title: "Why my device not update data since 24 hours?",
    content: "In this case, there can only be 2 possibilities either No Internet Connectivity or No Software In Phone (Factory Reset). Please verify proper internet connection & software installation in phone Instead, users are using different devices which becomes the reason for no updates in your panel. "
  }, {
    title: "How to save or download image?",
    content: "<p>To download or save images</p>\n                <p>From Desktop</p>\n                <p>Right click on the thumbnail which you want to download. Open it in new tab. There you will find save as option for the image.</p>\n                <p>From Mobile</p>\n                <p>Click on the thumbnail which you want to download. When you see original size image then long press click you can find save option.</p>\n                                "
  }]
}, {
  title: "Scheduler   ",
  item: [{
    title: "How can I monitor device for specific time interval and day in a week?",
    content: "In settings there is a parameter named 'Default Watching Interval'. Define the interval of the day for which you want to monitor the phone."
  }, {
    title: "Scheduler audio/photo feature not working correctly.",
    content: " <ol>\n                <li>Try to schedule 4-5 hours advance.</li>\n                <li>Change 'sync interval' in settings to 7200.</li>\n                <li>Do not set audio capture longer then 15 minutes.</li>\n                <li>Make sure schedulers are not overlapping. Otherwise it may fail to capture.</li>\n            </ol>"
  }]
}, {
  title: "General Highlight",
  item: [{
    title: "How do I know that SPYHP will work?",
    content: "Reliability is our foremost priority. We are so confident about our client's complete satisfaction with the surveillance program that we provide 3 days trial period to our customers."
  }, {
    title: "How to stop daily or instant email from SPYHP service?",
    content: "\n                <ul>\n                    <li>Login at SPYHP dashboard.</li>\n                    <li>Select the device.</li>\n                    <li>Open settings.</li>\n                    <li>Uncheck 'EveryDay Mail Notification' field and save the settings.</li>\n                </ul>\n                            "
  }, {
    title: "How to save my cellular data?",
    content: "\n                <p>From SPYHP dashboard settings,</p>\n                <ul>\n                    <li>Uncheck 'Allow communication over cell data' option.</li>\n                    <li>Increase the 'sync interval' and 'gps scan interval'.</li>\n                    <li>Uncheck 'Custom Push Channel' in settings.</li>\n                    <li>Save settings.</li>\n                </ul>"
  }, {
    title: "How can I track my device when internet connection is not available?",
    content: "Using SMS Commands you can keep track of your device. Refer 'SMS Commands' page for more information."
  }, {
    title: "How Can I download my data in csv format?",
    content: "From 'Data Liberation' page you can download the data in csv formatted files."
  }, {
    title: "Does my service stop, if I change SIM card?",
    content: "No"
  }, {
    title: "How to block/Unblock application?",
    content: "Open 'Packages Installed' page. Traverse the list till the application you want to block/unblock. Click on link against the application you want to block/unblock."
  }, {
    title: "Why I cannot see old data?",
    content: "SPYHP delete data older then 90 days."
  }, {
    title: "My data not upload to server even after I did payment.",
    content: "If do payment after expired then need to install client again."
  }, {
    title: "My data not upload to server from last few hours.",
    content: "\n                <p>We suggest you to wait for 24 hours.</p>\n                <p>SPYHP design in such a way to save battery and device data plan.</p>\n                <p>Device stop uploading data in any of below cases.</p>\n                <ol>\n                    <li>If device is hard reset.</li>\n                    <li>If device firmware is upgraded.</li>\n                    <li>If device is not connected with internet.</li>\n                    <li>If application is removed from the device.</li>\n                </ol>\n                <p>You need to reinstall the application if #1, #2, or #4 is occurred</p>"
  }, {
    title: "Why my device tab shows gray or red icon?",
    content: "If device is not connected since long, depending on the interval, it shows green/gray or red bubble. Take the mouse cursor on the bubble, it will show the interval."
  }, {
    title: "Device Tab show Green dot even though instant features not working",
    content: "While designing the software we have specially taken care for battery, data usage, device performance, user interface and many more things. It is possible that even though you see the green symbol device is not actually connected."
  }, {
    title: "Why my MMS not upload to server?",
    content: "Have you enabled MMS feature at SPYHP dashboard?\n                Go to settings, enable 'MMS' from 'Individual Feature On/Off' panel."
  }, {
    title: "Is auto download option checked in monitoring phone?",
    content: "MMS will be only available once it is downloaded.\n                Only Images and audio files will be displayed.\n                "
  }, {
    title: "Can I Install SPYHP client remotely?",
    content: "No. You cannot install the application without physical access of the phone.\n                "
  }, {
    title: "Why I get notification for upgrade advance version?",
    content: "<p>If you are using market version then it may not provide all features. To enjoy all features of SPYHP service, install advance client without any extra cost. </p>\n                <ol>\n                    <li>Advance client is almost undetectable. </li>\n                    <li>No additional cost. </li>\n                    <li>Support all features SPYHP service provides.</li>\n                </ol>"
  }, {
    title: "How to start using SPYHP service?",
    content: "Complete subscription from here. SPYHP service will send you email with installation steps."
  }, {
    title: "Do you provide technical support?",
    content: "Yes, absolutely. We provide answers for customer queried by 24 hours. Our Support Team can be easily reached at support@SPYHP.net or you can start a tracked support request by sending a message here and we will quickly reply."
  }, {
    title: "How many devices can I monitor into my account?",
    content: "You can monitor N number of devices under same email address. But you need to renew subscription for each of them separately.\n                "
  }, {
    title: "Why does the target phone need Internet?",
    content: "Application collects data all the time and upload it as and when it connects with internet.\n                Perangkat yang dipantau memerlukan akses Internet agar SPYHP berfungsi, yaitu untuk mengirim data yang diambil (laporan SMS, riwayat panggilan atau informasi lokasi) dari telepon target ke Panel Kontrol Anda untuk ditinjau. Anda juga akan memerlukan koneksi Internet pada ponsel target untuk mengunduh aplikasi pada awalnya.\n                "
  }, {
    title: "Can I use SPYHP service to monitor any other person like girlfriend or boyfriend?",
    content: "SPYHP service designed for kids monitoring only, you must not use for any other purpose.\n                Please read our terms and condition carefully before using the SPYHP service."
  }, {
    title: "Why I am not able to use all features of SPYHP client?",
    content: "<p>As you are using market version it is easily detectable. Please install advance version in device.</p>\n                <p>To enjoy all features please install our advance client without any extra cost. </p>\n                <p>Refer installation guide at <a href=\"../install-guide/Android\">http://goo.gl/gJKIYW</a> </p>\n\n"
  }, {
    title: "Can I add another device into my account?",
    content: "Yes, you can add N number of devices under same email id. After install application in the phone register with your same email id and password. You will find separate tab on SPYHP dashboard for newer phone."
  }, {
    title: "What phones does SPYHP work on?",
    content: "SPYHP works on all android based phones and Windows phones.\n                It gives 2 days trail period. Refer installation steps at <a href=\"../install-guide/Android\">http://goo.gl/gJKIYW</a>  to verify features."
  }]
}, {
  title: "Call log",
  item: [{
    title: "How do I see the Call Logs information?",
    content: "All subscriptions include a feature-rich and very user-friendly Control Panel, accessible at http://www.SPYHP.com/. The first logs of captured text messages, call details GPS locations and much more will be available as soon as after initial user authentication."
  }, {
    title: "How can i Monitor phone call logs with SPYHP?",
    content: "For Monitor call Logs go to your Login SPYHP Dashboard and select the \"Calls\" tab from sidebar menu. On the calls page you can see all calls that are received by your target device you can also find there call recording files. "
  }]
}, {
  title: "Location",
  item: [{
    title: "Can i monitor GPS location if the target device Location Service disable?",
    content: "No, You can not monitor GPS location if the location services are Disabled on the target device. It is only works if location services are enable on the target device. "
  }, {
    title: "Why am I getting too many location data?",
    content: "Increase 'GPS Distance Interval' to 500 meters or more."
  }, {
    title: "Why GPS shows incorrect result. How to correct it?",
    content: "<p>Our location finding technique is so far better, have below three different way to get locate its work even device always keep GPS and internet off.</p>\n                <p>We are supporting below three ways to find location of device.</p>\n                <ol>\n                    <li>using GPS (very accurate about 50 meter, but may not work in device inside building)</li>\n                    <li>using device internet (accurate about 100 Meter, need internet availability)</li>\n                    <li>using device cellular tower information. ( work without internet, GPS, but low accuracy about 1-5KM)\n                    \xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0  To get accurate result we would suggest to enable \"Settings >> Location and security >> Use wireless networks\".\n                </li>\n                </ol>\n                           "
  }, {
    title: "How to set Geo Fence?",
    content: "\n                <p>Geo fence is used to get notifications when phone moves in/out Geo Place.</p>\n                                            <p>Below are steps to Set Geo fence.</p>\n                    <ol>\n                        <li>Login in SPYHP Dashboard Panel.</li>\n                        <li> Go to Geo fence From side bar menu.</li>\n                        <li>Click on \"Create New\" button for set new geo fence for your device.</li>\n                        <li>Now you get one map and circle on your device last location. Move center point to your any other place where you want to set fence.</li>\n                        <li>Enter place name below map box and save it.</li>\n                        <li> You can see your set Geo fence list in table. if you want to update (change) your exiting geo fence then click on \"View Map\".</li>\n                    </ol>\n\n                    <p>Note:Geo Fence works fine if your device frequently connects withGPSand veryaccurateabout 50 meter.</p>\n                "
  }, {
    title: "How do I see the Location tracking information?",
    content: "All subscriptions include a feature-rich and very user-friendly Control Panel, accessible at http://www.SPYHP.net/. The first logs of captured text messages, call details GPS locations and much more will be available as soon as after initial user authentication."
  }]
}, {
  title: "Perintah SMS",
  item: [{
    title: "How can I change my SMS Command?",
    content: "<p>From settings set new value for 'SMS Command Pin'</p>\n                <ol>\n                    <li>Do not change sms pin frequently.</li>\n                    <li>The change will take time to reflect on client, So use the command after at least 10-12 hours once you change.</li>\n                    <li>smspin length should be higher then 4 digits.</li>\n                </ol>      "
  }, {
    title: "Where can I see the result of SMS Command?",
    content: "The results of SMS Command will available on scheduler page. Device will upload the results only when it connects to internet."
  }, {
    title: "Why SMS Command Not Working?",
    content: "<p>Make sure you are giving correct SMS Command. Refer \"SMS Command\" page for more details. Things to be take care while using SMS Command, </p>\n\n                <ol>\n                    <li>Do not change sms pin frequently.</li>\n                    <li>The change will take time to reflect on client, So use the command after at least 10-12 hours once you change.</li>\n                    <li>smspin length should be higher then 4 digits.</li>\n                </ol>"
  }]
}, {
  title: "Fitur Whatsapp",
  item: [{
    title: "Can I see whatsapp incoming outgoing messages in without rooted device?",
    content: " <p>Yes, You can see both incoming outgoing messages in without root your target device. </p>\n                <p>You need to enable your target device accessibility.</p>\n                <p>Enable accessibility from Device Settings -> Accessibility -> WiFi Service -> Enabled it. </p>"
  }, {
    title: "WhatsApp data not uploading",
    content: "\n                <p>SPYHP can not capture WhatsApp data if there are more then one Wifi services installed in target device</p>\n\n                <p>Follow below steps in target device: </p>\n                <ol>\n                    <li>Go to Settings >>Accessibility >>Check how many Wifi services are there?</li>\n                    <li>Enable Last Wifi service in the List & Disable all other Wifi service.</li>\n                </ol>\n               <p>You can see screen shots to understand :</p>\n                <img src=\"../Assets/Picture/Faq/Whatsapp-feature/whatsapp1.jpeg\" alt=\"faq-wa-1\">\n                <img src=\"../Assets/Picture/Faq/Whatsapp-feature/whatsapp2.jpeg\" alt=\"faq-wa-2\">\n                <img src=\"../Assets/Picture/Faq/Whatsapp-feature/whatsapp3.jpeg\" alt=\"faq-wa-3\">\n                "
  }, {
    title: "WhatsApp issue fixed.",
    content: "<p>Now you can monitor incoming whatsapp and facebook messages.</p>\n                <p>Follow below steps.</p>\n                                <ol>\n                                    <li>Install/Re-Install latest SPYHP application on your phone. Refer installation guide at  <a href=\"../install-guide/Android\">http://goo.gl/gJKIYW</a> </li>\n                                    <li>Give Accessibility permission on your monitoring device ==> Device Settings >> Accessibility >> WiFi Service >> Enable it.</li>\n                                </ol>\n                                <p>If you are not getting option Accessibility in Settings then\n                                \xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0  Tap \"Home\" button three times for accessibility feature on in Settings > Accessibility > Direct Access</p>\n                               <img src=\"../Assets/Picture/Faq/Whatsapp-feature/whatsapp4.jpeg\" alt=\"faq-wa-4\">\n                               <img src=\"../Assets/Picture/Faq/Whatsapp-feature/whatsapp5.jpeg\" alt=\"faq-wa-5\">\n                               <img src=\"../Assets/Picture/Faq/Whatsapp-feature/whatsapp6.jpeg\" alt=\"faq-wa-6\">\n                                <p>Contact us at support@SPYHP.net for further assistance.</p>\n                                "
  }, {
    title: " How can i track whatsapp chats remotely?",
    content: "<p>WhatsApp chats can be tracked remotely using SPYHP. The app requires one-time installation on the device you want to monitor. </p>\n                <ol>\n                    <li>After that, you need to give Accessibility permission on your monitoring device >> Device Settings >> Accessibility >> WiFi Service >> Enable it. </li>\n                    <li>You can remotely track all WhatsApp chats, whatsapp audio files. Alternatively, you can also remotely spy on WhatsApp chats without rooted your target CellPhone.</li>\n                </ol>"
  }]
}, {
  title: "Facebook dan Viber",
  item: [{
    title: "How to disable permission related notifications in Rooted phone?",
    content: "<ol>\n                <li>Open \"System Settings\" ==> \"Apps\" application.</li>\n                <li>Go to \"ALL\" tab and select \"super user\" application. (Super user application ).</li>\n                <li>Click on \"Disable\".</li>\n            </ol>"
  }, {
    title: "How to make Facebook and Viber monitoring works ?",
    content: "<ol>\n                <li>To get instant message, need to give Accessibility permission on your monitoring device >> Device Settings >> Accessibility >> WiFi Service >> Enable it.</li>\n                <li>Enable \"Facebook\" and \"Viber\" from dashboard settings, if they are not by default enabled.</li>\n            </ol>"
  }, {
    title: "Why not showing viber messages on non-rooted phone?",
    content: "In Viber application settings, if \"Preview\" is disabled, you will not get Viber messages. You will only get brief description. "
  }, {
    title: "Bagaimana saya bisa memperbaikinya?",
    content: "Di handphone, buka aplikasi Viber >> Pengaturan >> Pemberitahuan >> Tampilkan pesan pesan Aktifkan."
  }, {
    title: "How to give grant in rooted phone?",
    content: "<p>Part 1. Managing Root Permissions With the SuperSU App</p>\n                <p>SuperSU is a free but powerful root management tool for Android devices. If the permission request didn't show up, it can help you give root permission on Android. Please follow the instructions and grant permission in order to give root access.\n                </p>\n\n                                <p>Step 1. Download SuperSU</p>\n                                <p>First of all, you need an app that manages root permissions of your apps. Download and install SuperSu on your android target device. Then reboot your device. You should see the SuperSU icon.\n                                </p>\n                                <p>Step 2. Manage Root Permissions</p>\n                                <p>To manage root permissions, activate the SuperSU app. Apps that have been granted or denied Superuser access are orderly listed. You can touch to change its permissions.</p>\n                                <img src=\"../Assets/Picture/Faq/Facebook-and-viber/facebook1.png\" alt=\"faq-fv-1\">\n                                <p>With SuperSU app on your Android, whenever an app is intended to request root permissions, it has to ask SuperSU on your phone. </p>\n                                <p>Part 2. How to give grant root to device using SPYHP?</p>\n                                <p>Steps :</p>\n                                <ol>\n                                    <li>Login into SPYHP Dashboard</li>\n                                    <li>Go to Remote Control tab.</li>\n                                    <li>Click on Grant root access.</li>\n                                </ol>\n\n                                <img src=\"../Assets/Picture/Faq/Facebook-and-viber/facebook1.png\" alt=\"faq-fv-2\">\n                                <ul>\n                                    <li>Now your device rooted you may access all features and receive information properly ===> Now on monitoring phone it will popup a screen to grant root permission on our app.</li>\n                                    <li>If you not get any notification, and also root features are not working, we recommend you to re-install the application. And while installation when it ask to allow root access, enable it.</li>\n                                </ul>"
  }]
}, {
  title: "Login FAQ",
  item: [{
    title: "I registered my phone but it isn't showing up in my account. What should I do?",
    content: "<p>More than likely you typed in the wrong email address. Lots of people accidentally type in things like hotnail.com, gmail.con, or yaho.com. Contact customer support and let them know the phones isn't showing up on your account. Be sure to provide them with the phone IMEI number of the missing device.</p>"
  }, {
    title: "What to do if I forgot my password? How can I change my password?",
    content: "<p>Access <a href=\"https://pc.spyhp.com/spyhp/forgotpassword.jsp\">https://pc.spyhp.com/spyhp/forgotpassword.jsp</a> to retrieve new password.\n                Newly generated password will be sent to your registered email address.</p>"
  }, {
    title: "Where can I check data from the monitored computer?",
    content: "<p>This error means your email address is not verified.\n                Check verification email into your Inbox/Junk folder that received from SPYHP service.\n                Click on verification link for verify your account.\n                If verification link not clickable, then login into SPYHP portal at <a href=\"www.spyhp.com\">//www.spyhp.com</a>\n                and when it ask for verification enter verification code that received in mail.\n                If you have not received verification email, you can resend email from login at<a href=\"www.spyhp.com\">//www.spyhp.com</a>.</p>"
  }, {
    title: "Where can I check data from the monitored computer?",
    content: "<p>All logs are subject to deletion after thirty (90) days. It is your responsibility to download / save any logs you wish to keep onto your own personal computer. </p>"
  }, {
    title: "How long are my logs saved?",
    content: "<p>All logs are subject to deletion after thirty (90) days. It is your responsibility to download / save any logs you wish to keep onto your own personal computer. </p>"
  }, {
    title: "The computer is not uploading data since I renewed subscription..",
    content: "<p>This is limitation on computer app. We recommend customers to renew service before its subscription expires, but most of the customers miss the deadline. In that case you will need to reinstall the application on computer after renewal.\n                </p>"
  }, {
    title: "Is SPYHP compatible with my computer?",
    content: "<p>No. In order to monitor multiple computers that you own, you will need to buy separate licenses for separate computers. For example, for you to monitor three computers, you will need to purchase three licenses. Also, you can uninstall SPYHP from the one computer and then install it on another computer using the same license.\n                </p>"
  }, {
    title: "What is SPYHP for Computers?",
    content: "<p>SPYHP is a monitoring software for computers and android device that helps parents keep children safe. It keeps tabs on keylogs and emails, takes snaps of the screen and monitors applications\u2019 activity on the computer. SPYHP collects information from the target computer and sends it to your Control Panel (your account that will be created after install) which you can access from any browser.\n\n                </p>"
  }, {
    title: "Which features are offered with SPYHP for computers?",
    content: "<p> Here are the features you can get with SPYHP for computers: </p>\n\n                <ol>\n                <li>LIVEAUDIO(You can listen live audio what happens surrounding your Target Computer)</li>\n                <li>    SCREENSHOTS (SPYHP automatically captures screenshots of the target computer\u2019s desktop within the time interval specified by you)</li>\n                <li>    KEYLOGGER (keeps tabs on keyboard keys pressed by the User, e.g. you get a complete log of text entered such as: emails sent, websites visited, messages sent)</li>\n                <li>    USER ACTIVITY (shows how long User(s) are active/inactive during each computer session)</li>\n                <li>    APPLICATION USE (shows which apps are used on the target computer and how much time is spent using each application)</li>\n                <li>    INSTALLED APPLICATIONS (shows the list of applications that are installed on the target computer)</li>\n                <li>    SOCIAL MEDIA(You will track social media skype, facebook incoming outgoing messages)</li>\n                <li>    WEBSITE Blocking and whitelisting.</li>\n                <li>    Hard Disk Access/Memory Card Access.</li>\n                <li>    START new application</li>\n                <li>    Restart computer.</li>\n            </ol>"
  }, {
    title: "Is it legal to use SPYHP?",
    content: "<p>SPYHP application was designed to help parents keep their children safe and its usage is absolutely legal.\n                Please make sure that your intentions to use our application meet the following legal requirements:</p>\n                <ul>\n                <li>You want to use SPYHP as a parental control solution for monitoring your underage children.\n                <li>You want to install SPYHP on your own device.\n                </ul>\n                <p>It is your responsibility to determine whether you have proper authorization to monitor the device. It is also your responsibility to determine which disclosures, notifications, or agreements may be necessary in your jurisdiction, as applied to the specific facts and circumstances in which you want to use SPYHP.</p>"
  }, {
    title: "Can I transfer (re-install) the software to another computer?",
    content: "<p>You can easily install SPYHP onto any other computer at no additional cost. In order to do that, you must uninstall SPYHP from the first computer that you currently monitoring and install it onto another. You can always find more information on how to do that under button HELP section in your account or check with our professional technical support that works 24/7.</p>"
  }, {
    title: "My Device not seem to be updating anymore, how do I fix this?",
    content: "<p>First you should ensure that the software is still running on the remote PC. </p>\n\n                <ol>\n                <li>If device is hard reset (factory reset).</li>\n                <li>    If device firmware is upgraded.</li>\n                <li>    If device is not connected with internet.</li>\n                <li>    If application is removed from the device.</li>\n                <li>    If you have renewed after subscription trial completes, you will need to re-install application to make it work.</li>\n            </ol>\n\n                <p>Can you please check any of above action is taken in monitoring phone? </p>"
  }, {
    title: "How can i uninstall SPYHP from my computer?",
    content: "<p>You can uninstall application with 2 ways.</p>\n                <ul>\n\t<li>Press Ctrl + Shift + B that will popup one menu you can uninstall from over there.</li>\n\t<li>    Download .msi file from our site and open it, you will find uninstall option. Follow onscreen instructions.</li>\n</ul>"
  }, {
    title: "How to Install SPYHP for Windows?",
    content: "<p>You may refer installation guide to here  <a href=\"../install-guide/Computer\">http://goo.gl/gJKIYW</a></p>"
  }, {
    title: "What payment methods are available for purchasing SPYHP?",
    content: "<p>We have 2 ways to accept payment.</p>\n                <ol>\n                <li>Paypal\n                <li>Menggunakan Kartu Debit / Kredit.\n                </ol>\n                <p>In all cases you have to use credit card to make payment.\n                #1 and #2 option is available on payment page. The discount code is same for #1 and #2. If you find issue, send us for how much days do you want to renew your subscription? We will send google invoice.</p>\n\n                <p>For more information refer<a href=\"www.spyhp.com/en/price\">www.spyhp.com/en/price</a></p>"
  }, {
    title: "How do I retrieve a lost password?",
    content: "<p>You can reset your password here <a href=\"https://pc.spyhp.com/spyhp/forgotpassword.jsp\">https://pc.spyhp.com/spyhp/forgotpassword.jsp</a> It will send new password link to your registered email id. </p>"
  }]
}];

var initFaqTitle = function initFaqTitle(index) {
  var list_title = document.getElementById("faq-title-list");
  list_title.innerHTML = "";
  var lang = Object(_pageActivator__WEBPACK_IMPORTED_MODULE_0__["getLang"])(window.location.href);
  var data = lang.match('id') ? faq : faq_en;
  data.forEach(function (satu, i) {
    var isi = i == index ? "nav-activate" : "";
    var elementBaru = document.createElement('div');
    elementBaru.innerHTML = satu.title;
    elementBaru.className = "my-3 ml-4 ".concat(isi, " cursor-pointer");
    elementBaru.id = "faq-title-".concat(i);
    elementBaru.addEventListener('click', faqClick);
    list_title.appendChild(elementBaru);
  });
};

var initFaqContent = function initFaqContent(index) {
  var lang = Object(_pageActivator__WEBPACK_IMPORTED_MODULE_0__["getLang"])(window.location.href);
  var data = lang.match('id') ? faq : faq_en;
  var list_content = document.getElementById("faq-dropdown-list");
  list_content.innerHTML = "";
  list_content.innerHTML += " <h5 class=\"mt-lg-0 mt-5 subheading color-secondary-green-dark ml-5\">".concat(data[index].title, "</h5>");
  data[index].item.forEach(function (satu, i) {
    list_content.innerHTML += "\n            <div class=\"features-item features-alt-item\">\n                <input type=\"checkbox\" id=\"features".concat(i, "\" name=\"feature\" class=\"features-input\" onclick=\"Rotates(event)\">\n                <label for=\"features").concat(i, "\" class=\"features-label features-alt-label mb-0\">\n                    <span class=\"features-title features-alt-title text-dark\">").concat(i + 1, ". ").concat(satu.title, "</span>\n                    <i class=\"fas fa-caret-down features-arrow fa-2x text-dark\" id=\"arrow").concat(i, "\"></i>\n                </label>\n                <span class=\"features-droplist\">\n                    <li class=\"features-dropitem features-alt-drop-item mt-0\">\n                        <div class=\"container-fluid\">\n                            <div class=\"container-fluid\">").concat(satu.content, "</div>\n                        </div>\n                    </li>\n                </span>\n            </div>\n        ");
  });
};

var faqClick = function faqClick(e) {
  var faq_id = parseInt(e.target.id.slice(-2));

  if (faq_id < 0) {
    faq_id = faq_id * -1;
  }

  initFaqTitle(faq_id);
  initFaqContent(faq_id);
};

initFaqTitle(0);
initFaqContent(0);

/***/ })

}]);