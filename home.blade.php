<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pricing</title>
</head>
<body>
    <form>
        @csrf
        <div class="form-group">
          <label for="pricingName">Nama</label>
          <input type="text" class="form-control" id="pricingName" name="name">
          @if($errors->has('name'))
          <small id="nameHelp" class="form-text text-muted text-warning">{{ $errors->first('name') }}</small>
        @endif
        </div>
        <div class="form-group">
            <label for="pricingEmail">Email</label>
            <input type="email" class="form-control" id="pricingEmail" name="email">
            @if($errors->has('email'))
            <small id="nameHelp" class="form-text text-muted text-warning">{{ $errors->first('email') }}</small>
          @endif
          </div>
          <div class="form-group">
            <label for="pricingPhone">Phone</label>
            <input type="text" class="form-control" id="pricingPhone" name="phone">
            @if($errors->has('phone'))
            <small id="nameHelp" class="form-text text-muted text-warning">{{ $errors->first('phone') }}</small>
          @endif
          </div>
          <div class="form-group">
            <label for="pricingCity">City</label>
            <input type="text" class="form-control" id="pricingCity" name="city">
            @if($errors->has('city'))
            <small id="nameHelp" class="form-text text-muted text-warning">{{ $errors->first('city') }}</small>
          @endif
          </div>
          <div class="form-group">
            <label for="pricingPostCode">Postal Code</label>
            <input type="text" class="form-control" id="pricingPostCode" name="postCode">
            @if($errors->has('postCode'))
            <small id="nameHelp" class="form-text text-muted text-warning">{{ $errors->first('postCode') }}</small>
          @endif
          </div>
          <div class="form-group">
            <label for="pricingAddress">Address</label>
            <input type="text" class="form-control" id="pricingAddress" name="address">
            @if($errors->has('address'))
            <small id="nameHelp" class="form-text text-muted text-warning">{{ $errors->first('address') }}</small>
          @endif
          </div>
          <div class="form-group">
            <label for="pricingAddress">Address</label>
            <input type="text" class="form-control" id="pricingAddress" name="address">
            @if($errors->has('address'))
            <small id="nameHelp" class="form-text text-muted text-warning">{{ $errors->first('address') }}</small>
          @endif
          </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
</body>
</html>
