<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    protected $fillable = [
        'name','email','no_telp','city','postal_code','address','status','order_id'
    ];

}
