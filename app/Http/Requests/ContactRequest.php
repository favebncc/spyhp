<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\Captcha;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nameContact'=>'required | max:30',
            'emailContact'=>'required | email',
            'subjectContact'=>'required|max:50',
            'messageContact'=>'required | max:300',
            'g-recaptcha-response'=> new Captcha()
        ];
    }

    public function message()
    {
        return [
            'nameContact.required'=>'nameContact Must be Input to respond',
            'nameContact.max'=>'nameContact must be under 30 alphabet',
            'emailContact.required'=>'Email Must be Input to respond',
            'emailContact.email'=>'this is not formatted type email',
            'messageContact.required'=>'Messages must be input to respond',
            'messageContact.max'=>'Messages must be under 300 Alhabet',
            'subjectContact.required'=>'Messages must be input to respond',
            'subjectContact.max'=>'Messages must be under 50 Alhabet'
        ];
    }
}
