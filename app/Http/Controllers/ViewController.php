<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductItem;
use Illuminate\Support\Facades\App;

class ViewController extends Controller
{
    function viewHome(){
        return view('home');
    }

    function viewAbout(){
        return view('about');
    }

    function viewFeaturesComputer(){
        return view('features.features_computer');
    }

    function viewFeaturesAndroid(){
        return view('features.features_android');
    }

    function viewInstallAndroid(){
        return view('installGuide.installGuide_android');
    }

    function viewInstallComputer(){
        return view('installGuide.installGuide_computer');
    }

    function viewInstallMac(){
        return view('installGuide.installGuide_mac');
    }

    function viewFaq(){
        return view('faq');
    }

    function viewPrice(){
        $item =ProductItem::all();
        return view('price',compact('item'));
    }

    function viewFeature($lang, $id){
        return view('features.feature',['lang' => $lang, 'id' => $id]);
    }

    function viewContact(){
        return view('contact');
    }

    function viewHowSPYHP(){
        return view ('serverSPYHP');
    }

    function viewReport(){
        return view('report');
    }

    function viewPrivacy(){
        return view('privacy');
    }

    function viewLegal(){
        return view('legal');
    }

    function viewTestimony(){
        return view('testimony');
    }

    function viewKeylogger(){
        return view('keylogger');
    }
    function viewStatusPayment($lang,$status){
//dd($status,$lang);
        return view('success',compact('status','lang'));
    }
}

