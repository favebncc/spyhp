<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\ContactRequest;
use App\Rules\Captcha;
use App\Rules\ContactRule;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::orderBy('created_at', 'desc')->paginate(6);
        return view('contactAdmin',compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        // $this->googleCaptcha($request);
        $request->validate([
            'nameContact'=>'required|max:50',
            'emailContact'=>'required|email',
            'subjectContact'=>'required|max:50',
            'messageContact'=>'required|max:500',
            'g-recaptcha-response'=>new Captcha(),
        ]);
        Contact::create([
            'Name'=>$request->nameContact,
            'Email'=>$request->emailContact,
            'Subject'=>$request->subjectContact,
            'Messages'=>$request->messageContact,
        ]);
        return redirect()->back()->with('message','Your message has been sent');
    }
    public function googleCaptcha(Request $request){
        dd($request);
    }

}
