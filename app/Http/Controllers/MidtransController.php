<?php

namespace App\Http\Controllers;

use App\ProductItem;
use App\Buyer;
use Illuminate\Foundation\Console\Presets\React;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\BuyerController;

class MidtransController extends Controller
{
    public function viewProfile(Request $request){
        $type =$request->type;
        $item = ProductItem::where('type',$type)->first();
        return view('pricing',compact('item'));
    }
    public function midtransAPI(Request $request){

        $customer = app('App\Http\Controllers\BuyerController')->storeCustomerData($request);
        $client = new Client();
        $split = explode(" ", $request->pricingName);
        $firstname = array_shift($split);
        $lastname  = implode(" ", $split);
        $orderId = rand(1,999999999);
        $customer->order_id = $orderId;
        $customer->save();

        $url =['https'=>'https://app.midtrans.com/snap/v1/transactions/'];
        $headers = [
            'headers'=>[
            'Accept'=> 'application/json',
            'Content-Type'=> 'application/json',
            'Authorization'=>'Basic '.base64_encode(env('SERVER_KEY').':'),
            ],

            'body'=>json_encode([
                'transaction_details'=> [
                    'order_id'=> $orderId,
                    'gross_amount'=> $request->pricingPrice
                ],
                'item_details'=>[
                    'price'=>$request->pricingPrice,
                    'name'=>'SpyHp '.$request->pricingType.' for '.$request->pricingMonths.'months',
                    'quantity'=>'1'
                ],
                'customer_details'=> [
                    'first_name'=> $firstname,
                    'last_name'=> $lastname,
                    'email'=> $request->pricingEmail,
                    'phone'=> $request->pricingPhone,
                    'billing_address'=> [
                        'email'=> $request->pricingEmail,
                        'phone'=> $request->pricingPhone,
                        'address'=> $request->pricingAddress,
                        'city'=> $request->pricingCity,
                        'postal_code'=> $request->pricingPostCode,
                      ]
                    ],
            ])
        ];

        $response = $client->request('POST',$url['https'],$headers);

        $snapToken = json_decode($response->getBody())->token;
        return response()->json([
            'snapToken' => $snapToken
        ]);
    }
    public function notification(Request $request){
//dd($request);        
$request = json_decode($request->getContent(),true);
        $this->updateStatus($request);
    }
    public function finish(Request $request,$status){
        $request = (array)json_decode($request->input('response'));
        $this->updateStatus($request);
        return redirect('/payment/'.$status);
    }
    public function updateStatus($request){
        $customer = Buyer::where('order_id',$request['order_id'])->first();
        $customer->status = $request['transaction_status'];
        $customer->save();
        $input = $request['order_id'].$request['status_code'].$request['gross_amount'].env('SERVER_KEY');
        $signature = openssl_digest($input, 'sha512');
    }
}
