<?php

namespace App\Http\Controllers;

use App\Buyer;
use App\ProductItem;
use App\Report;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class BuyerController extends Controller
{

    public function viewItem()
    {
        $products = ProductItem::all();
        return view('itemProduct',compact('products'));
    }

    public function viewCustomer(){
        $customers = Buyer::orderBy('created_at', 'desc')->paginate(6);
        $reports = Report::orderBy('created_at', 'desc')->paginate(6);
        return view('adminLTE',compact('customers','reports'));
    }

    public function storeCustomerData(Request $request)
    {
        $request->validate([
            'pricingName'=>'required|String',
            'pricingEmail'=>'required|email',
            'pricingPhone'=>'required|min:10|max:15',
            'pricingCity'=>'required|string',
            'pricingPostCode'=>'required|regex:"^\d{5}$"',
            'pricingAddress'=>'required|string',
        ]);
        $data = Buyer::create([
            'name'=>$request->pricingName,
            'email'=>$request->pricingEmail,
            'no_telp'=>$request->pricingPhone,
            'address'=>$request->pricingAddress,
            'city'=>$request->pricingCity,
            'postal_code'=>$request->pricingPostCode,
            'status'=>'pending',
        ]);
            return $data;
    }

    public function editItem($id)
    {
        $item = ProductItem::find($id);
        return view('itemUpdate', compact('item'));
    }
    public function updateItem(Request $request, $id){
        $item = ProductItem::find($id);
        $item->update([
            'type' => $request->input('type'),
            'price' => $request->input('price'),
            'months' => $request->input('months'),
        ]);
        return redirect('/item');
    }
}
