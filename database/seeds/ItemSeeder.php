<?php

use App\ProductItem;
use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['type'=>'Basic', 'price'=>'550000', 'months'=>'3'],
            ['type'=>'Premium', 'price'=>'1300000', 'months'=>'12'],
            ['type'=>'Standard', 'price'=>'850000', 'months'=>'6'],
        ];
        foreach ($items as $item) {
            ProductItem::updateOrCreate(['type' => $item['type']], $item);
        }
    }
}
