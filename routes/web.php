<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\ViewController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::middleware('auth')->group(function(){
    Route::get('/admin', 'BuyerController@viewCustomer')->name('admin');
    Route::get('/admin/contact', 'ContactController@index');
    Route::get('/item','BuyerController@viewItem');
    Route::get('/item/{id}/edit', 'BuyerController@editItem');
    Route::post('/item/{id}/update', 'BuyerController@updateItem');
    // Route::post('/pricing','BuyerController@storeData')->name('getDataPayment');
    // Route::get('/pricing','MidtransController@midtransAPI')->name('getDataPayment');
    // Route::post('payment/success','MidtransController@finish');
    Route::get('/home', 'HomeController@index')->name('home');
});
Route::redirect('/','/id');
Route::redirect('/price','/id/price');
Route::redirect('/faq','/id/faq');
Route::redirect('/about','/id/about');
Route::redirect('/home','/id');
//Route::get('/payment/{status}','ViewController@viewStatusPayment')->name('viewStatus');
Route::post('/report/post','ReportController@store');
Route::group(['prefix'=>'{lang?}','where' => ['lang' => '[a-zA-Z]{2}'],], function () {
    Route::get('/pricing/{type}', 'MidtransController@viewProfile');

    Route::get('/payment', function () {
        return view('midtrans');
    });
Route::get('/payment/{status}','ViewController@viewStatusPayment')->name('viewStatus');
    Route::post('/post','ContactController@store')->name('addContact');
   // Route::post('/report/post', 'ReportController@store');

    Route::get('/', 'ViewController@viewHome')->name('viewHome');
    Route::get('/about','ViewController@viewAbout')->name('viewAbout');
    Route::get('/faq','ViewController@viewFaq')->name('viewFaq');
    Route::get('/price','ViewController@viewPrice')->name('viewPrice');
    Route::get('/contact','ViewController@viewContact')->name('viewContact');
    Route::get('/server','ViewController@viewHowSPYHP')->name('viewServer');
    Route::get('/report','ViewController@viewReport')->name('viewReport');
    Route::get('/privacy/#refund','ViewController@vieRefund')->name('viewRefund');
    Route::get('/privacy','ViewController@viewPrivacy')->name('viewPrivacy');
    Route::get('/legal-info','ViewController@viewLegal')->name('viewLegal');
    Route::get('/testimony','ViewController@viewTestimony')->name('viewTestimony');


    Route::get('/features/computer','ViewController@viewFeaturesComputer')->name('viewFeaturesComputer');
    Route::get('/features/android', 'ViewController@viewFeaturesAndroid')->name('viewFeaturesAndroid');
    Route::get('/features/keylogger','ViewController@viewKeylogger')->name('viewKeylogger');
    Route::get('/features/{id}','ViewController@viewFeature')->name('viewFeature');

    Route::get('/install-guide/Android','ViewController@viewInstallAndroid')->name('viewInstallAndroid');
    Route::get('/install-guide/Computer','ViewController@viewInstallComputer')->name('viewInstallComputer');
    Route::get('/install-guide/Mac_Os','ViewController@viewInstallMac')->name('viewInstallMac');


});

