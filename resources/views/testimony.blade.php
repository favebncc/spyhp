@extends('layouts.head')

@section('title','Testimony')

@section('content')
    <div class="container">
        <div class="d-flex justify-content-center align-items-center mt-4">
        <h1 class="heading">{{__('testimony.header')}}</h1>
        </div>

        <span>{{__('testimony.text')}}</span>
    <div class="mb-2 text-justify" id="list-testimony">



    </div>

    </div>


@endsection
