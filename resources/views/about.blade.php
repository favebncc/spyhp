@extends('layouts.head') @section('title','About') @show @section('content')
<div class="about-page">
    <section class="container py-5">
        <div class="row">
            <div class="col-lg-6 pr-4">
                <h1 class="heading">{{__('message.About')}}</h1>
                <p class="text-justify font-subheader">
                    {{__('message.aboutWelcome')}}</p>
            </div>
            <div class="col-lg-6">
                <iframe
                    width="100%"
                    height="310"
                    src="https://www.youtube.com/embed/Sk3hPPvkQhk"
                    frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
                ></iframe>
            </div>
        </div>
        <p class="text-justify font-subheader">
            {{__('message.aboutSoftware')}}
        </p>
        <p class="text-justify font-subheader">
            {{__('message.aboutFitur')}}
        </p>
    </section>
    <section
        class="banner d-flex justify-content-center align-items-center flex-column py-4"
    >
        <h1 class="heading banner-title">{{__('message.ideology')}}</h1>
        <p class="banner-paragraph-regular text-center mb-0 font-subheader">
            {{__('message.aboutKomitmen')}}
        </p>
    </section>
</div>
@endsection
