@extends('layouts.head')

@section('title','Keylogger')

@section('content')

<div class="container">
    <div class="d-flex justify-content-center text-center mt-3">
        <h1 class="heading">
            {{__('message.keylogHead')}}
        </h1>
    </div>
</div>

<div class="my-3 bg-secondary-white d-flex justify-content-center align-items-center flex-column">
    <span class="mt-3">White Spy {{__('message.keyBoard')}}</span>
    <span class="mt-3">{{__('message.keySpec')}}</span>

    <img src={{asset('Assets/Picture/Features/Feature_Details/Keylogger/keylogger1.png')}} class="my-4" alt="keylogger">
    <a name="" id="" class="btn btn-primary button my-3"
    href="https://s3-sa-east-1.amazonaws.com/ourdata/bruno/tecladobranco.html" role="button">Download Keylogger</a>
<span class="mb-3 mx-3">{{__('message.open')}} <a href="http://tiny.cc/keywhite">http://tiny.cc/keywhite</a> {{__('message.or')}} <a
        href="https://bit.do/keywhite">https://bit.do/keywhite</a>{{__('message.keyLink')}}</span>
        <div class="d-flex justify-content-center align-items-center flex-column">
            <img src={{asset('Assets/Picture/Features/Feature_Details/Keylogger/keylogger2.png')}} alt="keylogger-2">
            <span class="my-3 text-center text-lg-left">{{__('message.keySubHead')}}</span>
            <a name="" id="" class="btn btn-primary button my-3" href="https://s3-sa-east-1.amazonaws.com/ourdata/bruno/tecladobranco.html" role="button">{{__('message.downApp')}}</a>
            <span class="my-3 mx-4">{{__('message.open')}} <a href="http://tiny.cc/keywhite">http://tiny.cc/keywhite</a>  {{__('message.keyLink')}}</span>
            <span class="my-3 text-center text-lg-left">Black SPY {{__('message.keyBoard')}}</span>
            <img src={{asset('Assets/Picture/Features/Feature_Details/Keylogger/keylogger3.png')}} alt="keylogger-3">
            <span class="my-3 text-center">{{__('message.keySpec')}}</span>
            <a name="" id="" class="my-3 btn btn-primary button" href="https://bit.do/keywhite" role="button">Download Keylogger</a>
            <span class="text-center text-lg-left">{{__('message.open')}} <a href="http://bit.do/keyblack">http://bit.do/keyblack</a>{{__('message.keyLink')}}</span>
            <span class="mt-3">Computer SPY</span>
            <img src={{asset('Assets/Picture/Features/Feature_Details/Keylogger/keylogger4.png')}} alt="keylogger-4">
            <span class="my-3">{{__('message.keySubHead')}}</span>
            <a name="" id="" class="my-3 btn btn-primary button" href="http://bit.do/tispywin" role="button">Download Installer</a>
            <span class="mb-3">{{__('message.open')}} <a href="http://bit.do/tispywin">http://bit.do/tispywin</a>{{__('message.keyDownInst')}}</span>
        </div>

</div>
<div class="container">
    <h3 class="subheading">{{__('message.keySubHead2')}}</h3>
    <p class="text-justify">{{__('message.keySHead2Para1')}}</p>
    <p class="text-justify">{{__('message.keySHead2Para2')}}</p>
    <div class="d-flex flex-column justify-content-center">
        <div class="d-flex align-items-center my-3">
            <i class="fas  fa-file-alt fa-2x color-secondary-green-dark mr-4"></i>
            <span class="ml-2">{{__('message.keySeeContent')}}</span>
        </div>
        <div class="d-flex align-items-center mb-3">
            <i class="fas fa-unlock fa-2x color-secondary-green-dark mr-4"></i>
            <span class="ml-2">{{__('message.keySeePass')}}</span>
        </div>
        <div class="d-flex align-items-center mb-3">
            <i class="far fa-keyboard fa-2x color-secondary-green-dark mr-4"></i>
            <span>{{__('message.keySeeButton')}}</span>
        </div>
        <div class="d-flex align-items-center mb-3">
            <i class="fas fa-comments fa-2x color-secondary-green-dark mr-4"></i>
            <span>{{__('message.keySeeChat')}}</span>
        </div>
        <div class="d-flex align-items-center mb-3">
            <i class="fas fa-tachometer-alt fa-2x color-secondary-green-dark mr-4"></i>
            <span>{{__('message.keyEasyDash')}}.</span>
        </div>
        <div class="d-flex align-items-center mb-3">
            <i class="fas fa-language fa-2x color-secondary-green-dark mr-4"></i>
            <span>{{__('message.KeyBilng')}}</span>
        </div>
    </div>
    <p class="text-justify">{{__('message.keyVersion')}}</p>
    <p class="text-justify">{{__('message.keySpy')}}</p>
    <h3 class="subheading">{{__('message.keyStep')}}</h3>

    <ul class="ml-3">
        <li>{{__('message.keyStep1')}}</li>
        <li>{{__('message.keyStep2')}}</li>
        <li>{{__('message.keyStep3')}}</li>
        <li>{{__('message.keyStep4')}}</li>
        <li>{{__('message.keyStep5')}}</li>
        <li>{{__('message.keyStep6')}}</li>
        <li>{{__('message.keyStep7')}}</li>
        <li>{{__('message.keyStep8')}}</li>
        <li>{{__('message.keyStep9')}}</li>
        <li>{{__('message.keyStep10')}}</li>
    </ul>
    <h3 class="subheading">{{__('message.keyStepHide')}}</h3>
    <ul class="ml-3">
        <li>{{__('message.keyStepHide1')}}</li>
        <li>{{__('message.keyStepHide2')}}</li>
        <li>{{__('message.keyStepHide3')}}</li>
    </ul>
    <p class="text-justify">{{__('message.keyRemind')}}</p>

</div>


@endsection
