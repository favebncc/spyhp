@extends('layouts.head')

@section('title','Payment Status')
@section('content')
    <div style="height: 75vh; "  class="container my-3 d-flex flex-column justify-content-center align-items-center">
        <h1 class="heading mb-2">Your Payment is {{$status}}</h1>
    <p class="text-center">{{__('transaction.text')}}</p>
        <a href="{{route('viewHome',app()->getLocale())}}" class="btn btn-primary button">Ok, Back to Home</a>
    </div>
@endSection
