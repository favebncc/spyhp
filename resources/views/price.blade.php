@extends('layouts.head')

@section('title','Price')

@section('content')
<div class="container">
    <div class="d-flex justify-content-center flex-column align-items-center">
        <h1 class="heading mt-3">{{__('message.price')}}</h1>
        <div class=" d-none flex-column border-radius-alt justify-content-center p-3 m-4 align-items-center bg-secondary-white">
            <h5 class="subheading">Kode promo</h5>
            <form>
                <div class="d-flex justify-content-lg-around flex-column align-items-center flex-lg-row w-100 p-3">
                    <input type="text" class="shadow border-none border-radius d-flex text-center px-5 py-3 mb-lg-0 mb-3"
                        placeholder="Kode Promo Anda">
                <a href="#" type="submit" class="btn ml-lg-3  btn-primary button w-100">Gunakan Promo</a>
                </div>
            </form>
        </div>
        <span class="fa-sm d-none mb-5">Kode Promo <span class="d-none color-secondary-green-dark">PROMO CODE - diskon 45%</span> Telah
            Digunakan</span>
    </div>

    <div class="row d-flex align-items-end mb-5">
        <div class="order-lg-1 col-xl-4">
            <div class="shadow border-radius-alt overflow-hidden">
                <div class="bg-secondary-white d-flex flex-column align-items-center ">
                    <div class=" d-flex px-4 mt-3 justify-content-center align-items-center">
                        <sup class="mr-1 mb-4">Rp</sup>
                        <h1 class="heading text-dark"> {{number_format($item[0]->price / $item[0]->months ,0,'.','.')}}</h1><sub class="ml-1"> / {{__('message.months')}}</sub>
                    </div>
                    <h4 class="subheading text-dark pb-2">{{$item[0]->type}}</h4>
                </div>

                <div class="bg-white h-100">
                    <div class="d-flex flex-column justify-content-center align-items-center ">
                        <div class="d-flex justify-content-center align-items-center">
                            <h3 class="subheading text-dark m-3">{{$item[0]->months}} {{__('message.months')}}</h3>
                            <div class="d-flex align-items-center justify-content-center  flex-column">
                                <div>
                                    <span>Rp </span><span>{{number_format($item[0]->price ,0,'.','.')}}</span>
                                </div>
                            </div>
                        </div>
                        <a href="{{asset(app()->getLocale().'/pricing/'.$item[0]->type)}}" class="btn btn-primary button mb-3"value="">{{__('message.buy')}}</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="order-lg-2 order-first col-xl-4">
            <div class=" bg-secondary-green-dark d-flex flex-column align-items-center shadow overflow-hidden border-radius-alt">
                <div class=" d-flex px-4 mt-3 justify-content-center align-items-center">
                    <sup class="mr-1 mb-4 text-light">Rp</sup>
                    <h1 class="heading text-light"> {{number_format($item[1]->price / $item[1]->months ,0,'.','.')}}</h1><sub class="text-light ml-1"> / {{__('message.months')}}</sub>
                </div>
                <div class="border-radius bg-white d-flex px-3 py-1 justify-content-center align-items-center">
                    <img src={{asset('Assets/Picture/Pricing/crown.svg')}} alt="Crown">
                    <span class="color-secondary-green-dark ml-2">{{__('message.special')}}</span>
                </div>
                <h4 class="subheading pb-2 text-light">{{$item[1]->type}}</h4>
                <div class="bg-white w-100 h-100">
                    <div class="d-flex flex-column justify-content-center align-items-center ">
                        <div class="d-flex justify-content-center align-items-center">
                            <h3 class="subheading text-dark m-3">{{$item[1]->months}} {{__('message.months')}}</h3>
                            <div class="d-flex align-items-center justify-content-center  flex-column">
                                <div>
                                <span>Rp </span><span>{{number_format($item[1]->price,0,'.','.')}}</span>
                                </div>
                            </div>
                        </div>
                        <a href="{{asset(app()->getLocale().'/pricing/'.$item[1]->type)}}" class="btn btn-primary button mb-3">{{__('message.buy')}}</a>
                    </div>
                </div>

            </div>


        </div>
        <div class="order-lg-3 col-xl-4">
            <div class="shadow border-radius-alt overflow-hidden">
                <div class="bg-secondary-white d-flex flex-column align-items-center ">
                    <div class=" d-flex px-4 mt-3 justify-content-center align-items-center">
                        <sup class="mr-1 mb-4">Rp</sup>
                        <h1 class="heading text-dark"> {{number_format($item[2]->price / $item[2]->months ,0,'.','.')}}</h1><sub class="ml-1"> / {{__('message.months')}}</sub>
                    </div>
                    <h4 class="subheading text-dark pb-2">{{$item[2]->type}}</h4>
                </div>

                <div class="bg-white h-100">
                    <div class="d-flex flex-column justify-content-center align-items-center ">
                        <div class="d-flex justify-content-center align-items-center">
                            <h3 class="subheading text-dark m-3">{{$item[2]->months}} {{__('message.months')}}</h3>
                            <div class="d-flex align-items-center justify-content-center  flex-column">
                                <div>
                                    <span>Rp </span><span>{{number_format($item[2]->price ,0,'.','.')}}</span>
                                </div>
                            </div>
                        </div>
                        <a href="{{asset(app()->getLocale().'/pricing/'.$item[2]->type)}}" class="btn btn-primary button mb-3">{{__('message.buy')}}</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 ">
            <div class="features-item my-4">
                <input type="checkbox" id="features1" name="feature" class="features-input" onclick="Rotates(event)">
                <label for="features1" class="features-label mb-0 px-4">
                    <span class="features-title">{{__('message.price')}}</span>
                    <i class="fas fa-caret-down features-arrow fa-2x" id="arrow1"></i>
                </label>
                <ul class="features-droplist">
                    <li class="features-dropitem mt-0 px-2">
                        <div class="row w-100">
                            <div class="col-lg-4" >
                                <div id="price-content-1" >

                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div id="price-content-2">

                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div id="price-content-3">

                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </div>
    @isset($record)
        @elseisset
        
    @endisset
    <div class="mb-5 d-flex flex-column justify-content-center align-items-center">
        <img src={{asset('Assets/Picture/Pricing/10days-guarantee.png')}} alt="10days-guarantee" class="m-2" height="200px">
        <span>{{__('message.price2')}}</span>
    </div>

</div>

<div class="bg-secondary-white">
    <div class="container">
        <div class="d-flex justify-content-center flex-column align-items-center ">
            <h4 class="subheading my-4">{{__('message.price3')}}</h4>
            <div class="d-flex flex-column" id="price-content-list">

            </div>

        </div>
    </div>
</div>

@endsection
