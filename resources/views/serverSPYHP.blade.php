@extends('layouts.head')

@section('title','HOW SPYHP Work')

@section('content')
    <div class="container">
        <div class="d-flex justify-content-center align-items-center mt-5">
            <h1 class="heading text-center text-lg-left">{{__('serverSPYHP.spyhpFunction')}}</h1>
        </div>
        <section class="feature container-fluid">
            <div class="feature-item-container row no-gutters d-flex justify-content-center align-items-center">
                <div class="feature-item col-lg-3 m-4 pt-4">
                    <figure class="d-flex flex-column justify-content-center align-items-center">
                        <h5 class="subheading text-dark">{{__('serverSPYHP.head1')}}</h5>
                        <img src={{asset('Assets/Picture/phone-illustration.svg')}} alt="phone-illustration"
                            class="feature-image">
                        <figcaption class="feature-caption text-center px-2">{{__('serverSPYHP.text1')}}</figcaption>
                    </figure>
                </div>
                <div class="feature-item col-lg-3 m-4 pt-4">
                    <figure class="d-flex flex-column justify-content-center align-items-center">
                        <h5 class="subheading text-dark">{{__('serverSPYHP.head2')}}</h5>
                        <img src={{asset('Assets/Picture/How_Tispy_Work/spyhp_server.svg')}} alt="manage-calls-illustration"
                            class="feature-image">
                        <figcaption class="feature-caption text-center px-2">{{__('serverSPYHP.text2')}}</figcaption>
                    </figure>
                </div>
                <div class="feature-item col-lg-3 m-4 pt-4">
                    <figure class="d-flex flex-column justify-content-center align-items-center">
                        <h5 class="subheading text-dark">{{__('serverSPYHP.head3')}}</h5>
                        <img src={{asset('Assets/Picture/How_Tispy_Work/spyhp_dashboard.svg')}} alt="text-message-illustrations"
                            class="feature-image">
                        <figcaption class="feature-caption text-center px-2">{{__('serverSPYHP.text3')}}</figcaption>
                    </figure>
                </div>
            </div>
        </section>

        <iframe class="w-100" height="500" width="560"  src="https://www.youtube.com/embed/cdcsM9bTJSE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

        <section class="features">
            <h1 class="heading d-flex justify-content-center align-items-center m-4">{{__('message.Feature')}}</h1>
            <div class="features-container row no-gutters w-100 px-5">
                <div class="col-lg-4 col-md-6 features-list">
                    <div class="features-item">
                        <input type="checkbox" id="features1" name="feature" class="features-input" onclick="Rotates(event)">
                        <label for="features1" class="features-label mb-0">
                            <span class="features-title">{{__('serverSPYHP.headMonitor')}}</span>
                            <i class="fas fa-caret-down features-arrow fa-2x" id="arrow1"></i>
                        </label>
                        <ul class="features-droplist">
                            <li class="features-dropitem mt-0">
                                <a href="../{{app()->getLocale()}}/features/1" class="features-droplink">
                                    {{__('serverSPYHP.viewCall')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="../{{app()->getLocale()}}/features/2" class="features-droplink">
                                    {{__('serverSPYHP.viewTime')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="../{{app()->getLocale()}}/features/10" class="features-droplink">
                                    {{__('serverSPYHP.phonebook')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="../{{app()->getLocale()}}/features/11" class="features-droplink">
                                    {{__('serverSPYHP.block')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                    </div>

                    <div class="features-item">
                        <input type="checkbox" id="features2" name="feature" class="features-input" onclick="Rotates(events)">
                        <label for="features2" class="features-label mb-0">
                            <span class="features-title">{{__('serverSPYHP.monitorWeb')}}</span>
                            <i class="fas fa-caret-down features-arrow fa-2x" id="arrow2"></i>
                        </label>
                        <ul class="features-droplist">
                            <li class="features-dropitem mt-0">
                                <a href="../{{app()->getLocale()}}/features/24" class="features-droplink">
                                    {{__('serverSPYHP.viewTraffic')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="../{{app()->getLocale()}}/features/25" class="features-droplink">
                                    {{__('serverSPYHP.getVisitorData')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="../{{app()->getLocale()}}/features/26" class="features-droplink">
                                    {{__('serverSPYHP.oneClick')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                    </div>

                    <div class="features-item">
                        <input type="checkbox" id="features3" name="feature" class="features-input" onclick="Rotates(event)">
                        <label for="features3" class="features-label mb-0">
                            <span class="features-title">{{__('serverSPYHP.fileBrowse')}}</span>
                            <i class="fas fa-caret-down features-arrow fa-2x" id="arrow3"></i>
                        </label>
                        <ul class="features-droplist">
                            <li class="features-dropitem mt-0">
                                <a href="../{{app()->getLocale()}}/features/30" class="features-droplink">
                                    {{__('serverSPYHP.remoteAccess')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.viewAVP')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.download')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.checkFile')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                    </div>

                    <div class="features-item">
                        <input type="checkbox" id="features4" name="feature" class="features-input" onclick="Rotates(event)">
                        <label for="features4" class="features-label mb-0">
                            <span class="features-title">{{__('serverSPYHP.remoteMonitor')}}</span>
                            <i class="fas fa-caret-down features-arrow fa-2x" id="arrow4"></i>
                        </label>
                        <ul class="features-droplist">
                            <li class="features-dropitem mt-0">
                                <a href="" class="features-droplink">
                                    {{__('serverSPYHP.clearData')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    <i>{{__('serverSPYHP.factory')}}</i> {{__('serverSPYHP.device')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.unlockDevice')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.downData')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.wirelessSMS')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.avp')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                    </div>

                    <div class="features-item">
                        <input type="checkbox" id="features5" name="feature" class="features-input" onclick="Rotates(event)">
                        <label for="features5" class="features-label mb-0">
                            <span class="features-title">{{__('serverSPYHP.monSMS')}}</span>
                            <i class="fas fa-caret-down features-arrow fa-2x" id="arrow5"></i>
                        </label>
                        <ul class="features-droplist">
                            <li class="features-dropitem mt-0">
                                <a href="../{{app()->getLocale()}}/features/3" class="features-droplink">
                                    {{__('serverSPYHP.viewSMS')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="../{{app()->getLocale()}}/features/4" class="features-droplink">
                                    {{__('serverSPYHP.viewMMS')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="../{{app()->getLocale()}}/features/5" class="features-droplink">
                                    {{__('serverSPYHP.trackSender')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                    </div>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-6 features-list">
                    <div class="features-item">
                        <input type="checkbox" id="features6" name="feature" class="features-input" onclick="Rotates(event)">
                        <label for="features6" class="features-label mb-0">
                            <span class="features-title">{{__('serverSPYHP.monGPS')}}</span>
                            <i class="fas fa-caret-down features-arrow fa-2x" id="arrow6"></i>
                        </label>
                        <ul class="features-droplist">
                            <li class="features-dropitem mt-0">
                                <a href="../{{app()->getLocale()}}/features/6" class="features-droplink">
                                    {{__('serverSPYHP.viewLocation')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="../{{app()->getLocale()}}/features/7" class="features-droplink">
                                    {{__('serverSPYHP.loactionHistory')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="../{{app()->getLocale()}}/features/8" class="features-droplink">
                                    {{__('serverSPYHP.Geofence')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="../{{app()->getLocale()}}/features/9" class="features-droplink">
                                    {{__('serverSPYHP.notifGeofence')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="features-item">
                        <input type="checkbox" id="features7" name="feature" class="features-input" onclick="Rotates(event)">
                        <label for="features7" class="features-label mb-0">
                            <span class="features-title">{{__('serverSPYHP.monApp')}}</span>
                            <i class="fas fa-caret-down features-arrow fa-2x" id="arrow7"></i>
                        </label>
                        <ul class="features-droplist">
                            <li class="features-dropitem mt-0">
                                <a href="../{{app()->getLocale()}}/features/30" class="features-droplink">
                                    {{__('serverSPYHP.viewDeviceConnected')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.viewAVP')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.blockApp')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.activateApp')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.viewConnectedTime')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                    </div>

                    <div class="features-item">
                        <input type="checkbox" id="features8" name="feature" class="features-input" onclick="Rotates(event)">
                        <label for="features8" class="features-label mb-0">
                            <span class="features-title">{{__('serverSPYHP.convinient')}}</span>
                            <i class="fas fa-caret-down features-arrow fa-2x" id="arrow8"></i>
                        </label>
                        <ul class="features-droplist">
                            <li class="features-dropitem mt-0">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.easyUsage')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.easyInstall')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.uninstall')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.usefulInfo')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.webPort')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.workSecretive')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                    </div>

                    <div class="features-item">
                        <input type="checkbox" id="features9" name="feature" class="features-input" onclick="Rotates(event)">
                        <label for="features9" class="features-label mb-0">
                            <span class="features-title">{{__('serverSPYHP.secureCommand')}}</span>
                            <i class="fas fa-caret-down features-arrow fa-2x" id="arrow9"></i>
                        </label>
                        <ul class="features-droplist">
                            <li class="features-dropitem mt-0">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.instanLocation')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.sendMessage')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.hidePhone')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.filterContent')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.callback')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.autoreply')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.activateWifi')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                    </div>

                    <div class="features-item">
                        <input type="checkbox" id="features10" name="feature" class="features-input" onclick="Rotates(event)">
                        <label for="features10" class="features-label mb-0">
                            <span class="features-title">{{__('serverSPYHP.scheduler')}}</span>
                            <i class="fas fa-caret-down features-arrow fa-2x" id="arrow10"></i>
                        </label>
                        <ul class="features-droplist">
                            <li class="features-dropitem mt-0">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.scheduleRecord')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.schedulePhoto')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.scheduleMulti')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                    </div>

                </div>
                <div class="col-lg-4 col-md-6 features-list">
                    <div class="features-item">
                        <input type="checkbox" id="features11" name="feature" class="features-input" onclick="Rotates(event)">
                        <label for="features11" class="features-label mb-0">
                            <span class="features-title">{{__('serverSPYHP.instanAlert')}}</span>
                            <i class="fas fa-caret-down features-arrow fa-2x" id="arrow11"></i>
                        </label>
                        <ul class="features-droplist">
                            <li class="features-dropitem mt-0">
                                <a href="../{{app()->getLocale()}}/features/27" class="features-droplink">
                                    {{__('serverSPYHP.dailyAlert')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="../{{app()->getLocale()}}/features/28" class="features-droplink">
                                    {{__('serverSPYHP.alertlocation')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="../{{app()->getLocale()}}/features/29" class="features-droplink">
                                    {{__('serverSPYHP.alertSim')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.alertSystem')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                    </div>

                    <div class="features-item">
                        <input type="checkbox" id="features12" name="feature" class="features-input" onclick="Rotates(event)">
                        <label for="features12" class="features-label mb-0">
                            <span class="features-title">{{__('serverSPYHP.monClipboard')}}</span>
                            <i class="fas fa-caret-down features-arrow fa-2x" id="arrow12"></i>
                        </label>
                        <ul class="features-droplist">
                            <li class="features-dropitem mt-0">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.viewText')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.secureAccount')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.messageWA')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.checkUrl')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                    </div>

                    <div class="features-item">
                        <input type="checkbox" id="features13" name="feature" class="features-input" onclick="Rotates(event)">
                        <label for="features13" class="features-label mb-0">
                            <span class="features-title">{{__('serverSPYHP.supervisionA/V')}}</span>
                            <i class="fas fa-caret-down features-arrow fa-2x" id="arrow13"></i>
                        </label>
                        <ul class="features-droplist">
                            <li class="features-dropitem mt-0">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.liveAV')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.liveVideo')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.clearAudio')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.capturePhoto')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem">
                                <a href="#" class="features-droplink">
                                    {{__('serverSPYHP.screenshoot')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                    </div>

                    <div class="features-item">
                        <input type="checkbox" id="features14" name="feature" class="features-input" onclick="Rotates(event)">
                        <label for="features14" class="features-label mb-0">
                            <span class="features-title">{{__('serverSPYHP.monSosmed')}}</span>
                            <i class="fas fa-caret-down features-arrow fa-2x" id="arrow14"></i>
                        </label>
                        <ul class="features-droplist">
                            <li class="features-dropitem features-no-underline mt-0">
                                <a href="../{{app()->getLocale()}}/features/12" class="features-droplink features-underline">
                                    WhatsApp
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                                <a href="../{{app()->getLocale()}}/features/22" class="features-droplink features-underline">
                                    Hike
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem features-no-underline">
                                <a href="../{{app()->getLocale()}}/features/13" class="features-droplink features-underline">
                                    Facebook
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                                <a href="../{{app()->getLocale()}}/features/23" class="features-droplink features-underline">
                                    Hangouts
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem features-no-underline ">
                                <a href="../{{app()->getLocale()}}/features/14" class="features-droplink features-underline">
                                    Gmail
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                                <a href="../{{app()->getLocale()}}/features/31" class="features-droplink features-underline">
                                    Instagram
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem features-no-underline">
                                <a href="../{{app()->getLocale()}}/features/15" class="features-droplink features-underline">
                                    Skype
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                                <a href="../{{app()->getLocale()}}/features/32" class="features-droplink features-underline">
                                    Snapchat
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem features-no-underline">
                                <a href="../{{app()->getLocale()}}/features/16" class="features-droplink features-underline">
                                    Viber
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                                <a href="../{{app()->getLocale()}}/features/33" class="features-droplink features-underline">
                                    Zalo
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem features-no-underline">
                                <a href="../{{app()->getLocale()}}/features/17" class="features-droplink features-underline">
                                    Line
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                                <a href="../{{app()->getLocale()}}/features/34" class="features-droplink features-underline">
                                    IMO
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem features-no-underline">
                                <a href="../{{app()->getLocale()}}/features/18" class="features-droplink features-underline">
                                    Kik
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                                <a href="../{{app()->getLocale()}}/features/19" class="features-droplink features-underline">
                                    Tinder
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem features-no-underline">
                                <a href="../{{app()->getLocale()}}/features/20" class="features-droplink features-underline">
                                    Telegram
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                                <a href="../{{app()->getLocale()}}/features/21" class="features-droplink features-underline">
                                    Kakoa
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                            <li class="features-dropitem features-no-underline">
                                <a href="../{{app()->getLocale()}}/features/35" class="features-droplink features-underline">
                                    {{__('serverSPYHP.voip')}}
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>

                                </a>
                                <a href="../{{app()->getLocale()}}/features/35" class="d-none features-droplink features-underline">
                                    <i class="fas fa-external-link-alt fa-xs features-link"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
        </section>
    </div>


@endsection
