@extends('layouts.head')

@section('title', 'Privacy Policy')

@section('content')
    <div class="container">
        <div class="d-flex justify-content-center align-items-center mt-4">
        <h1 class="heading">{{__('privacy.head')}}</h1>

        </div>
        <p class="text-justify">{{__('privacy.text')}}</p>
        <h3 class="subheading text-dark">{{__('privacy.subheadCollect')}}</h3>
        <p>{{__('privacy.textCollect')}}</p>

        <ol class="ml-3 text-justify">
            <li>{{__('privacy.poin1')}}</li>
            <li>{{__('privacy.poin2')}}</li>
        </ol>
        <p class="text-justify">
            {{__('privacy.textKetentuan')}}
        </p>
        <p class="text-justify">
            {{__('privacy.textKebijakan')}}
        </p>
        <p class="text-justify">
            {{__('privacy.textMengesahkan')}}
        </p>
        <h5 class="subheading text-dark">{{__('privacy.headLink')}}</h5>
        <p class="text-justify">{{__('privacy.textLink')}}</p>

        <h5 class="subheading text-dark">{{__('privacy.subheadPrivate')}}</h5>
        <p class="text-justify">{{__('privacy.textPrivate1')}}</p>
        <p class="text-justify">{{__('privacy.textPrivate2')}}</p>
        <p class="text-justify">{{__('privacy.textPrivate3')}}</p>
        <p class="text-justify">{{__('privacy.textPrivate4')}}</p>
        <p class="text-justify">{{__('privacy.textPrivate5')}}</p>
        <p class="text-justify">{{__('privacy.textPrivate6')}}</p>
        <p class="text-justify">{{__('privacy.textPrivate7')}}</p>

        <h5 class="subheading text-dark">{{__('privacy.subheadAgregat')}}</h5>
        <p class="text-justify">{{__('privacy.textAgregat1')}}</p>
        <p class="text-justify">{{__('privacy.textAgregat2')}}</p>
        <p class="text-justify">{{__('privacy.textAgregat3')}}</p>
        <p class="text-justify">{{__('privacy.textAgregat4')}}</p>
        <p class="text-justify">{{__('privacy.textAgregat5')}}</p>
        <p class="text-justify">{{__('privacy.textAgregat6')}}</p>

        <h5 class="subheading text-dark">{{__('privacy.headOther')}}</h5>
        <p class="text-justify">{{__('privacy.textOther1')}}</p>

        <h5 class="subheading text-dark">{{__('privacy.headEncrypt')}}</h5>
        <p class="text-justify">{{__('privacy.textEncrypt1')}}</p>
        <p class="text-justify">{{__('privacy.textEncrypt2')}}</p>
        <p class="text-justify">{{__('privacy.textEncrypt3')}}</p>

        <h5 class="subheading text-dark">{{__('privacy.headLaw')}}</h5>
        <p class="text-justify">{{__('privacy.textLaw')}}</p>
        <ul class="ml-3">
            <li>{{__('privacy.poin1Law')}}</li>
            <li>{{__('privacy.poin2Law')}}</li>
            <li>{{__('privacy.poin3Law')}}</li>
            <li>{{__('privacy.poin4Law')}}</li>
        </ul>
        <p class="text-justify">{{__('privacy.text1Law')}}</p>
        <p class="text-justify">{{__('privacy.text2Law')}}</p>
        <p class="text-justify">{{__('privacy.text3Law')}}</p>
        <p class="text-justify">{{__('privacy.text4Law')}}</p>
        <p class="text-justify">{{__('privacy.text5Law')}}</p>
        <p class="text-justify">{{__('privacy.text6Law')}}</p>

        <h5 class="subheading text-dark">{{__('privacy.headChange')}}</h5>
        <p class="text-justify">{{__('privacy.text1Change')}}</p>
        <p class="text-justify">{{__('privacy.text2Change')}}</p>

        <h5 class="subheading text-dark">{{__('privacy.headPrivacy')}}</h5>
        <p class="text-justify">{{__('privacy.text1Privacy')}}</p>
        <p class="text-justify">{{__('privacy.text2Privacy')}}</p>
        <p class="text-justify">{{__('privacy.text3Privacy')}}</p>

        <h5 class="subheading text-dark">{{__('privacy.headCommitment')}}</h5>
        <p class="text-justify">{{__('privacy.text1Commitment')}}</p>

        <h5 class="subheading text-dark">{{__('privacy.headRenewal')}}</h5>
        <p class="text-justify">{{__('privacy.textRenewal')}}</p>

        <h5 class="subheading text-dark">{{__('privacy.headPolicy')}}</h5>
        <p class="text-justify">{{__('privacy.text1Policy')}}</p>
        <p class="text-justify">{{__('privacy.text2Policy')}}</p>

        <h5 class="subheading text-dark">{{__('privacy.headContact')}}</h5>
        <p class="text-justify">{{__('privacy.text1Contact')}}</p>
        <p class="text-justify">{{__('privacy.text2Contact')}}</p>
    </div>
@endsection
