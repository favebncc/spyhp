@extends('layouts.head')

@section('title','Legal Info')

@section('content')
    <div class="container">
        <div class="d-flex my-4 justify-content-center align-items-center">
            <h1 class="heading">{{__('legal.Head')}}</h1>
        </div>
        <h3 class="subheading">{{__('legal.subHead1')}}</h3>
        <p class="text-justify">{{__('legal.subhead1T1')}}</p>
        <p class="text-justify">{{__('legal.subhead1T2')}}</p>

        <h5 class="subheading text-dark">{{__('legal.subHead2')}}</h5></h5>
        <p class="text-justify">{{__('legal.subhead2T1')}}</p>

        <h5 class="subheading text-dark">{{__('legal.subHead3')}}</h5>
        <p class="text-justify">{{__('legal.subhead3T1')}}</p>

        <h3 class="subheading">{{__('legal.subHead4')}}</h3>
        <p class="text-justify">{{__('legal.subhead4T1')}}</p>
        <ol class="pl-3">
            <li>{{__('legal.syarat1')}}</li>
            <li>{{__('legal.syarat2')}}</li>
            <li>{{__('legal.syarat3')}}</li>
            <li>{{__('legal.syarat4')}}</li>
            <li>{{__('legal.syarat5')}}</li>
            <li>{{__('legal.syarat6')}}</li>
            <li>{{__('legal.syarat7')}}</li>
            <li>{{__('legal.syarat8')}}</li>
            <li>{{__('legal.syarat9')}}</li>
        </ol>

        <h5 class="subheading text-dark">{{__('legal.subheadPelanggar')}}</h5>
        <p class="text-justify">{{__('legal.textPelanggar')}}</p>

        <h5 class="subheading text-dark">{{__('legal.subheadIndemfinikasi')}}</h5>
        <p class="text-justify">{{__('legal.textIndemfinikasi')}}</p>

        <h5 class="subheading text-dark">{{__('legal.subheadBatasan')}}</h5>
        <p class="text-justify">{{__('legal.textBatasan')}}</p>
        <p>{{__('legal.textBatasan2')}}</p>

        <h5 class="subheading text-dark">{{__('legal.subheadResv')}}</h5>
        <p class="text-justify">{{__('legal.textResv')}}</p>

        <h5 class="subheading text-dark">{{__('legal.subheadRestrict')}}</h5>
        <p class="text-justify">
            {{__('legal.textRestrict')}}
        </p>
        <h5 class="subheading text-dark">{{__('legal.subheadData')}}</h5>
        <p class="text-justify">{{__('legal.textData1')}}</p>
        <p class="text-justify">{{__('legal.textData2')}}</p>
        <p class="text-justify">{{__('legal.textData3')}}</p>

        <h5 class="subheading text-dark">{{__('legal.headModif')}}</h5>
        <p class="text-justify">{{__('legal.textModif')}}</p>

        <h5 class="subheading text-dark" id="refund">{{__('legal.headReturn')}}</h5>
        <p class="text-justify">{{__('legal.textReturn')}}</p>

        <h3 class="subheading">{{__('legal.headReject')}}</h3>
        <p class="text-justify">{{__('legal.textReject1')}}</p>
        <p class="text-justify">{{__('legal.textReject2')}}</p>
        <p class="text-justify">{{__('legal.textReject3')}}</p>
        <p class="text-justify">{{__('legal.textReject4')}}</p>
        <p class="text-justify">{{__('legal.textReject5')}}</p>
        <p class="text-justify">{{__('legal.textReject6')}}</p>


        <h3 class="subheading">{{__('legal.headPrivacte')}}</h3>
        <p class="text-justify">{{__('legal.textPrivacte')}}</p>
        <p class="text-justify">{{__('legal.textPrivacte2')}}</p>
        <p class="text-justify">{{__('legal.textPrivacte3')}}</p>
        <p class="text-justify">{{__('legal.textPrivacte4')}} <a href="../privacy">https://spyhp.com/privacy</a></p>

        <h5 class="subheading text-dark">{{__('legal.headSecure')}}</h5>
        <p class="text-justify">{{__('legal.textSecure')}}</p>
    </div>


@endsection
