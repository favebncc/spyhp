@extends('layouts.head')

@section('content')
    <div class="d-flex justify-content-center align-items-center flex-column">
        <h1 class="heading p-4">{{__('layout.installGuide')}}</h1>
        <div id="install-os-choose" class="d-flex">
            <a name="computer-install-button" id="computer-install-button" class="btn btn-primary button-alt text-dark" href={{route('viewInstallComputer',app()->getLocale())}} role="button" onmouseover="removeDarkText(event)" onmouseout="addDarkText(event)">{{__('layout.computer')}}</a>
            <a name="android-install-button" id="android-install-button" class="btn btn-primary button-alt mx-4 text-dark" href={{route('viewInstallAndroid',app()->getLocale())}} role="button" onmouseover="removeDarkText(event)" onmouseout="addDarkText(event)">Android</a>
            <a name="mac-install-button" id="mac-install-button" class="btn btn-primary button-alt text-dark" href={{route('viewInstallMac',app()->getLocale())}} role="button" onmouseover="removeDarkText(event)" onmouseout="addDarkText(event)">Mac OS</a>
        </div>
        <div class="py-3 px-4 m-4 d-flex flex-column justify-content-center align-items-center border-radius-alt bg-secondary-white">
            <h2 class="subheading text-dark">{{__('layout.prerequisite')}}</h2>
            <div id="requirement" class="d-flex flex-column requirement justify-content-center align-items-center">

            </div>
        </div>
    </div>

    <div class="wrapper container mb-5">
        @yield('contentBaru')
    </div>
@endsection


