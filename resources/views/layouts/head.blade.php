<!doctype html>
<html lang="en">

<head>
    <title>@yield("title")</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="shortcut icon" type="image/png" href={{asset('Assets/Icon/Favicon/favicon.png')}}>
    <link rel="stylesheet" href={{asset('css/app.css')}}>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light d-flex justify-content-between nav">
        <a class="navbar-brand col col-lg-3 d-flex cursor-pointer justify-content-start justify-content-lg-around align-items-center" href={{route('viewHome',app()->getLocale())}}>
            <img src={{asset('Assets/Icon/sample-logo.png')}} height="45px" />
        </a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId"
            aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavId">
            <ul class="d-flex justify-content-center align-items-center w-100  navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item">
                    <a class="nav-link col col-lg mx-3" id="home" href={{route('viewHome',app()->getLocale())}}>{{__('layout.home')}}</a>
                </li>
                <li class="nav-item dropdown flex-column">

                    <a class="col col-lg mx-3 nav-link d-flex justify-content-center align-items-center dropdown-toggle"
                        href="#" id="features" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{__('layout.feature')}}
                    </a>
                    <div class="dropdown-menu absolute-center" aria-labelledby="features">
                        <a class="dropdown-item" href={{route('viewFeaturesAndroid',app()->getLocale())}}>{{__('layout.android')}}</a>
                        <a class="dropdown-item" href={{route('viewFeaturesComputer',app()->getLocale())}}>{{__('layout.pc')}}</a>
                    </div>

                </li>
                <li class="nav-item ">
                    <a class="nav-link col col-lg mx-3" id="about" href={{route('viewAbout',app()->getLocale())}}>{{__('message.About')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link col col-lg mx-3 text-center" id="install"
                        href={{route('viewInstallAndroid',app()->getLocale())}}>{{__('layout.installGuide')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link col col-lg mx-3" id="faq" href={{route('viewFaq',app()->getLocale())}}>FAQ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link col col-lg mx-3" id="price" href={{route('viewPrice',app()->getLocale())}}>{{__('layout.price')}} </a>
                </li>
                <li class="nav-item mr-3 mr-lg-0">
                    <a type="button" href="https://pc.spyhp.com/spyhp/login.jsp" class="button btn btn-primary text-light" id="login">Login</a>
                </li>
                <li class="d-flex align-items-center justify-content-center py-2 py-lg-0" style="width: 10rem">
                    <div class="lang col col-lg mx-3 d-flex justify-content-around align-items-center" href="#Pricing">

                        <a class="lang-id" id="lang-id" href="
                        @if(isset($id))
                            {{route(Route::currentRouteName(),['lang' => 'id', 'id' => $id])}}
                        @elseif(isset($status))
			    {{route(Route::currentRouteName(),['lang' => 'id', 'status' => $status])}}
			@else
                            {{route(Route::currentRouteName(),['lang' => 'id'])}}
                        @endif">
                            ID
                        </a>

                        <img src={{asset('Assets/Icon/Line.svg')}} height="20px" width="3px" />
                        <a class="lang-en" id="lang-en" href="
                        @if(isset($id))
                            {{route(Route::currentRouteName(),['lang' => 'en', 'id' => $id])}}
                        @elseif(isset($status))
			    {{route(Route::currentRouteName(),['lang' =>'en', 'status' => $status])}}
			@else
                            {{route(Route::currentRouteName(),['lang' => 'en'])}}
                        @endisset">
                            EN
                        </a>
                    </div>
                </li>
        </div>
    </nav>

    @section('content')
    @show



    <footer class="footer">
        <section class="footer-top pt-5 row no-gutters">
            <hr class="horizontal-line w-100 mt-5 mb-3">
            <div class="footer-col col-lg-2 col-md-4 col-6">
                <div class="footer-title">{{__('layout.footerAiO')}}</div>
                <ul class="footer-list">
                    <li class="footer-list-item"><a href={{route('viewFeaturesAndroid',app()->getLocale() )}}>{{__('layout.aio1')}}</a></li>
                    <li class="footer-list-item"><a href={{route('viewFeaturesAndroid',app()->getLocale())}}>{{__('layout.aio2')}}</a></li>
                    <li class="footer-list-item"><a href={{route('viewFeaturesAndroid',app()->getLocale())}}>{{__('layout.aio3')}}</a></li>
                    <li class="footer-list-item"><a href={{route('viewFeaturesAndroid',app()->getLocale())}}>{{__('layout.aio4')}}</a></li>
                    <li class="footer-list-item"><a href={{route('viewFeaturesAndroid',app()->getLocale())}}>{{__('layout.aio5')}}</a></li>
                    <li class="footer-list-item"><a href={{route('viewFeaturesAndroid',app()->getLocale())}}>{{__('layout.aio6')}}</a></li>
                    <li class="footer-list-item"><a href={{route('viewFeaturesAndroid',app()->getLocale())}}>{{__('layout.aio7')}}</a></li>
                    <li class="footer-list-item"><a href={{route('viewFeaturesAndroid',app()->getLocale())}}>{{__('layout.aio8')}}</a></li>
                    <li class="footer-list-item"><a href={{route('viewFeaturesAndroid',app()->getLocale())}}>Geo fencing</a></li>
                    <li class="footer-list-item"><a href={{route('viewFeaturesAndroid',app()->getLocale())}}>{{__('layout.aio9')}}</a></li>
                </ul>
            </div>
            <div class="footer-col col-lg-2 col-md-4 col-6">
                <div class="footer-title">&nbsp;</div>
                <ul class="footer-list">
                    <li class="footer-list-item"><a href={{route('viewFeaturesAndroid',app()->getLocale())}}>{{__('layout.aio10')}}</a></li>
                    <li class="footer-list-item"><a href={{route('viewFeaturesAndroid',app()->getLocale())}}>{{__('layout.aio11')}}</a></li>
                    <li class="footer-list-item"><a href={{route('viewKeylogger',app()->getLocale())}}>keylogger</a></li>
                </ul>
            </div>
            <div class="footer-col col-lg-2 col-md-4 col-6">
                <div class="footer-title">SPYHP Info</div>
                <ul class="footer-list">
                    <li class="footer-list-item"><a href={{route('viewAbout',app()->getLocale())}}>{{__('message.About')}}</a></li>
                    <li class="footer-list-item"><a href={{route('viewInstallAndroid',app()->getLocale())}}>{{__('layout.installGuide')}}</a></li>
                    <li class="footer-list-item"><a href={{route('viewTestimony',app()->getLocale())}}>{{__('layout.testimoni')}}</a></li>
                    <li class="footer-list-item"><a href="https://www.youtube.com/embed/Sk3hPPvkQhk">{{__('layout.forYou')}}</a></li>
                    <li class="footer-list-item"><a href={{route('viewFaq',app()->getLocale())}}>FAQ</a></li>
                    <li class="footer-list-item"><a href={{route('viewContact',app()->getLocale())}}>{{__('message.contactUs')}}</a></li>
                </ul>
            </div>
            <div class="footer-col col-lg-2 col-md-4 col-6">
                <div class="footer-title">Privacy</div>
                <ul class="footer-list">
                    <li class="footer-list-item"><a href={{route('viewLegal',app()->getLocale())}}>{{__('layout.info')}}</a></li>
                    <li class="footer-list-item"><a href={{route('viewPrivacy',app()->getLocale())}}>{{__('layout.guide')}}</a></li>
                    <li class="footer-list-item"><a href={{route('viewPrivacy',app()->getLocale())}}>{{__('layout.refund')}}</a></li>
                    <li class="footer-list-item"><a href={{route('viewServer',app()->getLocale())}}>{{__('layout.works')}}</a></li>
                    <li class="footer-list-item"><a href={{route('viewReport',app()->getLocale())}}>{{__('layout.report')}}</a></li>
                </ul>
            </div>
            <div class="footer-col col-lg-2 col-md-4 col-6">
                <div class="footer-title">Social</div>
                <ul class="footer-list">
                    <li class="footer-list-item"><a href="https://www.facebook.com/spyhp.tispy.7">facebook</a> </li>
                    <li class="footer-list-item"><a href="https://twitter.com/hp_spy">twitter</a> </li>
                    <li class="footer-list-item"><a href="https://www.youtube.com/channel/UCT9l1uaXpMUfloGOeZej6K">youTube</a> </li>
                </ul>
            </div>
            <div class="footer-col col-lg-2 col-md-4 col-6">
                <div class="footer-title">Secure Payment</div>
                <ul class="footer-list">
                    <li class="footer-list-item">
                        <a href={{route('viewPrice',app()->getLocale() )}}>
                            <img src={{asset('Assets/Logo/Payment/payment.png')}} alt="payment-logo"
                                class="footer-image">

                        </a>
                    </li>
                </ul>
            </div>
        </section>
        <section class="footer-bottom row no-gutters">
            <p class="footer-paragraph px-5 pt-4 text-justify">
                {{__('layout.footer')}}
            </p>
            <span class="footer-copyright d-flex justify-content-center align-items-center w-100 py-3">
                © Copyright 2020 spyhp.com. All rights reserved.
            </span>
        </section>
    </footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src={{asset('js/app.js')}}></script>

</body>

</html>
