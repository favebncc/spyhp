@extends('layouts.head')
@section('content')
<section class="features px-4">
    <h1 class="m-4 heading d-flex justify-content-center align-items-center">@yield('heading')</h1>

    <main id="listOfDropDownFeatures">
        
    </main>

    @yield('isi')
</section>
@endsection
