@extends('layouts.head')

@section('title','FAQ')

@section('content')
<div class="container my-5">

    <div class="row d-flex flex-column flex-lg-row">
        <div class="d-flex align-self-center align-self-lg-start  flex-column align-items-center col-lg-3 border-radius-alt shadow-lg p-0 overflow-hidden w-75 ">
            <div class=" bg-secondary-green-dark d-flex justify-content-center  w-100">
                <h1 class="subheading  text-light mb-0 py-2 ">FAQ</h1>
            </div>
            <div id="faq-title-list" class="d-md-flex flex-column justify-content-center w-100 bg-secondary-white faq-list">
               
              
            </div>
        </div>

        <div id="faq-dropdown-list" class="d-flex flex-column col-lg-9 ">
            
        </div>
    </div>

</div>
@endsection
