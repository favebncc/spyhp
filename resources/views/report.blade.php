@extends('layouts.head')

@section('title', 'Report')

@section('content')

    <div class="container">
        <div class="d-flex justify-content-center align-items-center">
        <h1 class="heading my-4 text-center text-lg-left">{{__('report.header')}}</h1>
        </div>
        <div class="d-flex row mb-4">
            <div class="col-lg-6 mb-3">
                <p class="text-justify">{{__('report.text')}}</p>

            </div>
            <div class="col-lg-6">
            <form action="{{asset('report/post')}}" class="form-group contact-form" method="POST">
                @if(session('success'))
                    <h1 class="heading my-4 text-center text-lg-left">{{session('success')}}</h1>
                @endif
                @csrf
            <input type="text" name="imei" id="home-form-imei" class="contact-form-input mt-0" placeholder="{{__('report.imei')}}">
                    @error('imei')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                <input type="text" name="emailReport" id="home-form-email" class="contact-form-input"
                    placeholder="{{__('report.email')}}">
                    @error('emailReport')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                <div class="my-3 g-recaptcha" data-sitekey="{{env('CAPTCHA_KEY')}}">
                    @error('g-recaptcha-response')
                    <span class="invalid-feedback">
                        <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>
                <input type="submit" id="" class="btn btn-primary button" value="Submit">
            </form>
            </div>

        </div>
    </div>



@endsection
