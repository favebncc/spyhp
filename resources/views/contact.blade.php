@extends('layouts.head')

@section('title','Contact Us')

@section('content')
@if('message')
	<div class="alert alert-success">
		{{session('message')}}
	</div>
@endif
    <div class="">
        <section class="contact">
        <h1 class="heading d-flex justify-content-center pt-5 pb-2">{{__('message.contactUs')}}</h1>
            <div class="contact-container row no-gutters">
                <div class="contact-section col-lg-6">
                    <p class="contact-paragraph">{{__('message.contact')}}</p>
                    <div class="row d-flex justify-content-center align-items-start">
                        <div class="col-md-6">
                            <div class="contact-address d-flex  justify-content-center align-items-center align-items-md-start flex-column mb-2">
                                <span class="contact-bold contact-email">{{__('message.contactEmail')}}</span>
                                <span>support@SPYHP.net</span>
                            </div>
                            <div class="contact-address d-flex flex-column align-items-center justify-content-center d-md-block">
                                <span class="contact-bold">{{__('message.contactMedsos')}}</span>
                                <div class="contact-social-media d-flex justify-content-around w-50">
                                    <a href="#"><i class="fab fa-2x fa-facebook-f"></i></a>
                                    <a href="#"><i class="fab fa-2x fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-2x fa-youtube"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <figure class="contact-msg d-flex justify-content-center align-items-center flex-column">
                                <figcaption class="contact-bold">{{__('message.contactFacebook')}}</figcaption>
                                <img src={{asset('Assets/Picture/tispy-msg-icon.png')}} alt="tispy-messager">
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="contact-section col-lg-6">

                <form action="{{route('addContact',app()->getLocale())}}" class="form-group contact-form" method="POST">
                        @csrf
                        <input type="text" name="nameContact" id="home-form-name" class="contact-form-input mt-0" placeholder="{{__('message.contactInputNama')}}">
                            @if($errors->has('nameContact'))
                                <div class="error">{{ $errors->first('nameContact') }}</div>
                            @endif
                        <input type="email" name="emailContact" id="home-form-email" class="contact-form-input"
                            placeholder="{{__('message.contactInputEmail')}}">
                            @if($errors->has('emailContact'))
                                <div class="error">{{ $errors->first('emailContact') }}</div>
                            @endif
                        <input type="text" name="subjectContact" id="home-form-subjek" class="contact-form-input"
                            placeholder="{{__('message.contactInputSubjek')}}">
                            @if($errors->has('subjectContact'))
                                <div class="error">{{ $errors->first('subjectContact') }}</div>
                            @endif
                        <textarea placeholder="Pesan Email" name="messageContact" id="email-content" cols="51" rows="5"
                            class="mb-0 contact-form-input"></textarea>
                            @if($errors->has('messageContact'))
                                <div class="error">{{ $errors->first('messageContact') }}</div>
                            @endif
                            <div class="my-3 g-recaptcha" data-sitekey="{{env('CAPTCHA_KEY')}}">
                                @if($errors->has('g-recaptcha-response'))
                                <span class="invalid-feedback">
                                    <strong>{{$errors->first('g-recaptcha-response')}}</strong>
                                </span>
                                @endif
                            </div>
                        <input type="submit" id="" class="btn btn-primary button" value="Submit">
                    </form>

                </div>
            </div>
        </section>
    </div>


@endsection
