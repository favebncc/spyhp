<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pricing</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script type="text/javascript"
            src="https://app.midtrans.com/snap/snap.js"
            data-client-key={{env('CLIENT-KEY')}}></script>
</head>
<body>

    <form class="container" onsubmit="return submitForm();" method="POST" enctype="multipart/form-data">
        @method('POST')
    <h1 class="heading">{{__('transaction.profile')}}</h1>
        @csrf
        <div class="form-group">
          <label for="pricingName">{{__('transaction.pricingName')}}</label>
          <input type="text" class="form-control" id="pricingName" name="name" placeholder="{{__('transaction.phName')}}">
          @if($errors->has('name'))
          <small id="nameHelp" class="form-text text-danger">{{ $errors->first('name') }}</small>
        @endif
        </div>
        <div class="form-group">
            <label for="pricingEmail">{{__('transaction.pricingEmail')}}</label>
            <input type="email" class="form-control" id="pricingEmail" name="email" placeholder="{{__('transaction.phEmail')}}">
            @if($errors->has('email'))
            <small id="nameHelp" class="form-text text-danger">{{ $errors->first('email') }}</small>
          @endif
          </div>
          <div class="form-group">
            <label for="pricingPhone">{{__('transaction.pricingPhone')}}</label>
            <input type="text" class="form-control" id="pricingPhone" name="phone" placeholder="{{__('transaction.phPhone')}}">
            @if($errors->has('phone'))
            <small id="nameHelp" class="form-text text-danger">{{ $errors->first('phone') }}</small>
          @endif
          </div>
          <div class="form-group">
            <label for="pricingCity">{{__('transaction.pricingCity')}}</label>
            <input type="text" class="form-control" id="pricingCity" name="city" placeholder="{{__('transaction.phCity')}}">
            @if($errors->has('city'))
            <small id="nameHelp" class="form-text text-danger">{{ $errors->first('city') }}</small>
          @endif
          </div>
          <div class="form-group">
            <label for="pricingPostCode">{{__('transaction.pricingPostalCode')}}</label>
            <input type="text" class="form-control" id="pricingPostCode" name="postCode" placeholder="{{__('transaction.phPostalCode')}}">
            @if($errors->has('postCode'))
            <small id="nameHelp" class="form-text text-danger">{{ $errors->first('postCode') }}</small>
          @endif
          </div>
          <div class="form-group">
            <label for="pricingAddress">{{__('transaction.pricingAddress')}}</label>
            <input type="text" class="form-control" id="pricingAddress" name="address" placeholder="{{__('transaction.phAddress')}}">
            @if($errors->has('address'))
            <small id="nameHelp" class="form-text text-danger">{{ $errors->first('address') }}</small>
          @endif
          </div>
          <button id="pay-button" class="btn btn-primary button my-3" value ="Submit">Pay!</button><br>
          <input type="hidden" name="price" id="pricingPrice" value="{{$item->price}}">
          <input type="hidden" name="type" id="pricingType" value="{{$item->type}}">
        <input type="hidden" name="months" id="pricingMonths" value="{{$item->months}}">
      </form>
      <script src={{asset('js/app.js')}}></script>
      <script type="text/javascript">
      function submitForm(){
        $.post("http://spyhp.com/api/pricing/store",
        {
            _method: 'POST',
            pricingName: $('#pricingName').val(),
            pricingEmail: $('#pricingEmail').val(),
            pricingPhone: $('#pricingPhone').val(),
            pricingCity: $('#pricingCity').val(),
            pricingPostCode: $('#pricingPostCode').val(),
            pricingAddress: $('#pricingAddress').val(),
            pricingPrice: $('#pricingPrice').val(),
            pricingType: $('#pricingType').val(),
            pricingMonths: $('#pricingMonths').val(),
        },
        (data, status) => {
          console.log('testing');
            if (data.status == 'error') {
                alert(data.message);
            } else {
                snap.pay(data.snapToken, {
                    // Optional
                    onSuccess: function (result) {
                        window.location.replace("http://spyhp.com/payment/success")
                    },
                    // Optional
                    onPending: function (result) {
                        window.location.replace("http://spyhp.com/{{app()->getLocale()}}/payment/pending")                    },
                    // Optional
                    onError: function (result) {
                        window.location.replace("http://spyhp.com/payment/failed")                    }
                });
            }
        });
        return false;
      }
      </script>
</body>
</html>
