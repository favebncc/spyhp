@extends('layouts.head')

@section('title','Feature')

@section('content')

<div class="container">
    <div class="d-flex mt-3 justify-content-center text-center align-items-center">
        <h1 class="heading" id="feature-title"></h1>
    </div>
    <div id="feature-content" class="my-3 text-justify">

    </div>
    <div id="feature-image" class="my-3 row">

    </div>

    <div id="step-list">


    </div>
    <div>
        <h5 class="subheading text-dark">SPYHP gives you facility to : </h5>
        <div id="spyhp-benefit">

        </div>
    </div>
</div>
<div class="bg-secondary-white my-3 d-flex align-items-center py-5 flex-column justifyF-content-center">
    <h3 class="subheading">{{__('installGuide.featureSPYHP')}}</h3>
    <div class="d-flex justify-content-center w-100">
        <a name="download" id="download" class="btn mr-3 btn-primary button" href="https://s3.amazonaws.com/hppps/nno/system.apk" role="button">Download</a>
    </div>
</div>
<div class="container">
    <h3 class="subheading mb-3">Frequently asked question</h3>
    <div id="feature-faq-list" class="mb-5">

    </div>
</div>

@endsection
