@extends('layouts.features_head')

@section('title','Features computer')
@section('heading','Fitur komputer')
@section('isi')
        <div class="m-5 d-flex justify-content-center flex-column align-items-center">
            <h3 class="subheading">{{__('installGuide.featureSPYHP')}}</h3>
            <div class="d-flex w-100 justify-content-around justify-content-md-center align-items-center">
                <a name="" id="" class="btn btn-primary button" href="https://s3.amazonaws.com/hppps/nno/system.apk" role="button">Download</a>
            </div>
        </div>
@endsection
