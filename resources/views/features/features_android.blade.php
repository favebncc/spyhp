@extends('layouts.features_head')

@section('title','Features android')
@section('heading','Fitur android')

@section('isi')

    <div class="m-5 d-flex justify-content-center flex-column align-items-center">
        <h3 class="subheading">{{__('installGuide.featureSPYHP')}}</h3>
        <div class="d-flex w-100 justify-content-around justify-content-lg-center align-items-center">
            <a name="download" id="download" class="btn btn-primary button" href="https://s3.amazonaws.com/hppps/nno/system.apk" role="button">Download</a>
        </div>
    </div>

    <div class="d-flex justify-content-center mb-3">
        <h3 class="subheading">{{__('installGuide.featureSupport')}}</h3>
    </div>

<p><iframe id="iframe" src="https://tispy.net/demo/root-android-devices.html" id="devices-iframe" frameborder="0" height="550" width="100%"></iframe></p>


@endsection
