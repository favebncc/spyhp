<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript"
            src="https://app.sandbox.midtrans.com/snap/snap.js"
            data-client-key={{env('CLIENT-KEY')}}></script>
            <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
  <body class="d-flex cointainer flex-column align-items-center justify-content-center ">
    <input id="snapToken" type="hidden" value={{$snapToken}}>
    <h1 id="message" class="heading"></h1>
    <p id="details"></p>
    <script type="text/javascript">
      const token = document.getElementById('snapToken');
      const message = document.getElementById('message');
      const details = document.getElementById('details');
      window.addEventListener('load', function () {
        snap.show();
        //post
        //response snap token
        snap.pay(token.value,{
    onSuccess: function(result){message.innerHTML = result.transaction_status;console.log(result);},
    onPending: function(result){message.innerHTML = result.status_message;details.innerHTML = "Please finish your payment and wait for the account to be sent via your email."},
    onError: function(result){message.innerHTML = result.transaction_status;location = "http://localhost:8000/price";console.log(result);},
    onClose: function(){message.innerHTML = "Transaction Canceled";location = "http://localhost:8000/price";}
  });
      });
    </script>
  </body>
</html>
