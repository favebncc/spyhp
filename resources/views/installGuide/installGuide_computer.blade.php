@extends('layouts.instalation_head')

@section('title','Install Guide for Computer')

@section('contentBaru')
<div class="features-item features-alt-item my-3">
    <input type="checkbox" id="features1" name="feature" class="features-input" onclick="Rotates(event)">
    <label for="features1" class="features-label features-alt-label mb-0">
        <span class="features-title features-alt-title">{{__('installGuide.pcStep1')}}</span>
        <i class="fas fa-caret-down features-arrow fa-2x text-dark" id="arrow1"></i>
    </label>
    <span class="features-droplist">
        <li class="features-dropitem features-alt-drop-item mt-0">
            <div class="container-fluid">
                <h6 class="subheading text-dark">{{__('installGuide.pcStep1Subhead1')}}</h6>
                <a class="button btn btn-primary my-3" href="#">{{__('installGuide.installer')}}</a>
                <ul class="install-guide-list">
                    <li>{{__('installGuide.pcinstallstep1')}}</li>
                    <li>{{__('installGuide.pcinstallstep2')}}</li>
                </ul>
                <div class="row my-3">
                    <div class="col-lg-6"><img src={{asset('Assets/Picture/Installation-Guide/Computer/computer1.png')}} alt="installation-guide-computer-1"></div>
                    <div class="col-lg-6"><img src={{asset('Assets/Picture/Installation-Guide/Computer/computer2.png')}} alt="installation-guide-computer-2"></div>
                </div>
                <div class="row my-3">
                    <div class="col-lg-6"><img src={{asset('Assets/Picture/Installation-Guide/Computer/computer3.png')}} alt="installation-guide-computer-4"></div>
                    <div class="col-lg-6"><img src={{asset('Assets/Picture/Installation-Guide/Computer/computer4.png')}} alt="installation-guide-computer-2"></div>
                </div>
                <ul class="install-guide-list">
                    <li>{{__('installGuide.pcinstallstep3')}}</li>
                </ul>
                <h6 class="subheading text-dark">{{__('installGuide.pcinstallStepAfter')}}/h6>
                <ul class="install-guide-list">
                    <li>{{__('installGuide.pcinstallstep4')}}</li>
                    <li>{{__('installGuide.pcinstallstep5')}}</li>
                </ul>
                <div class="row my-3">
                    <div class="col-lg-12"><img class="w-100" src={{asset('Assets/Picture/Installation-Guide/Computer/computer5.png')}} alt="installation-guide-computer-5"></div>
                </div>
            </div>
        </li>
    </span>
</div>

<div class="features-item features-alt-item my-3">
    <input type="checkbox" id="features2" name="feature" class="features-input" onclick="Rotates(event)">
    <label for="features2" class="features-label features-alt-label mb-0">
        <span class="features-title features-alt-title">{{__('installGuide.pcStep2')}}</span>
        <i class="fas fa-caret-down features-arrow fa-2x text-dark" id="arrow2"></i>
    </label>
    <span class="features-droplist">
        <li class="features-dropitem features-alt-drop-item mt-0">
            <div class="container-fluid">
                <h6 class="subheading text-dark">{{__('installGuide.Verif')}}</h6>
                <ul class="install-guide-list">
                    <li>{{__('installGuide.VerifStep1')}}</li>
                    <li>{{__('installGuide.VerifStep2')}}</li>
                </ul>

                <div class="row my-3">
                    <div class="col-lg-12 "><img class="w-100" src={{asset('Assets/Picture/Installation-Guide/Computer/computer6.png')}} alt="installation-guide-computer-6"></div>
                </div>

            </div>
        </li>
    </span>
</div>

<div class="features-item features-alt-item my-3">
    <input type="checkbox" id="features3" name="feature" class="features-input" onclick="Rotates(event)">
    <label for="features3" class="features-label features-alt-label mb-0">
        <span class="features-title features-alt-title">{{__('installGuide.pcStep3')}}</span>
        <i class="fas fa-caret-down features-arrow fa-2x text-dark" id="arrow3"></i>
    </label>
    <span class="features-droplist">
        <li class="features-dropitem features-alt-drop-item mt-0">
            <div class="container-fluid">
                <h5 class="subheading text-dark">{{__('installGuide.Op')}} 1</h5>
                <ul class="install-guide-list">
                    <li>{{__('installGuide.pcOp1Step1')}}</li>
                    <li>{{__('installGuide.pcOp1Step2')}}</li>
                    <li>{{__('installGuide.pcOp1Step3')}}</li>
                    <li>{{__('installGuide.pcOp1Step4')}}</li>
                </ul>

                <h5 class="subheading text-dark">{{__('installGuide.Op')}} 2</h5>
                <ul class="install-guide-list">
                    <li>{{__('installGuide.pcOp2Step1')}}</li>
                    <li>{{__('installGuide.pcOp2Step2')}}</li>
                </ul>

                <h5 class="subheading text-dark">{{__('installGuide.Op')}} 3</h5>
                <ul class="install-guide-list">
                    <li>{{__('installGuide.pcOp3Step1')}}</li>
                    <li>{{__('installGuide.pcOp3Step2')}}</li>
                    <li>{{__('installGuide.pcOp3Step3')}}</li>
                </ul>

            </div>
        </li>
    </span>
</div>
@endsection
