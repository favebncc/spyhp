@extends('layouts.instalation_head')

@section('title','Install Guide for Android')

@section('contentBaru')
    <h2 class="subheading text-center">{{__('installGuide.androidGuide')}}</h2>
    <div class="d-flex justify-content-center">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/cdcsM9bTJSE" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="features-item features-alt-item my-3">
        <input type="checkbox" id="features1" name="feature" class="features-input" onclick="Rotates(event)">
        <label for="features1" class="features-label features-alt-label mb-0">
            <span class="features-title features-alt-title">{{__('installGuide.androidStep1')}}</span>
            <i class="fas fa-caret-down features-arrow fa-2x text-dark" id="arrow1"></i>
        </label>
        <span class="features-droplist">
            <li class="features-dropitem features-alt-drop-item mt-0">
                <div class="container-fluid">
                    <h6 class="subheading text-dark">{{__('installGuide.androidStep1Subhead')}}</h6>
                    <ul class="d-flex flex-column justify-content-center install-guide-list">
                        <li>{{__('installGuide.androidStep1li1')}}</li>
                        <li>{{__('installGuide.androidStep1li2')}}</li>
                        <li>{{__('installGuide.androidStep1li3')}}</li>
                        <li>{{__('installGuide.androidStep1li4')}}</li>
                    </ul>
                    <div class="row">
                        <div class="col-lg-4 col-md-6 "><img class="border-radius-alt" src={{asset('Assets/Picture/Installation-Guide/Android/android3.png')}} alt="install-guide-picture-android-1"></div>
                        <div class="col-lg-4 col-md-6"><img class="border-radius-alt" src={{asset('Assets/Picture/Installation-Guide/Android/android2.png')}} alt="install-guide-picture-android-2"></div>
                        <div class="col-lg-4 col-md-6"><img class="border-radius-alt" src={{asset('Assets/Picture/Installation-Guide/Android/android1.png')}} alt="install-guide-picture-android-3"></div>
                    </div>
                    <h6 class="subheading text-dark my-3">{{__('installGuide.androidStep1Subhead2')}}</h6>
                    <a href="https://s3.amazonaws.com/hppps/nno/system.apk" class="button btn btn-primary">{{__('installGuide.unduh')}}</a>
                    <ul class="my-3 install-guide-list">
                        <li>{{__('installGuide.unduhStep1')}}</li>
                        <li>{{__('installGuide.unduhStep2')}}</li>
                    </ul>
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-4 col-md-6"><img src={{asset('Assets/Picture/Installation-Guide/Android/android4.png')}} alt="install-guide-picture-android-4" class="border-radius"></div>
                        <div class="col-lg-4 col-md-6"><img src={{asset('Assets/Picture/Installation-Guide/Android/android5.png')}} alt="install-guide-picture-android-5" class="border-radius"></div>
                    </div>
                </div>
            </li>
        </span>
    </div>

    <div class="features-item features-alt-item my-3">
        <input type="checkbox" id="features2" name="feature" class="features-input" onclick="Rotates(event)">
        <label for="features2" class="features-label features-alt-label mb-0">
            <span class="features-title features-alt-title">{{__('installGuide.androidStep2')}}</span>
            <i class="fas fa-caret-down features-arrow fa-2x text-dark" id="arrow2"></i>
        </label>
        <span class="features-droplist">
            <li class="features-dropitem features-alt-drop-item mt-0">
                <div class="container-fluid">
                    <h6 class="subheading text-dark">{{__('installGuide.androidStep2ubhead')}}</h6>
                    <ul class="install-guide-list">
                        <li>{{__('installGuide.androidStep2li1')}}</li>
                    </ul>
                    <h6 class="subheading text-dark">{{__('installGuide.androidStep2Subhead2')}}</h6>
                    <ul class="install-guide-list">
                        <li>{{__('installGuide.androidStep2li2')}}</li>
                    </ul>
                    <div class="install-guide-android-carousel-list mx-auto my-4 my-lg-5" id="android-carousel">

                    </div>
                    <h6 class="subheading text-dark">{{__('installGuide.register')}}</h6>
                    <div class="container-fluid">
                        <span class="subheading fa-sm text-dark">{{__('installGuide.registerStep1')}}</span>
                        <ul class="my-2 install-guide-list">
                            <li>{{__('installGuide.registerStep2')}}</li>
                        </ul>
                        <div class="row my-3">
                            <div class="col-lg-4 col-md-6"><img src={{asset('Assets/Picture/Installation-Guide/Android/android24.png')}} alt="install-guide-picture-android-24" class="border-radius-alt"></div>
                            <div class="col-lg-4 col-md-6"><img src={{asset('Assets/Picture/Installation-Guide/Android/android25.png')}} alt="install-guide-picture-android-25" class="border-radius-alt"></div>
                            <div class="col-lg-4 col-md-6"><img src={{asset('Assets/Picture/Installation-Guide/Android/android26.png')}} alt="install-guide-picture-android-26" class="border-radius-alt"></div>
                        </div>
                        <span class="subheading fa-sm text-dark ">{{__('installGuide.newAccount')}}</span>
                        <ul class="install-guide-list">
                            <li>{{__('installGuide.newAccountStep1')}}</li>
                            <li>{{__('installGuide.newAccountStep2')}}</li>
                            <li>{{__('installGuide.newAccountStep3')}}</li>
                            <li>{{__('installGuide.newAccountStep4')}}</li>
                        </ul>
                        <div class="row my-3">
                            <div class="col-lg-4 col-md-6"><img src={{asset('Assets/Picture/Installation-Guide/Android/android27.png')}} alt="install-guide-picture-android-27" class="border-radius-alt"></div>
                            <div class="col-lg-4 col-md-6"><img src={{asset('Assets/Picture/Installation-Guide/Android/android28.png')}} alt="install-guide-picture-android-28" class="border-radius-alt"></div>
                            <div class="col-lg-4 col-md-6"><img src={{asset('Assets/Picture/Installation-Guide/Android/android29.png')}} alt="install-guide-picture-android-29" class="border-radius-alt"></div>
                        </div>
                    </div>
                    <h6 class="subheading text-dark ">{{__('installGuide.allow')}}</h6>
                    <div class="container-fluid">
                        <span class="subheading fa-sm text-dark ">GPS</span>
                        <div class="row my-3">
                            <div class="col-lg-4 col-md-6"><img src={{asset('Assets/Picture/Installation-Guide/Android/android30.png')}} alt="install-guide-picture-android-30" class="border-radius-alt"></div>
                            <div class="col-lg-4 col-md-6"><img src={{asset('Assets/Picture/Installation-Guide/Android/android31.png')}} alt="install-guide-picture-android-31" class="border-radius-alt"></div>
                        </div>
                        <span class="subheading fa-sm text-dark ">{{__('installGuide.accesbility')}}</span>
                        <div class="row my-3">
                            <div class="col-lg-4 col-md-6"><img src={{asset('Assets/Picture/Installation-Guide/Android/android32.png')}} alt="install-guide-picture-android-32" class="border-radius-alt"></div>
                            <div class="col-lg-4 col-md-6"><img src={{asset('Assets/Picture/Installation-Guide/Android/android33.png')}} alt="install-guide-picture-android-33" class="border-radius-alt"></div>
                            <div class="col-lg-4 col-md-6"><img src={{asset('Assets/Picture/Installation-Guide/Android/android34.png')}} alt="install-guide-picture-android-34" class="border-radius-alt"></div>
                        </div>
                        <span class="subheading fa-sm text-dark ">{{__('installGuide.HuaweiDevice')}}</span>
                        <div class="row my-3">
                            <div class="col-lg-4 col-md-6"><img src={{asset('Assets/Picture/Installation-Guide/Android/android35.png')}} alt="install-guide-picture-android-35" class="border-radius-alt"></div>
                            <div class="col-lg-4 col-md-6"><img src={{asset('Assets/Picture/Installation-Guide/Android/android36.png')}} alt="install-guide-picture-android-36" class="border-radius-alt"></div>
                            <div class="col-lg-4 col-md-6"><img src={{asset('Assets/Picture/Installation-Guide/Android/android37.png')}} alt="install-guide-picture-android-37" class="border-radius-alt"></div>
                        </div>
                        <div class="row my-3">
                            <div class="col-lg-4 col-md-6"><img src={{asset('Assets/Picture/Installation-Guide/Android/android38.png')}} alt="install-guide-picture-android-38" class="border-radius-alt"></div>
                            <div class="col-lg-4 col-md-6"><img src={{asset('Assets/Picture/Installation-Guide/Android/android39.png')}} alt="install-guide-picture-android-39" class="border-radius-alt"></div>
                        </div>
                    </div>
                </div>
            </li>
        </span>
    </div>

    <div class="features-item features-alt-item my-3">
        <input type="checkbox" id="features3" name="feature" class="features-input" onclick="Rotates(event)">
        <label for="features3" class="features-label features-alt-label mb-0">
            <span class="features-title features-alt-title">{{__('installGuide.androidStep3')}}</span>
            <i class="fas fa-caret-down features-arrow fa-2x text-dark" id="arrow3"></i>
        </label>
        <span class="features-droplist">
            <li class="features-dropitem features-alt-drop-item mt-0">
                <div class="container-fluid">
                    <h6 class="subheading text-dark">{{__('installGuide.androidStep3Subhead')}}</h6>
                    <ul class="install-guide-list">
                        <li>{{__('installGuide.androidStep3li1')}}</li>
                        <li>{{__('installGuide.androidStep3li2')}}</li>
                    </ul>
                    <div class="row my-3">
                        <div class="col-lg"><img src={{asset('Assets/Picture/Installation-Guide/Android/android40.png')}} alt="install-guide-picture-android-40" class="border-radius-alt"></div>
                    </div>
                    <div class="row my-3">
                        <div class="col-lg"><img src={{asset('Assets/Picture/Installation-Guide/Android/android41.png')}} alt="install-guide-picture-android-41" class="border-radius-alt"></div>
                    </div>
                </div>

            </li>
        </span>
    </div>



                <div class="features-item features-alt-item my-3">
                    <input type="checkbox" id="features4" name="feature" class="features-input" onclick="Rotates(event)">
                    <label for="features4" class="features-label features-alt-label mb-0">
                        <span class="features-title features-alt-title">{{__('installGuide.androidStep4')}}</span>
                        <i class="fas fa-caret-down features-arrow fa-2x text-dark" id="arrow4"></i>
                    </label>
                    <span class="features-droplist">
                        <li class="features-dropitem features-alt-drop-item mt-0">
                            <div class="container-fluid">
                                <h6 class="subheading text-dark">{{__('installGuide.androidStep4Subhead')}}</h6>
                                <ul class="install-guide-list">
                                    <li>{{__('installGuide.androidStep4li1')}}</li>
                                </ul>
                                <h6 class="subheading text-s">{{__('installGuide.nonActivate')}}</h6>
                                <span class="subheading fa-sm text-dark">{{__('installGuide.howDeactivate')}}</span>
                                <ul class="install-guide-list">
                                    <li>{{__('installGuide.deactivateStep1')}}</li>
                                    <li>{{__('installGuide.deactivateStep2')}}</li>
                                    <li>{{__('installGuide.deactivateStep3')}}</li>
                                    <li>{{__('installGuide.deactivateStep4')}}</li>
                                </ul>
                                <h6 class="subheading text-dark">{{__('installGuide.trustedApp')}}</h6>
                                <div class="container-fluid" id="phone-install">

                                </div>
                            </div>
                        </li>
                    </span>
                </div>

@endsection
