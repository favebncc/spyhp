@extends('layouts.instalation_head')

@section('title','Install Guide for Mac OS')

@section('contentBaru')

<div class="features-item features-alt-item my-3">
    <input type="checkbox" id="features1" name="feature" class="features-input" onclick="Rotates(event)">
    <label for="features1" class="features-label features-alt-label mb-0">
        <span class="features-title features-alt-title">{{__('installGuide.macStep1')}}</span>
        <i class="fas fa-caret-down features-arrow fa-2x text-dark" id="arrow1"></i>
    </label>
    <span class="features-droplist">
        <li class="features-dropitem features-alt-drop-item mt-0">
            <div class="container-fluid">
                <h6 class="subheading text-dark">{{__('installGuide.macStep1Subhead1')}}</h6>
                <a href="#" class="my-2 btn btn-primary button">{{__('installGuide.installer')}}</a>
                <ul class="install-guide-list">
                    <li>{{__('installGuide.macInstallStep1')}}</li>
                </ul>
                <div class="row my-3">
                    <div class="col-lg-12"><img src={{asset('Assets/Picture/Installation-Guide/Mac/mac1.png')}}
                            alt="installation-guide-mac-1"></div>
                </div>
                <ul class="install-guide-list">
                    <li>{{__('installGuide.macInstallStep2')}} <code class="color-secondary-green-dark">"sudo spctl --master-disable;sudo
                            reboot"</code> {{__('installGuide.macInstallStep2Terminal')}}</li>
                    <li>{{__('installGuide.macInstallStep3')}}</li>
                    <li>{{__('installGuide.macInstallStep4')}}</li>
                </ul>
                <div class="row my-3">
                    <div class="col-lg-12"><img src={{asset('Assets/Picture/Installation-Guide/Mac/mac2.png')}}
                            alt="installation-guide-mac-2"></div>
                </div>
                <ul class="install-guide-list">
                    <li>{{__('installGuide.macInstallStep5')}}</li>
                </ul>
                <div class="row my-3">
                    <div class="col-lg-6"><img src={{asset('Assets/Picture/Installation-Guide/Mac/mac3.png')}} alt="installation-guide-mac-3"></div>
                    <div class="col-lg-6"><img src={{asset('Assets/Picture/Installation-Guide/Mac/mac4.png')}} alt="installation-guide-mac-4"></div>
                </div>
                <div class="row my-3">
                    <div class="col-lg-6"><img src={{asset('Assets/Picture/Installation-Guide/Mac/mac5.png')}} alt="installation-guide-mac-5"></div>
                    <div class="col-lg-6"><img src={{asset('Assets/Picture/Installation-Guide/Mac/mac6.png')}} alt="installation-guide-mac-6"></div>
                </div>
                <div class="row my-3">
                    <div class="col-lg-6"><img src={{asset('Assets/Picture/Installation-Guide/Mac/mac7.png')}} alt="installation-guide-mac-3"></div>
                </div>
                <ul class="install-guide-list">
                    <li>{{__('installGuide.macInstallStep6')}}</li>
                </ul>
                <div class="row my-3">
                    <div class="col-lg-6"><img src={{asset('Assets/Picture/Installation-Guide/Mac/mac8.png')}} alt="installation-guide-mac-8"></div>
                    <div class="col-lg-6"><img src={{asset('Assets/Picture/Installation-Guide/Mac/mac9.png')}} alt="installation-guide-mac-9"></div>
                </div>
                <ul class="install-guide-list">
                    <li>{{__('installGuide.macInstallStep7')}}</li>
                </ul>
                <div class="row my-3">
                    <div class="col-lg-6"><img src={{asset('Assets/Picture/Installation-Guide/Mac/mac10.png')}} alt="installation-guide-mac-10"></div>
                    <div class="col-lg-6"><img src={{asset('Assets/Picture/Installation-Guide/Mac/mac11.png')}} alt="installation-guide-mac-11"></div>
                </div>
                <div class="row my-3">
                    <div class="col-lg-6"><img src={{asset('Assets/Picture/Installation-Guide/Mac/mac12.png')}} alt="installation-guide-mac-12"></div>
                    <div class="col-lg-6"><img src={{asset('Assets/Picture/Installation-Guide/Mac/mac13.png')}} alt="installation-guide-mac-13"></div>
                </div>
                <div class="row my-3">
                    <div class="col-lg-6"><img src={{asset('Assets/Picture/Installation-Guide/Mac/mac14.png')}} alt="installation-guide-mac-10"></div>
                </div>
            </div>
        </li>
    </span>
</div>


<div class="features-item features-alt-item my-3">
    <input type="checkbox" id="features2" name="feature" class="features-input" onclick="Rotates(event)">
    <label for="features2" class="features-label features-alt-label mb-0">
        <span class="features-title features-alt-title">{{__('installGuide.macStep2')}}</span>
        <i class="fas fa-caret-down features-arrow fa-2x text-dark" id="arrow2"></i>
    </label>
    <span class="features-droplist">
        <li class="features-dropitem features-alt-drop-item mt-0">
            <div class="container-fluid">
                <h6 class="text-dark subheading">{{__('installGuide.Verif')}}</h6>
                <ul class="install-guide-list">
                    <li>{{__('installGuide.VerifStep1')}}</li>
                    <li>{{__('installGuide.VerifStep2')}}</li>
                </ul>
                <div class="row my-3">
                    <div class="col-lg-12"><img class="w-100" src={{asset('Assets/Picture/Installation-Guide/Mac/mac15.png')}} alt="installation-guide-mac-15"></div>
                </div>
            </div>
        </li>
    </span>
</div>


<div class="features-item features-alt-item my-3">
    <input type="checkbox" id="features3" name="feature" class="features-input" onclick="Rotates(event)">
    <label for="features3" class="features-label features-alt-label mb-0">
        <span class="features-title features-alt-title">{{__('installGuide.macStep3')}}</span>
        <i class="fas fa-caret-down features-arrow fa-2x text-dark" id="arrow3"></i>
    </label>
    <span class="features-droplist">
        <li class="features-dropitem features-alt-drop-item mt-0">
            <div class="container-fluid">
             <h6 class="text-dark subheading">{{__('installGuide.Op')}} 1</h6>
             <ul class="install-guide-list">
                 <li>{{__('installGuide.macOp1Step1')}} <img src={{asset('Assets/Picture/Installation-Guide/Mac/mac-uninstall-btn.png')}} alt="mac-uninstall-btn"></li>
                 <li>{{__('installGuide.macOp1Step2')}}</li>
                 <li>{{__('installGuide.macOp1Step3')}}</li>
             </ul>
             <h6 class="text-dark subheading">{{__('installGuide.Op')}} 2</h6>

             <ul class="install-guide-list">
                 <li>{{__('installGuide.macOp2Step1')}}</li>
                 <li>{{__('installGuide.macOp2Step2')}} <code class="color-secondary-green-dark">"sudo /user/local/.rm/postrm"</code> </li>
             </ul>
             <h6 class="text-dark subheading">{{__('installGuide.Op')}} 3</h6>

             <ul class="install-guide-list">
                 <li>{{__('installGuide.macOp3Step1')}}</li>
                 <li>{{__('installGuide.macOp3Step2')}}</li>
             </ul>
            </div>
        </li>
    </span>
</div>

@endsection
