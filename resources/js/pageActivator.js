const navbarTitle = ["home", "features", "about", "install", "faq", "price", "login"];
const activator = [
    [
        ''
    ],
    [
        'features',
    ],
    [
        ''
    ],
    [
        'installGuide',
        'installGuideAndroid'
    ],
    [
        'faq'
    ],
    [
        'price'
    ],
    [

    ]
]

const activatePage = () => {
    const langCode = getLang(window.location.href);
    document.getElementById(`lang-${langCode}`).classList.add('nav-activate')
    for (let i = 0; i < navbarTitle.length; i++) {
       const pageTitle = document.title;
       let firstwordTitle = pageTitle.substr(0, pageTitle.indexOf(" "));
        if (!firstwordTitle) {
            firstwordTitle = document.title;
        }

        firstwordTitle = firstwordTitle.toLowerCase();

        if (firstwordTitle.match(navbarTitle[i])) {
            activator[i].forEach(satu => {
                if(satu){
                    import(`./${satu}`);
                }
            })
            document
                .getElementById(navbarTitle[i])
                .classList.add("nav-activate");

        } else {
            continue;
        }
    }
};


const getLang = (url) => {
    var slashCount = 0;
    var langCode;

    for (var i = 0; i < url.length; i++) {
        if (url.charAt(i) == '/') {
            slashCount++;
        }

        if (slashCount == 3) {
            langCode = url.charAt(i + 1);
            langCode += url.charAt(i + 2);
            break;
        }
    }

    return langCode;
};
activatePage();

export {getLang}
