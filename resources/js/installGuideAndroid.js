const phone = [{
        'title': 'Samsung',
        'item': [
            'Pengaturan >> Aplikasi >> Tekan tombol :(3 titik) pada sudut kanan atas >> Tekan "Show system apps" >> Buka keamanan sistem >> Penyimpanan >> Tekan "Clear data" >> Tekan Hapus atau OK',
        ]
    },
    {
        'title': 'Samsung (Android 7 ke atas)',
        'item': [
            'Pengaturan >> Perawatan Perangkat >> Baterai >> (Tombol) Tekan pada aplikasi yang tidak di pantau >> Tambah aplikasi >> Pilih "Wifi Service"',
            'Pengaturan >> Aplikasi >> Layanan Wifi >> Data Seluler >> Izinkan pemakaian background data dan izinkan aplikasi pada mode penghematan data >> Nyalakan'
        ]
    },
    {
        'title': 'Samsung Galaxy 8.0 (Seperti Galaxy S9, S9, S8, S8+)',
        'item': [
            'Pengaturan >> Layar kunci & Keamanan >> Monitor izin aplikasi >> nonaktifkan "Wifi Service"',
            'Pengaturan >> Perawatan Perangkat >> Baterai >> Aplikasi yang Tidak Dimonitor >> Tambah  Aplikasi >> Layanan Wifi >> Selesai',
            'Pengaturan >> Aplikasi >> Layanan Wifi >> Data Seluler >> Izinkan pemakaian background data dan izinkan aplikasi pada mode penghematan data >> Nyalakan',
            'Pengaturan >> Aplikasi >> Layanan Wifi >> Baterai >> Izinkan Aktivitas latar belakang >> Nyalakan'
        ]
    },
    {
        'title': 'Huawei',
        'item': [
            'Manajer Telepon >> Penghematan Daya >> Aplikasi Terlindungi',
            'Pastikan "Wifi Service" terlindungi'
        ]
    },
    {
        'title': 'Huawei (Android 8 ke atas)',
        'item': [
            'Pengaturan >> Aplikasi & Notifikasi >> Aplikasi >> Layanan Wifi >> Baterai >> Dorongan Intensif-Daya >> Matikan Daya',
            'Pengaturan >> Aplikasi & Notifikasi >> Aplikasi >> Layanan Wifi >> Baterai >> Launch >> Atur Otomatis >> Matikan Daya',
            'Pengaturan >> Baterai >> Peluncuran aplikasi dan setel "WiFi Service" untuk mengelola secara manual dan memastikan semuanya telah dihidupkan',
            'Pengaturan >> Aplikasi >> Pengaturan >> Akses Spesial >> Abaikan Optimisasi Baterai >> Pilih izinkan "WiFi Service"',
        ]
    },
    {
        'title': 'REDMI',
        'item': [
            'Buka aplikasi "Keamanan" >> Izin >> Mulai Otomatis >> Aktifkan "Wifi Service"',
            'Pengaturan >> Pengaturan Tambahan >> Privasi >> Nonaktifkan "Verify Apps"',
            'Pengaturan >> Pengaturan Lanjutan >> 2 >> Manajer Baterai >> Rencana Daya di setel untuk dijalankan',
            'Pengaturan >> Pengaturan Lanjutan >> Manajer Baterai >> Aplikasi yang Terlindungi >> "WiFi Service" harus terlindungi',
            'Pengaturan >> Aplikasi >> "WiFi Service" >> Baterai >> Meminta intensifitas daya dan selalu berjalan setelah layar mati',
            'Pengaturan >> Pengaturan Tambahan >> Baterai & Performa >> Atur penggunaan daya oleh aplikasi >> Matikan mode hema daya >> Penghematan daya dielakang layar >> pilih aplikasi >> "WiFi Service" >> Background Settings >> Tidak ada pembatasan',
        ]
    },
    {
        'title': 'VIVO',
        'item': [
            'Buka menu mulai otomatis dan Aktifkan  "Wifi Service"',
            'Buka menu IManager aplikasi dan Aktifkan  "Wifi Service"'
        ]
    },
    {
        'title': 'INFINIX',
        'item': [
            'Buka "XOS family" >> X Manager >> Pengelola Mulai-Otomatis >> Aktifkan "Wifi Service"',
            'Pastikan "Wifi Service" terlindungi'
        ]
    },
    {
        'title': 'INFINIX (OS diatas 8.0/8.1)',
        'item': [
            'Master Telepon >> Kotak Peralatan >> Manajemen Mulai-Otomatis >> Aktifkan "Wifi Service"',
            'Master Telepon >> Saya (Me) >> Pengaturan >> Aplikasi Terlindungi >> Aktifkan "Wifi Service"'
        ]
    },
    {
        'title': 'OPPO',
        'item': [
            'Pengaturan >> Pengaturan Keamanan',
            'Tekan "Data Saving"',
            'Tekan "Add Apps that run in background " dan pilih "Wifi Service"'
        ]
    },
    {
        'title': 'OPPO (Android 6 ke atas)',
        'item': [
            'Pengaturan >> Baterai >> Penyimpan Daya >> "Wifi Service" >> Background Freeze >> Matikan Daya',
            'Pengaturan >> Baterai >> Penyimpan Daya >> "Wifi Service" >> Optimasi Aplikasi Abnormal >> Matikan Daya',
            'Pengaturan >> Manajer Aplikasi >> Terpasang >> "Wifi Service" >> Izin >> Izinkan Semua'
        ]
    },
    {
        'title': 'OPPO (Android 8 ke atas)',
        'item': [
            'Pengaturan >> Manajer Aplikasi >> Layanan Wifi >> Izinkan startup-otomatis >> aktifkan',
            'Pengaturan >> Manajer Aplikasi >> Layanan Wifi >> Perlindungan Konsumsi Daya >> Izinkan Berjalan di Latar Belakang'
        ]
    },
]

const phone_en = [{
        'title': 'Samsung (Android 7 or above)',
        'item': [
            'Settings >> Device maintenance >> Battery >> (bottom) Click on Unmonitored apps >> Add apps >> Select "WiFi Service"',
            'Settings >> Apps >> WiFi Service >> Mobile data >> Allow background data usage & Allow app while Data saver >> Turn ON'
        ]
    },
    {
        'title': 'Samsung Galaxy 8.0 (like Galaxy S9, S9, S8, S8+)',
        'item': [
            'Go to Settings >> Lockscreen & Security >> App Permission Monitor >> Disable "WiFi Service"',
            'Settings >> Device maintenance >> Battery >> Unmonitored apps >> Add apps >> WiFi Service >> DONE',
            'Settings >> Apps >> WiFi Service >> Mobile data >> Allow background data usage & Allow app while Data saver >> Turn ON',
            'Settings >> Apps >> WiFi Service >> Battery >> Allow background activity >> Turn ON'
        ]
    },
    {
        'title': 'Huawei',
        'item': [
            'Phone Manager >> Power saving >> Protected apps',
            'Make sure "WiFi Service" is protected.'
        ]
    },
    {
        'title': 'Huawei (Android 8 or above)',
        'item': [
            'Settings >> Apps & notifications >> Apps >> WiFi Service >> Battery >> Power-intensive prompt >> Power OFF',
            'Settings >> Apps & notifications >> Apps >> WiFi Service >> Battery >> Launch >> Manage automatically >> Power OFF',
            'Settings >> Battery >> App launch and then set "WiFi Service" to “Manage manually” and make sure everything is turned on.',
            'Settings >> Apps >> Settings >> Special access >> Ignore battery optimization >> select allow for "WiFi Service".'
        ]
    },
    {
        'title': 'REDMI',
        'item': [
            'Open Application Security >> Permissions >> Auto-start >> Enable "WiFi Service".',
            'Open Application Security >> Battery >> App Battery Saver >> "WiFi Service" >> No restriction',
            'Settings >> Advanced Settings >2> Battery Manager >> Power plan is set to Performance.',
            'Settings >> Advanced Settings >> Battery Manager >> Protected apps >>"WiFi Service" needs to be Protected',
            'Settings >> Apps >> "WiFi Service" >> Battery >> Power-intensive prompt and Keep running after screen off.',
            'Settings >> Additional Settings >> Battery & Performance >> Manage apps’ battery usage >> Switch Power Saving Modes to Off >> Saving Power in The Background >> Choose apps >> "WiFi Service" >> Background Settings >> No restrictions'
        ]
    },
    {
        'title': 'VIVO',
        'item': [
            'Open autostart application and enable "WiFi Service".',
            'Open IManager application and enable "WiFi Service".'
        ]
    },
    {
        'title': 'INFINIX',
        'item': [
            'Open Application "XOS family" >> X Manager >> Auto-Start Manager >> Enable "WiFi Service".',
            'Make sure "WiFi Service" is protected'
        ]
    },
    {
        'title': 'INFINIX (Above OS 8.0/8.1)',
        'item': [
            'Phone Master >> Toolbox >> Auto-start management >> Enable "WiFi Service".',
            'Phone Master >> Me >> Settings >> Protected app >> Enable "WiFi Service"'
        ]
    },
    {
        'title': 'OPPO',
        'item': [
            'Open "Settings" >> "Security Settings".',
            'Click on "Data Saving".',
            'Click on "Add apps that run in background" and select "WiFi Service".'
        ]
    },
    {
        'title': 'OPPO (Android 6 or Above)',
        'item': [
            'Settings >> Battery >> Energy Saver >> "WiFi Service" >> Background Freeze >> Power OFF',
            'Settings >> Battery >> Energy Saver >> "WiFi Service" >> Abnormal Apps Optimization >> Power OFF',
            'Settings >> App Management >> Installed >> "WiFi Service" >> Permissions >> Allow All'
        ]
    },
    {
        'title': 'OPPO (Android 8 or Above)',
        'item': [
            'Settings >> App Management >> wifi service >> Allow Auto Startup >> Enable',
            'Settings >> App Management >> wifi service >> Power saver >> Allow background running'
        ]
    },
]


$(document).ready(() => {
    android_carousel = document.getElementById('android-carousel')
    for (let i = 6; i < 24; i++) {
        android_carousel.innerHTML += `
            <div>
                <img src='../../Assets/Picture/Installation-Guide/Android/Carousell/android${i}.png' class='border-radius-alt install-guide-android-carousel' alt="install-guide-android-carousel-${i}">
            </div>
        `
    }

    $('#android-carousel').slick({
        centerMode: true,
        centerPadding: '120px',
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        infinite: false,
        prevArrow: '<span class="install-guide-android-carousel-arrow install-guide-android-carousel-arrow-left d-flex justify-content-center align-items-center"> <i class="mx-auto px-2 pr-3 fas fa-caret-left fa-2x color-secondary-green-dark"></i></span>',
        nextArrow: '<span class="install-guide-android-carousel-arrow install-guide-android-carousel-arrow-right d-flex justify-content-center align-items-center"> <i class="mx-auto px-2 pl-3 fas fa-caret-right fa-2x color-secondary-green-dark"></i></span>',

        responsive: [{
                breakpoint: 992,
                settings: {
                    infinite: false,
                    arrows: true,
                    centerMode: true,
                    centerPadding: '160px',
                    slidesToShow: 1,
                    dots: false
                }
            },
            {
                breakpoint: 576,
                settings: {
                    infinite: false,
                    arrows: true,
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 1,
                    dots: false
                }
            }
        ]
    })

    var language = window.location.pathname.substr(1).split("/")[0];
    var phone_content = language === "id" ? phone : phone_en;

    phone_install = document.getElementById('phone-install')

    for (let i = 0; i < phone_content.length; i++) {
        phone_install.innerHTML += `
    <span class="subheading fa-sm text-dark">${phone_content[i].title}</span>
    <ul class="install-guide-list">
    ${phone_content[i].item.map(satu => `<li>${satu}</li>`).join(' ')}
    </ul>`
    }
})
