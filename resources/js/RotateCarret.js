

isLetter = (str) => {
    return str.match(/[a-z]/i);
  }

Rotates = (e) => {
    let id_target = (e.target.id.substr(-2));
    if(isLetter(id_target.charAt(0))){
        id_target = id_target.slice(1)
    } 
    const arrow = document.getElementById('arrow' + id_target);
    arrow.classList.contains("fa-rotate-180") ? arrow.classList.remove("fa-rotate-180") : arrow.classList.add("fa-rotate-180")
}

Rotate = (angka) => {
    const cek = document.getElementById('features' + angka)
    const arrow = document.getElementById('arrow' + angka);
    if (cek.checked == true) {
        arrow.classList.add("fa-rotate-180")
    } else {
        arrow.classList.remove("fa-rotate-180")
    }
}

RotatesAll = () => {
const total_features = document.getElementsByClassName("features-input").length;
    for (let i = 1; i <= total_features; i++) {
        Rotate(i);
    }
}

// RotatesAll();

