removeDarkText = (e) => {
    e.target.classList.remove('text-dark');
}

addDarkText = (e) => {
    if (e.target.classList.contains('button')) {
        return
    } else {
        e.target.classList.add('text-dark');
    }
}

const styleActiveButton = (button) => {
    button.classList.remove("button-alt")
    button.classList.remove("text-dark")
    button.classList.add("button")
}

const Req = [
    "Android OS 4.4 atau lebih tinggi",
    "Mac OS X 10.13 ke atas",
    "Windows 8 ke atas",
    "Paket data yang tidak terbatas direkomendasikan"
]

const Req_en = [
    "Android OS 4.4 or above",
    "Mac OS X 10.13 or above",
    "Windows 8 or above",
    "Unlimited data plan recommended."
]

requirement_content = (idx_content_req) => {
    var language = window.location.pathname.substr(1).split("/")[0];

    const full_requirement = `
    <span>${Req[idx_content_req]}</span>
    <span>${Req[3]}</span>
    `;

    const full_requirement_en = `
    <span>${Req_en[idx_content_req]}</span>
    <span>${Req_en[3]}</span>
    `;

    return language === "id" ? full_requirement : full_requirement_en;
}

let requirement = document.getElementById("requirement")

fill_Requirement = device => {
    const angka = {
        android: 0,
        mac: 1,
        computer: 2
    };
    button = document.getElementById(`${device}-install-button`);
    styleActiveButton(button);
    requirement.innerHTML = requirement_content(angka[device]);
};

if (document.title.match("Install Guide for Android")) {
    device = "android";
    fill_Requirement(device);
} else if (document.title.match("Install Guide for Mac")) {
    device = "mac";
    fill_Requirement(device);
} else if (document.title.match("Install Guide for Computer")) {
    device = "computer";
    fill_Requirement(device);
}
