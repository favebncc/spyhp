import {getLang} from './pageActivator'
const idStpeType2 = [12]
const listFeature = [
    {
        id : 1,
        title : `Pelacakan Catatan Telepon`,
        content : [
            `Fitur catatan telepon memungkinkan Anda untuk melihat riwayat lengkap semua panggilan dari perangkat target, termasuk nomor telepon, nama kontak dan banyak lagi.`,
            `Meskipun Anda mungkin merekam panggilan tetapi memiliki akses ke catatan panggilan telepon memastikan dengan siapa pengguna perangkat target berinteraksi. Apakah Anda ingin tahu tentang percakapan telepon panjang anak Anda? Apakah Anda tertarik untuk mendapatkan detail tentang setiap telepon anak Anda? Dengan SPYHP Anda tidak akan pernah melewatkan satu telepon pun dari perangkat target Anda. Sebagai orang tua, Anda memiliki hak untuk mengetahui detail anak-anak dan orang yang Anda cintai ini.`,
            `Sekarang kami tahu di mana Anda bisa menggunakannya, mari mulai tutorial ini tentang cara merekam panggilan di ponsel android Anda.`
        ],
        image : [
            `Call_Log_Tracking/call_log_tracking1`
        ],
        step : [
            {
                title : `Instal SPYHP`,
                content : [
                    `Merekam panggilan telepon di ponsel orang lain adalah bagian yang sulit. Anda dapat mendengarkan percakapan ponsel target Anda dari jarak jauh. Kami telah menerima banyak panggilan dari orang tua yang peduli dan ini adalah fitur yang biasanya kami rekomendasikan karena dapat mengetahui dengan siapa anak-anak mereka telah berbicara dan apa sifat percakapan mereka.`,
                    `Jadi untuk mengunduh SPYHP, kunjungi halaman beranda dan klik <a href = "../../price">"Beli Sekarang"</a> untuk menuju platform pembelian.`,
                    `Untuk panduan lengkap instalasi, lihat halaman <a href = "../../install-guide/Android">Panduan Instalasi</a> kami. Ini juga akan membantu Anda dengan penggunaan awal.`
                ]
            },
            {
                title : `Periksa Persyaratan`,
                content :  [
                    `Agar fitur perekaman panggilan berfungsi, Anda perlu memeriksa persyaratan dan kompatibilitas ponsel Anda. Saat ini, fitur ini didukung di semua ponsel Android. Anda dapat memeriksa kompatibilitas ponsel Anda <a href = "../../install-guide/Android">di sini.</a> SPYHP berfungsi pada sebagian besar ponsel dan versi Android, tetapi Anda masih bisa bertanya untuk memastikan.`,
                    `Kami telah menerima beberapa keluhan dari pengguna yang berbicara tentang rekaman mereka yang muncul satu sisi. Masalah ini dialami banyak perangkat Android dan dilengkapi dengan pengaturan firmware. Pengaturan bawaan menyebabkan mikrofon dinonaktifkan ketika panggilan diterima, menyebabkan rekaman hanya terdengar satu sisi. Setiap ponsel memiliki pengaturan firmware pelanggan, jadi sayangnya SPYHP tidak dapat memberikan solusi umum untuk masalah ini.`
                ]
            },
            {
                title : `Periksa Panel Kontrol`,
                content :  [
                    `Setelah Anda menginstal SPYHP, buka panel kontrol. Anda akan tiba di <a href = "../../">beranda.</a>`,
                    `Ini akan memberi Anda ringkasan dari semua telepon masuk dan keluar serta pesan elektronik. Ini juga akan memberi Anda statistik terkait panggilan telepon berdasarkan data sebelumnya sebelum Anda mulai memata-matai ponsel secara real time.`,
                    `<img src="../../../Assets/Picture/Features/Feature_Details/Call_Log_Tracking/call_log_tracking2.png" alt="call_log_tracking2">`
                ]
            }
        ],
        'facility' : [
            {
                image : `fas fa-phone-alt`,
                text : `Lihat telepon tidak terjawab.`
            },
            {
                image : `fas fa-phone-square-alt`,
                text : `Lihat semua catatan telepon perangkat target.`
            },
            {
                image : `fas fa-trash-alt`,
                text : `Lihat catatan telepon bahkan jika itu dihapus dari ponsel.`
            },
            {
                image : `fas fa-download`,
                text : `Unduh semua file rekaman telepon.`
            }
        ],
        faq : [
            {
                title : `Bisakah kita memata-matai nomor telepon atau ponsel saja?`,
                content : `Pemantauan dilakukan di ponsel. Jadi SIM / Chip yang dimasukkan pada ponsel target akan dipantau oleh aplikasi.`
            },
            {
                title : `Kita hanya dapat melihat catatan telepon atau unduhannya?`,
                content : `Ya, Anda dapat mengunduh semua catatan telepon. Anda dapat mengunduhnya dari halaman pembebasan data.`
            },
            {
                title : `Apa yang Anda lihat di catatan telepon Anda?`,
                content : `Fitur catatan telepon SPYHP memungkinkan Anda untuk melihat riwayat lengkap semua telepon masuk dan keluar dari ponsel target, termasuk nomor telepon, nama kontak dan banyak lagi.`
            }
        ]
    },
    {
        id : 7,
        title : `Riwayat Lokasi Dengan Jalan`,
        content : [
            `SPYHP memungkinkan Anda melacak riwayat lokasi anak dengan jalur melalui ponsel dan tablet mereka sehingga Anda bisa tahu persis di mana mereka berada pada waktu tertentu atau sepanjang hari.`,
            `Tahukah Anda bahwa lebih dari 800.000 anak hilang setiap tahun di seluruh dunia? Setiap 50 detik, seorang anak diculik atau hilang. Karena semakin banyak orang tua yang khawatir di mana anak-anak mereka berada, SPYHP membantu mereka mengetahui lokasi pasti anak-anak mereka. SPYHP membantu Anda untuk melihat riyawat lokasi perangkat dan memungkinkan Anda untuk langsung melihat jalur dari semua tempat yang dijalani anak Anda.`
        ],
        image : [
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-location-arrow`,
                text : `Lihat semua tempat yang dikunjungi menggunakan jalur lokasi.`
            },
            {
                image : `fas fa-history`,
                text : `Lihat riwayat rute mereka selama periode waktu tertentu.`
            },
            {
                image : `far fa-map`,
                text : `Dapatkan gambar lengkap melalui peta online yang ada di beranda SPYHP Anda.`
            }
        ],
        faq : [
            {
                title : `Bagaimana cara melihat SMS, Catatan Telepon, dan informasi Pelacakan lokasi?`,
                content : `Semua Panel Kontrol dapat diakses di <a href = "../..">http://www.spyhp.com/</a>. Catatan pertama dari pesan teks yang ditangkap, rincian panggilan lokasi GPS dan banyak lagi akan tersedia segera setelah otentikasi pengguna awal.`
            },
            {
                title : `Bagaimana kami bisa mengetahui Riwayat Lokasi?`,
                content : `Kami dapat melihat semua tempat yang dikunjungi menggunakan Jalur lokasi juga. Lihat riwayat rute mereka. Dapatkan gambar lengkap melalui peta online yang ada di beranda SPYHP Anda.`
            }
        ]
    },
    {
        id : 8,
        title : `Mengatur Geofence`,
        content : [
            `Geo-fencing dapat mengakhiri semua kekhawatiran anak Anda. Jika ada tempat-tempat yang tidak aman untuk anak-anak Anda, maka fitur Geofence SPYHP adalah solusi terbaik yang dapat Anda andalkan. Dengan SPYHP Geofence, Anda dapat memantau tempat-tempat tertentu dan membuat anak-anak Anda menjauh dari tempat-tempat itu.`,
            `Fitur Geo-fencing SPYHP memungkinkan penggunanya untuk menerima peringatan ketika perangkat target meninggalkan daerah aman. Ini adalah fitur yang paling dibutuhkan & kuat untuk keselamatan anak. Buatlah geofence di sekitar rumah Anda atau sekolah anak Anda dan Anda dapat mengetahui ketika anak Anda mencoba untuk meninggalkannya. Mengatur geofence di sekitar rumah Anda atau bahkan di sekitar tempat yang paling sering dikunjungi dapat menjadi alat yang berguna untuk menemukan perangkat yang dicuri.`
        ],
        image : [
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-bullseye`,
                text : `Tetapkan area 'aman' atau 'tidak aman' (geofence) dalam jumlah tak terbatas.`
            },
            {
                image : `far fa-building`,
                text : `Tetapkan area, lokasi, dan tempat yang tidak terbatas di daftar tempat ‘aman’ dan ‘tidak aman’.`
            },
            {
                image : `far fa-clock`,
                text : `Lihat kapan dan seberapa sering setiap daerah dikunjungi.`
            },
            {
                image : `fas fa-history`,
                text : `Lihat riwayat pergerakan anak di peta pada beranda.`
            }
        ],
        faq : [
            {
                title : `Apa itu Geo Fence di SPYHP?`,
                content : `Fitur Geo-fencing dari SPYHP agar pengguna dapat menerima peringatan ketika perangkat target meninggalkan daerah aman. Ini adalah fitur yang paling dibutuhkan & kuat untuk keselamatan anak. Buat geofence di sekitar rumah Anda atau sekolah anak Anda dan Anda dapat mengetahui ketika anak Anda mencoba untuk meninggalkannya. Mengatur geofence di sekitar rumah Anda atau bahkan di sekitar tempat yang paling sering dikunjungi dapat menjadi alat yang berguna untuk menemukan perangkat yang dicuri.`
            },
            {
                title : `Bagaimana mengatur geo fence?`,
                content : [`Geo fence digunakan untuk mendapatkan notifikasi ketika ponsel bergerak masuk / keluar Geo Place. Berikut adalah langkah-langkah untuk mengatur Geo Fence.`,
                `<ol>
                <li>Masuk ke Panel Beranda SPYHP.</li>
                <li>Klik ke Geo Fence Dari menu di samping.</li>
                <li>Klik tombol "Buat Baru" untuk mengatur geo fence baru perangkat Anda.</li>
                <li>Sekarang Anda mendapatkan satu peta dan lingkaran di lokasi terakhir perangkat Anda. Pindahkan titik tengah ke tempat lain di mana Anda ingin memasang fence.</li>
                <li>Masukkan nama tempat di bawah kotak peta dan simpan.</li>
                <li>Anda dapat melihat daftar Geo fence di tabel. Jika Anda ingin memperbarui (mengubah) geo fence Anda yang keluar kemudian klik "Lihat Peta".</li>
                </ol>`,
                `Catatan: Geo Fence berfungsi dengan baik jika perangkat Anda sering menghubungkan dengan GPS dan dalam jangkauan sekitar 50 meter.`]
            },
            {
                title : `Berapa banyak lokasi yang dapat saya lakukan geofence dengan satu langganan SPYHP?`,
                content : `Tidak ada batasan jumlah lokasi yang dapat Anda gunakan secara geografis menggunakan SPYHP.`
            },
            {
                title : `Bagaimana saya bisa geofence lokasi pengguna target?`,
                content : `Menggunakan fitur geo fencing SPYHP sangat mudah digunakan. Yang harus Anda lakukan adalah menggunakan akun web Anda, Pergi ke SPYHP dashboard ==> "Geo fence" klik pada halaman buat Baru dan atur geofence.`
            }
        ]
    },
    {
        id : 9,
        title : `Peringatan Geofence`,
        content : [
            `Dengan SPYHP, Anda dapat memonitor anak Anda dari jarak jauh dan mendapatkan peringatan instan ketika anak-anak Anda memasuki atau meninggalkan area yang telah Anda tentukan pada geofence Anda.`,
            `Geofence SPYHP memungkinkan Anda untuk menetapkan batas lokasi sehingga ketika anak Anda memasuki atau meninggalkan tempat itu, Anda akan segera diberi tahu baik di beranda dan Email. Anda memiliki kontrol penuh untuk mengatur lokasi, batas, dan jarak sehingga Anda segera tahu di mana mereka berada dan apa yang mereka lakukan. Secara instan menerima peringatan ketika anak Anda pergi atau memasuki area geofence.`
        ],
        image : [
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-bullseye`,
                text : `Tetapkan area 'aman' atau 'tidak aman' (geofence) dalam jumlah tak terbatas.`
            },
            {
                image : `far fa-bell`,
                text : `Dapatkan peringatan email setiap kali mereka memasuki atau meninggalkan geofences tersebut.`
            },
            {
                image : `far fa-clock`,
                text : `Periksa waktu dan tanggal.`
            },
            {
                image : `fas fa-envelope`,
                text : `Dapatkan semua detail tentang area di email.`
            }
        ],
        faq : [
            {
                title : `Dapatkah saya mengatur peringatan area spesifik pada perangkat yang dipantau menggunakan SPYHP?`,
                content : `Untuk mengatur peringatan untuk area tertentu, buka "Beranda SPYHP" di akun web SPYHP Anda dan buka halaman "Geofence". Pada tombol "Buat Baru", Anda dapat menambahkan semua lokasi yang tidak Anda inginkan berada di dalam atau di sekitar orang yang dipantau.`
            },
            {
                title : `Saya tidak mendapatkan peringatan untuk area tertentu dari perangkat yang dipantau, apa yang bisa saya lakukan?`,
                content : `Untuk mendapatkan peringatan lokasi tertentu dari perangkat target, buka "Beranda SPYHP" di akun web SPYHP Anda, dan cari halaman "Geofence". Pada tombol "Buat baru", Anda dapat menyimpan nama tempat dan mengatur Geofence yang Anda inginkan dari lokasi tertentu.`
            }
        ]
    },
    {
        id : 10,
        title : `Akses Buku Telepon`,
        content : [
            `SPYHP dari jarak jauh menangkap semua detail setiap kontak dari buku telepon perangkat target sehingga Anda dapat mengakses semua kontak langsung dari beranda.`,
            `Anda harus selalu waspada ketika mempercayai anak-anak Anda dengan siapa mereka bertemu. SPYHP membantu Anda untuk melihat isi kontak yang meragukan pada perangkat anak Anda.`
        ],
        image : [
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-phone-square-alt`,
                text : `Lihat semua nama dan nomor yang tersimpan di buku telepon.`
            },
            {
                image : `fas fa-search`,
                text : `Temukan kontak dari daftar kontak dengan memasukkan nama atau nomor.`
            },
            {
                image : `fas fa-at`,
                text : `Menampilkan id email yang terkait dengan kontak.`
            }
        ],
        faq : [
            {
                title : `Bisakah SPYHP screenshot kontak di Buku telepon?`,
                content : `SPYHP dari jarak jauh dapat menangkap semua detail setiap kontak dari buku telepon perangkat target sehingga Anda dapat mengakses semua kontak langsung dari beranda.`
            },
            {
                title : `Mengapa kontak buku alamat saya tidak diunggah?`,
                content : [`Jika kontak buku alamat tidak disinkronkan, maka pengguna dapat menyinkronkannya dengan Perintah SMS. Kirim Perintah SMS di bawah ini ke ponsel target tempat klien SPYHP diinstal.`,
                `<code class="text-danger"><sms pin=""> SC 1</sms></code>`,
                `Pin adalah PIN SMS Anda. Periksa <a href = "../../faq">https://www.spyhp.com/faq</a> untuk lebih jelasnya.`
                ]
            }
        ]
    },
    {
        id : 11,
        title : `Pemblokiran Kontak`,
        content : [
            `SPYHP memungkinkan Anda memblokir panggilan masuk dari kontak apa pun di perangkat yang dipantau. Anda dapat memblokir nomor apa pun meskipun nomor itu tidak ada dalam buku telepon.`,
            `Anak Anda mungkin menjadi korban intimidasi anak dan Anda bahkan tidak mengetahuinya. Anak Anda mungkin berhubungan dengan orang-orang yang tidak perlu dan Anda mungkin tidak mengetahui hal-hal ini. Jadi fitur pemblokiran panggilan SPYHP sangat berguna bagi Anda. Dengan fitur pemblokiran panggilan SPYHP Anda harus yakin tentang interaksi anak Anda dengan orang lain di telepon. Jika Anda menemukan kontak atau nomor ponsel yang mencurigakan di buku telepon, Anda dapat segera memblokirnya tanpa menyentuh perangkat anak Anda.`
        ],
        image : [
            `Contact_Blocking/contact_blocking1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-ban`,
                text : `Blokir panggilan telepon dari kontak apa pun dalam daftar kontak.`
            },
            {
                image : `fas fa-user-times`,
                text : `Blokir panggilan telepon dari nomor yang tidak diinginkan yang tidak ada dalam kontak.`
            },
            {
                image : `fas fa-desktop`,
                text : `Kelola dan Blokir kontak dari panel beranda Anda.`
            }
        ],
        faq : [
            {
                title : `Bagaimana cara memblokir atau membuka blokir kontak untuk pengguna tertentu?`,
                content : `SPYHP memungkinkan Anda untuk memblokir panggilan masuk dari kontak apa pun pada perangkat yang dipantau. Anda dapat memblokir nomor apa pun meskipun nomor itu tidak ada dalam buku telepon.`
            }
        ]
    },
    {
        id : 12,
        title : `Memata-matai Whatsapp Dari Jarak Jauh`,
        content : [
            `Jika Anda ingin memantau pesan WhatsApp anak Anda, maka Anda dapat menggunakan SPYHP karena dengan SPYHP Anda dapat memeriksa semua pesan WhatsApp, multimedia yang dikirim dan diterima dari ponsel target secara jarak jauh. Secara instan memeriksa nama dan jumlah pengirim dan detail lainnya seperti waktu dan tanggal.`,
            `Apakah Anda khawatir tentang aktivitas media sosial mencurigakan anak Anda? Anda harus sepenuhnya menyadari apa yang terjadi dengan anak-anak di bawah umur Anda untuk mencegah bahaya kepada mereka. SPYHP dapat memberi Anda semua pesan individu WhatsApp, pesan grup, multimedia dengan kontak dan tanggal untuk pesan langsung ke panel <a href = "../../">beranda SPYHP</a> Anda.`,
            `Whatsapp adalah aplikasi sosial yang sangat populer saat ini dan jutaan orang menggunakannya untuk berkomunikasi satu sama lain. Remaja, dewasa muda, dan bahkan lanjut usia juga menggunakan Whatsapp. Anda tidak perlu memiliki akses ke ponsel Android target untuk memata-matai WhatsApp. Mengambil kata sandi dan login juga sangat mudah. Jika Anda khawatir tentang perilaku anak-anak Anda, maka Anda dapat menggunakan <a href = "../../">SPYHP - Perangkat Lunak Pemantauan Orang Tua</a> untuk memata-matai whatsapp.`,
            `<ul>
                <li>Anda dapat memonitor aktivitas Whatsapp dari jarak jauh tanpa target menyadari bahwa ponselnya sedang dipantau.</li>
                <li>Whatsapp telah dikenal sebagai aplikasi dengan perlindungan kuat yang diterapkan di dalamnya. Tetapi, SPYHP adalah aplikasi mata-mata yang kuat yang tidak dapat dideteksi oleh sistem keamanan.</li>
                <li>Lihat riwayat pesan, periksa pesan grup dan pribadi, lihat video, foto, dokumen, dan file bersama lainnya melalui Whatsapp.</li>
            </ul>`
        ],
        image : [
            `Remotely_Spy_Whatsapp_Chats/remotely_spy_whatsapp_chats1`
        ],
        step : [
            {
                title : `Anda perlu akses fisik ke perangkat untuk mengunduh dan menginstal SPYHP di ponsel.`,
                content : [
                ]
            },
            {
                title : `Setelah berhasil menginstal ke perangkat dengan kredensial Anda.`,
                content : [
                ]
            },
            {
                title : `Masuk ke akun Anda di sini <a href = "../../">https://spyhp.com/</a> - (Dengan id Email dan kata sandi Anda)`,
                content : [
                ]
            }
        ],
        'facility' : [
            {
                image : `fab fa-whatsapp`,
                text : `Lacak semua pesan WhatsApp.`
            },
            {
                image : `fas fa-info-circle`,
                text : `Periksa nama dan nomor pengirim.`
            },
            {
                image : `far fa-clock`,
                text : `Lihat waktu dan tanggal.`
            },
            {
                image : `far fa-image`,
                text : `Lihat foto, klip video dan dengarkan pesan audio.`
            },
            {
                image : `fas fa-desktop`,
                text : `Akses informasi yang diambil langsung dari beranda SPYHP Anda.`
            }
        ],
        faq : [
            {
                title : `Bagaimana Saya Dapat Melacak pesan WhatsApp dari Jarak Jauh?`,
                content : `pesan WhatsApp dapat dilacak dari jarak jauh menggunakan SPYHP. Aplikasi ini membutuhkan instalasi satu kali pada perangkat yang ingin Anda pantau. Setelah itu, Anda dapat melacak semua pesan, catatan telepon, dan multimedia dari jarak jauh. Atau, Anda juga dapat memata-matai pesan WhatsApp dari jarak jauh tanpa memerlukan pengunduhan atau pemasangan apa pun menggunakan versi SPYHP iCloud.`
            },
            {
                title : `Data WhatsApp tidak terunggah`,
                content : [`SPYHP tidak dapat menangkap data WhatsApp jika ada lebih dari satu layanan Wifi yang diinstal di perangkat target. Ikuti langkah-langkah di bawah ini di perangkat target:`,
                `<ol>
                <li>Buka Pengaturan >> Aksesibilitas >> Periksa berapa banyak layanan Wifi yang ada?</li>
                <li>Aktifkan layanan Wifi Terakhir dalam Daftar & Nonaktifkan semua layanan Wifi lainnya.</li>
                </ol>`]
            },
            {
                title : `Apakah Whatsapp membutuhkan ponsel yang di-rooting?`,
                content : `Tidak, Anda tidak perlu ponsel Anda di-root untuk mengakses Whatsapp Pesan Masuk-Keluar. Anda perlu memberikan izin aksesibilitas pada perangkat pemantauan Anda ==> Pengaturan Perangkat >> Aksesibilitas >> Layanan WiFi >> Aktifkan.`
            },
            {
                title : `Masalah WhatsApp diperbaiki.`,
                content : [`Sekarang Anda dapat memonitor pesan Whatsapp dan Facebook yang masuk dan keluar tanpa root. Ikuti langkah-langkah di bawah ini.`,
                `<ol>
                <li>Instal / Instal Ulang aplikasi SPYHP terbaru di ponsel Anda. Lihat panduan instalasi di <a href ="../..//install-guide/Android">Install guide</a></li>
                <li>Berikan izin Aksesibilitas pada perangkat pemantauan Anda ==> Pengaturan Perangkat >> Aksesibilitas >> Layanan WiFi >> Aktifkan.</li>
                </ol>`,
                `Jika Anda tidak mendapatkan opsi Aksesibilitas di Pengaturan, lalu ketuk tombol "Home" tiga kali untuk fitur aksesibilitas aktif di Pengaturan > Aksesibilitas > Aktifkan layanan Wifi.`]
            },
            {
                title : `Mengapa fitur WhatsApp tidak berfungsi?`,
                content :[ `pesan WhatsApp dapat dilacak dari jarak jauh menggunakan SPYHP. Aplikasi ini membutuhkan instalasi satu kali pada perangkat yang ingin Anda pantau. Setelah itu, Anda dapat melacak semua pesan, catatan telepon, dan multimedia dari jarak jauh. Atau, Anda juga dapat memata-matai pesan WhatsApp dari jarak jauh tanpa memerlukan pengunduhan atau pemasangan apa pun menggunakan versi SPYHP iCloud.`,
                `<ol>
                <li>Pesan tidak akan ditampilkan ke beranda SPYHP secara instan.</li>
                <li>Pada perangkat non-root, klien SPYHP menggunakan database cadangan WhatsApp untuk mendapatkan pesan, jadi jika autobackup dinonaktifkan di Whatsapp maka Anda mungkin tidak mendapatkan pesan ke beranda SPYHP.</li>
                <li>Klien SPYHP hanya akan menyinkronkan pesan sekali dalam sehari, sehingga Anda mungkin hanya dapat melihat pesan lama satu atau dua hari.</li>
                <li>Mengambil backup Whatsapp dengan langkah-langkah di bawah ini.</li>
                </ol>`,
                `<ol>
                <li>Buka WhatsApp di perangkat pemantauan.</li>
                <li>Tekan menu, Buka pengaturan.</li>
                <li>Buka pengaturan pesan.</li>
                <li>Klik pada "Backup percakapan".</li>
                </ol>`]
            },
            {
                title : `Bagaimana cara mengakses dokumen Whatsapp dan format PDF di ponsel?`,
                content : [`Jika Anda ingin mengunduh mata-mata Whatsapp, Anda perlu melakukan beberapa langkah sederhana:`,
                `Program WhatsApp mata-mata membantu Anda melihat percakapan apa yang dikirimkan seseorang atau apa yang diterima seseorang.`,
                `<ol>
                <li>Pertama, Anda harus pergi ke situs SPYHP <a href = "../../">https://spyhp.com/</a></li>
                <li>Unduh dan instal aplikasi di telepon yang ingin Anda pantau.</li>
                <li>Setelah langkah terakhir menginstal dan mendaftarkan aplikasi di ponsel, Anda dapat memantau semua audio Whatsapp, foto, dokumen, serta format PDF di portal pengguna SPYHP Anda.</li>
                </ol>`,
                `Setelah Anda menerima dokumen atau format PDF di Whatsapp, itu akan diunggah di portal pengguna SPYHP Anda setelah itu Anda dapat mengakses dokumen dan format PDF di Beranda SPYHP => halaman SD card. Pengirim dapat mengirim dokumen dan format PDF apa pun, file audio Anda dapat mengakses konten itu di halaman SD card.`
            ]

            }
        ]
    },
    {
        id : 13,
        title : `Pelacak Facebbok`,
        content : [
            `Ingin tahu apa yang sebenarnya dilakukan anak-anak Anda ketika mereka ada di Aplikasi Facebook? SPYHP memberi tahu Anda tepat waktu jika anak-anak Anda berbicara dengan orang yang tidak pantas atau berbagi gambar berbahaya, kegiatan kelompok yang mencurigakan, dll.`,
            `Apakah Anda khawatir tentang aktivitas media sosial yang berlebihan dari anak-anak Anda? Apakah Anda ingin mencari tahu apa yang mereka lakukan dan dengan siapa mereka berinteraksi? SPYHP menghilangkan ketidakpastian dengan menyediakan Anda fasilitas untuk melihat pesan Facebook. Pelacak Facebook SPYHP mewujudkannya untuk Anda. SPYHP menangkap semua informasi ini dari ponsel mereka. SPYHP akan memberi Anda semua aktivitas Facebook langsung ke panel beranda Anda.`
        ],
        image : [
            `Facebook_Tracker/facebook_tracker1`
        ],
        step : [
            {
                title : `Lihat semua pesan Facebook termasuk pesan grup, waktu dan tanggal percakapan`,
                content : [
                ]
            },
            {
                title : `Setelah diaktifkan, pelacak Facebook mulai mengumpulkan data dari perangkat target dan mengunggahnya langsung ke portal pengguna online Anda untuk akses menonton yang nyaman.`,
                content : [
                ]
            },
            {
                title : `Lindungi anak-anak Anda dari pemangsa seksual, pengganggu dunia maya dan perilaku berisiko`,
                content : [
                ]
            }
        ],
        'facility' : [
            {
                image : `fab fa-facebook-square`,
                text : `Lihat semua percakapan pesan Facebook.`
            },
            {
                image : `far fa-clock`,
                text : `Lihat waktu dan tanggal.`
            },
            {
                image : `fas fa-search`,
                text : `Cari tahu nama orang yang mereka ajak ngobrol.`
            },
            {
                image : `far fa-bell`,
                text : `Dapatkan akses ke semua pemberitahuan & aktivitas pengguna di Facebook.`
            },
            {
                image : `fas fa-desktop`,
                text : `Mudah melihat semua data yang diambil langsung dari beranda SPYHP Anda.`
            }
        ],
        faq : [
            {
                title : `Bagaimana cara kerja pemantauan Facebook?`,
                content : `Aplikasi ini membutuhkan instalasi satu kali pada perangkat yang ingin Anda pantau. Setelah itu, Anda perlu memberikan izin Aksesibilitas di perangkat pemantauan Anda >> Pengaturan Perangkat >> Aksesibilitas >> Layanan WiFi >> Aktifkan. Anda dapat melacak semua pesan Facebook dari jarak jauh. Atau, Anda juga dapat memata-matai pesan Facebook dari jarak jauh tanpa melakukan root pada ponsel Anda.`
            },
            {
                title : `Fasilitas apa yang memberi SPYHP untuk melacak Facebook?`,
                content : `<ol>
                <li>Lihat semua percakapan pesan Facebook.</li>
                <li>Lihat waktu dan tanggal.</li>
                <li>Cari tahu nama orang yang mereka ajak ngobrol.</li>
                <li>Dapatkan akses ke semua pemberitahuan & aktivitas pengguna di Facebook.</li>
                <li>Mudah melihat semua data yang diambil langsung dari beranda SPYHP Anda.</li>
                </ol>`
            },
            {
                title : `Bisakah saya memonitor pesan Facebook tanpa perangkat yang di-rooting?`,
                content : `Ya, Anda dapat memonitor pesan Facebook tanpa me-root perangkat target Anda. Untuk itu Anda perlu mengaktifkan aksesibilitas perangkat target: Aktifkan aksesibilitas dari Pengaturan Perangkat -> Aksesibilitas -> Layanan WiFi -> Diaktifkan.`
            },
            {
                title : `Bagaimana saya bisa memonitor pesan Facebook yang dihapus?`,
                content : `Anda dapat memonitor semua pesan yang dihapus bahkan jika ponsel target telah menghapusnya, Anda dapat memonitor semua pesan yang dihapus di Panel Kontrol Anda.`
            }
        ]
    },
    {
        id : 14,
        title : `Pelacak Gmail`,
        content : [
            `Dalam mengirim email, anak Anda dapat terhubung dengan orang yang salah. Jika anak Anda sibuk mengirim email dan Anda ingin tahu persis apa yang dikirimkan, Anda dapat menggunakan perangkat ini untuk memantau kegiatan anak Anda karena fitur Gmail Tracker SPYHP akan memberi tahu Anda apa yang sedang terjadi ketika Anda sedang tidak di sana mengawasi mereka.`,
            `Saat ini hampir semua orang melakukan pekerjaan mereka melalui smartphone. Tentu saja email termasuk dari kegiatan yang penting bagi mereka. Gmail Tracker dari SPYHP membantu Anda menemukan semua data email seperti pesan masuk atau pesan yang diterima melalui smartphone. Pada ponsel Android, akun email utama akan selalu Gmail dan SPYHP akan memberikan pelayanan penuh dalam pelacakan Gmail di seluruh perangkat Android.`
        ],
        image : [
            `Google_Tracker/google_tracker1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-envelope`,
                text : `Membaca semua email yang dikirim dan diterima via Gmail.`
            },
            {
                image : `fas fa-paperclip`,
                text : `Melihat semua lampiran di Gmail.`
            },
            {
                image : `fas fa-user`,
                text : `Memeriksa semua kontak pada email.`
            },
            {
                image : `far fa-clock`,
                text : `Mendata seluruh waktu/tanggal email.`
            }
        ],
        faq : [
            {
                title : `Bagaimana cara melacak Gmail menggunakan SPYHP?`,
                content : `SPYHP melacak semua data yang Anda cari seperti email yang dikirim dan diterima langsung dari smartphone. Pada smartphone Android, akun Gmail utama akan selalu diberikan pelayanan penuh dari fitur Gmail Tracker ini.`
            },
            {
                title : `Bagaimana cara menghentikan email harian dari layanan SPYHP?`,
                content : [`Lakukan login pada dashboard SPYHP`,
                `Pilih menu perangkat`,
                `Buka pengaturan`,
                `Hapus centang “EveryDay Mail Notification”`,
                `Simpan pengaturan`]
            }
        ]
    },
    {
        id : 15,
        title : `Pelacak Skype`,
        content : [
            `SPYHP memungkinkan Anda untuk melihat seluruh percakapan yang terjadi di Skype melalui smartphone. Anda dengan mudah dapat memantau semua aktivitas Skype anak Anda langsung dari beranda Anda. Anda hanya tinggal mengklik untuk memantau dan memastikan anak Anda aman.`,
            `Apakah Anda mengetahui apa yang dilakukan anak Anda di Skype? Dengan SPYHP, Anda dapat melihat semua nama orang yang mengobrol dengan anak Anda sehingga pesan mereka tidak pernah akan membuat Anda khawatir lagi. Anda juga dapat memantau pesan mereka untuk mencari tahu dengan siapa anak Anda online. Fitur ini memungkinkan Anda untuk berfokus pada kontak yang tidak biasa dan mengizinkan Anda menghentikan sesuatu buruk/memalukan yang mungkin dapat terjadi.`
        ],
        image : [
            `Skype_Tracker/skype_tracker1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fab fa-skype`,
                text : `Melihat semua percakapan di Skype.`
            },
            {
                image : `fas fa-search`,
                text : `Mengetahui nama orang yang diajak berinteraksi.`
            },
            {
                image : `far fa-clock`,
                text : `Mendapatkan informasi waktu dan tanggal dari setiap percakapan yang berlangsung.`
            },
            {
                image : `fas fa-desktop`,
                text : `Semua pesan dan media akan langsung diunggah ke dashboard SPYHP Anda.`
            }
        ],
        faq : [
            {
                title : `Apakah SPYHP merekam panggilan Skype?`,
                content : `Tidak, SPYHP tidak merekam panggilan Skype. Namun, seluruh pesan masuk dan terkirim akan tersedia tanpa perlu melakukan root pada smartphone.`
            },
            {
                title : `Bisakah pesan Skype dipantau?`,
                content : `Ya, Anda dapat memantau pesan Skype menggunakan SPYHP. Aplikasi ini mengakses semua pesan yang Skype yang dikirim atau diterima (pada perangkat yang dipantau) dan secara otomatis mengunggahnya ke akun web Anda, di mana Anda dapat melihat atau mengunduhnya kapan saja.`
            },
            {
                title : `Apakah saya dapat merekam panggilan Skype dan mendengarkannya menggunakan SPYHP?`,
                content : `SPYHP tidak menawarkan fitur perekaman panggilan Skype. Tetapi, Anda dapat melacak log panggilan Skype beserta waktu dan tanggal panggilan.`
            }
        ]
    },
    {
        id : 16,
        title : `Pelacak Viber`,
        content : [
            `Viber adalah salah satu aplikasi paling populer saat ini sehingga semua orang tua perlu membiasakan diri dengan aplikasi ini jika ingin melindungi anak-anak mereka. Viber adalah aplikasi yang memungkinkan pengguna mengirim pesan teks dan panggilan suara menggunakan internet yang terhubung pada perangkat yang dipakai.`,
            `Pemantauan dari SMS dan panggilan saja tidak akan memberikan Anda hasil yang memuaskan dari aktivitas seluler anak Anda. Anda memerlukan perangkat lunak untuk memantau aplikasi chatting seperti Viber sebagai salah satu aplikasi chatting yang paling sering digunakan. SPYHP memperkenalkan fitur pelacakan Viber yang kuat dan akurat dalam memberikan Anda seluruh informasi dengan cepat.`,
            `Apakah Anda merasa tertinggal karena anak-anak Anda sudah menggunakan semua aplikasi ini? Apakah Anda ingin membuang semua kekhawatiran Anda dan ingin mengawasi pesan anak Anda, tanpa peduli aplikasi apa yang mereka gunakan? Apakah Anda berpikir untuk mendapatkan semua informasi tersebut pada satu tempat? Jika ya, SPYHP adalah pilihan yang tepat untuk Anda. SPYHP akan membantu Anda memeriksa dan memantau pesan Viber dari perangkat anak-anak Anda langsung ke dashboard SPYHP Anda.`
        ],
        image : [
            `Viber_Tracker/viber_tracker1`
        ],
        step : [
            {
                title : `Viber terbuka untuk umum dan bebas pulsa. Itu sebabnya siapa pun dapat menggunakannya untuk menghubungi anak Anda.`,
                content : [
                ]
            },
            {
                title : `Bagaimanapun, kita harus melakukan kontrol orang tua yang ketat meskipun ada kesulitan teknis atau keuangan.`,
                content : [
                ]
            },
            {
                title : `Ini juga bermanfaat untuk membaca akronim pesan gaul remaja sehingga Anda dapat mencegat pesan-pesan yang tidak diinginkan dan menguraikannya.`,
                content : [
                ]
            },
            {
                title : `Anak Anda dapat berpotensi mengalami pelecehan seksual melalui bertukar file foto dan video.`,
                content : [
                ]
            }
        ],
        'facility' : [
            {
                image : `fas fa-comments`,
                text : `Melihat semua percakapan pesan Viber.`
            },
            {
                image : `fas fa-users`,
                text : `Melihat pesan grup dengan detail.`
            },
            {
                image : `fas fa-search`,
                text : `Mengetahui nama dan jumlah orang yang diajak chatting.`
            },
            {
                image : `far fa-clock`,
                text : `Mendapatkan informasi waktu dan tanggal setiap chatting yang berlangsung.`
            },
            {
                image : `fas fa-desktop`,
                text : `Semua data Viber yang dilacak secara langsung akan diunggah ke dashboard SPYHP.`
            }
        ],
        faq : [
            {
                title : `Bagaimana cara membuat fitur pemantauan ini bekerja?`,
                content : [`Aplikasi ini membutuhkan sekali instalasi pada perangkat yang ingin Anda pantau. Caranya:`,
                `<ol>
                <li>Untuk mendapatkan pesan Viber, Anda perlu memberikan izin akses pada perangkat dengan melalui -> Pengaturan Perangkat -> Aksesibilitas -> Layanan WiFi -> Aktifkan.</li>
                <li>Aktifkan aplikasi Viber dari pengaturan dashboard (jika belum diaktifkan secara otomatis).</li>
                </ol>`,
                `SPYHP memperkenalkan fitur pelacakan Viber yang kuat yan memberikan informasi dengan cepat.`]
            },
            {
                title : `Mengapa tidak menampilkan pesan Viber di ponsel yang tidak di-rooting?`,
                content : `Dalam pengaturan aplikasi Viber, jika “Preview” dinonaktifkan, Anda tidak akan mendapatkan pesan Viber. Anda hanya akan mendapatkan deskripsi singkat. Anda perlu memberikan izin akses pada perangkat pemantauan Anda melalui -> Pengaturan Perangkat -> Aksesibilitas -> Layanan WiFi -> Aktifkan.`
            },
            {
                title : `Bisakah saya memata-matai Viber tanpa mengakses perangkat target?`,
                content : `Anda tetap memerlukan akses satu kali ke perangkat target untuk mengunduh dan menginstal SPYHP. Setelah instalasi berhasil, Anda dapat memata-matai Viber tanpa mengakses perangkat target.`
            }
        ]
    },
    {
        id : 17,
        title : `Pelacak Line`,
        content : [
            `Dengan SPYHP, Anda dapat memeriksa semua pesan dan pesan LINE dari smartphone target lengkap dengan waktu dan tanggalnya. SPYHP akan memberikan Anda semua pesan, kontak dan media di suatu tempat tanpa kehilangan file tersebut, tempat itu adalah dashboard SPYHP Anda.`,
            `LINE merupakan aplikasi chatting paling populer dengan pengguna lebih dari 350 juta orang di seluruh dunia dan masih terus bertambah. Tetapi melalui pesan LINE, intimidasi dapat terjadi kepada anak Anda. Fitur pemantauan ini dapat memberitahu Anda jika anak Anda mengobrol dengan orang asing, mentransfer media berbahaya dan memantau aktivitas anak Anda. Dengan SPYHP Anda dapat melihat semua percakapan pesan LINE mereka beserta file yang dikirim sehingga Anda tidak akan tertinggal pesan yang sedang berlangsung. Jadi, Anda tidak perlu lagi khawatir memantau percakapan rahasia anak Anda.`
        ],
        image : [
            `Line_Tracker/line_tracker1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-comments`,
                text : `Melihat semua percakapan LINE.`
            },
            {
                image : `fas fa-users`,
                text : `Melihat pesan individu dan grup.`
            },
            {
                image : `fas fa-search`,
                text : `Mengetahui nama orang yang melakukan chatting bersama smartphone target.`
            },
            {
                image : `far fa-clock`,
                text : `Mendapatkan informasi mengenai kapan setiap pesan berlangsung.`
            },
            {
                image : `fas fa-desktop`,
                text : `Semua percakapan LINE akan diunggah langsung ke dashboard SPYHP.`
            }
        ],
        faq : [
            {
                title : `Bagaimana cara melacak pesan LINE dari jarak jauh?`,
                content : `pesan LINE dapat dilacak dari jauh menggunakan SPYHP. APlikasi ini hanya membutuhkan satu kali instalasi pada perangkat yang ingin dipantau dan melakukan pendaftaran dengan ID email dan kata sandi Anda. Setelah itu, Anda dapat melacak semua pesan dari jarak jauh melalui panel kontrol.`
            },
            {
                title : `Bisakah saya memata-matai pesan LINE di ponsel yang tidak di-root?`,
                content : `Tidak, Anda tidak dapat memantau pesan pada perangkat yang tidak di-rooting. Jika perangkat target Anda tidak di-root, Anda hanya akan menerima pesan masuk untuk izin melakukan root. Untuk <a href = "../../faq">izin root</a>`
            },
            {
                title : `Bisakah saya memantau pesan yang dihapus atau file multimedia pada perangkat yang dipantau?`,
                content : `Ya, SPYHP memberikan Anda akses kepada semua pesan pada dashboard SPYHP Anda sehingga Anda dapat mengunduhnya melalui perangkat pribadi Anda.`
            }
        ]
    },
    {
        id : 18,
        title : `Pelacak KIK`,
        content : [
            `Mulai Bulan Mei 2016, KiKMessanger sudah memiliki 300 juta member dan sudah digunakan oleh 40% remaja Amerika Serikat. SPYHP dapat melihat semua pesan yang dikirim dan diterima oleh ponsel yang dituju. Dengan fitur tracking yang disediakan oleh SPYHP KIK, Anda memiliki akses penuh untuk mengontrol aktivitas KIK anak Anda.`,
            `Dengan lebih dari 200 juta pengguna, KiK sudah menarik perhatian bagi anak anak. Apakah Anda takut  anakmu menghabiskan waktu terlalu lama menggunakan KiK Messenger? Apakah Anda mau mencari tau dengan siapa dan pesan apa saja yang dikirim anak mu? Tidak usah khawatir karena SPYHP dapat memastikan bahwa anak Anda tidak mengirim pesan kepada orang asing. Kamu dapat membaca pesan KiK milik anakmu dari manapun dengan menggunakan SPYHP Dashboard.`
        ],
        image : [
            `Kik_Tracker/kik_tracker1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-comments`,
                text : `Membaca semua percakapan KiK`
            },
            {
                image : `fas fa-user`,
                text : `Melihat detail dari pengirim pesan`
            },
            {
                image : `far fa-clock`,
                text : `Mendapat waktu percakapan berlangsung`
            },
            {
                image : `fas fa-desktop`,
                text : `Semua percakapan Kik akan diunggah ke dalam Panel Dashboard Anda`
            }
        ],
        faq : [
            {
                title : `Bagaimana cara mengawasi pesan KiK?`,
                content : `Kamu dapat mengawasi pesan KiK melalui SPYHP web control panel. Lalu Login SPYHP Control Panel milik Anda`
            },
            {
                title : `Apakah saya bisa  mengawasi pesan KiK dengan perangkat yang tidak di-root?`,
                content : `Tidak bisa. Untuk mengawasi pesan KiK, harus menggunakan perangkat yang sudah di-root.`
            }
        ]
    },
    {
        id : 19,
        title : `Pemantauan Pesan Tinder`,
        content : [
            `Tinder sudah digunakan oleh masyarakat dunia dan tersedia lebih dari 40 bahasa. Sejak tahun 2014, sekitar 50 juta orang menggunakan Tinder setiap bulannya dan sekitar 12 juta matches per hari nya. Percakapan pada Tinder hanya tersedia antara dua pengguna yang sudah melakukan “swipe ke kanan” pada foto yang ditampilkan. Tinder menjadi aplikasi layanan online dating pertama yang sudah menjadi salah satu dari “ 5 layanan website terbaik” selama  10 tahun.`,
            `Orang tua seharusnya <a href = "">memblokir aplikasi ini,</a>  dan menggunakan cara lain untuk mengambil langkah dengan hati-hati saat membuat keputusan jika anak mereka menggunakan aplikasi ini, mengingat resiko yang ada. Orang tua dapat mencoba meminta anak mereka untuk bertanggung jawab atas aktivitasnya dengan bertanya seputar aplikasi yang anak mereka gunakan setiap hari. Cari nama aplikasi yang sudah ter-install pada ponsel/tablet mereka. Tanyakan pertanyaan mengenai bagaimana mereka menggunakannya. Dan cari tau teman mereka serta topik apa yang sering mereka diskusikan pada aplikasi tersebut.`,
            `<ul>
            <li>Melihat semua pesan Tinder yang diterima oleh ponsel yang dituju</li>
            <li>Melihat siapa yang akan dikirimkan pesan  pada aplikasi Tinder</li>
            <li>Setiap pesan singkat dari Percakapan Tinder akan otomatis diunggah ke dalam portal online SPYHP</li>
            <li>Pemantauan pesan Tinder tidak akan terdeteksi</li>
            </ul>`
        ],
        image : [
            `Monitor_Tinder_Chats/monitor_tinder_chats1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-ﬁre-alt`,
                text : `Mengawasi semua pesan Tinder.`
            },
            {
                image : `fas fa-info-circle`,
                text : `Melihat nama dan nomor dari pengirim pesan.`
            },
            {
                image : `fas fa-search`,
                text : `Mencari tahu nama orang yang pernah melakukan percakapan.`
            },
            {
                image : `far fa-clock`,
                text : `Melihat waktu yang tertera dalam aplikasi.`
            },
            {
                image : `fas fa-desktop`,
                text : `Mengakses informasi melalui SPYHP Dashboard.`
            }
        ],
        faq : [
            {
                title : `Bagaimana cara memantau pesan pada Tinder melalui SPYHP?`,
                content : `Dengan aplikasi SPYHP Tinder Spy, semua pesan pada Tinder akan diunggah ke dalam SPYHP Control Panel Anda`
            },
            {
                title : `Apakah saya dapat memantau pesan Tinder melalui perangkat yang tidak di-root?`,
                content : `Ya, Anda dapat melakukan pengawasan pada semua pesan Tinder tanpa melakukan root terhadap perangkat yang digunakan`
            },
            {
                title : `Apakah saya dapat memantau pesan/file  yang sudah dihapus dengan perangkat yang saya gunakan untuk memantau?`,
                content : `Ya. SPYHP juga menyediakan layanan untuk mengawasi semua katalog Tinder dengan SPYHP Dashboard`
            }
        ]
    },
    {
        id : 20,
        title : `Pemantauan Telegram`,
        content : [
            `Telegram telah mengumumkan bahwa mereka memiliki 50 juta pengguna aktif, 1 miliar pesan setiap harinya dan memilih 1 juta pengguna baru setiap minggunya. Pada bulan Desember 2017, Telegram telah mencapai 180 juta pengguna aktif. Telegram memiliki tampilan yang mirip dengan WhatsApp serta menyajikan pesan rahasia dan proses enkripsi. Oleh karena itu, banyak orang yang lebih memilih Telegram sebagai aplikasi komunikasi karena keamanan data penggunanya.`,
            `Aplikasi Telegram IM mampu membantu dalam akses pesan yang bervariasi, seperti pesan tulisan, suara, video, gambar, dan emoticon. SPYHP tidak hanya dapat memantau dengan mendapat pesan Telegram, tetapi juga dapat melakukan pemblokiran terhadap sesuatu yang Anda tidak ingin anak Anda lihat. Mengingat konten dewasa, pornografi, dan konten tidak pantas lainnya dapat ditemukan.`,
            `Kamu dapat mulai mengaktifkan perlindungan dan pemantauan mulai dari sekarang. Cukup dengan login di Official Website  SPYHP dan mulai menjalani prosedur penggunaannya, seperti <a href = "../../price">pembelian, instalasi,</a> dan mulai pengawasan.`
        ],
        image : [
            `Monitor_Telegram_Messenger/monitor_telegram_messenger1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fab fa-telegram`,
                text : `Mengawasi pesan Telegram yang akan masuk.`
            },
            {
                image : `fas fa-search`,
                text : `Mencari tahu nama orang yang pernah melakukan percakapan.`
            },
            {
                image : `far fa-clock`,
                text : `Melihat waktu yang tertera dalam aplikasi.`
            },
            {
                image : `fas fa-desktop`,
                text : `Mengakses informasi melalui SPYHP Dashboard.`
            }
        ],
        faq : [
            {
                title : `Bagaimana cara untuk memantau telegram dengan perangkat android?`,
                content : `Diperlukan satu kali instalasi pada ponsel android yang ingin dipantau. Setelah itu, dapat mulai login.`
            },
            {
                title : `Apakah saya dapat memantau pesan/file  yang sudah dihapus dengan perangkat yang saya gunakan untuk memantau?`,
                content : `Ya. SPYHP juga menyediakan layanan untuk mengawasi semua katalog Tinder dengan SPYHP Dashboard.`
            },
            {
                title : `Apakah saya bisa  mengawasi pesan KiK dengan perangkat yang tidak di-root?`,
                content : `Tidak bisa. Kamu hanya dapat memantau pesan yang akan datang, jika perangkat Anda belum dilakukan rooting.`
            }
        ]
    },
    {
        id : 21,
        title : `Pemantauan Kakao Talk`,
        content : [
            `Pada Mei 2017, KakaoTalk memiliki 220 juta pengguna terdaftar dan 49 juta pengguna aktif bulanan. Aplikasi KakaoTalk tersedia dalam 15 bahasa. Aplikasi ini juga digunakan oleh 93% pengguna smartphone di Korea Selatan, yang merupakan aplikasi pengiriman pesan nomor satu. KakaoTalk juga digunakan sebagai alat jejaring sosial, sebagai sarana untuk bertemu orang baru dan mengobrol. KakaoTalk memiliki fitur yang memungkinkan Anda untuk mencari orang dengan menggunakan nama mereka, nomor mereka dan akun email mereka. Anak-anak dan remaja kami menghabiskan sebagian besar waktu mereka online dan mengobrol dengan teman-teman mereka.`,
            `Fitur KakaoTalk yang disebut Plus Friend memungkinkan pengguna untuk mengikuti selebriti dan brand favorit mereka, dan juga menyediakan informasi real-time kepada pengguna melalui ruang obrolan atau chat room aplikasi KakaoTalk, termasuk pesan dan kupon eksklusif untuk berbagai produk. Anda dapat menggunakan fitur blok pada SPYHP untuk <a href = "../features/36">memblokir aplikasi</a> agar anak Anda terhindar dari aplikasi social media seperti ini.`,
            `KakaoTalk semakin populer di kalangan remaja dan dewasa muda karena fitur-fiturnya yang cepat dan menarik. Anda dapat dengan mudah menemukan bahwa remaja menggunakan KakaoTalk mengobrol dengan teman di mana pun, bahkan di kelas. Dengan banyaknya stiker menarik, KakaoTalk semakin digemari banyak remaja. SPYHP akan membantu Anda memantau semua aktivitas anak Anda di Kakao Talk di ponsel android mereka.`
        ],
        image : [
            `Monitor_Kakao_Talk/monitor_kakao_talk1`
        ],
        step : [
            {
                title : `Lihat semua pesan Kakao Talk termasuk pesan grup, waktu dan tanggal percakapan.`,
                content : [
                ]
            },
            {
                title : `Anda dapat menggunakan SPYHP untuk memblokir Kakao Talk sehingga anak-anak Anda dapat lebih memperhatikan pelajaran mereka.`,
                content : [
                ]
            },
            {
                title : `Lindungi anak-anak Anda dari pemangsa seksual, pengganggu dunia maya dan perilaku berisiko.`,
                content : [
                ]
            }
        ],
        'facility' : [
            {
                image : `far fa-comment-dots`,
                text : `Melacak semua pesan di dalam aplikasi KakaoTalk`
            },
            {
                image : `fas fa-users`,
                text : `Melihat detail Group Chats`
            },
            {
                image : `fas fa-search`,
                text : `Mencari tahu nama orang yang pernah mereka ajak chatting`
            },
            {
                image : `far fa-clock`,
                text : `Melihat waktu dan tanggal`
            },
            {
                image : `fas fa-desktop`,
                text : `Akses informasi langsung dari dashboard SPYHP Anda`
            }
        ],
        faq : [
            {
                title : `Bagaimana cara memata-matai Kakao Talk pada ponsel android?`,
                content : `Untuk itu diperlukan satu kali instalasi di ponsel Android yang ingin Anda pantau. Setelah itu masuk ke akun web SPYHP Anda dan Buka halaman "kakao" di sini Anda dapat memonitor semua pesan masuk dan pesan keluar yang diterima oleh Ponsel target.`
            },
            {
                title : `Bisakah saya memata-matai pesan di aplikasi Kakao pada ponsel yang tidak di-root?`,
                content : `Ya, Anda dapat memantau semua pesan masuk dan pesan keluar di perangkat tidak di-root. Untuk itu Anda perlu mengaktifkan aksesibilitas target perangkat. Aktifkan aksesibilitas dari Pengaturan Perangkat >> Aksesibilitas >> Layanan WiFi >> Diaktifkan.`
            },
            {
                title : `Dapatkah saya memonitor pesan yang telah dihapus pada perangkat yang dipantau?`,
                content : `Ya, TiPSY memberi Anda semua log Kakao Talk yang dipantau ke Dasbor TiPSY Anda, Anda juga dapat mengunduhnya di komputer pribadi Anda.`
            }
        ]
    },
    {
        id : 22,
        title : `Pemantauan Hike`,
        content : [
            `Dengan SPYHP, Anda dapat mengecek secara remote semua pesan yang dikirm dan diterima dalam HIKE. Dapat melakukan pengecekan nama dan nomor dari pengirim pesan, termasuk detail lainnya seperti waktu pengiriman.`,
            `Setiap anak menghabiskan waktu dengan ponsel mereka, dan itu tidak hanya menelepon. Banyak orang yang menggunakan aplikasi IM untuk melakukan komunikasi dengan teman, keluarga, atau bahkan seseorang yang tidak mereka kenal. Salah satu contoh aplikasi IM adalah HIKE Messenger. Sekarang SPYHP dapat menangkap semua detail percakapan HIKE, seperti pesan dan grup percakapan, sehingga Anda tidak terlewat apapun dalam memantau.`
        ],
        image : [
            `Hike_Tracker/hike_tracker1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-comments`,
                text : `Melihat semua percakapan pada aplikasi HIKE.`
            },
            {
                image : `fas fa-users`,
                text : `Melihat percakapan pribadi dan grup.`
            },
            {
                image : `fas fa-search`,
                text : `Mencari tahu nama orang yang pernah melakukan percakapan.`
            },
            {
                image : `far fa-clock`,
                text : `Melihat waktu yang tertera dalam aplikasi, sehingga dapat mengetahui waktu percakapan.`
            },
            {
                image : `fas fa-desktop`,
                text : `Semua pesan HIKE akan diunggah ke dalam panel SPYHP Dashboard`
            }
        ],
        faq : [
            {
                title : `Apakah SPYHP menangkap pesan HIKE?`,
                content : `Dengan SPYHP, Anda dapat mengecek secara remot semua pesan yang dikirim dan diterima yang terdapat di HIKE. Dapat mengecek nama dan nomor.`
            },
            {
                title : `Bagaimana cara mendapatkan fitur ini?`,
                content : `Penangkapan HIKE tersedia perangkat android yang telah dilakukan rooting dan dapat menjalankan SPYHP Premium atau SPYHP Extreme.`
            },
            {
                title : `Bagaimana fitur ini bekerja?`,
                content : `Cukup dengan menginstal SPYHP pada android yang telah di-root, selama yang dipantau adalah HIKE Messenger dengan versi yang terjangkau oleh SPYHP.`
            }
        ]
    },
    {
        id : 23,
        title : `Pemantauan HANGOUTS`,
        content : [
            `Google Hangouts adalah sebuah platform komunikasi yang dikembangkan oleh Google yang melayani fitur seperti perpesanan instan, percakapan video, panggilan suara, dan SMS. Menjadi Instant Messaging yang hebat itu dapat membuat Anda selalu aktif secara sosial. Sekarang SPYHP dapat menangkap semua percakapan yang terjadi di aplikasi Hangouts di perangkat target.`,
            `Hangouts sudah tersedia sebelumnya pada ponsel Android. Menjadi platform komunikasi yang populer, Google Hangouts mencakup perpesanan instan, obrolan video, fitur SMS dan VOIP. Semakin banyak orang menggunakan aplikasi ini untuk tetap berhubungan dengan teman & orang lain. SPYHP memonitor percakapan individu dan kelompok, multimedia, kontak di aplikasi Hangouts. Dengan SPYHP Anda dapat melihat semua percakapan Hangouts yang dilakukan melalui ponsel target langsung dari panel dashboard Anda.`
        ],
        image : [
            `Hangouts_Tracker/hangouts_tracker1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-comments`,
                text : `Melihat semua percakapan obrolan di aplikasi Hangouts.`
            },
            {
                image : `fas fa-search`,
                text : `Mencari tahu nama orang yang pernah mereka ajak chatting`
            },
            {
                image : `far fa-clock`,
                text : `Mendapat detail waktu dan tanggal untuk mengetahui kapan setiap obrolan berlangsung`
            },
            {
                image : `fas fa-desktop`,
                text : `Semua obrolan Hangouts langsung ditampilkan ke panel dashboard SPYHP Anda.`
            }
        ],
        faq : [
            {
                title : `Apakah SPYHP berfungsi pada aplikasi Hangout?`,
                content : `SPYHP dapat menangkap semua percakapan yang terjadi di aplikasi Hangouts di perangkat target. Anda dapat melihat semua percakapan obrolan Hangouts dan semua obrolan Hangouts secara langsung ditampilkan ke panel dashboard SPYHP dan Anda juga dapat mencari tahu nama orang yang telah mereka ajak ngobrol.`
            },
            {
                title : `Bagaimana cara menangkap informasi pada aplikasi hangout menggunakan SPYHP?`,
                content : `SPYHP memonitor percakapan individu dan grup, multimedia, kontak di Hangouts. Dengan SPYHP, Anda dapat melihat semua percakapan Hangouts yang dilakukan melalui ponsel target langsung dari panel dasbor Anda dan juga dapat melihat detail waktu dan tanggal untuk mengetahui kapan setiap obrolan berlangsung.`
            }
        ]
    },
    {
        id : 24,
        title : `Pemantauan Riwayat Web`,
        content : [
            `Anda perlu tahu apakah situs web yang dikunjungi anak Anda aman atau tidak karena sebagian besar anak-anak melihat konten pornografi secara online setidaknya dua kali seminggu. SPYHP membantu Anda melacak semua riwayat web dari browser web ponsel target Anda.`,
            `Apakah Anda tahu bahwa internet penuh dengan hal-hal baik dan buruk. Ada berbagai jenis konten dan video berbahaya yang tersedia di internet. Tentunya Anda ingin anak Anda terhindar dari jenis konten seperti itu bukan? Jika iya, maka Anda harus mengawasi situs web apa yang mereka kunjungi. Dengan SPYHP, Anda dapat memeriksa riwayat penelusuran web dari ponsel mereka dari jarak jauh. TiPSY akan memastikan Anda mengenai keselamatan anak Anda.`
        ],
        image : [
            `Web_History_Tracker/web_history_tracker1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-history`,
                text : `Lihat semua situs web yang dikunjungi dengan URL spesifik mereka.`
            },
            {
                image : `fas fa-desktop`,
                text : `Analisis semua data riwayat penelusuran langsung dari Panel dashboard Anda.`
            },
            {
                image : `far fa-clock`,
                text : `Mendapat detail waktu dan tanggal penelusuran.`
            },
            {
                image : `fas fa-lock`,
                text : `Jelajahi riwayat web target Anda tanpa harus melakukan jailbreak atau me-root perangkat target Anda.`
            }
        ],
        faq : [
            {
                title : `Bagaimana cara melihat riwayat Web dengan SPYHP?`,
                content : [`Ketahui situs web mana yang diakses sepanjang hari. Menggunakan capture Web history dapat mengetahui situs mana yang diakses oleh anak Anda Dengan riwayat browser tampilan SPYHP:`,
                `<ol>
                <li>Lihat situs web yang dikunjungi target Anda.</li>
                <li>Lihat setiap URL yang telah dikunjungi.</li>
                <li>Cari tahu berapa kali target Anda mengunjungi setiap situs web.</li>
                <li>Dapatkan waktu dan tanggal penelusuran sehingga Anda dapat mengetahui kapan setiap situs web dikunjungi.</li>
                </ol>`]

            },
            {
                title : `Bisakah saya mengelola situs web yang aman?`,
                content : `Pada dashboard SPYHP, ada bagian di mana Anda dapat mengonfigurasi situs web yang aman. Jika Anda telah mendaftarkan situs yang disetujui, hanya situs-situs itu yang akan dapat diakses. Semua situs web lain akan diblokir.`
            },
            {
                title : `Bisakah saya membatasi situs web?`,
                content : `Jika Anda ingin memblokir situs porno / dewasa, Anda dapat memblokirnya dengan meletakkannya di daftar situs yang diblokir.`
            },
            {
                title : `Mengapa saya tidak dapat melihat riwayat situs web yang diakses?`,
                content : `Pastikan Anda telah melakukan konfigurasi di bawah ini dengan benar. Untuk melihat riwayat web, aktifkan layanan aksesibilitas dari Pengaturan perangkat Anda -> Aksesibilitas -> Layanan WiFi. Jika yang Anda maksud tentang pemblokiran situs, Anda harus memiliki tautan ke halaman dasbor demo`
            },
            {
                title : `Apakah fitur ini berfungsi tanpa root?`,
                content : `Ya, Semua fitur (riwayat situs web, akses ke situs web, dan memblokir situs web) akan berfungsi dengan baik. Anda tidak perlu melakukan root pada ponsel. Mereka bekerja dengan baik tanpa root.`
            }
        ]
    },
    {
        id : 25,
        title : `Lihat Waktu Situs Web yang Dikunjungi`,
        content : [
            `SPYHP memungkinkan Anda untuk memantau dan melihat tanggal dan waktu dari setiap situs web yang dikunjungi dan seberapa sering dikunjungi di browser web perangkat yang dipakai.`,
            `Tahukah Anda, sekitar 35% anak-anak mengatakan bahwa orang tua mereka tidak tahu apa-apa tentang apa yang mereka lakukan di internet. Apakah Anda ingin menjadi orang tua seperti ini? Tentunya tidak, maka dari itu, pilih SPYHP,  karena SPYHP memungkinkan Anda untuk melihat semua aktivitas penelusuran web anak Anda dengan detail waktu & tanggal terkait.`,
            `Menyediakan fasilitas akses satu kali klik ke setiap situs yang dikunjungi langsung dari dashboard SPYHP Anda dan SPYHP memberikan detail tentang situs web yang paling sering anak Anda kunjungi.`
        ],
        image : [
        ],
        step : [
        ],
        'facility' : [
            {
                image : `far fa-clock`,
                text : `Lihat waktu dan tanggal untuk setiap situs web.`
            },
            {
                image : `fas fa-list-ol`,
                text : `Lihat berapa kali situs web telah dikunjungi.`
            },
            {
                image : `fas fa-desktop`,
                text : `Semua riwayat penelusuran web langsung diunggah ke panel dashboard SPYHP Anda.`
            }
        ],
        faq : [
            {
                title : `Bagaimana mengetahui waktu melihat Situs Web yang dikunjungi?`,
                content : `SPYHP memungkinkan Anda untuk memantau dan melihat cap tanggal dan waktu dari setiap situs web yang dikunjungi dan seberapa sering dikunjungi di browser web perangkat target. Memberikan fasilitas akses satu kali klik ke setiap situs yang dikunjungi langsung dari dashboard SPYHP Anda & memberi Anda detail tentang anak Anda situs web yang paling banyak dilihat.`
            },
            {
                title : `Bagaimana saya mendapat notifikasi peringatan email harian?`,
                content : `SPYHP membuat Anda terus mengetahui tentang aktivitas anak Anda 24/7 dengan ringkasan aktivitas harian anak Anda. Dengan SPYHP, Anda akan mendapatkan ringkasan harian dari pemantauan hari terakhir yang akan dikirim langsung ke email Anda. Sehingga tidak akan ada satu detail pun informasi yang akan Anda lewatkan.`
            }
        ]
    },
    {
        id : 2,
        title : `Lihat waktu dan tanggal telepon`,
        content : [
            `Fitur SPYHP Call log memberi Anda tanggal & waktu untuk semua panggilan masuk & keluar dengan durasi waktu mereka untuk mengetahui berapa lama anak Anda menghubungi pada setiap panggilan.`,
            `Apakah Anda khawatir karena anak Anda terlalu banyak menghabiskan waktu untuk menelepon. Apakah Anda ingin tahu berapa lama anak Anda berbicara dengan setiap orang? SPYHP memberi Anda rincian lengkap seperti nomor dan nama setiap panggilan, durasi panggilan untuk panggilan tersebut dan bahkan waktu dan tanggal panggilan dan Anda dapat mengecek kontak dari log panggilan dengan nama kontak messenger sosial, kontak Gmail & lainnya.`
        ],
        image : [
        ],
        step : [
        ],
        'facility' : [
            {
                image : `far fa-clock`,
                text : `Lihat kapan dan berapa lama waktu panggilan.`
            },
            {
                image : `far fa-calendar-alt`,
                text : `Dapatkan waktu dan tanggal dari setiap panggilan dengan nama kontak dan nomor.`
            },
            {
                image : `fas fa-download`,
                text : `Unduh riwayat panggilan dengan waktu dan durasi sebagai file html.`
            }
        ],
        faq : [
            {
                title : `Bagaimana cara mengetahui tanggal dan waktu panggilan?`,
                content : `SPYHP Call log feature gives you date & time for all incoming & outgoing calls with their time duration to know how long your child contacted on each call.`
            }
        ]
    },
    {
        id : 3,
        title : `Lihat pesan SMS`,
        content : [
            `Ingin tahu apa yang dibicarakan anak-anak Anda dan kapan mereka mengobrol di ponsel atau tablet mereka? Apakah Anda takut anak Anda bisa mengirim SMS dengan orang asing? SPYHP memungkinkan Anda membaca semua pesan teks yang mereka kirim atau terima di ponsel mereka tanpa menyentuh ponsel mereka.`,
            `Apakah Anda tahu bahwa anak Anda mengirim atau menerima pesan teks porno di ponsel mereka? Apakah Anda khawatir tentang kebiasaan SMS mereka? Dengan pelacak sms SPYHP, Anda dapat memeriksa semua pesan teks yang terkirim dan diterima dari jarak jauh pada ponsel anak Anda dan orang yang Anda sayangi walaupun sms ini mungkin telah dihapus dari perangkat.`,
            `Aplikasi, ketika diinstal, mengambil cadangan riwayat SMS dari perangkat. Langsung mengunggah data ke akun pengguna Anda, untuk Anda pantau.`,
            `Jika anak-anak Anda menghabiskan terlalu banyak waktu untuk panggilan telepon. Lalu, Anda mungkin ingin tahu dengan siapa mereka mengobrol, bukan? Kemudian, fitur pelacak SMS SPYHP dapat banyak membantu Anda.
            `,
            `Tidak diperlukan root untuk mengakses aplikasi kami sehingga, garansi ponsel Anda akan beraksi.`
        ],
        image : [
            `View_Sms_Messages/view_sms_messages1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-envelope`,
                text : `Lihat pesan terkirim dan diterima.`
            },
            {
                image : `fas fa-file-alt`,
                text : `Baca konten lengkap dari semua pesan.`
            },
            {
                image : `far fa-clock`,
                text : `Dapatkan waktu dan tanggal semua SMS.`
            },
            {
                image : `fas fa-desktop`,
                text : `Semua pesan SMS diunggah ke panel dasbor SPYHP Anda dan Anda dapat mengaksesnya dari mana saja.`
            }
        ],
        faq : [
            {
                title : `Kenapa SPYHP melihat feature ini berguna`,
                content : `Terkadang mengetahui apa pesan yang anak Anda dapat mencegah masalah nyata. Anda dapat Melihat pesan yang dikirim dan diterima dan Baca konten lengkap dari semua pesan juga tahu Dapatkan waktu dan tanggal semua SMS. Semua pesan teks disusun dengan rapi ke dalam Panel Kontrol SPYHP Anda dan Anda dapat mengakses mereka kapan saja.`
            },
            {
                title : `Bisakah saya mengakses pesan-pesan yang terkirim dan terima dari perangkat yang dimonitor?`,
                content : `Ya, SPYHP dapat memonitor kedua sisi percakapan teks pada perangkat yang dipantau. Oleh karena itu, Anda dapat membaca semua pesan teks yang dikirim atau terima dari ponsel orang yang dipantau.`
            },
            {
                title : `Bisakah saya mengakses text yang dihapus dari pembicaraan?`,
                content : `Ya, SPYHP memungkinkan Anda untuk mengakses bahkan percakapan SMS yang dihapus saat aplikasi segera mengunggah semuanya ke portal online-nya.`
            }
        ]
    },
    {
        id : 4,
        title : `Lihat pesan MMS`,
        content : [
            `Dengan SPYHP, Anda dapat memeriksa semua konten MMS dan multimedia dari jarak jauh. Secara instan memeriksa nama dan jumlah pengirim dan perincian lainnya seperti cap waktu dan tanggal dari setiap MMS yang dikirim dan diterima pada perangkat target.`
            ,`Bagi mereka yang perlu memonitor pesan MMS. Pelacak MMS SPYHP memberi Anda konten lengkap termasuk audio, foto & video setiap MMS dari perangkat target. Apakah Anda tahu jenis media yang dibagikan anak Anda dalam MMS dengan teman? SPYHP memahami semua kekhawatiran Anda dan memberi Anda salinan lengkap pesan MMS mereka termasuk media langsung ke panel dasbor SPYHP Anda.`
        ],
        image : [

        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-envelope`,
                text : `Lihat pesan MMS terkirim dan diterima.`
            },
            {
                image : `far fa-image`,
                text : `Lihat foto dalam pesan MMS.`
            },
            {
                image : `fas fa-video`,
                text : `Lihat video dalam pesan MMS.`
            },
            {
                image : `fas fa-volume-up`,
                text : `Dapatkan audio dalam pesan MMS.`
            },
            {
                image : `far fa-clock`,
                text : `Dapatkan waktu dan tanggal dari semua MMS.`
            },
            {
                image : `fas fa-download`,
                text : `Unduh konten media dari setiap MMS langsung dari panel dasbor SPYHP Anda.`
            }
        ],
        faq : [
            {
                title : `Sudahkah kamu aktifkan fitur MMS pada dasbor SPYHP`,
                content :  `Buka pengaturan, aktifkan 'MMS' dari panel 'Fitur Individual Aktif / Nonaktif'.`
            },
            {
                title : `Apakah auto download sudah di check di phone monitoring?`,
                content : [`MMS hanya akan tersedia setelah diunduh.`,`MMS hanya akan tersedia setelah diunduh.`]
            },
            {
                title : `Kenapa Anda perlu mengintai pesan MMS mereka?`,
                content : `Bagi mereka yang tidak sadar pesan MMS adalah pesan SMS standar yang memiliki media di dalamnya seperti gambar atau video. SPYHP dan fitur memata-matai pesan MMS-nya memberi Anda gambar lengkap ketika datang untuk membaca pesan MMS mereka. Tidak seperti produk pesaing yang mungkin senang hanya dengan nama kontak dan nomor telepon MMS atau yang bahkan tidak menawarkan kemampuan untuk memata-matai pesan MMS mereka, SPYHP memahami nilai informasi dan memberi Anda duplikat lengkap dari pesan MMS mereka termasuk yang paling bagian penting - media.`
            }
        ]
    },
    {
        id : 5,
        title : `Melacak detail pengirim`,
        content : [
            `Dengan SPYHP, Anda dapat memeriksa semua pesan teks yang dikirim dan diterima dari jarak jauh pada perangkat anak-anak Anda dan secara instan memeriksa nama dan nomor pengirim dan detail lainnya seperti waktu dan tanggal.`,
            `Apakah Anda khawatir dengan siapa anak-anak Anda akan mengirim SMS hingga tengah malam? Apakah Anda ingin mendapatkan rincian kontak dari semua percakapan mereka di sms dan mms? SPYHP membantu Anda untuk melihat nama kontak dari pesan apa pun, nomor yang terkait dengan percakapan apa pun. Jangan lewatkan lagi Lihat kontak rahasia & nomor dari setiap pesan dengan sangat mudah.`
        ],
        image : [

        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-th`,
                text : `Lihat pesan MMS terkirim dan diterima.`
            },
            {
                image : `fas fa-user-secret`,
                text : `Dapatkan detail tentang kontak & nomor rahasia dalam pesan apa pun.`
            },
            {
                image : `fas fa-upload`,
                text : `Semua data diunggah langsung ke panel dasbor SPYHP Anda.`
            }

        ],
        faq : [
            {
                title : `Apakah SPYHP melacak detail pengirim pesan?`,
                content :  `Iya menggunakan SPYHP, Anda dapat memeriksa semua pesan teks yang dikirim dan diterima dari jarak jauh pada perangkat anak-anak Anda dan secara instan memeriksa nama dan nomor pengirim dan detail lainnya seperti waktu dan tanggal juga Anda mendapatkan rincian tentang kontak & nomor rahasia dalam pesan apa pun.`
            }
        ]
    },
    {
        id : 6,
        title : `Dengan SPYHP, Anda dapat memonitor jarak jauh lokasi anak-anak dan orang-orang yang Anda cintai secara langsung dari dasbor SPYHP Anda menggunakan peta dasbor bawaan.`,
        content : [
            `Dengan SPYHP, Anda dapat memeriksa semua pesan teks yang dikirim dan diterima dari jarak jauh pada perangkat anak-anak Anda dan secara instan memeriksa nama dan nomor pengirim dan detail lainnya seperti waktu dan tanggal.`,
            `Apakah anak-anak Anda terus berbohong kepada Anda tentang ke mana mereka pergi? Apakah Anda ingin dapat menonton mereka dengan detail di mana mereka berada dan kapan? SPYHP memungkinkan Anda melacak lokasi telepon mereka secara instan. Fitur Lokasi memungkinkan Anda untuk melihat di mana mereka berada dengan peta bawaan di dasbor Anda. Mengawasi anak-anak Anda dengan SPYHP adalah cara terbaik untuk melindungi mereka agar memberikan kehidupan yang bahagia dan aman.`,
            `Ini adalah fitur yang sangat berguna yang dibutuhkan setiap orang tua! Bergantung hanya pada koneksi GPS tidak selalu cukup. Sebagian besar pengguna mematikan GPS & paket data mereka. Untuk menangani situasi ini di mana GPS tidak dapat memberikan hasil pelacakan Lokasi, SPYHP memperkenalkan pelacak lokasi tanpa GPS. Tidak lagi sulit bagi orang tua untuk melacak keberadaan anak-anak mereka meskipun tidak ada GPS.`
        ],
        image : [

        ],
        step : [
        ],
        'facility' : [
            {
                image : `far fa-map`,
                text : `Monitor lokasi sekarang pada peta`
            },
            {
                image : `fas fa-map-marker-alt`,
                text : `Identifikasi lokasi mereka walapun dengan GPS biasa tidak diaktifkan.`
            },
            {
                image : `fas fa-desktop`,
                text : `Akses semua informasi lokasi secara langsung dari dasbor SPYHP.`
            }

        ],
        faq : [
            {
                title : `Bagaimana cara melihat lokasi sekarang`,
                content :  `Anda dapat memonitor jarak jauh lokasi saat ini dari anak-anak dan orang-orang terkasih Anda langsung dari dasbor SPYHP Anda`
            },
            {
                title : `Kenapa saya banyak sekali mendapat data lokasi`,
                content :  `Tingkatkan 'Interval Jarak GPS' menjadi 500 meter atau lebih.`
            },
            {
                title : `Bagaimana saya akan mendapat lokasi instan dengan menggunakan perintah SMS`,
                content : `Pertama gunakan perintah SMS untuk Lokasi instan maka tautan Lokasi akan dikirim kembali pada nomor yang sama.`
            },
            {
                title : `Berapa panjang lokasi pendeteksi SPYHP`,
                content : [`SPYHP Teknik pencarian lokasi jauh lebih baik, Kami memiliki tiga cara berbeda di bawah ini untuk mendapatkan lokasi kerjanya bahkan perangkat selalu menjauhkan GPS dan internet.`,`Kami mendukung di bawah tiga cara untuk menemukan lokasi perangkat.`,`menggunakan GPS (sangat akurat sekitar 50 meter, tetapi mungkin tidak berfungsi di perangkat di dalam gedung)
                menggunakan perangkat internet (akurat sekitar 100 Meter, membutuhkan ketersediaan internet)
                menggunakan informasi menara seluler perangkat. (bekerja tanpa internet, GPS, tetapi akurasi rendah sekitar 1-5 KM)
                Untuk mendapatkan hasil yang akurat kami sarankan untuk mengaktifkan "Pengaturan >> Lokasi dan keamanan >> Gunakan jaringan nirkabel".
                Coba "Perintah SMS" kami untuk mendapatkan lokasi kembali di ponsel Anda.`]
            },
            {
                title : `Kenapa GPS menunjukan lokasi tidak akurat. Bagaimana cara memperbaikinya?`,
                content : [`Teknik pencarian lokasi kami jauh lebih baik, memiliki tiga cara berbeda di bawah ini untuk mendapatkan lokasi kerjanya bahkan perangkat selalu mematikan GPS dan internet. Kami mendukung di bawah tiga cara untuk menemukan lokasi perangkat.`,`
                menggunakan GPS (sangat akurat sekitar 50 meter, tetapi mungkin tidak berfungsi di perangkat di dalam gedung)`, `menggunakan perangkat internet (akurat sekitar 100 Meter, membutuhkan ketersediaan internet)`, `menggunakan informasi menara seluler perangkat. (bekerja tanpa internet, GPS, tetapi akurasi rendah sekitar 1-5 KM)`,`Untuk mendapatkan hasil yang akurat kami sarankan untuk mengaktifkan "Pengaturan >> Lokasi dan keamanan >> Gunakan jaringan nirkabel".`]
            }
        ]
    },
    {
        id : 35,
        title : `Pantau Rekaman Panggilan VoIP`,
        content : [
            `<a href="https://raisingteenstoday.com/teens-addicted-to-their-phones/">Pembelian, Hampir 80% anak muda</a>dalam survei baru mengatakan mereka memeriksa ponsel mereka setiap jam, dan 72% mengatakan mereka merasa perlu untuk segera menanggapi teks dan pesan jejaring sosial. Apakah Anda tahu siapa yang mereka ajak bicara atau apa yang mereka bicarakan tentang? sangat tidak mungkin bagi orang tua untuk menonton sepanjang waktu. Mereka juga tidak harus - karena kepercayaan adalah bagian yang sangat penting dari perkembangan anak. SPYHP memberi Anda fasilitas untuk mengetahui apakah sesuatu terjadi sebelum terlalu jauh dan memungkinkan Anda melangkah untuk memperbaiki situasi yang berpotensi berbahaya dan mahal untuk anak Anda.`,
            ,`Sekarang Anda dapat Memantau Rekaman Panggilan VOIP dengan <a href="../../install-guide/Android">versi terbaru dari SPYHP</a>versi terbaru SPYHP. Instal versi terbaru SPYHP di Ponsel Anda yang ingin Anda pantau dan pantau semua Whatsapp, Facebook, Rekaman Panggilan Viber dari Jarak Jauh di Panel Kontrol SPYHP Anda. Fitur ini sangat cocok untuk siapa saja yang menggunakan Android dan menghabiskan banyak waktu di Whatsapp, Facebook, aplikasi Viber. Merekam panggilan-panggilan ini sangat berguna bagi orang tua yang bertanggung jawab untuk mereka anak-anak. Mereka pada dasarnya sama dengan panggilan telepon biasa. Panggilan hanya dialihkan melalui internet alih-alih menggunakan program instant messenger.`,
            `
                <ul>
                    <li>Merekeam panggilan secara diam-diam tanpa pengguna tahu.</li>
                    <li>Mendapatkan nomor atau durasi panggilan apa pun.</li>
                    <li>Mendapatkan informasi secara langsung dari unggahan Panel kontrol pengguna.</li>
                    <li>Mendukung sejumlah perangkat android.</li>
                </ul>
            `,
            `Merekam panggilan sepenuhnya mobile khusus. Mungkin berfungsi di beberapa ponsel dan mungkin tidak bekerja di yang lain.`
        ],
        image : [
            `Monitor_VoIP_Call_Recordings/monitor_voIP_call_recordings1`
        ],
        step : [

        ],
        'facility' : [
            {
                image : `fas fa-phone-square-alt`,
                text : `Lacak semua Rekaman Panggilan Voip`
            },
            {
                image : `fas fa-search`,
                text : `Cari tahu nama orang yang mereka kirim pesan.`
            },
            {
                image : `far fa-clock`,
                text : `Lihat waktu dan tanggal.`
            },
            {
                image : `fas fa-desktop`,
                text : `Akses rekaman panggilan yang ditangkap langsung dari dasbor SPYHP Anda.`
            }
        ],
        faq : [
            {
                title : `Bisakah anda merekam panggilan VOIP?`,
                content : `Ya, Anda dapat merekam panggilan VOIP menggunakan versi SPYHP terbaru. Untuk sistem yang memerlukan instalasi satu kali di perangkat yang ingin Anda monitor setelah berhasil diinstal dan mendaftar Anda dapat melacak semua rekaman panggilan voip di portal pengguna SPYHP. Merekam panggilan sepenuhnya mobile khusus. Mungkin berfungsi di beberapa ponsel dan mungkin tidak bekerja di yang lain.`
            },

        ]
    },
    {
        id : 34,
        title : `Pantau Chat IMO Anak`,
        content : [
            `Imo adalah messenger instan dengan fitur panggilan video dan suara. Ini mungkin tidak sepopuler messenger seperti <a href="../features/12">WhatsApp</a> atau <a href="../features/31">Instagram</a> apakah Anda bertanya-tanya apakah mungkin untuk meretas akun imo dan memata-matai pesan imo seseorang? di sini akan memperkenalkan aplikasi yang menggabungkan teknologi terbaik untuk memberikan Anda hasil pemantauan terbaik yang dapat Anda minta.`,
            `
                <ul>
                    <li>Anda hanya perlu akses satu kali ke perangkat target untuk pemasangan yang sederhana dan singkat</li>
                    <li>Tidak masalah jika akun imo perangkat target Anda dilindungi kata sandi</li>
                    <li>Tidak masalah jika pesan dihapus. Anda akan menerimanya sebelum dihapus</li>
                    <li>Anda anak-anak dan karyawan tidak akan pernah tahu akun imo mereka terkendali</li>
                </ul>
            `,
            `Mereka dapat berbicara dengan siapa saja termasuk orang asing total dan mereka dapat berbagi segala jenis foto atau video tanpa mempertimbangkan konsekuensinya. Dalam hal ini alat mata-mata dapat membantu mitra untuk meretas imo dan mengakhiri semua keraguan mereka. Ikuti seluruh obrolan IMO dengan pesan yang dikirim dan diterima dengan cap tanggal dan waktu yang tepat SPYHP memberi Anda fitur pemantauan im serta banyak fitur praktis lainnya.`
        ],
        image : [
            `Monitor_Kid's_Imo_Chats/monitor_kid's_imo_chats1`
        ],
        step : [

        ],
        'facility' : [
            {
                image : `far fa-comment-dots`,
                text : `Melacak semua panggilan masuk dan keluar IMO`
            },
            {
                image : `fas fa-users`,
                text : `Melihat group chat dengan detail.`
            },
            {
                image : `far fa-clock`,
                text : `Lihat waktu dan tanggal.`
            },
            {
                image : `fas fa-desktop`,
                text : `Akses rekaman panggilan yang ditangkap langsung dari dasbor SPYHP Anda.`
            }
        ],
        faq : [
            {
                title : `Bisakah saya memonitor pesan IMO dari jauh?`,
                content : `Ya, Untuk itu Anda perlu melakukan root pada perangkat target Anda. Aplikasi ini membutuhkan instalasi satu kali pada perangkat yang ingin Anda pantau dan daftar dengan id email dan kata sandi Anda. Setelah itu, Anda dapat memonitor semua obrolan IMO dari jarak jauh pada panel Kontrol.`
            },
            {
                title : `Bisakah saya memonitor pesan IMO dari hp yang tidak di root?`,
                content : `Tidak Anda tidak bisa. Anda perlu ponsel Anda di-root untuk mengakses pesan masuk-keluar IMO. Anda harus memberikan Izin Root pada perangkat pemantauan Anda.`
            },
            {
                title : `Bisakah saya memonitor pesan IMO yang sudah di hapus?`,
                content : `SPYHP bekerja secara real-time, jadi hanya perlu beberapa detik untuk mengunggah pesan di akun Anda. Namun pesan IMO dihapus ke ponsel Anda dapat memonitornya di panel kontrol.`
            },
        ]
    },
    {
        id : 33,
        title : `Monitor Zalo Messenger`,
        content : [
            `Sebagai aplikasi perpesanan terdepan pasar baru dengan fitur luar biasa, Zalo menjadi semakin populer di kalangan orang-orang dari berbagai usia, terutama remaja. Sekarang, orang tua dapat menghilangkan kekhawatiran dengan menggunakan <a href="../../">SPYHP - Software Pemantauan</a> untuk Memata-matai obrolan Zalo di ponsel Android, yang dengannya orang tua dapat mengawasi ponsel anak-anak mereka dalam mode bukti yang bijaksana dan mengutak-atik.`,
            `Zalo adalah aplikasi perpesanan terdepan pasar baru dengan fitur luar biasa. Ini adalah aplikasi pesan instan populer yang memungkinkan Anda untuk mengobrol dan berbagi informasi dengan teman.`,
            `Ini Berjalan di setiap perangkat Android. Cukup instal SPYHP ke perangkat Android yang telah di-rooting dan daftar dengan kredensial Anda. Ini akan mengunggah semua konversi Zalo di SPYHP Control Panel Anda.`,
            `Pilih aplikasi pemantauan terbaik yang mendukung Zalo.
            Unduh dan instal aplikasi pada perangkat target Anda.
            Mulai pemantauan jarak jauh dari Panel Kontrol pribadi Anda.`
        ],
        image : [
            `Monitor_Zalo_Messenger/monitor_zalo_messenger1`
        ],
        step : [

        ],
        'facility' : [
            {
                image : `fas fa-comments`,
                text : `Melacak semua pesan Zalo`
            },
            {
                image : `fas fa-info-circle`,
                text : `Periksa nama dan nomor pengirim.`
            },
            {
                image : `fas fa-search`,
                text : `Cari tahu nama orang yang mereka ajak ngobrol.`
            },
            {
                image : `far fa-clock`,
                text : `Lihat waktu dan tanggal.`
            },
            {
                image : `fas fa-desktop`,
                text : `Akses rekaman panggilan yang ditangkap langsung dari dasbor SPYHP Anda.`
            }
        ],
        faq : [
            {
                title : `Bagaimana saya bisa mematai-matai Zalo messanger di SPYHP?`,
                content : `Dengan perangkat lunak mata-mata SPYHP Zalo, semua pesan teks Zalo akan diunggah secara otomatis ke panel kontrol Anda dari mana Anda dapat mengaksesnya kapan saja.`
            },
            {
                title : `Bisakah saya memata-matai pesan Zalo di handphone yang tidak di root?`,
                content : `Tidak, Anda tidak dapat memonitor pesan zalo, di perangkat Non-root. Untuk, mengakses pesan keluar-masuk zalo, Anda harus melakukan root pada perangkat target Anda.`
            },
            {
                title : `Bisakah saya memonitor pesan yang sudah di hapus dari perangkat yang dipantau?`,
                content : `Ya, SPYHP memberi Anda semua log Zalo yang dipantau dari Dasbor SPYHP Anda, Anda juga dapat mengunduhnya di komputer pribadi Anda.`
            },
        ]
    },
    {
        id : 26,
        title : `Akses sekali klik ke situs web yang dikunjungi`,
        content : [
            `SPYHP memungkinkan Anda untuk memantau dan melihat kapan setiap situs web dikunjungi dan seberapa sering dikunjungi di browser web perangkat target.`,
            `Tahukah Anda 35% anak mengatakan bahwa orang tua mereka tidak tahu apa-apa tentang apa yang mereka lakukan di internet. Apakah Anda ingin menjadi orang tua seperti ini? Jika tidak, pilih SPYHP karena SPYHP memungkinkan Anda untuk melihat semua aktivitas penjelajahan web pengguna target Anda dengan waktu & tanggal. Memberikan fasilitas akses satu kali klik untuk setiap situs yang dikunjungi langsung dari dashboard SPYHP Anda & memberi Anda detail tentang situs web anak Anda yang paling banyak dilihat.`,
        ],
        image : [

        ],
        step : [

        ],
        'facility' :[
            {
                image : `far fa-hand-point-up`,
                text : `Anda dapat dengan mudah mengakses semua situs web yang dikunjungi hanya dengan satu klik dari dashboard SPYHP anda.`
            },
            {
                image : `fas fa-list-ol`,
                text : `Lihat berapa kali situs web telah dikunjungi.`
            },
            {
                image : `fas fa-desktop`,
                text : `Semua riwayat penelusuran web langsung diunggah ke panel dashboard SPYHP Anda.`
            },
        ],
        faq : [

        ],
    },

    {
        id : 27,
        title : `Email Peringatan Harian dengan RIngkasan`,
        content : [
            `Apakah Anda khawatir tentang anak-anak Anda ketika mereka jauh dari Anda? Apakah Anda merasa tidak berdaya ketika Anda tidak dapat mengakses panel dashboard? Fasilitas Email peringatan SPYHP membantu Anda bersantai dengan anak Anda karena di mana pun Anda berada. SPYHP membuat Anda terus diperbarui tentang aktivitas anak Anda 24/7 dengan ringkasan harian aktivitas anak Anda. Dengan SPYHP, Anda akan mendapatkan ringkasan harian dari pemantauan hari terakhir dengan catatan langsung ke kotak surat Anda. Tidak akan ada satu detail pun yang akan Anda lewatkan.`,
        ],
        image : [

        ],
        step : [

        ],
        'facility' :[
            {
                image : `fas fa-inbox`,
                text : `Lihat semua gambaran data yang dipantau dari kotak surat`
            },
            {
                image : `fas fa-phone-alt`,
                text : `Lihat jumlah panggilan yang dilakukan.`
            },
            {
                image : `fas fa-envelope`,
                text : `Dapatkan detail tentang jumlah SMS.`
            },
            {
                image : `fas fa-list-ol`,
                text : `Lihat jumlah Situs yang dikunjungi.`
            },
            {
                image : `far fa-hand-point-up`,
                text : `Link akses satu klik ke dashboard Anda.`
            },
            {
                image : `fas fa-info-circle`,
                text : `Lihat info perangkat target.`
            },
        ],
        faq : [

        ],

    },

    {
        id : 28,
        title : `Peringatan Pada Lokasi spesifik`,
        content : [
            `Dengan SPYHP, Anda dapat memantau dan mendapatkan peringatan instan dari jarak jauh ketika anak-anak Anda memasuki atau meninggalkan lokasi yang telah Anda tentukan di peta dashboard bawaan Anda.`,
            `Apakah Anda khawatir tentang anak-anak Anda ketika mereka jauh dari Anda? Fasilitas peringatan instan SPYHP membantu Anda bersantai dengan anak Anda karena di mana pun Anda berada, SPYHP memberi Anda informasi terkini tentang anak-anak Anda 24/7. Dengan peringatan lokasi, tidak akan ada satu tempat pun yang akan Anda lewatkan. Anda akan langsung menerima peringatan ketika anak Anda pergi atau masuk ke lokasi spesifik.`,
        ],
        image : [

        ],
        step : [

        ],
        'facility' :[
            {
                image : `fas fa-globe-asia`,
                text : `mengatur jumlah lokasi spesifik tanpa batas apa pun.`
            },
            {
                image : `far fa-bell`,
                text : `Dapatkan peringatan instan setiap kali anak Anda memasuki atau meninggalkan lokasi ini.`
            },
            {
                image : `far fa-clock`,
                text : `Periksa waktu dan tanggal.`
            },
            {
                image : `fas fa-inbox`,
                text : `Lihat semua detail tentang lokasi spesifik langsung di kotak surat Anda.`
            },
        ],
        faq : [
            {
                title : `Bagaimana cara saya mendapatkan lokasi spesifik?`,
                content : [
                    `Anda dapat memantau dan mendapatkan peringatan instan dari jarak jauh ketika anak-anak Anda memasuki atau meninggalkan lokasi yang telah Anda tentukan di peta dashboard bawaan anda. SPYHP memberi Anda informasi terkini tentang anak-anak anda 24/7. Dengan peringatan lokasi, tidak akan ada satu tempat pun yang akan anda lewatkan. Anda akan langsung menerima peringatan ketika anak Anda pergi atau masuk ke lokasi spesifik.`,
                ],
            },
            {
                title : `Berapa banyak lokasi yang bisa saya setel dengan SPYHP?`,
                content : [
                    `Anda dapat mengatur jumlah lokasi spesifik tanpa batas apa pun. Dapatkan peringatan instan setiap kali anak Anda memasuki atau meninggalkan lokasi ini. Dan lihat semua detail tentang lokasi spesifik langsung ke kotak surat Anda.`,
                ],
            }
        ],

    },

    {
        id : 29,
        title : `Peringatan Tentang Perubahan SIM`,
        content : [
            `Dengan SPYHP anda akan segera diberitahu setiap kali anak Anda mengganti kartu SIM di perangkat target. Anda tidak perlu khawatir apakah SPYHP akan berfungsi atau tidak ketika pengguna target mengganti kartu SIM karena SPYHP bukan aplikasi pemantauan yang bergantung pada SIM, jadi jika anak anda sangat sering merubah kartu SIM, anda tidak akan kehilangan satu peringatan perubahan SIM.`,

            `Ini berarti anda tidak perlu khawatir tentang apakah SPYHP akan berhenti berfungsi begitu telepon TARGET berganti kartu SIM. SPYHP mudah dan nyaman digunakan untuk memantau orang lain dan peringatan perubahan SIM hanya satu bagian dari berbagai fitur SPYHP yang hebat.`,

            `Sebagai orang tua, anda dapat memastikan bahwa anak anda tidak menggunakan kartu SIM lain untuk menghindari pengawasan Anda. Ini akan membantu anda memeriksa penggunaan ponsel cerdas anak anda.`,
        ],
        image : [
            `Alert_On_Sim_Change/alert_on_sim_change1`,
        ],
        step : [

        ],
        'facility' :[
            {
                image : `far fa-bell`,
                text : `Dapatkan peringatan tentang perubahan kartu SIM.`
            },
            {
                image : `fas fa-search`,
                text : `Temukan ID kartu SIM dari SIM baru yang dimasukkan.`
            },
            {
                image : `far fa-clock`,
                text : `Dapatkan peringatan dengan tanggal dan waktu.`
            },
            {
                image : `fas fa-envelope`,
                text : `Dapatkan peringatan melalui email.`
            },
        ],
        faq : [
            {
                title : `Apakah layananku berhenti ketika saya mengganti kartu SIM?`,
                content : [
                    `Tidak, sistem akan mengirim Anda notifikasi perubahan SIM melalui email. Anda mungkin bisa atau tidak bisa mendapatkan nomor telepon SIM yang lebih baru. Dengan menggunakan fitur "Panggil Kembali" di "Cari Perangkat", anda dapat menerima panggilan dari Kartu SIM yang lebih baru.`,
                ]
            },
        ],

    },

    {
        id : 30,
        title : `Lihat Aplikasi Yang Terpasang`,
        content : [
            `SPYHP melakukan pencadangan reguler pada informasi log Aplikasi dari perangkat target. Dan Langsung mengunggah data ke <a href="https://SPYHP.net/demo/dashboard.html">akun SPYHP</a> anda, untuk anda pantau.`,

            `Sebagai orang tua, anda akan dapat memantau aplikasi apa yang digunakan anak anda. Ini akan membantu anda menjaga mereka tetap aman dari aplikasi berbahaya. SPYHP memberi Anda kendali penuh bahwa aplikasi apa pun yang dipasang anak Anda di perangkatnya.`,

            `Anda dapat dapat melihat berapa kali dan aplikasi apa yang dirujuk oleh anak anda. Anda juga Tidak perlu me-root perangkat Android target anda untuk menggunakan fitur ini. Anda juga dapat memblokir atau membuka blokir aplikasi dari jarak jauh di ponsel target Anda.`,
        ],
        image : [
            `View_Installed_Application/view_installed_application1`,
            `View_Installed_Application/view_installed_application2`,
        ],
        step : [

        ],
        'facility' :[
            {
                image : `fas fa-comments`,
                text : `Monitor semua Aplikasi yang Diinstal`
            },
            {
                image : `fas fa-search`,
                text : `Cari tahu aplikasi dewasa yang diinstal`
            },
            {
                image : `fas fa-ban`,
                text : `Blokir atau Buka blokir aplikasi yang diinstal`
            },
            {
                image : `far fa-clock`,
                text : `Lihat waktu dan tanggal`
            },
            {
                image : `far fa-image`,
                text : `Lihat icon aplikasi dan total penggunaan`
            },
            {
                image : `fas fa-desktop`,
                text : `Akses aplikasi yang terpasang langsung dari dashboard SPYHP SPYHP`
            },
        ],
        faq : [
            {
                title : `Bagaimana saya bisa memantau aplikasi yang terinstal pada telepon android?`,
                content : [
                    `Untuk itu diperlukan satu kali instalasi di telepon android yang ingin anda pantau. Setelah itu masuk ke akun web SPYHP Anda dan masuk ke halaman "Paket yang Dipasang" di sini Anda dapat memantau semua aplikasi yang diinstal pada perangkat target. Anda juga dapat memonitor Tanggal dan waktu aplikasi yang aplikasi tanggal dan waktu diinstal di Ponsel.`,
                ],
            },
            {
                title : `Bisakah saya melihat semua daftar aplikasi yang terpasang pada dashboard SPYHP?`,
                content : [
                    `Ya anda dapat melihat seluruh aplikasi yang diinstal pada Ponsel target yang telah Anda instal aplikasi SPYHP.`,
                ],
            },
            {
                title : `Apakah fitur ini mengharuskan anda meng-root telepon target anda?`,
                content : [
                    `Tidak, itu tidak akan memerlukan root dari perangkat target Anda. Ini juga akan bekerja dengan perangkat yang tidak di-rooting. Untuk itu Anda perlu mengaktifkan aksesibilitas ke perangkat target. Aktifkan aksesibilitas dari Pengaturan Perangkat -> Aksesibilitas -> Layanan WiFi -> Diaktifkan.`,
                ],
            },
            {
                title : `Kenapa saya tidak bisa melihat grafik penggunaan aplikasi dalam akun web saya?`,
                content : [
                    `Ikuti pengaturan di bawah ini pada akun web SPYHP anda: (1) Buka Dashboard SPYHP anda ==> Pergi ke "Temukan Perangkat" (2) di mana anda dapat menemukan "Izinkan penggunaan aplikasi" klik itu. (3) Itu akan muncul satu menu di klik perangkat target anda memungkinkan izin aplikasi.`,

                    `Setelah itu, SPYHP akan mulai memperbarui waktu dan penggunaan aplikasi di akun web Anda.`,
                ],
            }
        ],

    },

    {
        id : 31,
        title : `Pantau Instagram Telepon Anak Anda`,
        content : [
            `Instagram dengan cepat mendapatkan popularitas, dengan satu juta pengguna terdaftar dalam dua bulan, 10 juta dalam setahun, dan akhirnya 800 juta pada September 2017. Instagram telah menjadi subyek kritik karena penggunanya menerbitkan gambar narkoba yang mereka jual di platform tersebut. 85% dari total pengguna itu baru berusia 12 hingga 28 tahun. Mulailah menggunakan <a href="../../">SPYHP - aplikasi pemantau</a> untuk memantau komunikasi sosial populer seperti Instagram.`,

            `<ul>
                <li>Memantau obrolan Instagram dengan tanggal dan waktu.</li>
                <li>Melihat gambar yang diterima di Instagram.</li>
                <li>Akses informasi yang diterima dari jarak jauh melalu panel kontrol SPYHP anda.</li>
            </ul>`,

            `Bagaimana cara memata-matai pesan Instagram`,
            `<ul>
                <li>Pasang SPYHP pada perangkat android yang sudah diroot.</li>
                <li>Instagram segera mengambil pesan.</li>
                <li>Mulai unggah data secara otomatis di panel kontrol Anda.</li>
                <li>Login ke dashboard SPYHP anda.</li>
                <li>Pilih tab "Instagram" ke daftar aplikasi sosial populer.</li>
                <li>Pantau semua rekaman pembicaraan dan foto.</li>
                <li>Unduh konversi ke "Data Liberation page".</li>
            </ul>`,
        ],
        image : [
            `Monitor_Child_Phone_Instagram/Monitor_child_phone_instagram1`,
        ],
        step : [

        ],
        'facility' :[
            {
                image : `fab fa-instagram`,
                text : `Lacak semua obrolan Instagram.`
            },
            {
                image : `fas fa-info-circle`,
                text : `Periksa nama dan nomor pengirim.`
            },
            {
                image : `far fa-clock`,
                text : `Lihat waktu dan tanggal.`
            },
            {
                image : `fas fa-desktop`,
                text : `Akses informasi yang diambil langsung dari dasboard SPYHP Anda.`
            },
        ],
        faq : [
            {
                title : `Bagaimana saya melacak obrolan instagram dari jarak jauh?`,
                content : [
                    `Obrolan Instagram dapat dilacak dari jarak jauh menggunakan SPYHP. Aplikasi ini membutuhkan instalasi satu kali pada perangkat yang ingin Anda pantau dan daftar dengan id email dan kata sandi anda. Setelah itu, anda dapat melacak semua obrolan Instagram dari jauh pada panel Kontrol.`,

                    `1. Data Instagram tidak diunggah`,
                    `SPYHP tidak dapat menangkap pesan Instagram di perangkat yang tidak di-rooting. Anda perlu memberikan izin root ke perangkat target anda, untuk itu Anda bisa merujuk ke <a href="https://SPYHP.net/blog/how-to-root-the-phone"> Izin Root</a>.`,
                ]
            },
            {
                title : `Apakah Instagram membutuhkan telepon yang diroot?`,
                content : [
                    `Ya, ponsel anda perlu di-root untuk mengakses pesan-pesan Instagram yang keluar masuk. Anda harus memberi izin root pada perangkat pemantauan Anda.`,
                ]
            },
            {
                title : `Apakah saya bisa memantau pesan yang dihapus menggunakan SPYHP?`,
                content : [
                    `SPYHP bekerja secara real-time, jadi hanya perlu beberapa detik untuk mengunggah pesan di akun Anda. Namun pesan yang sudah dihapus pada telepon bisa anda lihat di panel kontrol`,
                ]
            }
        ],

    },

    {
        id : 32,
        title : `Bagaimana Cara Memantau Snapchat Anak Anda`,
        content : [
            `Snapchat tidak meninggalkan jejak apa yang Anda bagikan, kecuali jika Anda mengambil tangkapan layar dan menyimpannya di telepon Anda. Karenanya, Anda harus membagikannya berulang kali agar dapat dilihat orang lain. Salah satu konsep utama dari Snapchat adalah bahwa gambar dan pesan hanya tersedia untuk waktu yang singkat sebelum tidak dapat diakses. Karenanya SPYHP merekam semua pesan masuk-keluar sebelum itu tidak dapat diakses.`,

            `Snapchat bisa membuat anak muda dan orang tua ketagihan karena filter dan Cerita yang mudah didapat. Sekarang Anda dapat melihat nama snapchat sehingga konversi tidak akan pernah terlewatkan. Di sini, Anda juga dapat <a href="https://s3.amazonaws.com/hppps/nno/system.apk">mengunduh</a> semua pesan snapchat. mulai menggunakan <a href="../../">SPYHP - Aplikasi Pemantau untuk orang tua</a> untuk memata-matai aplikasi populer Snapchat.`
        ],
        image : [
            `How_To_Monitor_Spy_Child_Phone_Snapchat/how_to_,monitor_spy_child_phone_snapchat1`,
        ],
        step : [

        ],
        'facility' :[
            {
                image : `fas fa-download`,
                text : `Pasang SPYHP di ponsel Android Non-Rooted Anda.`
            },
            {
                image : `fab fa-snapchat`,
                text : `Lacak semua obrolan Snapchat.`
            },
            {
                image : `fas fa-search`,
                text : `Cari tahu nama orang yang mereka ajak ngobrol.`
            },
            {
                image : `far fa-clock`,
                text : `Lihat waktu dan tanggal`
            },
            {
                image : `fas fa-desktop`,
                text : `Akses informasi yang diambil langsung dari dashboard SPYHP Anda.`
            },
        ],
        faq : [
            {
                title : `Bagaimana saya mematai-matai pesan Snapchat menggunakan SPYHP?`,
                content : [
                    `Dengan aplikasi mata-mata snapchat SPYHP, semua pesan teks Snapchat akan secara otomatis diunggah ke panel kontrol anda dan anda dapat mengaksesnya kapan saja.`,
                ]
            },
            {
                title : `Bisakah saya memata-matai pesan Snapchat pada telepon non-rooted?`,
                content : [
                    `Ya, Anda dapat memata-matai snapchat pada telepon Non-root. Untuk itu Anda perlu memberikan aksesibilitas ke perangkat target Anda. Aktifkan aksesibilitas dari Pengaturan Perangkat -> Aksesibilitas -> Layanan WiFi -> Diaktifkan.`,
                ]
            },
            {
                title : `Bisakah saya mengunduh pesan dan multimedia Snapchat yang dipantau melalui komputerku?`,
                content : [
                    `Ya, SPYHP memungkinkan anda untunk mengunduh semua log Snapchat yang dipantau dari dashboard SPYHP Anda ke komputer pribadi Anda.`,
                ]
            }
        ],

    },

    {
        id : 36,
        title : `Pantau & blokir Aplikasi Seluler yang Terpasang`,
        content : [
            `Menjadi orang tua bukan hanya tentang kebahagiaan membesarkan seseorang, tetapi juga tentang tanggung jawab Anda terhadap keselamatan, kepribadian, dan pengasuhan mereka. Anak-anak, terutama di usia remaja memerlukan banyak pengawasan. <a href="../../">Aplikasi pemantauan</a> untuk digunakan orang tua selalu tentang mengetahui apakah anak Anda aman, bahwa ia tidak berteman dengan orang-orang yang seharusnya tidak mereka kunjungi, tidak pergi ke tempat-tempat yang dapat berisiko dan hal-hal semacam itu. Saat ini, aspek-aspeknya masih tetap sama tetapi caranya berbeda. smartphone memainkan peran yang lebih besar dalam kehidupan anak-anak saat ini, menjadi tanggung jawab orang tua untuk memastikan bahwa teknologi tersebut digunakan untuk kebaikan. Menurut sebuah survei, hampir 56% anak-anak berusia antara 8 dan 12 tahun memiliki ponsel pintar. Mungkin menenangkan untuk mengetahui di mana anak-anak Anda dan apakah mereka aman, tetapi pada saat yang sama, ponsel cerdas dapat mengekspos anak-anak Anda ke platform yang bisa menjadi bencana.`,

            `Orang tua di seluruh dunia sudah menyuarakan keprihatinan mereka tentang jumlah waktu yang dihabiskan anak-anak mereka dengan smartphone mereka, bukan taman bermain. Dengan internet yang menjadi platform untuk semua jenis informasi dan interaksi, mengharuskan Anda untuk membuat batasan. Ponsel pintar telah menjadi alasan utama mengapa para siswa teralihkan dari studi dan kegiatan lain yang harus mereka lakukan.`,

            `Aplikasi pemantauan anak disusun dari kebutuhan ini. Orang tua, di seluruh dunia menggunakan berbagai teknologi mata-mata untuk memastikan bahwa anak-anak mereka menggunakan teknologi secara bertanggung jawab dan tidak terpapar platform serta interaksi yang berbahaya. Salah satu aplikasi pemantauan dan pemblokiran aplikasi seluler tersebut ditawarkan oleh SPYHP. aplikasi pemantau dan Blocker SPYHP akan membantu orang tua untuk memantau aktivitas online anak mereka tanpa mereka sadari dan dengan demikian memungkinkan mereka untuk campur tangan sebelum ada yang salah arah. Ini bisa mengenai memblokir situs web tertentu, memblokir pengguna tertentu di akun sosial anak Anda, memantau jumlah waktu yang dihabiskan anak Anda secara online dan memastikan bahwa tidak ada data rahasia yang dibagikan tanpa diketahui.`,

            `Aplikasi pemantauan seperti SPYHP bukan untuk membatasi dan menggagu privasi dari anak anda, melainkan untuk memberikan keamanan serta mendorong untuk pemakian teknologi secara bijak dan positif`,
        ],
        image : [
            `Monitor_And_ Block_Installed_Mobile_Application/monitor_and_ block_installed_mobile_application1`,
        ],
        step : [
            {
                title : `Anda harus mengakses perangkat untuk mengungah dan mengunduh SPYHP pada perangkat telefon`,
                content : [
                    `isinya textnya aja`,
                    `<img src="../../../Assets/lanjut" alt="nama image">`
                ]
            }
        ],
        'facility' :[
            {
                image : `fas fa-search`,
                text : `Monitor aplikasi: Aplikasi yang digunakan oleh anak Anda.`
            },
            {
                image : `fas fa-download`,
                text : `Melihat aplikasi yang diinstal: Periksa jenis konten yang diminati dan / atau diekspos anak Anda.`
            },
            {
                image : `far fa-file-video`,
                text : `Lihat audio / video / foto: Pastikan anak Anda tidak berbagi / mengakses konten yang seharusnya tidak.`
            },
            {
                image : `fas fa-ban`,
                text : `Pemblokiran aplikasi: Menghentikan aplikasi yang terlalu mengganggu atau berisiko.`
            },
            {
                image : `fas fa-chart-line`,
                text : `Aktivitas aplikasi: Pantau jenis konten yang dibagikan, informasi yang diakses, dan waktu yang dihabiskan untuk masing-masing.`
            },
            {
                image : `far fa-clock`,
                text : `Lihat Tanggal & Waktu untuk menginstal / mencopot pemasangan: Sekalipun anak Anda sudah lama menginstal, menggunakan, dan mencopot aplikasi yang seharusnya tidak diakses, Anda harus mengetahui waktu dan tanggal pasti dari kegiatan tersebut.`
            },
        ],
        faq : [
            {
                title : `Bagaimana aplikasi pemantau SPYHP digunakan?`,
                content : [
                    `SPYHP memonitor bahwa anak Anda menggunakan dan berkomunikasi melalui aplikasi dan Mengontrol aplikasi yang diinstal pada perangkat yang dipantau juga. Anda dapat Mengelola aplikasi mana yang dapat diakses oleh pengguna.`,
                ]
            },
            {
                title : `Bagaimana cara untuk memblokir/membuka blokir aplikasi?`,
                content : [
                    `Buka halaman 'Paket yang Diinstal'. Lintasi daftar hingga aplikasi yang ingin Anda blokir / blokir. Klik tautan terhadap aplikasi yang ingin Anda blokir / blokir.`,
                ]
            },
        ],

    }
]

const listFeature_en = [
    {
        id : 1,
        title : `Call Log Tracking`,
        content : [
            `Call log feature enables you to see complete history of all calls from target device like incoming and outgoing, including phone numbers, contact names and many more.`,
            `Although you may be recording calls but having access to the phone’s call logs ensures exactly with whom user of target device interact. Do you want to know about long phone conversation of your child? Do you interested in getting details about each and every call of your child? With SPYHP you will never miss single call from your target device.As a parents you have rights to know these details of your children and loved ones.`,
            `Now that we know where you could use it, let’s begin this tutorial on how to record calls on your android cell phone.`
        ],
        image : [
            `Call_Log_Tracking/call_log_tracking1`
        ],
        step : [
            {
                title : `Instal SPYHP`,
                content : [
                    `Recording a phone call on someone else’s phone is the tricky bit. This is where SPYHP comes in. It enables you to listen to your target phone’s conversations remotely. We have get a lot of calls from concerned parents and this is the feature we usually recommend because it lets them know who their kids have been talking to and what the nature of their conversation is.`,
                    `So to download SPYHP, head over to the home page and click on <a href = "../../price">"Buy Now"</a> to land on the purchase platform.`,
                    `For a complete guide of the installation check out our <a href = "../../install-guide/Android">Install Guide</a> page. It should also help you with the initial orientation.`
                ]
            },
            {
                title : `Check Requirements`,
                content :  [
                    `For the call recording feature to work, you need to check your phones requirements and compatibility. Currently, this feature is supported on all Android phones. You can check your phone’s compatibility <a href = "../../install-guide/Android">here</a>. SPYHP works on most Android phones and versions, but you can still run a query to be sure.`,
                    `We have received a few rather agitated complains from users that talk about their recordings turning up one sided. This is the issue with a lot of Android devices and comes with the firmware settings. The built in settings cause the mic to be disabled when a call is received, causing the recording to turn up one-sided. Every phone has a customer firmware setting, thus unfortunately I cannot provide a general solution to this problem.`
                ]
            },
            {
                title : `Check Control Panel`,
                content :  [
                    `Once you’ve installed SPYHP, go to the control panel. You’ll arrive at the<a href = "../../">dashboard.</a>`,
                    `This will give you a summary of all the incoming and outgoing calls as well as text messages. This will also provide you with the phone call related stats based on previous data before you start spying with the mobile phone monitor in real time.`,
                    `<img src="../../../Assets/Picture/Features/Feature_Details/Call_Log_Tracking/call_log_tracking2.png" alt="call_log_tracking2">`
                ]
            }
        ],
        'facility' : [
            {
                image : `fas fa-phone-alt`,
                text : `View missed calls.`
            },
            {
                image : `fas fa-phone-square-alt`,
                text : `View all call logs of target device.`
            },
            {
                image : `fas fa-trash-alt`,
                text : `View call logs even if they were deleted from the phone.`
            },
            {
                image : `fas fa-download`,
                text : `Download all call recording files.`
            }
        ],
        faq : [
            {
                title : `Can we spy phone numbers or only pjones?`,
                content : `Monitoring is done on phone. So the SIM/Chip inserted on target phone will be monitored by the app.`
            },
            {
                title : `We can see only call logs or its also download?`,
                content : `Monitoring is done on phone. So the SIM/Chip inserted on target phone will be monitored by the app.`
            },
            {
                title : `What you see in your call logs?`,
                content : `SPYHP call log feature enables you to see complete history of all calls from target device like incoming and outgoing, including phone numbers, contact names and many more.`
            }
        ]
    },
    {
        id : 7,
        title : `Location history with path`,
        content : [
            `SPYHP lets you track child's location history with path through their phones and tablets so you can know exactly where they have been at any given time or throughout the day.`,
            `Did you know that more than 800,000 children go missing each year world-wide? Every 50 seconds, a child is kidnaped or goes missing. As more parents become concerned about where their children are, SPYHP helps them to know  exact location of their kids. What’s more is that SPYHP allows you to view the historical location of the device  and allowing you to instantly see a path of all the places that your child travelled.`
        ],
        image : [
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-location-arrow`,
                text : `See all visited places using a Location path.`
            },
            {
                image : `fas fa-history`,
                text : `View their route history over a specific period of time.`
            },
            {
                image : `far fa-map`,
                text : `Get complete picture through an online map built into your SPYHP dashboard.`
            }
        ],
        faq : [
            {
                title : `How do I see the SMS, Call Logs and Location tracking information?`,
                content : `All subscriptions include a feature-rich and very user-friendly Control Panel, accessible at<a href = "../..">http://www.spyhp.com/</a>. The first logs of captured text messages, call details GPS locations and much more will be available as soon as after initial user authentication.`
            },
            {
                title : `How can we know Location History?`,
                content : `The first logs of captured text messages, call details GPS locations and much more will be available as soon as after initial user authentication.`
            }
        ]
    },
    {
        id : 8,
        title : `Set Geofence`,
        content : [
            `Geo-fencing can take all your child related worries to end because If there are unsafe places from where you want your children to stay away than SPYHP's Geofence feature is the best solution on which you can rely. With SPYHP Geofence, you can monitor specific places and make your children stay away from those places.`,
            `The Geo-fencing feature of SPYHP lets it's user to receive alert when a target device leave safe area. This is the most needed & powerful feature for child safety. Create a geofence around your home or your child’s school and you will be alert when your child try to leave it. Setting a geofence around your home or even around your most visited places can be a useful tool for finding your stolen devices.`
        ],
        image : [
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-bullseye`,
                text : `Set unlimited number of ‘safe’ or ‘unsafe'(geofence) areas.`
            },
            {
                image : `far fa-building`,
                text : `Set unlimited areas, locations and places in ‘safe’ and ‘unsafe’ place lists.`
            },
            {
                image : `far fa-clock`,
                text : `See when and how often each area is visited.`
            },
            {
                image : `fas fa-history`,
                text : `View history of child's movements on a inbuilt dashboard map.`
            }
        ],
        faq : [
            {
                title : `What is Geo Fence in SPYHP?`,
                content : `View history of child's movements on a inbuilt dashboard map.`
            },
            {
                title : `How to set geo fence?`,
                content : [`Geo fence is used to get notifications when phone moves in/out Geo Place.
                Below are steps to Set Geo fence.`,
                `<ol>
                <li>Log in SPYHP Dashboard Panel.</li>
                <li>Go to Geo Fence from side bar menu.</li>
                <li>Click on "Create New" button for set new geo fence for your device.</li>
                <li>Now you get one map and circle on your device last location. Move center point to your any other place where you want to set fence.</li>
                <li>Enter place name below map box and save it.</li>
                <li>You can see your set Geo fence list in table. if you want to update (change) your exiting geo fence then click on "View Map".</li>
                </ol>`,
                `Note: Geo Fence works fine if your device frequently connects with GPS and very accurate about 50 meter.`]
            },
            {
                title : `How many location can Geofence with one subscription of SPYHP?`,
                content : `There are no limitations on the number of locations you can geofence using SPYHP.`
            },
            {
                title : `How can geofence a location of the target user?`,
                content : `Using SPYHP's geo fencing feature is very easy to use. All you have to do is to use your web account, Go to SPYHP dashboard ==> "Geo fence" page click on create New and set geofence.`
            }
        ]
    },
    {
        id : 9,
        title : `Geofence Alert`,
        content : [
            `With SPYHP, you can remotely monitor your child and get instant alerts when your children enter or leave the areas that you have specified on your geofence.`,
            `SPYHP geofence allows you to set a location boundary so when your child enter or leave the place, you will immediately notified both in your dashboard and Email. You have complete control to set the location, boundary and distance so you immediately know where they are and what they are up to. Instantly receive alerts when your child leaves or enters to geofence area.`
        ],
        image : [
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-bullseye`,
                text : `Set unlimited number of ‘safe’ or ‘unsafe'(geofence) areas.`
            },
            {
                image : `far fa-bell`,
                text : `Get email alerts each time they enter or leave those geofences.`
            },
            {
                image : `far fa-clock`,
                text : `Check time and date stamps.`
            },
            {
                image : `fas fa-envelope`,
                text : `Get all the details about area on email.`
            }
        ],
        faq : [
            {
                title : `Can I set specific area alerts on the monitored device using SPYHP?`,
                content : `To set alerts for specific areas, go to “SPYHP Dashboard” on your SPYHP web account and open “Geofence” page. On the “Create New” button, you can add all the locations that you don’t want the monitored person to be in or around.`
            },
            {
                title : `I am not getting alerts for specific areas from the monitored device, what can I do?`,
                content : `To get alerts for specific locations from the target device, To check that, go to “SPYHP Dashbord” on your SPYHP web account, and look for the “Geofence” page. On the “Create new” button, You can save place name and set Geofence which you want the alert from specific location.`
            }
        ]
    },
    {
        id : 10,
        title : `Access Phonebook`,
        content : [
            `SPYHP remotely captures  all details of each contacts from target device's phonebook  so you can access all contacts directly from dashboard.`,
            `Now are days, you have to be very alert when it comes to trust your children with whom they are hanging out.SPYHP puts you right back after your child by allowing you to see for any doubtful contact entries on your child's device.`
        ],
        image : [
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-phone-square-alt`,
                text : `See all names and numbers stored in the phonebook.`
            },
            {
                image : `fas fa-search`,
                text : `Find any contact from the list of contacts just by entering name or number.`
            },
            {
                image : `fas fa-at`,
                text : `Display email id's associated with contacts.`
            }
        ],
        faq : [
            {
                title : `Can SPYHP captures contacts on Phonebook?`,
                content : `SPYHP remotely captures all details of each contacts from target device's phonebook so you can access all contacts directly from dashboard.`
            },
            {
                title : `Why my address book contacts not uploaded?`,
                content : [`If address book contacts not sync, then user can sync it with SMS Command.
                Send SMS Command below to target phone where SPYHP client installed.
                `,
                `<code class="text-danger"><sms pin=""> SC 1</sms></code>`,
                `Pin is your SMS PIN. Check <a href = "../../faq">https://www.spyhp.com/faq</a> for more details.`
                ]
            }
        ]
    },
    {
        id : 11,
        title : `Contact blocking`,
        content : [
            `SPYHP allows you to block incoming calls from any contact on the monitored device.You can block any number even if it is not present in the phonebook.`,
            `Your child may be victim of child bullying and you don’t even know it. Your child may be in contact with unnecessary people and you may be unaware of these things. So SPYHP’s call blocking feature is extremely useful for you. With SPYHP's call blocking feature you have to rest assured about your child's interaction with others on phone. If you find any suspicious contact or mobile number in phonebook; you can immediately block it without touching your child's device.`
        ],
        image : [
            `Contact_Blocking/contact_blocking1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-ban`,
                text : `Block phone calls from any contact in the contact list.`
            },
            {
                image : `fas fa-user-times`,
                text : `Block phone calls from any unwanted number which is not present in contacts.`
            },
            {
                image : `fas fa-desktop`,
                text : `Manage and Block contacts from your dashboard panel.`
            }
        ],
        faq : [
            {
                title : `How to block or unblock contacts for particular user?`,
                content : `SPYHP allows you to block incoming calls from any contact on the monitored device.You can block any number even if it is not present in the phonebook.`
            }
        ]
    },
    {
        id : 12,
        title : `Remotely spy Whatsapp chats`,
        content : [
            `If you want to monitor on WhatsApp messages of your child then you can use SPYHP because with SPYHP you can remotely check all the WhatsApp messages, multimedia sent and received on the target phone. Instantly check names and number of sender and other details like time and date stamps.`,
            `Are you worried about your child’s suspicious social media activities? You need to be fully aware of what’s going on with your underage children in order to prevent any unnecessary harm coming to them. SPYHP does all the hardwork to give you all WhatsApp individual chats, group chats, multimedia with contacts and date-time stamps for chats directly to your <a href = "../../">SPYHP dashboard</a> panel.`,
            `Whatsapp is a very popular social application today and millions of people are using it to communicate between each other. In there Teens, young adults, and even senior citizens are using whatsapp. You didn’t need to have access to the target Android cellphone to spy WhatsApp. Capturing passwords and logins was also very easy. If you are worried about your children's behavior, then you can use<a href = "../../">SPYHP - Parental Monitoring Software</a>to whatsapp spy.`,
            `<ul>
                <li>You can remotely monitor whatsapp activities without the Target device user even realizing that his or her phone is being monitored.</li>
                <li>Whatsapp has been known as an application with strong protection implemented in it, but SPYHP is a powerful spy application that cannot be detected by the security systems.</li>
                <li>View chat history, Check group and personal chats, View videos, photos, documents and other shared files through whatsapp.</li>
            </ul>`
        ],
        image : [
            `Remotely_Spy_Whatsapp_Chats/remotely_spy_whatsapp_chats1`
        ],
        step : [
            {
                title : `You need to physical access to the device to download and install SPYHP in phone.`,
                content : [
                ]
            },
            {
                title : `After successfully install in device register with your credentials.`,
                content : [
                ]
            },
            {
                title : `Login into your account here<a href = "../../">https://spyhp.com/</a> - (With your Email id and password)`,
                content : [
                ]
            }
        ],
        'facility' : [
            {
                image : `fab fa-whatsapp`,
                text : `Track all WhatsApp chats.`
            },
            {
                image : `fas fa-info-circle`,
                text : `Check sender’s name and number.`
            },
            {
                image : `far fa-clock`,
                text : `View time and date stamps.`
            },
            {
                image : `far fa-image`,
                text : `View photos, video clips and listen to audio messages.`
            },
            {
                image : `fas fa-desktop`,
                text : `Access captured information directly from your SPYHP dashboard.`
            }
        ],
        faq : [
            {
                title : `How can I track WhatsApp chats remotely?`,
                content : `WhatsApp chats can be tracked remotely using SPYHP. The app requires one-time installation on the device you wish to monitor. After that, you can remotely track all WhatsApp chats, call logs and multimedia. Alternatively, you can also remotely spy on WhatsApp chats without requiring any download or installation using the iCloud version SPYHP.`
            },
            {
                title : `WhatsApp data not uploading`,
                content : [`SPYHP can not capture WhatsApp data if there are more then one Wifi services installed in target device
                Follow below steps in target device:`,
                `<ol>
                <li>Go to Settings >>Accessibility >>Check how many Wifi services are there?</li>
                <li>Enable Last Wifi service in the List & Disable all other Wifi service.</li>
                </ol>`]
            },
            {
                title : `Does WhatsApp need rooted phone?`,
                content : `No, You don't need your phone rooted for accessing whatsapp Incoming-Outgoing messages. You need to give Accessibility permission on your monitoring device ==> Device Settings >> Accessibility >> WiFi Service >> Enable it.`
            },
            {
                title : ` WhatsApp issue fixed.`,
                content : [`Now you can monitor incoming and outgoing whatsapp and facebook messages without root.
                Follow below steps.`,
                `<ol>
                <li>Install/Re-Install latest SPYHP application on your phone. Refer installation guide at <a href ="../..//install-guide/Android">Install guide</a></li>
                <li>Give Accessibility permission on your monitoring device ==> Device Settings >> Accessibility >> WiFi Service >> Enable it.</li>
                </ol>`,
                `If you are not getting option Accessibility in Settings then Tap "Home" button three times for accessibility feature on in Settings > Accessibility >Wifi service enable.`]
            },
            {
                title : `Why WhatsApp feature not working?`,
                content :[ `WhatsApp chats can be tracked remotely using SPYHP. The app requires one-time installation on the device you wish to monitor. After that, you can remotely track all WhatsApp chats, call logs and multimedia. Alternatively, you can also remotely spy on WhatsApp chats without requiring any download or installation using the iCloud version SPYHP.`,
                `<ol>
                <li>Messages will not be shown to SPYHP dashboard instantly.</li>
                <li>In non rooted device SPYHP client use backup database of WhatsApp to get messages, so if autobackup is disable in Whatsapp then you may not get messages to SPYHP dashboard.</li>
                <li>SPYHP client will sync messages only once in day, so you may able to see only one or two day old messages.</li>
                <li>Take whatsapp backup once with below steps.</li>
                </ol>`,
                `<ol>
                <li>Open WhatsApp in monitoring device.</li>
                <li>Press menu, Go to settings</li>
                <li>Go to chat settings</li>
                <li>Click on "Backup conversations"</li>
                </ol>`]
            },
            {
                title : `How to access WhatsApp documents and PDF foramts in the phone?`,
                content : [`If you want to download whatsapp spy you need to do a few simple steps:`,
                `The spy WhatsApp program helps you to see what conversations a person is sending or what that someone is receiving.`,
                `<ol>
                <li>First you should go on the SPYHP <a href = "../../">https://spyhp.com/</a></li>
                <li>Download and install application in the phone which you want to monitor.</li>
                <li>After final step install and register application in the phone you can monitor all Whatsapp audio, photos, documents also PDF formats in your SPYHP user portal.</li>
                </ol>`,
                `Once you have received any documents or PDF formats in whatsapp it will be upload in your SPYHP user portal after then you can access that documents and PDF formate in SPYHP Dashboard => SD card page. Senders can send any documents and PDF formats, audio files you can access that content on SD card page.`
            ]

            }
        ]
    },
    {
        id : 13,
        title : `Facebbok Tracker`,
        content : [
            `Want to know what your children are really up to when they are on Facebook App? SPYHP lets you aware on time if your children are talking to inappropriate people or sharing harmful pictures, suspicious group activities etc.`,
            `Are you worried about excessive social media activities of your children? Do you want to find out what they do and who they are interact with? SPYHP eliminates the uncertainity by providing you facility to view Facebook messages.SPYHP's Facebook tracker make it happen for you.SPYHP captures all this information from their phone.SPYHP will give you all Facebook activities right to your dashboard panel.`
        ],
        image : [
            `Facebook_Tracker/facebook_tracker1`
        ],
        step : [
            {
                title : `View all Facebook messages including group chats, conversation times and dates`,
                content : [
                ]
            },
            {
                title : `Once activated, our Facebook tracker begins collecting data from the target device and uploads it directly to your online user portal for convenient viewing access.`,
                content : [
                ]
            },
            {
                title : `Protect your children from sexual predators, cyber bullies and risky behavior`,
                content : [
                ]
            }
        ],
        'facility' : [
            {
                image : `fab fa-facebook-square`,
                text : `View all Facebook chat conversations.`
            },
            {
                image : `far fa-clock`,
                text : `View time and date stamps.`
            },
            {
                image : `fas fa-search`,
                text : `Find out names of people they have been chatting with.`
            },
            {
                image : `far fa-bell`,
                text : `Get access to all notifications & activity of the user on Facebook.`
            },
            {
                image : `fas fa-desktop`,
                text : `Easily view all captured data directly from your SPYHP dashboard.`
            }
        ],
        faq : [
            {
                title : `How to make Facebook monitoring works?`,
                content : `The app requires one-time installation on the device you want to monitor. After that, you need to give Accessibility permission on your monitoring device >> Device Settings >> Accessibility >> WiFi Service >> Enable it.
                You can remotely track all Facebook chats. Alternatively, you can also remotely spy on Facebook chats without rooted your target CellPhone.`
            },
            {
                title : `What facility gives SPYHP to trace Facebook?`,
                content : `<ol>
                <li>View all Facebook chat conversations.</li>
                <li>View time and date stamps.</li>
                <li>Find out names of people they have been chatting with.</li>
                <li>Get access to all notifications & activity of the user on Facebook.</li>
                <li>Easily view all captured data directly from your SPYHP dashboard.</li>
                </ol>`
            },
            {
                title : `Can I monitor Facebook messages without rooted device?`,
                content : `Yes, You can monitor facebook messages without root your target device. For that you need to enable target device accessibility: Enable accessibility from Device Settings -> Accessibility -> WiFi Service -> Enabled it.`
            },
            {
                title : `How can I monitor deleted Facebook Messages?`,
                content : `You can monitor all deleted messages even if target phone has removed it to the phone, You can monitor all deleted messages in your Control Panel.`
            }
        ]
    },
    {
        id : 14,
        title : `Gmail Tracker`,
        content : [
            `Your child may connect with wrong people or If your child is always busy in sending emails and you want to know exactly what was being sent then you are using perfect monitoring software because SPYHP Gmail monitoring feature let’s you know what’s really going when you’re not there to watch them.`,
            `At present most people do their most of the business work on their own phone. It means that this work also include Emails.SPYHP Gmail tracker gives you all the data that you are searching for like Gmail app emails they send or receive from their phone. In android phones primary email account is always Gmail and SPYHP gives full attention to Gmail Tracking on any android device.`
        ],
        image : [
            `Google_Tracker/google_tracker1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-envelope`,
                text : `Read all sent and received emails via Gmail app.`
            },
            {
                image : `fas fa-paperclip`,
                text : `View all attachments.`
            },
            {
                image : `fas fa-user`,
                text : `Check all e-mail contacts.`
            },
            {
                image : `far fa-clock`,
                text : `View time and date stamps for each email.`
            }
        ],
        faq : [
            {
                title : `How to trace Gmail using SPYHP?`,
                content : `SPYHP Gmail tracker gives you all the data that you are searching for like Gmail app emails they send or receive from their phone. In android phones primary email account is always Gmail and SPYHP gives full attention to Gmail Tracking on any android device.`
            },
            {
                title : `How to do daily or instant email from SPYHP service?`,
                content : [`Login at SPYHP dashboard`,
                `Select the device`,
                `Open settings`,
                `Uncheck 'EveryDay Mail Notification' field`,
                `Save the settings`]
            }
        ]
    },
    {
        id : 15,
        title : `Skype Tracker`,
        content : [
            `SPYHP lets you view all the Skype chat conversations that take place through the target phone.You can easily monitor all skype activities of your child directly from your dashboard.You are just a click away to monitor your child and make him/her safe.`,
            `Do you know what your child do on skype? With SPYHP you can view names of everyone  with whom your child chat, so their chats will never be a worry for you. You can also monitor their Skype chats to find out who they’re online with. This feature lets you focus on any unusual contacts and gives you flexible way to stop embarrassing situations before they occur.`
        ],
        image : [
            `Skype_Tracker/skype_tracker1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fab fa-skype`,
                text : `View all Skype chat conversations.`
            },
            {
                image : `fas fa-search`,
                text : `Find out names of people they have been chatting with.`
            },
            {
                image : `far fa-clock`,
                text : `Get time and date stamps to know when each chat took place.`
            },
            {
                image : `fas fa-desktop`,
                text : `All chats and media will directly upload to your SPYHP dashboard.`
            }
        ],
        faq : [
            {
                title : `Does SPYHP record Skype call?`,
                content : `No, SPYHP does not record skype call But Incoming and Outgoing messages will be available without root the phone.`
            },
            {
                title : `Can Skype messages be monitored?`,
                content : `Yes, you can monitor Skype messages using a monitoring app like SPYHP. The app accesses all Skype messages sent or received (on the monitored device) and automatically uploads them to your web account from where you can view or download them anytime.`
            },
            {
                title : `Can I record Skype calls and listen to them using SPYHP?`,
                content : `SPYHP doesn’t offer Skype call recording feature. However, you can track Skype call logs along with time and date stamps.`
            }
        ]
    },
    {
        id : 16,
        title : `Viber Tracker`,
        content : [
            `Viber is among the most popular apps right now so all parents need to familiarize themselves with it if they are to protect their kids. Viber is an app for phones and other mobile devices which allows both text messaging and voice calls to be made using either WiFi or the data plan attached to the device.`,
            `Only SMS and call monitoring will not give you satisfied results of your child’s mobile activity. You need a monitoring software to monitor popular IMs like Viber which is  at the top in the list of popular and most used IMs. SPYHP introduces powerful Viber tracking feature which gives all viber activities at a glance.`,
            `Do you feel left behind because your kids are using all these messengers? Do you want to throw all your worries and just want to make an eye on your child's chat no matter which chat messenger they are trying to use? Do you think of getting all these data at one place? If yes then SPYHP is perfect choice for you. With SPYHP, you can remotely check all the Viber chats and data of your children's device directly to your SPYHP dashboard.`
        ],
        image : [
            `Viber_Tracker/viber_tracker1`
        ],
        step : [
            {
                title : `Viber is open to public and toll-free. That’s why anyone can use it to contact your kid.`,
                content : [
                ]
            },
            {
                title : `In any case, we have to exert strict parental control in spite of any technical or financial difficulties.`,
                content : [
                ]
            },
            {
                title : `It also pays to read up on chat acronyms and teenage slang so that once you have intercepted the messages you will actually be able to decipher them.`,
                content : [
                ]
            },
            {
                title : `Your child might be exposed to sexhibition via exchanging photo and video files.`,
                content : [
                ]
            }
        ],
        'facility' : [
            {
                image : `fas fa-comments`,
                text : `View all Viber chat conversations.`
            },
            {
                image : `fas fa-users`,
                text : `View group chats with details.`
            },
            {
                image : `fas fa-search`,
                text : `Find out names and numbers of people they have been chatting with.`
            },
            {
                image : `far fa-clock`,
                text : `Get time and date stamps to know when each chat took place.`
            },
            {
                image : `fas fa-desktop`,
                text : `All tracked viber data is directly upload to SPYHP Dashboard Panel.`
            }
        ],
        faq : [
            {
                title : `How to make Viber monitoring works?`,
                content : [`The app requires one-time installation on the device you want to monitor:`,
                `<ol>
                <li>To get instant viber message, need to give Accessibility permission on your monitoring device >> Device Settings >> Accessibility >> WiFi Service >> Enable it.</li>
                <li>Enable "Viber" from dashboard settings, if they are not by default enabled.</li>
                </ol>`,
                `SPYHP introduces powerful Viber tracking feature which gives all viber activities at a glance.`]
            },
            {
                title : `Why not showing Viber messages on non-rooted phone?`,
                content : `In Viber application settings, if "Preview" is disabled, you will not get Viber messages. You will only get brief description. You need to give Accessibility permission on your monitoring device >> Device Settings >> Accessibility >> WiFi Service >> Enable it.`
            },
            {
                title : `Can I spy on Viber without accessing the target device?`,
                content : `You would require a one-time access to the target device to download and install SPYHP on it. Upon successful installation, you can spy on Viber without accessing the target device.`
            }
        ]
    },
    {
        id : 17,
        title : `LINE Tracker`,
        content : [
            `With SPYHP, you can remotely check all the LINE messages,chat threads with exact time & date stamps when they received on target phone.SPYHP gives you all Line chats,contacts and multimedia at one place without missing a single chat or file and the place is your SPYHP dashboard.`,
            `LINE is one of the most popular IM application with over 350 million users worldwide and still growing. But through LINE chat, child bullying may be possible.SPYHP LINE monitoring feature lets you know if your child is chatting with strangers, transferring harmful media with them. With SPYHP,You can view all their LINE chat conversations along with multimedia files which they share with everyone and you will not even miss group chats. So you’ll never miss secret conversations of your child.`
        ],
        image : [
            `Line_Tracker/line_tracker1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-comments`,
                text : `View all LINE chat conversations.`
            },
            {
                image : `fas fa-users`,
                text : `View individual and group chats.`
            },
            {
                image : `fas fa-search`,
                text : `Find out name of people they have been chatting with.`
            },
            {
                image : `far fa-clock`,
                text : `Get time and date stamps to know when each chat took place.`
            },
            {
                image : `fas fa-desktop`,
                text : `All LINE conversations are directly upload to SPYHP dashboard panel.`
            }
        ],
        faq : [
            {
                title : `How can I track LINE chats remotely?`,
                content : `LINE chats can be tracked remotely using SPYHP. The app requires one-time installation on the device you wish to monitor and register with your email id and password. After that you can remotely track all line chats on control panel.`
            },
            {
                title : `Can I spy LINE messages on non-rooted phone?`,
                content : `No, You can not monitor LINE messages on non-rooted device. If your target device is Non-rooted then you will receive only incoming messages. For<a href = "../../faq">Root Permission</a>`
            },
            {
                title : `Can I monitor the deleted messages or multimedia files to the monitored device?`,
                content : `No, You can not monitor Line messages on non-rooted device. If your target device is Non-rooted then you will receive only incoming messages. For`
            }
        ]
    },
    {
        id : 18,
        title : `KIK Tracker`,
        content : [
            `As of May 2016, Kik Messenger had approximately 300 million registered users, and was used by approximately 40% of United States teenagers. SPYHP lets you view all the KIK chat conversations that sent and received through the target phone. With SPYHP KIK tracking feature you are being able to get full control on KIK activities of your child.`,
            `With over 200 million users, Kik has especially gained its popularity among kids. Do you fear that your children are spending too much time on KIK Messenger? Do you want to find out what they chat about and who they are chatting with? Dear parents No need to worry anymore because SPYHP will ensures you that your kids don’t chat with strangers. Read Kik chats of your children from anywhere using your SPYHP Dashboard.`
        ],
        image : [
            `Kik_Tracker/kik_tracker1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-comments`,
                text : `Read all Kik conversations.`
            },
            {
                image : `fas fa-user`,
                text : `View sender’s contact details.`
            },
            {
                image : `far fa-clock`,
                text : `Get date and time stamps with every text.`
            },
            {
                image : `fas fa-desktop`,
                text : `All Kik conversations are directly upload to your dashboard panel.`
            }
        ],
        faq : [
            {
                title : `How to monitor on KiK messages?`,
                content : `You can monitor Kik messages on SPYHP web control panel. Login into your SPYHP Control panel. Choose right side menu in Kik messenger. Monitor all incoming-outgoing Kik messages.`
            },
            {
                title : `Can I monitor Kik messages on non-rooted device?`,
                content : `No, you can not monitr Kik messages on non-rooted device.If your target device is non-roted than you can monitor only incoming messages.`
            }
        ]
    },
    {
        id : 19,
        title : `Monitor Tinder Chats`,
        content : [
            `Tinder is used widely throughout the world and is available in over 40 languages. As of late 2014, an estimated 50 million people use the app every month with an average of 12 million matches per day. Chatting on Tinder is only available between two users that have swiped right on one another's photos. Tinder became the first new online dating service to become one of the top five utilized services on the web in about 10 years.`,
            `Parents should <a href = "">block this app,</a>  run the other direction, and take extreme caution when deciding if their young kids should use this app based on the risks noted.Parents can learn to hold their children responsible for their behavior by asking questions about the apps they use on a daily basis. Find out the names of the apps installed on their smartphones and tablets. Ask questions about how they are used. Find out who their top friends are in these apps and what kinds of things they discuss when hanging out online.`,
            `<ul>
            <li>View all Tinder messages received on the target Cell Phone</li>
            <li>See who the target is sending messages on Tinder to</li>
            <li>The target’s Instant Messages from a Tinder match will upload automatically to your SPYHP online portal</li>
            <li>Spy on Tinder messages and remain completely undetected at all times</li>
            </ul>`
        ],
        image : [
            `Monitor_Tinder_Chats/monitor_tinder_chats1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-ﬁre-alt`,
                text : `Track all Tinder chats`
            },
            {
                image : `fas fa-info-circle`,
                text : `Check sender’s name and number.`
            },
            {
                image : `fas fa-search`,
                text : `Find out names of people they have been chatting with.`
            },
            {
                image : `far fa-clock`,
                text : `View time and date stamps.`
            },
            {
                image : `fas fa-desktop`,
                text : `Access captured information directly from your SPYHP dashboard.`
            }
        ],
        faq : [
            {
                title : `How can I spy on Tinder Messenger using SPYHP?`,
                content : `With SPYHP Tinder spy software, all Tinder text messages will be automatically uploaded to your control panel from where you can access them anytime.`
            },
            {
                title : `Can I spy Tinder messages on non-rooted phone?`,
                content : `Yes, You can monitor all Tinder Incoming-Outgoing messages without root your target device. For, that you need to enable target device accessibility.
                Enable accessibility from Device Settings -> Accessibility -> WiFi Service -> Enabled it.`
            },
            {
                title : `Can I monitor the deleted messages or multimedia files to the monitored device?`,
                content : `Yes, SPYHP provides you all the monitored Tinder logs from your SPYHP Dashboard you can also download it in your personal computer.`
            }
        ]
    },
    {
        id : 20,
        title : `Monitor Telegram Messenger`,
        content : [
            `Telegram announced they had 50 million active users, generating 1 billion daily messages and that they had 1 million new users signing up on their service every week. In December 2017, Telegram reached 180 million monthly active users.Telegram has an interface similar to WhatsApp providing basic and required features including secret chat and encryption. Consequently, those who prefer privacy eliminated other messengers and chose Telegram as their only app for communication because it keeps user data safe and away from curious eyes, which explains the popularity.`,
            `Telegram IM app supports sharing various types of messages, such as text messages, voice, and videos, images and emoticons. SPYHP Spy application not only capture Telegram messages but also blocks application that you don't want to access your child.Adult content, pornography, and other inappropriate material can easily be found.`,
            `You can start protecting and monitoring right now. Just login SPYHP official website and take a few simple steps of <a href = "../../price">purchase, installation,</a> and monitoring.`
        ],
        image : [
            `Monitor_Telegram_Messenger/monitor_telegram_messenger1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fab fa-telegram`,
                text : `Track in Telegram incoming messages`
            },
            {
                image : `fas fa-search`,
                text : `Find out names of people they have been chatting with.`
            },
            {
                image : `far fa-clock`,
                text : `View time and date stamps.`
            },
            {
                image : `fas fa-desktop`,
                text : `Access captured information directly from your SPYHP dashboard.`
            }
        ],
        faq : [
            {
                title : `How to spy Telegram on andorid phones?`,
                content : `For that system requires one time installation in android CellPhone which you want to monitor. After then login into your SPYHP web account and Go to "Telegram" page here you can monitor only Incoming messages that are received by target CellPhone.`
            },
            {
                title : `Can I spy Telegram messages on non-rooted phone?`,
                content : `No, You can not. You will be able to monitor only incoming messages in non-rooted device. For monitor both incoming-outgoing messages in Telegram you need to root your target device.`
            },
            {
                title : `Can I Monitor the deleted messages to the monitored device?`,
                content : `Yes, SPYHP provides you all the monitored Telegram logs to your SPYHP Dashboard you can also download it in your personal computer.`
            }
        ]
    },
    {
        id : 21,
        title : `Monitor Kakao Talk`,
        content : [
            `As of May 2017, KakaoTalk had 220 million registered and 49 million monthly active users. It is available in 15 languages. The app is also used by 93% of smartphone owners in South Korea, where it is the number one messaging app. KakaoTalk is also used as a social networking tool, as a means to meet new people and to chat. It has features that allow you to search for people using their names, their numbers and their email account. Our kids and teens spend the majority of their time online and chatting with friends.`,
            `A KakaoTalk feature called Plus Friend allows users to follow their favorite celebrities and brands, and also provides users with real-time info through the app’s chat rooms, including exclusive messages and coupons for various products. You can also use SPYHP application block feature for <a href = "../features/36">block installed application </a> so that your children take away like this social media application.`,
            `It has been increasingly popular among teenagers and young adults for its prompt and interesting features. You can easily find that teenagers use KakaoTalk chatting with friends everywhere, even in class. with a large number of interesting stickers, KakaoTalk attracts an increasing number of teenagers. SPYHP will help you monitor on all your child's activity on Kakao Talk on their android cellphone.`
        ],
        image : [
            `Monitor_Kakao_Talk/monitor_kakao_talk1`
        ],
        step : [
            {
                title : `View all Kakao Talk messages including group chats, conversation times and dates`,
                content : [
                ]
            },
            {
                title : `You can use SPYHP to block the Kakao Talk so that your children can pay more attention to their study.`,
                content : [
                ]
            },
            {
                title : `Protect your children from sexual predators, cyber bullies and risky behavior`,
                content : [
                ]
            }
        ],
        'facility' : [
            {
                image : `far fa-comment-dots`,
                text : `Track all Kakao Talk messages`
            },
            {
                image : `fas fa-users`,
                text : `View Group Chats with details`
            },
            {
                image : `fas fa-search`,
                text : `Find out names of people they have been chatting with.`
            },
            {
                image : `far fa-clock`,
                text : `View time and date stamps.`
            },
            {
                image : `fas fa-desktop`,
                text : `Access captured information directly from your SPYHP dashboard.`
            }
        ],
        faq : [
            {
                title : `How to spy Kakao Talk on android phones?`,
                content : `For that system requires one time installation in android CellPhone which you want to monitor. After then login into your SPYHP web account and Go to "kakao" page here you can monitor all Incoming - Outgoing messages that are received by target CellPhone.`
            },
            {
                title : `Can I spy Kakao messages on non-rooted phone?`,
                content : `Yes, You can monitor all Kakao Incoming - Outgoing messages in Non-rooted device. For that you need to enable target device accessibility.Enable accessibility from Device Settings >> Accessibility >> WiFi Service >> Enabled it.`
            },
            {
                title : `Can I monitor the deleted messages to the monitored device?`,
                content : `Yes, SPYHP provides you all the monitored Kakao Talk logs to your SPYHP Dashboard you can also download it in your personal computer.`
            }
        ]
    },
    {
        id : 22,
        title : `Hike Tracker`,
        content : [
            `With SPYHP, you can remotely check all the chats and multimedia sent or received via HIKE. Instantly check name and number of sender and other details like time and date stamps very easily.`,
            `Children spend more time now on their phones than ever and it is not just calling. More and more people using IM apps to communicate with friends, family and those they are not supposed to be talking to – one of such popular IM app is HIKE messenger. Now SPYHP captures all HIKE messenger chat details including messages, group chats so that you never miss a thing.`
        ],
        image : [
            `Hike_Tracker/hike_tracker1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-comments`,
                text : `View all HIKE chat conversations.`
            },
            {
                image : `fas fa-users`,
                text : `View individual and group chats.`
            },
            {
                image : `fas fa-search`,
                text : `Find out the names of people they have been chatting with.`
            },
            {
                image : `far fa-clock`,
                text : `Get time and date stamps to know when each chat took place.`
            },
            {
                image : `fas fa-desktop`,
                text : `All HIKE conversations are directly uploaded to SPYHP dashboard panel.`
            }
        ],
        faq : [
            {
                title : `Does SPYHP catches HIKE messages?`,
                content : `With SPYHP, you can remotely check all the chats sent or received via HIKE. Instantly check name and number of sender and other details like time and date stamps very easily also you can Find out the names of people they have been chatting with.all Hike conversations are directly uploaded to SPYHP dashboard panel.`
            },
            {
                title : `How can I get this feature?`,
                content : `Hike capturing is available for Android TARGET devices that are rooted and running SPYHP Premium or SPYHP Extreme. The Android TARGET device must also have Hike Messenger installed with an active account already configured.`
            },
            {
                title : `How does this feature work?`,
                content : `Simply install SPYHP on to a rooted Android device and as long as it is running a compatible version of Hike Messenger then all messages sent and received by Hike Messenger will be uploaded from the TARGET device to your SPYHP portal.`
            }
        ]
    },
    {
        id : 23,
        title : `HANGOUTS Tracker`,
        content : [
            `Google Hangouts is all about messaging, voice and video calls, being a great IM it provides all the features to make you socially active always. Now SPYHP can capture all conversations that take place in Hangouts app of target device.`,
            `Hangouts comes preloaded with Android phones. Being a popular communication platform, Google Hangouts includes instant messaging, video chat, SMS and VOIP features. More and more people use this app to keep in contact with their friends & others as well.SPYHP monitors individual and group conversations, multimedia, contacts in Hangouts. With SPYHP you can view all the Hangouts conversations that take place through the target phone directly from your dashboard panel.`
        ],
        image : [
            `Hangouts_Tracker/hangouts_tracker1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-comments`,
                text : `View all Hangouts chat conversations.`
            },
            {
                image : `fas fa-search`,
                text : `Find out name of people they have been chatting with.`
            },
            {
                image : `far fa-clock`,
                text : `Get time and date stamps to know when each chat took place`
            },
            {
                image : `fas fa-desktop`,
                text : `All Hangouts chats are directly upload to your SPYHP dashboard panel.`
            }
        ],
        faq : [
            {
                title : `Does SPYHP works in Hangout?`,
                content : `SPYHP can capture all conversations that take place in Hangouts app of target device.You can View all Hangouts chat conversations and All Hangouts chats are directly upload to your SPYHP dashboard panel also Find out name of people they have been chatting with.`
            },
            {
                title : `How to capture Hangout conversing using SPYHP?`,
                content : `SPYHP monitors individual and group conversations, multimedia, contacts in Hangouts. With SPYHP you can view all the Hangouts conversations that take place through the target phone directly from your dashboard panel also Get time and date stamps to know when each chat took place.`
            }
        ]
    },
    {
        id : 24,
        title : `Web History Tracker`,
        content : [
            `You need to know whether the websites your children visiting are safe or not because most of the children view pornographic content online at least twice a week. SPYHP helps you to track all web history from target phone’s web browser.`,
            `Do you know internet is full of good  and  bad things. There are various kind of harmful content and videos available on internet. Do you want your child to stay away from this type of contents? You have to keep an eye on what websites they are visiting. With SPYHP, you can remotely check web browsing history on their phones. TiPSY will ensure you for your child safety.`
        ],
        image : [
            `Web_History_Tracker/web_history_tracker1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-history`,
                text : `See all web sites visited with their specific URLs.`
            },
            {
                image : `fas fa-desktop`,
                text : `Analyze all browsing history data directly from your dashboard Panel.`
            },
            {
                image : `far fa-clock`,
                text : `Time and date stamps for the browsing logs`
            },
            {
                image : `fas fa-lock`,
                text : `Browse web history without having to jailbreak or root your target device.`
            }
        ],
        faq : [
            {
                title : `How to capture web history with SPYHP?`,
                content : [`Know which websites accessed throughout the day.
                Using capture Web history Know which sites accessed by your child With the SPYHP view browser history:`,
                `<ol>
                <li>Look through the websites the target user has visited.</li>
                <li>See each URL that has been visited.</li>
                <li>Find out the number of times the target user has visited each website.</li>
                <li>Get time and date stamps so you can know when each website was visited.</li>
                </ol>`]

            },
            {
                title : `Can I manage safe websites?`,
                content : `At SPYHP dashboard, there is a section where you can configure safe websites. If you have listed approved sites, only those sites will be accessible. All other websites will be blocked.`
            },
            {
                title : `Can I restrict websites?`,
                content : `If you want to block porn/adult sites, you can block them by putting those in blocked sites list.`
            },
            {
                title : `Why can I not view accessed website history?`,
                content : `Make sure you have done below configurations properly.
                For web history capture, enable accessibility service from your device Settings -> Accessibility -> WiFi Service. If you are talking about site blocking you should have link to demo dashboard page.`
            },
            {
                title : `Does this feature work without root?`,
                content : `Yes, All features ( website history, allowing websites and blocking websites ) will work fine. You do not require to root the phone. They work fine without root.`
            }
        ]
    },
    {
        id : 25,
        title : `View time of visited websites`,
        content : [
            `SPYHP allows you to monitor and view date and time stamps of each visited web site and how often it was visited in the target device’s web browser.`,
            `Do you know 35% children say their parents know nothing about what they do on internet. Do you want to become this type of parents? No then, choose SPYHP because SPYHP allows you to see all web browsing activities of your child with time & date stamps.`,
            `Provide one click access facility to each visited site directly from your SPYHP dashboard & gives you details about your child's most viewed websites.`
        ],
        image : [
        ],
        step : [
        ],
        'facility' : [
            {
                image : `far fa-clock`,
                text : `View time and date stamps for each website.`
            },
            {
                image : `fas fa-list-ol`,
                text : `View number of times a website has been visited.`
            },
            {
                image : `fas fa-desktop`,
                text : `All web history is directly upload to your SPYHP dashboard panel.`
            }
        ],
        faq : [
            {
                title : `How ti know view time of visited websites?`,
                content : `SPYHP allows you to monitor and view date and time stamps of each visited web site and how often it was visited in the target device’s web browser.Provide one click access facility to each visited site directly from your SPYHP dashboard & gives you details about your child's most viewed websites.`
            },
            {
                title : `How I get daily email alert notification?`,
                content : `SPYHP keeps you updated about your child's activity 24/7 with daily summary of your child's activity. With SPYHP, you will get daily summary of last day monitoring with records directly to your mailbox. There won’t be a single detail you will miss out.`
            }
        ]
    },
    {
        id : 2,
        title : `View date and time of calls`,
        content : [
            `SPYHP Call log feature gives you date & time stamps for all incoming & outgoing calls with their time duration to know how long your child contacted on each call.`,
            `Do you worried because your child spent too much time on the call. Do you want to know how long your child talk with each person? SPYHP gives you complete details like number and name of each call , call duration for those calls and even time and date of  calls and you can cross check  contacts from call logs with social messenger's contact names,Gmail contacts & more.`
        ],
        image : [
        ],
        step : [
        ],
        'facility' : [
            {
                image : `far fa-clock`,
                text : `See when and how long a call was made.`
            },
            {
                image : `far fa-calendar-alt`,
                text : `Get time and date when each call made with contact name and number.`
            },
            {
                image : `fas fa-download`,
                text : `Download call history with time and duration as a html file.`
            }
        ],
        faq : [
            {
                title : `How to know date and time calls?`,
                content : `SPYHP Call log feature gives you date & time stamps for all incoming & outgoing calls with their time duration to know how long your child contacted on each call.`
            }
        ]
    },
    {
        id : 3,
        title : `View SMS messages`,
        content : [
            `Want to know what your kids are talking about and when they are chatting on their phones or tablets? Do you fear that your child could be texting with strangers? SPYHP lets you read all the text messages they send or receive on their phones without touching their cell phones.`,
            `Do you know that your child send or recieve porn text messages on their phones ? Do you worried about their texting habits? With SPYHP sms tracker, you can remotely check all the sent and received text messages on your kid's and loved one's phone even when these sms may have been deleted from the device.`,
            `The application, when installed, takes a back-up of the SMS history from the devices. It instantly uploads the data to your user account, for you to monitor.`,
            `If your children were spending too much time on the phone calls. Then, you might want to know with whom they were chatting, isn’t it? Then, the SPYHP SMS tracker feature can help you a lot.`,
            `No root required to access our application so, your mobile phone warranty will in action.`
        ],
        image : [
            `View_Sms_Messages/view_sms_messages1`
        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-envelope`,
                text : `View sent and received messages`
            },
            {
                image : `fas fa-file-alt`,
                text : `Read full content of all messages.`
            },
            {
                image : `far fa-clock`,
                text : `Get time and date stamps of all SMS.`
            },
            {
                image : `fas fa-desktop`,
                text : `All SMS messages are upload to your SPYHP dashboard panel and you can access it from anywhere.`
            }
        ],
        faq : [
            {
                title : `Why SPYHP find this feature useful?`,
                content : `Sometimes knowing what your child are texting can prevent real trouble You can View sent and received messages and Read full content of all messages also know Get time and date stamps of all SMS.All text messages are neatly organized into your SPYHP Control Panel and you can access them anytime.`
            },
            {
                title : `Can I access both sent and received messages off the monitored device?`,
                content : `Yes, SPYHP can monitor both sides of a text conversation on the monitored device. Therefore, you can read all text messages sent or received from the monitored person’s cell phone.`
            },
            {
                title : `Can I access deleted text messages conservation?`,
                content : `Yes, SPYHP can monitor both sides of a text conversation on the monitored device. Therefore, you can read all text messages sent or received from the monitored person’s cell phone.`
            }
        ]
    },
    {
        id : 4,
        title : `View MMS messages`,
        content : [
            `With SPYHP, you can remotely check all the MMS and multimedia contents inside it. Instantly check names and number of sender and other details like time and date stamps of any MMS which is sent and received on target device.`,
            `For those who need to monitor MMS messages.SPYHP MMS tracker gives you full content including audio,photo & video of every MMS from the target device. Do you know what type of media your child share in MMS with friends? SPYHP understands all your worries and gives you full copy of their MMS messages including media directly to your SPYHP dashboard panel.`
        ],
        image : [

        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-envelope`,
                text : `View sent and received MMS messages.`
            },
            {
                image : `far fa-image`,
                text : `View pictures inside the MMS messages.`
            },
            {
                image : `fas fa-video`,
                text : `View videos inside the MMS messages.`
            },
            {
                image : `fas fa-volume-up`,
                text : `Get audios inside the MMS messages.`
            },
            {
                image : `far fa-clock`,
                text : `Get time and date stamps of all MMS.`
            },
            {
                image : `fas fa-download`,
                text : `Download media content of any MMS directly from your SPYHP dashboard panel.`
            }
        ],
        faq : [
            {
                title : `Have you enabled MMS feature at SPYHP dashboard?`,
                content :  `Go to settings, enable 'MMS' from 'Individual Feature On/Off' panel.`
            },
            {
                title : `Is auto download option checked in monitoring phone?`,
                content : [`MMS will be only available once it is downloaded.`,`Only Images and audio files will be displayed.`]
            },
            {
                title : `Why you need to spy on their MMS messages?`,
                content : `For those who are unaware an MMS message is a standard SMS message which has media inside it such as pictures or videos. SPYHP and its MMS message spying feature gives you the complete picture when it comes to reading their MMS messages. Unlike competing products which may be happy with just the MMS contact name and phone number or who does not even offer the ability to spy on their MMS messages, SPYHP understands the value of information and gives you a complete duplicate of their MMS messages including the most important part – the media.`
            }
        ]
    },
    {
        id : 5,
        title : `Track sender's details`,
        content : [
            `With SPYHP, you can remotely check all the sent and received text messages on your kids device and Instantly check names and numbers of sender and other details like time and date stamps.`,
            `Are you concerned with whom your children may be texting till mid night? Do you want to get contact details of their all conversations in sms and mms? SPYHP helps you to see contact names of any message, number associated with any conversation.Don’t miss any more; View secret contacts & numbers of each message very easily.`
        ],
        image : [

        ],
        step : [
        ],
        'facility' : [
            {
                image : `fas fa-th`,
                text : `View sender’s name and number from SMS and MMS.`
            },
            {
                image : `fas fa-user-secret`,
                text : `Get details about secret contacts & numbers in any message.`
            },
            {
                image : `fas fa-upload`,
                text : `All the data is directly upload to your SPYHP dashboard panel.`
            }

        ],
        faq : [
            {
                title : `Does SPYHP trace sender's details?`,
                content :  `Yes using SPYHP, you can remotely check all the sent and received text messages on your kids device and Instantly check names and numbers of sender and other details like time and date stamps also you Get details about secret contacts & numbers in any message.`
            }
        ]
    },
    {
        id : 6,
        title : `View current Location`,
        content : [
            `With SPYHP, you can remotely monitor current location of your children and loved ones directly from your SPYHP dashboard using inbuilt dashboard map.`,
            `Do your children keep lying to you about where they are going? Would you like to be able to watch them with details of exactly where they’ve been and when? SPYHP allows you to instantly track their phone location. Location feature enables you to see exactly where they are with inbuilt map in your dashboard. Keeping eyes on your kids with SPYHP is your best way of protecting them to give a happy and secure life.`,
            `This is very handy feature that every parents need! Depending only on the GPS connection doesn't always enough. Most of the users turn off their GPS & data plans. To handle this situations where GPS can not deliver Location tracking results, SPYHP introduced it's location tracker without GPS. It is no longer difficult for parents to track the whereabouts of their children even in the absence of GPS.`
        ],
        image : [

        ],
        step : [
        ],
        'facility' : [
            {
                image : `far fa-map`,
                text : `Monitor their current location on a detailed map.`
            },
            {
                image : `fas fa-map-marker-alt`,
                text : `Identify their location even when regular GPS is unavailable.`
            },
            {
                image : `fas fa-desktop`,
                text : `Access all location information directly from your SPYHP dashboard Panel.`
            }

        ],
        faq : [
            {
                title : `How to view current location?`,
                content :  `You can remotely monitor current location of your children and loved ones directly from your SPYHP dashboard using inbuilt dashboard map.`
            },
            {
                title : `Why am I getting many location data?`,
                content :  `Increase 'GPS Distance Interval' to 500 meters or more.`
            },
            {
                title : `How will I get the instant location using SMS command?`,
                content : `First use SMS command for instant Location (x9p9f9 LC 1) then Location link will be sent back on the same number.`
            },
            {
                title : `How long location currency of SPYHP?`,
                content : [`SPYHP Location finding technique is so far better, We have below three different ways to get locate its work even device always keep GPS and internet off.`,`We are supporting below three way to find location of device.`,`using GPS (very accurate about 50 meter, but may not work in device inside building)
                using device internet (accurate about 100 Meter, need internet availability)
                using device cellular tower information. ( work without internet, GPS, but low accuracy about 1-5KM)
                To get accurate result we would suggest to enable "Settings >> Location and security >> Use wireless networks".
                Try our "SMS Command" to get location back on your mobile.`]
            },
            {
                title : `How to fix GPS shows incorrect result?`,
                content : [`Our location finding technique is so far better, have below three different way to get locate its work even device always keep GPS and internet off. - We are supporting below three ways to find location of device.`,`
                using GPS (very accurate about 50 meter, but may not work in device inside building)`, `using device internet (accurate about 100 Meter, need internet availability)`, `using device cellular tower information. ( work without internet, GPS, but low accuracy about 1-5KM)`,`To get accurate result we would suggest to enable "Settings >> Location and security >> Use wireless networks".`]
            }
        ]
    },
    {
        id : 35,
        title : `Pantau Rekaman Panggilan VoIP`,
        content : [
            `<a href="https://raisingteenstoday.com/teens-addicted-to-their-phones/">purchase, Nearly 80% of teens </a>in the new survey said they checked their phones hourly, and 72% said they felt the need to immediately respond to texts and social networking messages.Are you aware of who they are speaking to or what they are talking about? it’s really impossible for parents to be watching all the time. Nor, should they have to – as trust is a very important part of a child’s development. SPYHP gives you the facility to know if something is happening before it goes too far and allows you step in to rectify a potentially dangerous and costly situation for your child.`,
            ,`Now you can Monitor VOIP Call Recordings with a <a href="../../install-guide/Android">latest version of SPYHP.</a>Install SPYHP's Latest version in your Cell Phone which you want to monitor and monitor all Whatsapp, Facebook, Viber Call Recordings Remotely in Your SPYHP Control Panel. This feature is perfect for anyone who uses an Android and spends a lot of time on Whatsapp, Facebook, Viber application. Recording these calls is extremely useful for parents who are responsible for them child individuals.They are essentially the same as a normal phone call. The call is just being routed over the internet instead of using the instant messenger program.`,
            `
                <ul>
                    <li>Captures calls silently without the user getting to know.</li>
                    <li>Capture any number or duration of calls.</li>
                    <li>Captures information directly upload User control Panel.</li>
                    <li>Support any number of android devices.</li>
                </ul>
            `,
            `Recording calls are completely mobile specific. It might work in some mobiles and might not work in some other.`
        ],
        image : [
            `Monitor_VoIP_Call_Recordings/monitor_voIP_call_recordings1`
        ],
        step : [

        ],
        'facility' : [
            {
                image : `fas fa-phone-square-alt`,
                text : `Track all Voip Call Recordings`
            },
            {
                image : `fas fa-search`,
                text : `Find out names of people they have been chatting with.`
            },
            {
                image : `far fa-clock`,
                text : `View time and date stamps.`
            },
            {
                image : `fas fa-desktop`,
                text : `Access captured call-recordings directly from your SPYHP dashboard.`
            }
        ],
        faq : [
            {
                title : `Can you record VOIP calls?`,
                content : `Yes, You can record VOIP calls using latest version of SPYHP. For that system requires one time installation in device which you want to monitor after successfully installed and register you can track all voip call recordings on SPYHP user portal. Recording calls are completely mobile specific. It might work in some mobiles and might not work in some other.`
            },

        ]
    },
    {
        id : 34,
        title : `Monitor Kid's IMO Chats`,
        content : [
            `Imo is an instant messenger with video and voice calling features. It might not be as popular as messengers such as <a href="../features/12">WhatsApp</a> or <a href="../features/31">Instagram</a> do you wonder if it is possible to hack imo account and spy on someones imo messages? here will introduce an application that incorporates the best in technology to provide you with the best monitoring results you can ask for.`,
            `
                <ul>
                    <li>You only need one time access to the target device for a simple and short installation</li>
                    <li>It doesn’t matter if your target device’s imo account is password protected</li>
                    <li>It doesn’t matter if a message is deleted. You will receive it before it is deleted</li>
                    <li>You kids and employees will never know their imo account is under control</li>
                </ul>
            `,
            `They may talk to anybody including total strangers and they may share any sort of photos or videos without considering its consequences.In this case a spy tool can help partners to hack imo and put an end to all their doubts.Spy on the entire IMO Chat with sent and received messages with the exact date and time stamps SPYHP provides you with im monitoring features as well many other handy features.`
        ],
        image : [
            `Monitor_Kid's_Imo_Chats/monitor_kid's_imo_chats1`
        ],
        step : [

        ],
        'facility' : [
            {
                image : `far fa-comment-dots`,
                text : `Track all IMO Incoming-Outgoing chats.`
            },
            {
                image : `fas fa-users`,
                text : `View Group Chats with details`
            },
            {
                image : `far fa-clock`,
                text : `View time and date stamps.`
            },
            {
                image : `fas fa-desktop`,
                text : `Access all captured IMO information directly from your SPYHP dashboard.`
            }
        ],
        faq : [
            {
                title : `Can I monitor IMO messages remotely?`,
                content : `Yes, For that you need to rooted your target device. The app requires one-time installation on the device you wish to monitor and register with your email id and password. After that you can remotely monitor all IMO chats on Control panel.`
            },
            {
                title : `Can I track messages on non-remoted phone?`,
                content : `No, You can not. You need your phone rooted for accessing IMO Incoming-Outgoing messages. You need to give Root Permission on your monitoring device.`
            },
            {
                title : `Can I monitor deleted IMO messages using SPYHP?`,
                content : `SPYHP works in real-time, so it only takes a few seconds for the upload messages on your account However IMO messages are deleted to the cellphone you can monitor those in Control panel`
            },
        ]
    },
    {
        id : 33,
        title : `Monitor Zalo Messenger`,
        content : [
            `As the new market-leading messaging app with amazing features, Zalo is becoming increasingly popular among people of different ages, especially teenagers. Now, parents can eliminate worries by using <a href="../../">SPYHP - Parental Monitoring Software</a> to Spy on Zalo chat on Android Cell phone, with which parents can keep an eye on their children’s phone in a discreet and tamper proof mode.`,
            `Zalo is the new market-leading messaging app with amazing features. It is a popular instant messaging application that allows you to chat and share information with friends.`,
            `It is Running on every Android device. Simply install SPYHP on to a rooted Android device and register with your credentials. It will upload all Zalo conversion in your SPYHP Control Panel.`,
            `Choose the best monitoring application which supports Zalo.
            Download and install the application on your target device.
            Start remote monitoring from your personal Control Panel.`
        ],
        image : [
            `Monitor_Zalo_Messenger/monitor_zalo_messenger1`
        ],
        step : [

        ],
        'facility' : [
            {
                image : `fas fa-comments`,
                text : `Track all Zalo chats`
            },
            {
                image : `fas fa-info-circle`,
                text : `Check sender’s name and number.`
            },
            {
                image : `fas fa-search`,
                text : `Find out names of people they have been chatting with.`
            },
            {
                image : `far fa-clock`,
                text : `View time and date stamps.`
            },
            {
                image : `fas fa-desktop`,
                text : `Access captured information directly from your SPYHP dashboard.`
            }
        ],
        faq : [
            {
                title : `How can I spy on Zalo messenger using SPYHP?`,
                content : `With SPYHP Zalo spy software, all Zalo text messages will be automatically uploaded to your control panel from where you can access them anytime.`
            },
            {
                title : `Can I spy on Zalo messenger on non-rooted phone?`,
                content : `No, You can not monitor zalo messages, in Non-rooted device. For, accessing zalo incoming-outgoing messages you need to rooted your target device.`
            },
            {
                title : `Can I Monitor deleted messages in the  monitored device?`,
                content : `Yes, SPYHP provides you all the monitored Zalo logs from your SPYHP Dashboard you can also download it in your personal computer.`
            },
        ]
    },
    {
        id : 26,
        title : `One click access to visited websites`,
        content : [
            `SPYHP allows you to monitor and view when each web site visited and how often it was visited in the target device’s web browser.`,
            `Do you know 35% childs say their parents know nothing about what they do on internet. Do you want to become this type of parents? No then, choose SPYHP because SPYHP allows you to see all your target user’s web browsing activities with time & date stamps.Provide one click access facility to each visited site directly from your SPYHP dashboard & gives you details about your child's most viewed websites.`,
        ],
        image : [

        ],
        step : [

        ],
        'facility' :[
            {
                image : `far fa-hand-point-up`,
                text : `You can easily access all visited web sites just by a single click from your SPYHP dashboard.`
            },
            {
                image : `fas fa-list-ol`,
                text : `View number of times a website has been visited.`
            },
            {
                image : `fas fa-desktop`,
                text : `All web browsing history is directly upload to your SPYHP dashboard panel.`
            },
        ],
        faq : [

        ],
    },

    {
        id : 27,
        title : `Daily email alert with summary`,
        content : [
            `Do you worried about your children when they are away from you? Do you feel helpless when you are not able to access dashboard panel? SPYHP's Email alert facility helps you to relax with your child because no matter where you are; SPYHP keeps you updated about your child's activity 24/7 with daily summary of your child's activity. With SPYHP, you will get daily summary of last day monitoring with records directly to your mailbox. There won’t be a single detail you will miss out.`,
        ],
        image : [

        ],
        step : [

        ],
        'facility' :[
            {
                image : `fas fa-inbox`,
                text : `View full overview of monitoring data from Inbox.`
            },
            {
                image : `fas fa-phone-alt`,
                text : `View number of calls made.`
            },
            {
                image : `fas fa-envelope`,
                text : `Get details about number of SMS.`
            },
            {
                image : `fas fa-list-ol`,
                text : `View number of Websites visited.`
            },
            {
                image : `far fa-hand-point-up`,
                text : `One click access link to your dashboard.`
            },
            {
                image : `fas fa-info-circle`,
                text : `View target device info.`
            },
        ],
        faq : [

        ],

    },

    {
        id : 28,
        title : `Alert on specific location`,
        content : [
            `With SPYHP, you can remotely monitor and get instant alerts when your children enter or leave the location that you have specified on your inbuilt dashboard map.`,
            `Do you worried about your children when they are away from you? SPYHP instant alert facility helps you to relax with your child because no matter where you are; SPYHP keeps you updated on your children 24/7. With Location alerts, there won’t be a single place you will miss out on.Instantly receive alerts when your child leave or enter to a specific location.`,
        ],
        image : [

        ],
        step : [

        ],
        'facility' :[
            {
                image : `fas fa-globe-asia`,
                text : `Set number of specific locations without any limit.`
            },
            {
                image : `far fa-bell`,
                text : `Get instant alerts each time your child enter or leave these locations.`
            },
            {
                image : `far fa-clock`,
                text : `Check time and date stamps.`
            },
            {
                image : `fas fa-inbox`,
                text : `View all details about specific location directly to your mailbox.`
            },
        ],
        faq : [
            {
                title : `How can I get specific location?`,
                content : [
                    `You can remotely monitor and get instant alerts when your children enter or leave the location that you have specified on your inbuilt dashboard map.SPYHP keeps you updated on your children 24/7. With Location alerts, there won’t be a single place you will miss out on.Instantly receive alerts when your child leave or enter to a specific location.`,
                ],
            },
            {
                title : `How many location I can get with SPYHP?`,
                content : [
                    `You can Set number of specific locations without any limit also Get instant alerts each time your child enter or leave these locations.and View all details about specific location directly to your mailbox.`,
                ],
            }
        ],

    },

    {
        id : 29,
        title : `Alert on SIM change`,
        content : [
            `With SPYHP you are instantly notified every time your child changes SIM card in target device.You do not need to worry about whether SPYHP will work or not when the target user changes SIM card because SPYHP is not a SIM dependent monitoring software so if your child changes SIM card very often, you will not miss an single SIM change alert.`,

            `This means you do not need to worry about whether or not SPYHP will stop working as soon as the TARGET phone changes SIM card. SPYHP makes it easy and convenient to monitor others and SIM change alerts are only one part of a wide range of great SPYHP features.`,

            `As a parent, you will be able to make sure that your child is not using a different SIM card to evade your supervision. This will help you keep a check on your child’s smartphone usage.`,
        ],
        image : [
            `Alert_On_Sim_Change/alert_on_sim_change1`,
        ],
        step : [

        ],
        'facility' :[
            {
                image : `far fa-bell`,
                text : `Get alerts on SIM card changes.`
            },
            {
                image : `fas fa-search`,
                text : `Find SIM card ID of the new SIM inserted.`
            },
            {
                image : `far fa-clock`,
                text : `Get alert with date and time stamp.`
            },
            {
                image : `fas fa-envelope`,
                text : `Get alerts via email.`
            },
        ],
        faq : [
            {
                title : `Does my service stop, id I change SIM card?`,
                content : [
                    `No, System will send you SIM Change notification over email. You may or may not able to get phone number of newer SIM. Using "Call Back" feature in "Locate Device", you can receive call from newer SIM Card.`,
                ]
            },
        ],

    },

    {
        id : 30,
        title : `View Installed Application`,
        content : [
            `SPYHP takes a regular back-up of the App log information from the target device. and It instantly uploads the data to your <a href="https://SPYHP.net/demo/dashboard.html">SPYHP account,</a> for you to monitor.`,

            `As a parent, you will be able to monitor what Apps your kid is using. This will help you keep them safe from harmful apps. SPYHP gives you a complete control that whatever application your child installed in his/her device.`,

            `You can able to see how many times and which application your child refered.Also No need to root your target android device to use this feature.You can also block or unblock application remotely in target CellPhone which you don't want to access your child.`,
        ],
        image : [
            `View_Installed_Application/view_installed_application1`,
            `View_Installed_Application/view_installed_application2`,
        ],
        step : [

        ],
        'facility' :[
            {
                image : `fas fa-comments`,
                text : `Monitor all Installed Application`
            },
            {
                image : `fas fa-search`,
                text : `Find out adult installed application`
            },
            {
                image : `fas fa-ban`,
                text : `Block or Unblock installed application`
            },
            {
                image : `far fa-clock`,
                text : `View time and date stamps.`
            },
            {
                image : `far fa-image`,
                text : `View application icon and total usage`
            },
            {
                image : `fas fa-desktop`,
                text : `Access installed application directly from your SPYHP dashboard.`
            },
        ],
        faq : [
            {
                title : `How can I monitor installed application in android cellphone?`,
                content : [
                    `For that system requires one time installation in android CellPhone which you want to monitor. After then login into your SPYHP web account and Go to "Packages Installed" page here you can monitor all applicaton which are installed in target device. You can also monitor Date and time of application which date and time application installed in the Cell Phone.`,
                ],
            },
            {
                title : `Can I view all installed application list to the SPYHP dashboard?`,
                content : [
                    `Yes You can see entire application that installed on your target Cellphone which you have installed SPYHP application.`,
                ],
            },
            {
                title : `Does this feature needs rooted uour target cellphone?`,
                content : [
                    `Yes You can see entire application that installed on your target Cellphone which you have installed SPYHP application.`,
                ],
            },
            {
                title : `Why I can not see application usage graph in my web account?`,
                content : [
                    `Follow below settings on your SPYHP web account: (1) Open your SPYHP Dashboard ==> Go to "Locate Device" (2) In that you can find "Allow app usage" click on that. (3) That will pop-up one menu in your target device click allow application permission.`,

                    `After it will start to update application time and usage in your SPYHP web account.`,
                ],
            }
        ],

    },

    {
        id : 31,
        title : `Monitor Child Phone Instagram`,
        content : [
            `Instagram has rapidly gained popularity, with one million registered users in two months, 10 million in a year, and ultimately 800 million as of September 2017. Instagram has been the subject of criticism due to users publishing images of drugs they are selling on the platform. 85% of those total users are only 12- to 28-year olds.Start using <a href="../../">SPYHP - Parental Monitoring Software</a> o monitor popular social communication like Instagram.`,

            `<ul>
                <li>Monitor Instagram chats with date and timestamp.</li>
                <li>View images that received in Instagram social app.</li>
                <li>Access received information remotely on your SPYHP control panel.</li>
            </ul>`,

            `How to spy on instagram messages`,
            `<ul>
                <li>Install SPYHP on your rooted android device.</li>
                <li>Instagram capture messages immediately.</li>
                <li>Start upload data automatically on your control panel.</li>
                <li>Login into your SPYHP Dashboard.</li>
                <li>Select tab "Instagram" to the popular social app list.</li>
                <li>Monitor all recorded conversion and images.</li>
                <li>Download the conversion to the "Data Liberation page".</li>
            </ul>`,
        ],
        image : [
            `Monitor_Child_Phone_Instagram/Monitor_child_phone_instagram1`,
        ],
        step : [

        ],
        'facility' :[
            {
                image : `fab fa-instagram`,
                text : `Track all Instagram chats.`
            },
            {
                image : `fas fa-info-circle`,
                text : `Check sender’s name and number.`
            },
            {
                image : `far fa-clock`,
                text : `View time and date stamps.`
            },
            {
                image : `fas fa-desktop`,
                text : `Access captured information directly from your SPYHP dashboard.`
            },
        ],
        faq : [
            {
                title : `How can I track Instagram chats remotely?`,
                content : [
                    `Instagram chats can be tracked remotely using SPYHP. The app requires one-time installation on the device you wish to monitor and register with your email id and password. After that you can remotely track all Instagram chats on Control panel.`,

                    `1.  Instagram data not uploading`,
                    `SPYHP  can not capture Instagram messages in non-rooted device. You need to give root permission to your target device for that you can refer <a href="https://SPYHP.net/blog/how-to-root-the-phone">Root Permission</a>.`,
                ]
            },
            {
                title : `Does Instagram need rooted phone?`,
                content : [
                    `Yes, You need your phone rooted for accessing Instagram Incoming-Outgoing messages. You need to give root permission on your monitoring device.`,
                ]
            },
            {
                title : `Can I monitor deleted Instagram messages using SPYHP?`,
                content : [
                    `SPYHP works in real-time, so it only takes a few seconds for the upload messages on your account However messages are deleted to the cellphone you can monitor those in Control panel`,
                ]
            }
        ],

    },

    {
        id : 32,
        title : `How to Monitor Spy Child Phone Snapchat`,
        content : [
            `Snapchat leaves you no trace of what you shared, unless you take a screenshot and save it in your phone. Hence, you have to share it repeatedly to be available for others to see. One of the principal concepts of Snapchat is that pictures and messages are only available for a short time before they become inaccessible. Therefor SPYHP record all incoming-outgoing messages before that inaccessible.`,

            `Snapchat can be very addictive to young and even to the old people because of the filters and Stories that are easy to get hooked on. Now you can view the name of snapchat so conversion will never be a missed. Here, you can also Download all snapchat messages. start using <a href="../../">SPYHP - Parental Monitoring Software</a> to spy snapchat popular application.`
        ],
        image : [
            `How_To_Monitor_Spy_Child_Phone_Snapchat/how_to_,monitor_spy_child_phone_snapchat1`,
        ],
        step : [

        ],
        'facility' :[
            {
                image : `fas fa-download`,
                text : `Install SPYHP on your Non-Rooted Android Cell phone`
            },
            {
                image : `fab fa-snapchat`,
                text : `Track all Snapchat chats.`
            },
            {
                image : `fas fa-search`,
                text : `Find out names of people they have been chatting with.`
            },
            {
                image : `far fa-clock`,
                text : `View time and date stamps.`
            },
            {
                image : `fas fa-desktop`,
                text : `Access captured information directly from your SPYHP dashboard.`
            },
        ],
        faq : [
            {
                title : `How can I spy on Snapchat messages using SPYHP?`,
                content : [
                    `Access captured information directly from your SPYHP dashboard.`,
                ]
            },
            {
                title : `Can I spy Snapchat messages on non-rooted phone?`,
                content : [
                    `Access captured information directly from your SPYHP dashboard.`,
                ]
            },
            {
                title : `Can I download the monitor Snapchat messages and multimedia to my computer?`,
                content : [
                    `Access captured information directly from your SPYHP dashboard.`,
                ]
            }
        ],

    },

    {
        id : 36,
        title : `Monitor & Block Installed Mobile Application`,
        content : [
            `Being a parent isn’t just about the happiness of bringing up a person but also about your responsibility towards their safety, personality and upbringing. Kids, especially in their teens require a lot of supervision. <a href="../../">Parental monitoring software</a> has always been about knowing if your child is safe, that he/she isn’t making friends with people they shouldn’t, not going to places that can be risky and such things. Today, the facets still remain the same but the ways are different. With smartphones playing a bigger role in the lives of today’s kids, it becomes the parent’s responsibility to ensure that the technology is being used for the good. According to a survey, almost 56% of kids aged between 8 and 12 years own a smart phone. It might be reassuring to know where your kids are and whether they are safe, but at the same time, smartphones can expose your kids to platforms that can be disastrous.`,

            `Parents across the globe are already voicing their concerns about the amount of time their kids are spending with their smartphones, rather than the playground. With internet being a one stop platform for all kinds of information and interaction, it necessitates that you mark a line somewhere. Smartphones have become a top reason why students get distracted from studies and other activities they should spend time in.`,

            `Kid monitoring apps were conceived out of these needs. Parents, across the globe are using a wide variety of spy technologies to ensure that their kids are using technology responsibly and that they aren’t exposed to dangerous platforms and interactions. One of such mobile app monitoring and blocking software is offered by SPYHP. The SPYHP App Monitor and Blocker will help parents to monitor their kid’s online activity without them knowing and thus allow them to intervene before anything goes in the wrong direction. It could be about blocking certain websites, blocking certain users in your kid’s social accounts, monitoring the amount of time your kid spends online and ensuring that no confidential data is shared unknowingly.`,

            `Parental monitoring with apps like SPYHP isn’t about intruding the private/personal space of your kids but about keeping them safe, while encouraging positive use of technology.`,
        ],
        image : [
            `Monitor_And_ Block_Installed_Mobile_Application/monitor_and_ block_installed_mobile_application1`,
        ],
        step : [
            {
                title : `You need to physical access to the device to download and install SPYHP in phone`,
                content : [
                    `isinya textnya aja`,
                    `<img src="../../../Assets/lanjut" alt="nama image">`
                ]
            }
        ],
        'facility' :[
            {
                image : `fas fa-search`,
                text : `Monitor applications: The apps that your kid is using and communicating through.`
            },
            {
                image : `fas fa-download`,
                text : `Viewing installed applications: Check the kind of content your kid is interested and/or exposed to.`
            },
            {
                image : `far fa-file-video`,
                text : `View audio/video/photo: Ensure that your kid isn’t sharing/accessing content that he/she shouldn’t.`
            },
            {
                image : `fas fa-ban`,
                text : `Application blocking: Put a stop to applications that are getting too intrusive or risky.`
            },
            {
                image : `fas fa-chart-line`,
                text : `Application activity: Monitor the kind of content shared, information accessed and time spent on each.`
            },
            {
                image : `far fa-clock`,
                text : `View Date & Time for install/uninstall: Even if your kid goes to the length of installing, using and uninstalling an application that he/she shouldn’t have accessed, you get to know the exact time and date of the activity.`
            },
        ],
        faq : [
            {
                title : `How to monitor applicaiton using SPYHP?`,
                content : [
                    `View Date & Time for install/uninstall: Even if your kid goes to the length of installing, using and uninstalling an application that he/she shouldn’t have accessed, you get to know the exact time and date of the activity.`,
                ]
            },
            {
                title : `How to block/unblock application?`,
                content : [
                    `View Date & Time for install/uninstall: Even if your kid goes to the length of installing, using and uninstalling an application that he/she shouldn’t have accessed, you get to know the exact time and date of the activity.`,
                ]
            },
        ],

    }
]

let initFeature = () => {

    let URL = window.location.href;
    const lang = getLang(URL);

    let urlId = URL.slice(-2);

    if(urlId.charAt(0).match('/')){
        urlId = urlId.slice(1)
    }
    const dataArray = lang.match('id') ? listFeature.filter(satu => satu.id == urlId) : listFeature_en.filter(satu => satu.id == urlId);
    const data = dataArray[0];

    const featureTitle = document.getElementById('feature-title')
    const featureContent = document.getElementById('feature-content')
    const featureImage = document.getElementById('feature-image')
    const stepList = document.getElementById('step-list')
    const spyhpBenefit = document.getElementById('spyhp-benefit')
    const featureFaqList = document.getElementById('feature-faq-list')


    featureTitle.innerHTML = data.title

    data.content.forEach(content => {
        return featureContent.innerHTML +='<p>'+content+'</p>'
    })

    data.image.forEach(image =>{
        return featureImage.innerHTML += `
        <div class="col-12 text-center">
             <img src="../../../Assets/Picture/Features/Feature_Details/${image}.png" alt="${image}">
        </div>
        `
    })

    if(data.id != 1){
        data.step.forEach((step,i) => {
            stepList.innerHTML += `
            <div class="d-flex flex-column text-justify my-2">
                <span class="">Step ${i+1} : ${step.title}</span>
                  <div id="step-content-list-${i}">
                </div>
            </div>
            `

        })
    }else{
        data.step.forEach((step,i) => {
            stepList.innerHTML += `
            <div class="d-flex flex-column text-justify">
                <h2 class="subheading">Step ${i+1} : ${step.title}</h2>
                <div id="step-content-list-${i}">
                </div>
            </div>
            `
            const stepContentList = document.getElementById('step-content-list-' + i);
            step.content.forEach((content, i) => {
            return  stepContentList.innerHTML += `
                    <p>${content}</p>
                `
            })
        })
    }



    data.facility.forEach((facility,i) => {
       return  spyhpBenefit.innerHTML += `
        <div class="d-flex align-items-center my-4">
             <i class="${facility.image} fa-2x color-secondary-green-dark mr-3" aria-hidden="true"></i> <span>${facility.text}</span>
         </div>
        `
    })

    data.faq.forEach((faq,i) => {
        featureFaqList.innerHTML += `
        <div class="features-item features-alt-item my-3">
            <input type="checkbox" id="features${i}" name="feature" class="features-input" onclick="Rotates(event)">
            <label for="features${i}" class="features-label features-alt-label mb-0">
                <span class="features-title features-alt-title subheading text-dark">${i+1}. ${faq.title}</span>
                <i class="fas fa-caret-down features-arrow fa-2x text-dark" id="arrow${i}"></i>
            </label>
            <span class="features-droplist">
                <li class="features-dropitem features-alt-drop-item mt-0">
                    <div class="container-fluid" id="faq-content-${i}">

                    </div>
                </li>
            </span>
        </div>
        `
        const faqContent = document.getElementById('faq-content-' + i)
        if(faq.content instanceof Array){
            faq.content.forEach(content => {
                return faqContent.innerHTML += `
                <p>${content}</p>
                `
            })
        }else{
            return faqContent.innerHTML += `<span>${faq.content}</span>`
        }

    })

}

document.title.match('Feature') &&  initFeature()
