//vendor
require('./bootstrap');
require('slick-carousel');
//utility
require('./rotateCarret');

//custom
require('./pageActivator')
require('./featureGen')
require('./testimony')
