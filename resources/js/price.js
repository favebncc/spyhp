const price_benefit_list = [
    [

        "Pengawasan Audio / Video yang Kuat.",
        "Nikmati Siaran Langsung Audio & Video dari Sekitar Handphone.",
        "Tonton Video Live Tanpa Penundaan.",
        "Mendengarkan Audio Live dari Handphone Dengan Suara yang Jelas.",
        "Mengambil Gambar Live. (Menggunakan Kedua Kamera)",
        "Live Screenshot Layar Handphone.",
        "Fitur Pemantauan Jarak Jauh yang Hebat.",
        "Pantau Panggilan & Kontak",
        "Lihat Log Panggilan",
        "Lihat Waktu dan Tanggal Panggilan",
        "Akses Buku Telepon",
        "Blokir Kontak Spesifik",
        "Pantau Lokasi GPS",
        "Lihat Lokasi Sekarang",
        "Riwayat Lokasi dengan Jalur",
        "Setel Geofence",
        "Dapatkan Email Peringatan di Geofence",
        "Pantau dari Web",
        "Lihat Semua Website yang Dikunjungi",
        "Dapatkan Waktu & Tanggal dari Setiap Website yang Dikunjungi",
        "One Click Access Untuk Semua Website yang Dikunjungi",
        "Peringatan Instan 24/7",
        "Email Peringatan dengan Ringkasan Setiap Hari",
        "Peringatan Tentang Lokasi Khusus",
        "Peringatan Tentang Perubahan SIM",
        "Sistem Peringatan Kejadian",
        "Memantau Aplikasi",
        "Lihat Aplikasi yang Terinstall",
        "Lihat Audio, Video & Foto.",
        "Blokir Aplikasi",
        "Aktivitas Aplikasi",
    ],
    [

        "Lihat Tanggal dan Waktu untuk Memasang/Menghapus Pemasangan.",
        "Pantau dengan Clipboard",
        "Lihat Teks yang Disalin.",
        "Dapatkan User-id & Password yang disalin untuk akun yang diamankan.",
        "Dapatkan Semua Pesan WhatsApp yang Diteruskan.",
        "Cek Id Email & URL Web",
        "Penjelajah FIle Lanjutan",
        "Akses Jarak Jauh untuk Memori & SD Card Handphone",
        "Lihat Audio, Video & Foto.",
        "Download File Multimedia, Dokumen, Zip.",
        "Memeriksa Semua File untuk Konten Berbahaya",
        "TIdak Perlu Banyak Usaha",
        "Mudah Dipakai",
        "Mudah Dipasang",
        "Hapus Pemasangan / Meningkatkan / Memperbarui.",
        "Pesan Informasi",
        "Portal Web Interaktif",
        "Bekerja Diam-Diam",
        "Menghapus Data Perangkat Sasaran.",
        "Reset Pabrik Perangkat.",
        "Buka Kunci Perangkat untuk waktu tertentu.",
        "Unduh Semua Data Handphone.",
        "Puluhan Perintah SMS (Tidak Menggunakan Internet)",
        "Audio/Video/Foto Live",
        "Perintah perlindungan anti-pencurian terkuat",
        "Lokasi Instan",
        "Mengirim Pesan",
        "Putar Ringtone (Bahkan Dalam Mode Senyap)",
        "Panggil Kembali",
        "jawaban Otomatis",
        "Aktifkan Wifi"
    ],
    [

        "Pantau Semua Aktivitas Sosial",
        "WhatsApp",
        "Facebook",
        "Tinder",
        "Snapchat",
        "Telegram",
        "Instagram",
        "Gmail",
        "Skype",
        "Viber",
        "Line",
        "Kik",
        "Hike",
        "Hangouts",
        "Zalo",
        "IMO",
        "KakaoTalk",
        "BIP messenger",
        "Just talk",
        "BOTIM",
        "Wechat",
        "Pantau SMS dan MMS",
        "Lihat Semua SMS",
        "Lihat Semua MMS",
        "Lacak Detail Pengirim",
        "Penjadwal All-in-one",
        "Jadwalkan Rekaman Audio",
        "Jadwalkan Pengambilan Foto",
        "Jadwalkan Pengambilan Foto Layar (Screenshot)",
        "Penaganan Penjadwal Ganda",
        "Termasuk Live Chat dan Email untuk Dukungan Teknis"
    ]
]

const price_benefit_list_en = [
    [

        "Powerful Audio/video Surveillance",
        "Enjoy Live Audio & Video telecast of phone surroundings.",
        "Watch Live Video without Delay.",
        "Listen Live audio of phone with clear voice.",
        "Take Live Photo.(Using Both camara)",
        "Live Screenshot of phone.",
        "Remote monitoring with Impressive features",
        "Monitor calls & contacts",
        "View Call logs",
        "View time and date of calls",
        "Access phonebook",
        "Block Specific contacts",
        "Monitor GPS location",
        "View current Location",
        "Location history with Path",
        "Set Geofence",
        "Get Email alerts on Geofence",
        "Monitor on Web",
        "View all website visited",
        "Obtain time & date of each visited website",
        "One click access to all visited websites",
        "24/7 Instant Alerts",
        "Every days email Alert with Summary",
        "Alert on Specific Location",
        "Alert on Sim Change",
        "System Event Alerts",
        "Monitor Application",
        "View Installed Application",
        "View Audio,video & photos.",
        "Application blocking",
        "Application Activity",
    ],
    [

        "View date & time for install/un-install.",
        "Monitor with Clipboard",
        "View Copied text like rich,simple text.",
        "Get copid User-id & password for Secured accounts.",
        "Get All WhatsApp forwarded messages.",
        "Check Email ids & Web URLs.",
        "Advanced File explorer",
        "Remote access of phone memory & sd card",
        "View Audio,video & photos.",
        "Download Multimedia,Documents,Zips files.",
        "Check All files for harmful contents.",
        "It is Effortless",
        "Easy to use",
        "Easy to install",
        "Un-install/Update/Renew.",
        "Informational messages",
        "Interactive Web portal",
        "Silent Working",
        "Wipe target device data.",
        "Factory reset device.",
        "Unlock device for specific time.",
        "Download All phone data.",
        "Dozens of SMS commands (No internet use)",
        "Live Audio/Video/Photos.",
        "Strongest Anti-theft protection commands",
        "Instant location",
        "Send message",
        "Play ringtone (Even in Silent mode)",
        "Call Back",
        "Auto answer",
        "Enable Wi-fi"
    ],
    [

        "Monitor All social activities",
        "WhatsApp",
        "Facebook",
        "Tinder",
        "Snapchat",
        "Telegram",
        "Instagram",
        "Gmail",
        "Skype",
        "Viber",
        "Line",
        "Kik",
        "Hike",
        "Hangouts",
        "Zalo",
        "IMO",
        "KakaoTalk",
        "BIP messenger",
        "Just talk",
        "BOTIM",
        "Wechat",
        "Monitor SMS and MMS",
        "View all SMS",
        "View all MMS",
        "Track sender's details",
        "All-in-one Scheduler",
        "Schedule audio recording",
        "Schedule Photo capturing",
        "Schedule screen capturing",
        "Multiple Scheduler handling",
        "Includes 24/7 Live chat and email technical support"
    ]
]

const price = [{
        title: `Berapa lama yang dibutuhkan untuk mendapatkan softwarenya setelah membeli SPYHP?`,
        content: `Anda akan mendapat email yang berisi link untuk mendownload dan panduang pemasangan setelah pesanan anda dikonfirmasi dan pembayaran anda diproses. `
    },
    {
        title: `Berapa lama yang dibutuhkan untuk memasang software pada perangkat sasaran?`,
        content: `Tergantung kecepatan internet anda, dibutuhkan hamper 10-15 menit agar software bisa berjalan pada handphone atau tablet sasaran.`
    },
    {
        title: `Apakah detail pembayaran saya aman dengan SPYHP?`,
        content: `Saat anda melakukan pembelian, Pembayaran anda dijamin akan diterima oleh gateway pembayaran. Anda akan dialihkan ke gateway pembayaran 
        yang banyak digunakan dan 100% aman.`
    },
    {
        title: `Apakah SPYHP akan muncul pada tagihan kartu kredit saya?`,
        content: `Tidak, nama SPYHP tidak akan muncul pada tagihan kartu kredit anda. Semua dan setiap pembelian SPYHP akan dikenakan biaya sebagai, “Teknologi Vartit” atau “Sistem Techinnovative”.`
    },
    {
        title: `Bagaimana saya mendapatkan pengembalian uang jika saya tidak puas dengan SPYHP?`,
        content: `Anda berhak atas pengembalian uang 100% jika Anda tidak puas dengan software SPYHP dalam 7 hari pertama pembelian anda. Namun, 
        pengembalian uang dapat dihindari jika anda meminta bantuan profesional dari Customer Support kami. Harap dicatat bahwa setelah 7, tidak ada 
        kasus pengembalian uang terkait dengan kepuasan umum yang akan dilayani. Untuk semua kasus pengembalian uang dan kasus sebaliknya, 
        silakan periksa halaman Kebijakan pengembalian uang kami.`
    },
    {
        title: `Saya memiliki pertanyaan lain terkait pembelian. Siapa yang saya tanya?`,
        content: `Anda dapat memeriksa FAQ kami atau menghubungi salah satu agen Customer Support kami kapan saja atau kirim email kepada kami di 
        support@tispy.net`
    }
]

const price_en = [{
        title: `How long it takes to get the software after purchasing SPYHP?`,
        content: `You will get the email containing your link to download the software and installation instructions right after your order gets confirmed and your payment processed. `
    },
    {
        title: `How long does it take to install the software on target device?`,
        content: `Depending on the speed of your Internet, it takes almost 10-15 minutes to get the software up and running on the target mobile phone or tablet.`
    },
    {
        title: `Are my payment details safe with SPYHP?`,
        content: `As you make the purchase, be assured that payments will be accepted by Payment Gateways. You will be redirected to 100% safe and secure widely used Payment Gateways.`
    },
    {
        title: `Will SPYHP show up on my credit card bill?`,
        content: `No, SPYHP name will not appear on your credit card bill. All and any SPYHP purchases will be charged as, "Vartit Technology" or "Techinnovative Systems".`
    },
    {
        title: `How can I get a refund if I’m not happy with SPYHP?`,
        content: `You are entitled to a 100% refund if you are not satisfied with SPYHP software within the 1st 7-days of your purchase. However, refunds can be avoided if you request professional help from our Customer Support. Please note that after the initial 7 days, no refund cases related to the general satisfaction will be entertained. For all refund and no-refund cases, please check out our Refund Policy page.`
    },
    {
        title: `I have other purchase-related questions. Who do I ask?`,
        content: `You can either check the complete list of our FAQs or you can contact one of our Customer Support agents anytime or email us at support@tispy.net`
    }
]


initPriceContent = () => {

    price_benefit_list.forEach((content, i) => {
        console.log(i)

        let element = document.getElementById('price-content-' + (i + 1));
        content.forEach((satu, i) => {
            element.innerHTML += `
            <div class="d-flex my-2">
            <div>
              <i class="fas fa-check color-secondary-green-dark"></i>
            </div>
            <div class="ml-2 w-100">
                <span>${satu} ${addUnique(element,i)}</span>
            </div>
            </div>
            `

        })
    })

    let elementContent = document.getElementById('price-content-list');

    price.forEach((satu, i) => {
        elementContent.innerHTML += `
       <div class="d-flex my-3">
                    <h4 class="subheading text-dark mr-4">${i+1}.</h4>
                    <div>
                        <h4 class="subheading text-dark">${satu.title}</h4>
                        <span>${satu.content}</span>
                    </div>
                </div>
       
       `
    })
}



addUnique = (element, i) => {
    if (element.id.match('price-content-1') && i <= 6) {
        return '<span class="unique fa-sm">Unique</span>'
    } else {
        return ''
    }
}

initPriceContent();
