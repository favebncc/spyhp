const features_computer_content = [
    `Menangkap aktivitas langsung sangat mudah dengan SPYHP. Dari beranda SPYHP, Anda dapat mengambil foto langsung.
    Anda juga bisa mendapatkan lokasi instan dan fitur lainnya. Jika handphone Anda dicuri oleh seseorang, dengan mengakses deringkan handphone, maka handphone Anda akan mulai berdering keras. Bahkan Anda tidak dapat menghentikan dering sampai Anda membuka kata sandi.`,

    `Home SPYHP akan memungkinkan Anda untuk melihat layar yang diambil oleh perangkat.

    Banyak pengguna menggunakan SPYHP sebagai penyimpanan gambar mereka. Bahkan jika foto itu dihapus dari perangkat, itu akan tersedia di beranda.
    `,
    `Beranda SPYHP dapat melihat masing-masing dan setiap gambar yang diambil oleh kamera.
    Banyak pengguna menggunakan SPYHP sebagai penyimpanan gambar mereka. Bahkan jika foto itu dihapus dari handphone, akan tetap ada di beranda Anda.`,

    `SPYHP memberi Anda fasilitas untuk menjadwalkan pengambilan foto. Jadwalkan pengambilan foto yang sudah direncanakan sebelumnya.
    Fitur ini mulai mengambil foto untuk waktu tertentu, jika internet tidak tersedia pada waktu itu maka akan mengunggah data tersebut ketika handphone terhubung dengan internet.`,

    `Awasi akun sosial populer pengguna untuk mengetahui tentang apa yang mereka kirim lewat SMS. Dengan SPYHP, Anda dapat memonitor Facebook, Whatsapp, Viber, Line, Hike, Skype, Hangout, Tinder, Kik messages dan email.
    Jika ponsel Anda tidak di-root, Anda hanya akan mendapatkan pesan masuk. Sedangkan handphone yang di-root, Anda akan bisa mendapatkan pesan masuk dan keluar, walaupun pengguna menghapus catatan. Mereka akan tersedia di beranda SPYHP.
    Lihat lebih lanjut: WhatsApp, Facebook, Gmail, Skype, Viber, Line, Kik, Tinder, Telegram, Kakoa, , Hike, Hangouts`,

    `Mengidentifikasi aplikasi dan program mana yang dapat diakses oleh handphone. Memblokir aplikasi yang tidak pantas dari portal SPYHP.
    Anda sekarang dapat memiliki kontrol penuh atas aplikasi yang diinstal pada handphone karena dipantau dengan fitur Aplikasi Blockir SPYHP. SPYHP juga memberikan fitur untuk memblokir panggilan masuk / keluar dan SMS masuk untuk kontak yang tidak diinginkan.
    `,

    `Android Keylogger juga menyediakan sinkronisasi data waktu nyata, yang berarti bahwa setelah penekanan tombol dilakukan pada keyboard, maka handphone yang ditargetkan terlihat di panel kontrol.
    Keylogger gratis terbaik tersedia sebagai fitur penting dari aplikasi SPYHP. Terlepas dari kenyataan, ini adalah perangkat lunak keylogger gratis, ini adalah salah satu perangkat lunak paling canggih.`,

    `Sekarang Anda dapat memonitor semua teks yang disalin oleh anak Anda. Misalnya anak Anda menyalin pesan dan mengirimkannya ke orang lain. Atau anak Anda menyalin beberapa artikel dari web dan mencoba menyiarkan menggunakan aplikasi media sosial apa pun.
    Itu akan secara otomatis ditangkap dan tersedia di bagian clipboard di beranda SPYHP.
    `,

    `SPYHP mencatat semua URL yang dikunjungi pengguna.
    Anda akan dapat melihat apakah seseorang telah melihat sesuatu yang seharusnya tidak dilihatnya! Banyak pengguna secara khusus membeli layanan SPYHP hanya karena fitur ini.
    Lihat lebih lanjut: Situs web, Waktu & tanggal yang dikunjungi`,


    `Apakah Anda tahu aplikasi mana yang paling banyak digunakan di ponsel Anda? SPYHP akan memperlihatkan semua aplikasi yang terinstal di ponsel.
    Ini juga menunjukkan aplikasi mana yang lebih sering digunakan. Sebagai orang tua, ini akan menjadi fitur yang berharga, karena Anda selalu dapat memblokir aplikasi dewasa dari beranda SPYHP. Banyak pengguna memuji SPYHP untuk fitur ini.
    Lihat lebih lanjut: Akses sekali klik, Peringatan email dengan ringkasan, Lokasi tertentu, Peringatan tentang perubahan SIM
    `,

    `Sekarang SPYHP bisa memperhatikan kesehatan anak Anda. Anda dapat memantau kesehatan anak Anda di panel kontrol SPYHP.
    SPYHP bisa menunjukkan kesehatan Anak Anda di panel kontrolnya. Ini menceritakan berapa jauh dia berjalan dan berapa banyak kalori yang dia bakar dari latihannya. Tettapi hanya akan bekerja pada beberapa versi handphone tertentu yang memiliki sensor.`

]

const features_computer_title = [
    "Layar langsung",
    "Screenshot",
    "Pengambilan gambar kamera",
    "Penjadwalan",
    'Pemantau Skype | Facebook',
    'Akses penyimpanan',
    'Keylogger',
    'clipboard',
    'sejarah penelurusan web',
    'aktivitas aplikasi dan blokir',
    'kontrol admin',
]


const features_android_title = [
    "Mengatur telepon",
    "Melacak pesan online",
    "Melacak Lokasi & Geofencing",
    "Melacak File Multimedia",
    "Mengakses Buku Alamat",
    "WhatsApp, Facebook, Kenaikan, Skype, Mail, Pemantauan IM, dan banyak lagi",
    "Memonitor Penggunaan Internet",
    "Memonitor dan Mengontrol Aplikasi dan Program",
    "Penjadwalan",
    "Fitur Langsung",
    "Melacak Acara di Kalender",
    "Mengontrol Aplikasi dan Kontak",
    "Keystroke Logger",
    "Menyalin Teks",
    "Memonitor Kesehatan"

]

const features_android_content = [
    `Semua telepon yang terjadi berada di beranda. Ini juga akan menampilkan nomor penelepon / yang ditelepon, berapa lama setiap telepon berlangsung.
    Jika nomornya terdaftar dalam buku telepon, Anda akan mendapatkan nama / gambar dari nama itu. Sistem juga akan mengirim detail log panggilan ke email terdaftar Anda.
    Lihat lebih lanjut: <a href="./1">Log telepon </a>,<a href="./2">Lihat tanggal dan Waktu telepon</a>`,

    `Anda dapat membaca pesan apa pun yang diterima atau dikirim dari handphone.
    Pesan-pesan tersebut masuk ke akun SPYHP Anda dari nomor pengirim, nomor penerima, data, waktu dan pesan, sehingga Anda dapat membacanya BAHKAN JIKA pemegang handphone menghapusnya. Sistem juga akan mengirim pesan online ke email Anda yang terdaftar.
    Lihat lebih lanjut: <a href="./3">SMS</a>, <a href="./4">MMS</a>, <a href="./5">Rincian pengiriman</a>`,

    `Anda akan selalu tahu dimana anak-anak Anda. Kami memberikan peta interaktif dengan waktu nyata.
    Dapat menerima pemberitahuan saat mereka tiba di tujuan. Dan juga bisa memberitahu Anda jalur yang dilewati oleh ponsel.
    Lihat lebih lanjut: <a href="./6">Lokasi sekarang</a>, <a href="./7">Riwayat lokasi</a>, <a href="./8">Geofence</a>, <a href="./9">Peringatan tentang geofence</a>
    `,

    `Beranda SPYHP dapat melihat masing-masing dan setiap gambar yang diambil oleh kamera.
    Banyak pengguna menggunakan SPYHP sebagai penyimpanan gambar mereka. Bahkan jika foto itu dihapus dari handphone, akan tetap ada di beranda Anda.`,

    `SPYHP memberi akses buku telepon dari handphone pemantau.
    Saat data kontak baru dibuat di handphone Anda, akan terunggah di beranda juga. Anda selalu dapat menggunakannya sebagai cadangan Buku Telepon jika ponsel Anda berhenti bekerja. Ini juga memberikan ketentuan untuk mengunduh kontak dalam format csv.
    Lihat lebih lanjut: <a href="./10">Buku telepon</a>, <a href="./11">Blokir Kontak</a>`,

    `Awasi akun sosial populer pengguna untuk mengetahui tentang apa yang mereka kirim lewat SMS. Dengan SPYHP, Anda dapat memonitor Facebook, Whatsapp, Viber, Line, Hike, Skype, Hangout, Tinder , Kik messages dan email.
    Jika ponsel Anda tidak di-root, Anda hanya akan mendapatkan pesan masuk. Sedangkan handphone yang di-root, Anda akan bisa mendapatkan pesan masuk dan keluar, walaupun pengguna menghapus catatan. Mereka akan tersedia di beranda SPYHP.
    Lihat lebih lanjut: <a href="./12">WhatsApp</a>, <a href="./13">Facebook</a>, <a href="./14">Gmail</a>, <a href="./15">Skype</a>, <a href="./16">Viber</a>, <a href="./17">Line</a>, <a href="./18">Kik</a>, <a href="./19">Tinder</a>, <a href="./20">Telegram</a>, <a href="./21">Kakao</a> , <a href="./22">Hike</a>, <a href="./23">Hangout</a>`,
    `SPYHP mencatat semua URL yang dikunjungi pengguna.
    Anda akan dapat melihat apakah seseorang telah melihat sesuatu yang seharusnya tidak dilihatnya! Banyak pengguna secara khusus membeli layanan SPYHP hanya karena fitur ini.
    Lihat lebih lanjut: <a href="./24">Situs Web</a>,<a href="./25"> Waktu & tanggal yang dikunjungi </a>`,
    `Apakah Anda tahu aplikasi mana yang paling banyak digunakan di ponsel Anda? SPYHP akan memperlihatkan semua aplikasi yang terinstal di ponsel.
    Ini juga menunjukkan aplikasi mana yang lebih sering digunakan. Sebagai orang tua, ini akan menjadi fitur yang berharga, karena Anda selalu dapat memblokir aplikasi dewasa dari beranda SPYHP. Banyak pengguna memuji SPYHP untuk fitur ini.
    Lihat lebih lanjut: <a href="./26"> Akses sekali klik</>, <a href="./27"> Peringatan email dengan ringkasan </a>, <a href="./28"> Lokasi tertentu</a>, <a href="./29"> Peringatan tentang perubahan SIM</a>
    `,
    `SPYHP memberi Anda fasilitas untuk menjadwalkan pengambilan foto. Jadwalkan pengambilan foto yang sudah direncanakan sebelumnya.
    Fitur ini mulai mengambil foto untuk waktu tertentu, jika internet tidak tersedia pada waktu itu maka akan mengunggah data tersebut ketika handphone terhubung dengan internet.`,

    `Menangkap aktivitas langsung sangat mudah dengan SPYHP. Dari beranda SPYHP, Anda dapat mengambil foto langsung.
    Anda juga bisa mendapatkan lokasi instan dan fitur lainnya. Jika handphone Anda dicuri oleh seseorang, dengan mengakses deringkan handphone, maka handphone Anda akan mulai berdering keras. Bahkan Anda tidak dapat menghentikan dering sampai Anda membuka kata sandi.`,

    `SPYHP melacak setiap aktivitas kalender yang dimasukkan ke dalam handphone yang dapat ditampilkan ke dalam Daftar Kalender.
    Klien SPYHP dapat melihat rapat, pengingatan, dan memonitor tugas terjadwal yang dibuat di handphone.`,

    `Mengidentifikasi aplikasi dan program mana yang dapat diakses oleh handphone. Memblokir aplikasi yang tidak pantas dari portal SPYHP.
    Anda sekarang dapat memiliki kontrol penuh atas aplikasi yang diinstal pada handphone karena dipantau dengan fitur Aplikasi Blockir SPYHP. SPYHP juga memberikan fitur untuk memblokir panggilan masuk / keluar dan SMS masuk untuk kontak yang tidak diinginkan.
    `,

    `Android Keylogger juga menyediakan sinkronisasi data waktu nyata, yang berarti bahwa setelah penekanan tombol dilakukan pada keyboard, maka handphone yang ditargetkan terlihat di panel kontrol.
    Keylogger gratis terbaik tersedia sebagai fitur penting dari aplikasi SPYHP. Terlepas dari kenyataan, ini adalah perangkat lunak keylogger gratis, ini adalah salah satu perangkat lunak paling canggih.`,

    `Sekarang Anda dapat memonitor semua teks yang disalin oleh anak Anda. Misalnya anak Anda menyalin pesan dan mengirimkannya ke orang lain. Atau anak Anda menyalin beberapa artikel dari web dan mencoba menyiarkan menggunakan aplikasi media sosial apa pun.
    Itu akan secara otomatis ditangkap dan tersedia di bagian clipboard di beranda SPYHP.
    `,

    `Sekarang SPYHP bisa memperhatikan kesehatan anak Anda. Anda dapat memantau kesehatan anak Anda di panel kontrol SPYHP.
    SPYHP bisa menunjukkan kesehatan Anak Anda di panel kontrolnya. Ini menceritakan berapa jauh dia berjalan dan berapa banyak kalori yang dia bakar dari latihannya. Tettapi hanya akan bekerja pada beberapa versi handphone tertentu yang memiliki sensor.`

]

const fill_features = (jenis) => {

    let features_content = [];
    let features_title = [];

    if (jenis.match("computer")) {
        features_content = features_computer_content.slice();
        features_title = features_computer_title.slice();
    } else {
        features_content = features_android_content.slice();
        features_title = features_android_title.slice();
    }

    for (let i = 0; i < features_title.length; i++) {
        document.getElementById("listOfDropDownFeatures").innerHTML += `
            <div class="features-item features-alt-item">
            <input type="checkbox" id="features${i+1}" name="feature" class="features-input" onclick="Rotates(event)">
            <label for="features${i+1}" class="features-label features-alt-label mb-0">
                <span class="features-title text-capitalize features-alt-title"><img src='../Assets/Icon/list-point.svg' alt="list-point" class="pr-2"> ${features_title[i]}</span>
                <i class="fas fa-caret-down features-arrow fa-2x text-dark" id="arrow${i+1}"></i>
            </label>
            <span class="features-droplist">
                <li class="features-dropitem features-alt-drop-item mt-0">
                    <img src='../Assets/Picture/Features/${jenis.charAt(0).toUpperCase() + jenis.slice(1)}Features/${jenis}-feature-${i+1}.png' alt="${jenis}-features-${i+1}" class="features-alt-image">
                    <span class="px-4">${features_content[i]}</span>
                </li>
            </span>
        </div>
            `;
    }



}

document.title.match("Features computer") && fill_features("computer");
document.title.match("Features android") && fill_features("android");
