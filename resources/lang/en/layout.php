<?php

    return [
        'installGuide'=>'Install Guide',
        'computer'=>'Computer',
        'prerequisite'=>'Prerequisite',

        'home'=>'Home',
        'feature'=>'Features',
        'android'=>'Android SPY',
        'pc'=>'Komputer SPY',
        'price'=>'Price',

        'footerAiO'=>'All-in-one',
        'aio1'=>'Manage Calls',
        'aio2'=>'Track text messages',
        'aio3'=>'Track Multimedia Files',
        'aio4'=>'Social Media Tracker',
        'aio5'=>'Application Monitoring',
        'aio6'=>'Live Panel',
        'aio7'=>'Device Tracker',
        'aio8'=>'Location Tracking',
        'aio9'=>'Access Phone Book',
        'aio10'=>'Internet Monitoring',
        'aio11'=>'Scheduler',

        'testimoni'=>'Testimonials',
        'forYou'=>'SPYHP for you!',

        'info'=>'Legal Info',
        'guide'=>'Privacy Policy',
        'refund'=>'Refund Policy',
        'works'=>'How SPYHP works?',
        'report'=>'Report Violation/Abuse',

        'footer'=>'SOFTWARE INTENDED FOR LEGAL USES ONLY. SPYHP service software is designed for monitoring your children on a smartphone or other device you own it or have proper consent to monitor. It is the violation of the jurisdiction law to install surveillance software, onto a mobile phone or other device you do not have the right to monitor. The law generally requires you to notify users/owners of the device that it is being monitored. The violation of this requirement could result in severe monetary and criminal penalties imposed on the violator. You should consult your own legal advisor with respect to legality of using the Software in the manner you intend to use it prior to downloading, installing, and using it. You take full responsibility for determining that you have the right to monitor the device on which the Software is installed. SPYHP cannot be held responsible if a User chooses to monitor a device the User does not have the right to monitor; nor can SPYHP provide legal advice regarding the use of the Software.',
    ]
?>
