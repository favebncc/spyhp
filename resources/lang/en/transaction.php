<?php

    return [
        'profile'=>'Data diri',
        'pricingName'=>'Nama',
        'pricingEmail'=>'Email ',
        'pricingPhone'=>'No HP',
        'pricingAddress'=>'Alamat',
        'pricingCity'=>'Kota',
        'pricingPostalCode'=>'Kode pos',

        'phName'=>' Nama Anda',
        'phEmail'=>'Alamat Email Anda',
        'phPhone'=>'Nomor HP yang sedang digunakan ',
        'phAddress'=>'Alamat Anda Sekarang',
        'phCity'=>'Kota Anda Sekarang ',
        'phPostalCode'=>'Kode Pos Anda Sekarang',

        'text'=>'Silahkan lanjutkan pembayaran anda. Apabila Transaksi anda sudah sukses, kami akan melakukan konfirmasi ke akun email anda.'
    ]
?>
