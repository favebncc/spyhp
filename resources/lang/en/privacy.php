<?php

return[
    'head'=>'PRIVACY POLICY',
    'text'=>"This Privacy Policy describes the privacy practices for the Website(s) operated by TiSPY (hereinafter 'Website' if singular, or, 'Websites)'. It also describes the choices available to you regarding our use of your personal information and how you can access and update this information.",
    'subheadCollect'=>"INFORMATION COLLECTION AND USE",
    'textCollect'=>'We collect and use two types of information:',
    'poin1'=>'personal information that you supply voluntarily when you sign-up or place an order for any subscription plan of the TiSPY Software, or enter certain other areas of our Website(s) as noted below in this Privacy Policy, and aggregate tracking and site usage information that we gather automatically as you access our Website(s).',
    'poin2'=>'“Personal Information” describes information that (i) can be associated with a specific user and used to identify that person, for example, full name (first and last); (ii) a home or other physical address including street name and city or town; (ii) telephone number; or (iii) online contact information such as an email address or screen name.',

    'textKetentuan'=>'Your provision to us of any information through our Website(s), or through other sites that link to our Website(s), is deemed your consent to this Privacy Policy. Personal Information may be submitted by you in your interaction with the Services. Other information, that is not Personal Information, may be collected automatically by visiting and/or using our Services.',
    'textKebijakan'=>'It is our policy to use the personal information we acquire at our Website(s) for internal uses only and to provide content, products and services to you that you value and request.',
    'textMengesahkan'=>"We do not authorize the use of your personal information by any third party (other than exceptional conditions as described under 'Legal Matters' below). We operate and maintain a variety of online security measures to safeguard and keep your personal information private and secured.",

    'headLink'=>'LINKS TO OTHER WEBSITES',
    'textLink'=>'Our Website includes links to other websites, which privacy practices may differ from those of TiSPY. If you submit personal information to any of those sites, your information is governed by their privacy policies. We encourage you to carefully read the privacy policy of any website you visit.',

    'subheadPrivate'=>'PERSONAL INFORMATION YOU PROVIDE TO US',
    'textPrivate1'=>'Anyone can access our Website(s) without necessity to provide its personal information.',
    'textPrivate2'=>'When you purchase TiSPY Software and install it on a target device you are requested to fill the Billing Information and Payment Method, which should contain your personal information as ID (first name, last name, full address, email and credit card details), which is stored by the Payment Provider, not by TiSPY.',
    'textPrivate3'=>"Based upon the personal information you provide us at registration, we may send you a welcoming e-mail to verify your login and password. We will also communicate with you in response to your inquiries, regarding any information or services you request, and to manage your 'account'. We will communicate with you by e-mail or other method as directed by you. If you decline to supply or maintain with us certain information while using our Website(s), please understand that you may not be able to use or participate in some or all of the features offered through the Website(s).",
    'textPrivate4'=>'We strive to keep all personal information that you voluntarily provide us private. We do not authorize your personal information to be shared with third parties without your consent.',
    'textPrivate5'=>'Certain information supplied by you to our Website(s) may be password-protected, and you should take all necessary measures to protect the secrecy of your password.',
    'textPrivate6'=>'If you use a computer in a public place or share a computer with others, remember to log out/sign out and close your browser window when you finish accessing our Website in order to prevent others from accessing your personal information. You are solely responsible for the control and use of each password you create.',
    'textPrivate7'=>'From time to time, we may obtain e-mail addresses from third party commercial sources to be used for promotional purposes. You may opt out of receiving any messages from us in the future by either clicking the appropriate box, radio button or link in the e-mail message received, or by contacting us as instructed at the end of this Privacy Policy.',

    'subheadAgregat'=>'AGGREGATE INFORMATION THAT IS AUTOMATICALLY COLLECTED. COOKIES. LOG FILES. PIXEL TAGS',
    'textAgregat1'=>'We also collect and store information that is generated automatically as you navigate through our Website(s) to enhance your experience on our Website(s) by using tracking technologies as Cookies, Log Files (including IP addresses) and Pixel tags.',
    'textAgregat2'=>"As you navigate our Website(s) information that we automatically collect is information in 'log files' about your device’s connection to the Internet such as a user’s IP address (the number assigned automatically to your device whenever you use the Internet), length of time spent on the Website(s), and the pages accessed during each visit to the Website(s). We use this information to analyze trends, administer the Website, track user movement on the Website(s), and gather broad demographic information for aggregate use. IP addresses are not linked to personally identifiable information except if necessary to provide certain premium content or services for which you have registered. In general, aggregate information allows us to improve the delivery of our web pages to you and to measure traffic on the Website(s).",
    'textAgregat3'=>"Cookies are small files that your web browser places on your hard drive for record-keeping purposes. By showing how and when visitors use the Website, Cookies help us track user trends and patterns. They also prevent you from having to re-enter your preferences on certain areas of the Website where you have entered preference information before.",
    'textAgregat4'=>'TiSPY also may use Pixel tags (single-pixel image files also known as transparent GIFs, clear GIFs or web beacons) to access Cookies and to count users who visit the Website or open our HTML-formatted e-mail messages.',
    'textAgregat5'=>'The information that is collected through the use of tracking technologies is not a personal information and is used only to track responses to our advertising and promotional efforts.',
    'textAgregat6'=>'In addition, third party advertisers or third party ad servers that place and present advertising on the Website(s) also may collect information from you via cookies, pixel tags or similar technologies. These third party advertisers and ad servers may use the information they collect to help present their advertisements, to help measure and research the advertisements’ effectiveness, or for other uses. Please be aware that we are not responsible for the privacy practices of such other sites. We encourage users to be aware that when they leave our Website(s) for another site, even if through a link, they should read the privacy policies of each and every Website that collects personally identifiable information. The provision of external links to other Website(s) is a common practice on the Internet and it in no way indicates that we have any responsibility or control over the linked company, its products, services, or Website(s).',

    'headOther'=>'OTHER AGGREGATE INFORMATION',
    'textOther1'=>"If we gather demographic information from you (for example, your age, education level or household income), we will not share that information in a manner that identifies you as an individual with any other third party. Any information that might be shared with third parties will be aggregate information only ('aggregate' meaning demographic information speaking only to the broad characteristics of a group), unless we are required to release additional information under conditions described in the section 'Legal Matters'.",

    'headEncrypt'=>'ENCRYPTED DATA',
    'textEncrypt1'=>'We take all reasonable precautions to protect the privacy of your information. We work with third party service providers and vendors that use encryption and authentication to maintain the confidentiality of your personal information. If stored, we house personal information on systems behind firewalls that are only accessible to limited personnel. Also sensitive data like messages/contents are stored in encrypted way.',
    'textEncrypt2'=>'User’s credentials are user’s login is stored in DB. User’s password is not stored. We store only hash of the password generated by md5 hash function from user’s passwords.',
    'textEncrypt3'=>'Encryption data: We get opened data from devices using encrypted https protocol. The data delivered to user panel is also encrypted.',

    'headLaw'=>'LEGAL MATTERS',
    'textLaw'=>'TiSPY considers your use of its service to be private. However, we may disclose your personal information stored in your account and/or on TiSPY servers and databases, in order to:',
    'poin1Law'=>'Comply with the law or legal process served on us',
    'poin2Law'=>'Enforce and investigate potential violations of this contract; including use of this service to participate in, or facilitate, activities that violate the law;',
    'poin3Law'=>'Investigate potential fraudulent activities; or',
    'poin4Law'=>'Protect the rights, property, or safety of TiSPY, its employees, its customers or the public.',

    'text1Law'=>'You give explicit consent to the collect and use of your personal information by TiSPY.',
    'test2Law'=>'In the event of a change of control of TiSPY (such as asset transfers through a merger, sale, assignment or liquidation of the business entity, or any of its properties, assets or equity) or, in the event of a direct or indirect sale of any of its publishing properties and/or its Website(s), personal information of users of the affected Website(s) in our possession will be transferred to the new owner/successor. We are not obligated to notify Website users of any such transaction. You may always change or delete your information or opt out by contacting us as provided below, or if the acquirer posts a new Privacy Policy with new contact information, you may change or delete your information or opt out by following any new instructions that are posted.',
    'test3Law'=>'We may share Aggregate Information with third parties, including strategic partners, for marketing and promotional purposes.',
    'test4Law'=>'In the event we share personal information with third parties, we require those third parties to comply with this Privacy Policy and take appropriate confidentiality and security measures.',
    'test5Law'=>'As well we may share and disclose the personal information to service providers that we engage for the sole purpose of processing information on our and your behalf.',
    'test6Law'=>'Please be advised that all logs and data downloaded from a target device with use of the TiSPY Software stored in TiSPY database dated more than 90 days will be automatically deleted for the reasons of security. Logs can be downloaded before the determined term above for further storage, should the necessity arise.',

    'headChange'=>'HOW DO I OPT OUT OR CHANGE MY PERSONAL INFORMATION?',
    'text1Change'=>'To opt out of further e-mail communications from us, just click on the opt out controls in the e-mail, or contact us as instructed at the end of this Privacy Policy. We may need up to 3 (three) business days to assure compliance with your request.',
    'text2Change'=>'To change your personal information that you have provided us, please contact us as instructed at the end of this Privacy Policy.',

    'headPrivacy'=>'CHILDREN’S ON-LINE PRIVACY INFORMATION',
    'text1Privacy'=>'This Website is not intended for use by children under the age of 18.',
    'text2Privacy'=>'TiSPY do not knowingly collect information from children and minors. We encourage parents and legal guardians to monitor their children’s Internet usage and to help enforce our Privacy Policy by instructing their children to never provide information on our Website(s) without their permission.',
    'text3Privacy'=>'In case you will use the Software for the parental control aim we will inform you how we may collect, use personal information from children under 13 years of age and that we do not disclose it. In case you use the Software for the parental control you should provide us with the explicit consent to collect and use of personal information of your child (children) by TiSPY.',

    'headCommitment'=>'OUR COMMITMENT TO DATA SECURITY',
    'text1Commitment'=>'Any employee with access to personal information is required to protect this information in a manner that is consistent with this Privacy Policy and may not use the information for any purpose other than to carry out the services they are performing for TiSPY. These individuals may be subject to discipline, including termination and criminal prosecution, if they fail to meet these obligations. Although we will exercise high levels of care in providing secure transmission of information between your computer and our servers and in storing that information on our systems, no method of transmission over the Internet, or of electronic storage, is 100% secure. As such, we cannot ensure or otherwise guarantee the security of information transmitted to us over the Internet.',

    'headRenewal'=>'CHANGES OR UPDATES TO THIS PRIVACY POLICY',
    'textRenewal'=>'We may modify this Privacy Policy at any time, so please review it frequently. We will post any changes to the privacy policy on the Website(s), but we have no other obligation to notify you of such changes. We indicate the date of the current version of this privacy policy below, so you know when it was last updated. Any changes will affect all of the information we have previously collected from you, as well as the information we collect after such changes, and may include without limitation additional uses or disclosures of your information. If any of the changes represent a material change that expands our use of personal information, the changes will be posted in advance of taking effect. We may post a prompt on the home page of our site for users to review the new privacy policy but are not required to do so. If you object to the changes, please contact us as provided below and we will remove your information that we previously collected. However, your subsequent provision of information to us through any of our Website(s) is deemed your agreement to use of that information in accordance with the then-current privacy policy.',

    'headPolicy'=>'GOVERNING LAW',
    'text1Policy'=>'This Privacy Policy is governed by and construed with the laws of India, and will be interpreted in accordance with the Indian courts. The Indian courts shall have exclusive jurisdiction to settle any claim or dispute which might arise out of or in connection with this Privacy Policy.',
    'text2Policy'=>'Those who access the website from locations outside of the India are responsible for compliance with local laws if and to the extent that local laws are applicable.',

    'headContact'=>'CONTACT US',
    'text1Contact'=>'If you have any questions about this Privacy Policy or you wish to make changes to your personal information or remove yourself from our database, please contact us in one of the following ways:',
    'text2Contact'=>"E-mail us at: support@spyhp.com and insert only the words 'Re: Privacy Policy' in the subject line header of the e-mail. Versi: 16 Mei 2018.",

]
?>
