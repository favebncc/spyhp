<?php
    return[
        'spyhpFunction'=>'How SPYHP works ?',
        'head1'=>'1. Target Mobile',
        'text1'=>"Install and Register SPYHP application into child's mobile.",
        'head2'=>'2. Server SPYHP',
        'text2'=>"SPYHP client sends monitored mobile's data to SPYHP portal.",
        'head3'=>'3. User Dashboard',
        'text3'=>'Access SPYHP portal anywhere to monitor and control child phone remotely.',

        'feature'=>"Check out all SPYHP great spy phone features",
        'headMonitor'=>'Monitor calls & contacts',
        'viewCall'=>'View Calllogs',
        'viewTime'=>'View time and date of calls',
        'phonebook'=>'Access Phonebook',
        'block'=>'Block Specific contacts',

        'monitorWeb'=>'Monitor on Web',
        'viewTraffic'=>'View all website visited',
        'getVisitorData'=>'Obtain time &date of each visited website',
        'oneClick'=>'One cliick access to all visited websites',

        'fileBrowse'=>'Advanced File explorer',
        'remoteAccess'=>'Remote access of phone memory & sd card',
        'viewAVP'=>'View Audio,video & photos.',
        'download'=>'Download Multimedia,Documents,Zips files.',
        'checkFile'=>'Check All files for harmful contents.',

        'remoteMonitor'=>'Remote monitoring with Impressive features',
        'clearData'=>'Wipe target device data.',
        'factory'=>'Factory reset',
        'device'=>'device',
        'unlockDevice'=>'Unlock device for specific time.',
        'downData'=>'Download All phone data.',
        'wirelessSMS'=>'Dozens of SMS commands (No internet use)',
        'avp'=>'Live Audio/Video/Photos.',

        'monSMS'=>'Monitor SMS and MMS',
        'viewSMS'=>'View all SMS',
        'viewMMS'=>'View all MMS',
        'trackSender'=>"Track sender's details",

        'monGPS'=>'Monitor GPS location',
        'viewLocation'=>'View current Location',
        'loactionHistory'=>'Location history with Path',
        'Geofence'=>'Set Geofence',
        'notifGeofence'=>'Get Email alerts on Geofence',

        'monApp'=>'Monitor application',
        'viewDeviceConnected'=>'View Installed Application',
        'blockApp'=>'Application blocking',
        'activateApp'=>'Application Activity',
        'viewConnectedTime'=>'View date & time for install/un-install.',

        'convinient'=>'It is Effortless',
        'easyUsage'=>'Easy to use',
        'easyInstall'=>'Easy to install',
        'uninstall'=>'Un-install/Update/Renew.',
        'usefulInfo'=>' Informational messages',
        'webPort'=>'Interactive Web portal',
        'workSecretive'=>'Silent Working',

        'secureCommand'=>'Strongest Anti-theft protection commands',
        'instanLocation'=>'Instant location',
        'sendMessage'=>'Send message',
        'hidePhone'=>'Play ring (Even in Silent mode)',
        'filterContent'=>'Check All files for harmful contents.',
        'callback'=>'Call Back',
        'autoreply'=>'Auto answer',
        'activateWifi'=>'Enable Wi-fi',

        'scheduler'=>'All-in-one Scheduler',
        'scheduleRecord'=>'Schedule audio recording',
        'schedulePhoto'=>'Schedule Photo capturing.',
        'scheduleMulti'=>'Multiple Scheduler handling.',

        'instanAlert'=>'24/7 Instant Alerts',
        'dailyAlert'=>'Every days email Alert with Summary',
        'alertlocation'=>'Alert on Specific Location',
        'alertSim'=>'Alert on SIM Change',
        'alertSystem'=>'System Event Alerts',

        'monClipboard'=>'Monitor with clipboard',
        'viewText'=>'View Copied text like rich,simple text.',
        'secureAccount'=>'Get copid User-id & password for Secured accounts.',
        'messageWA'=>'Get All WhatsApp forwarded messages.',
        'checkUrl'=>'Check Email ids & Web URLs.',


        'supervisionA/V'=>'Powerful Audio/video Surveillance',
        'liveAV'=>'Enjoy Live Audio & Video telecast of phone surroundings.',
        'liveVideo'=>'Watch Live Video without Delay.',
        'clearAudio'=>'Listen Live audio of phone with clear voice.',
        'capturePhoto'=>'Take Live Photo.(Using Both camara)',
        'screenshoot'=>'Live Screenshot of phone.',

        'monSosmed'=>'Monitor All social activities',
        'voip'=>'Voip Call Recordings',

        ]
?>
