<?php

    return [
        'header'=>'Report Abuse/Violationn',
        'text'=>'SPYHP shall be used only with the aim of parental control and monitoring of their children. SPYHP does not encourage and shall not be used for monitoring other people secretly, which is a violation to privacy. If you find your devices are being monitored by others using the SPYHP software or service without your valid permissions, please report abuse/violation here and provide us the necessary information, so that we could terminate the monitoring service and block the violater`s account. We are glad to help you get rid of SPYHP and tell you how to prevent the same thing from happening again.',
        'imei'=>'Your Device IMEI ',
        'email'=>'Your Email Address',
    ]
?>
