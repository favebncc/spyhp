<?php

    return [
        'installGuide'=>'panduan pemasangan',
        'computer'=>'Komputer',
        'prerequisite'=>'Persyaratan Sistem',

        'home'=>'Beranda',
        'feature'=>'Fitur-Fitur',
        'android'=>'Fitur-Fitur Android',
        'pc'=>'Fitur-Fitur Komputer',
        'price'=>'Harga',

        'footerAiO'=>'Semua dalam satu',
        'aio1'=>'atur panggilan',
        'aio2'=>'melacak pesan',
        'aio3'=>'melacak file media',
        'aio4'=>'melacak social media',
        'aio5'=>'monitor aplikasi',
        'aio6'=>'panel langsung',
        'aio7'=>'pelacak perangkat',
        'aio8'=>'melacak lokasi',
        'aio9'=>'akses buku alamat',
        'aio10'=>'monitor internet',
        'aio11'=>'penjadwalan',

        'testimoni'=>'testimoni',
        'forYou'=>'SPYHP untuk anda!',

        'info'=>'informasi legal',
        'guide'=>'paduan privasi',
        'refund'=>'kebijakan pengembalian',
        'works'=>'cara kerja SPYHP?',
        'report'=>'laporkan pelanggaran',

        'footer'=>'PERANGKAT LUNAK DIMAKSUDKAN UNTUK PENGGUNAAN LEGAL SAJA. Perangkat lunak layanan SPYHP dirancang untuk
        memantau anak-anak Anda di handphone atau perangkat lain yang Anda miliki atau memiliki izin yang tepat
        untuk memantau. Merupakan pelanggaran hukum yurisdiksi untuk menginstal perangkat lunak pengawasan, ke
        ponsel atau perangkat lain yang tidak berhak Anda pantau. Undang-undang umumnya mengharuskan Anda untuk
        memberi tahu pengguna / pemilik perangkat bahwa ia sedang dipantau. Pelanggaran terhadap persyaratan ini
        dapat mengakibatkan hukuman moneter dan pidana yang berat yang dijatuhkan pada pelanggar. Anda harus
        berkonsultasi dengan penasihat hukum Anda sendiri sehubungan dengan legalitas menggunakan Perangkat
        Lunak dengan cara yang Anda inginkan sebelum mengunduh, menginstal, dan menggunakannya. Anda bertanggung
        jawab penuh untuk menentukan bahwa Anda memiliki hak untuk memantau perangkat tempat perangkat lunak
        diinstal. SPYHP tidak dapat dianggap',
    ]
?>
