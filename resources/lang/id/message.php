<?php

return [
    // repetition
    'About'=>'Tentang Kami',
    'Feature'=>'fitur spyhp',
    'Device'=>'perangkat',
    'open'=>'Buka',
    'or'=>'atau ',
    'downApp'=>'Download Apk',
    'keyBoard'=>'papan ketik',
    'buy'=>'Beli Sekarang',
    'months'=>'Bulan',
    'special'=>'Paling Disukai',
    'price'=>'Harga',
    'ideology'=>'Ideologi',
    // Konten About Us Page
    'aboutWelcome'=>'Selamat Datang Di SPYHP. Software Dan Aplikasi Eksklusif
    Untuk Orang Tua. Kami Menawarkan Aplikasi Pemantauan Telepon
    Secara Lengkap Dengan Teknologi Terdepan Untuk Membantu Anda
    Memastikan Keamanan Penggunaan Ponsel Orang Tersayang.',
    'aboutSoftware'=>'Software ini berpusat di Ahmedabad, India didirikan tahun 2010
    karena kebutuhan akan cara yang lebih baik untuk mengamankan dan
    memantau aktivitas orang tersayang anda. Ada banyak bahaya
    tersembunyi di kehidupan online dan lingkungan anda sendiri,
    sehingga bahaya dapat segera dipantau, dikendalikan, dan
    diantisipasi segera. Layanan pemantauan ini bersifat rahasia dan
    tersembunyi serta terus menerus memberikan informasi sehingga anda
    tenang.',
    'aboutFitur'=>'Dengan banyaknya fitur dan kemudahan penggunaan SPYHP adalah
    aplikasi yang benar-benar dibutuhkan. Kami menawarkan panel langsung
    untuk semua SMS, Facebook, MMS, Whatsapp, Viber dan lain sebagainya.
    Anda bahkan masih dapat membaca semua pesan yang sudah dihapus dan
    keberadaan orang tersayang anda melalui GPS pada ponsel mereka.',
    'aboutKomitmen'=>'Komitmen kami adalah filosofi kami. Kualitas premium & layanan hemat
    biaya dengan kepuasan pelanggan mutlak telah mengangkat SPYHP ke
    tingkatan seperti saat ini. Kami berusaha keras untuk tetap pada
    tugas kami untuk membuat Anda tahu dan anak-anak Anda aman.',

    'welcomeSpyhp'=>'Selamat datang di SPYHP. Software dan aplikasi eksklusif untuk orang tua. Kami menawarkan aplikasi
    pemantauan telepon secara lengkap dengan teknologi terdepan untuk membantu anda memastikan keamanan
    penggunaan ponsel orang tersayang.',

    'aboutMore'=>'cari tahu tentang kami',
    'aboutTrust'=>'terpercaya di seluruh dunia oleh ribuan keluarga dan orang tua',

    'trustUse'=>'Anak Terlindungi',
    'trustPuas'=>'Orang yang telah puas',
    'trustProtect'=>'anak diawasi',
    'trustParent'=>'ibu yang telah puas',
    // Konten Contact
    'contactUs'=>'Hubungi kami',
    'contact'=>'Jika anda memiliki pertanyaan tentang SPYHP maka hubungi kami di email kami. Gunakan alamat email anda yang terdaftar. Yang akan membantu kami menjawab anda dengan cepat.',
    'contactEmail'=>'Email Kami :',
    'contactMedsos'=>'Media Sosial :',
    'contactFacebook'=>'Facebook Messenger',
    'contactInputNama'=>'Nama',
    'contactInputEmail'=>'Alamat Email',
    'contactInputSubjek'=>'Subjek Email',
    'contactbutton'=>'kirim email',

    // Konten Home
    'homeTrial' =>'2 Hari Masa Percobaan',
    'homeBestApp'=>'aplikasi monitoring terbaik sampai kapanpun! Unduh dan uji semua fitur layanan SPYHP',
    'homeApp'=>'Aplikasi pemantauan Orang tua terbaik',
    'homeHowto'=>'tidak tahu cara melakukan instalasi?',
    'homeGuide'=>'Lihat Panduan Penginstalan ',
    'homeFeatures'=>'Fitur utama',
    'homeFeatureSocial'=>'Interaksi Sosial',
    'homeFeatureManage'=>'Atur panggilan',
    'homeFeatureTrack'=>'Melacak pesan terkirim',

    // // header Fitur
    // 'ftCall'=>'Monitor Panggilan & Kontak',
    // 'ftShowCall'=>'Melihat Panggilan',
    // 'ftDuration'=>'Melihat waktu dan tanggal panggilan',
    // 'ftBook'=>'Akses Phonebook',
    // 'ftBlock'=>'Blokir kontak secara spesifik',

    // 'ftMonitor'=>'Monitor di Web',
    // 'ftTrack'=>'Melihat semua website yang dikunjungi',
    // 'ftDate'=>'Mendapatkan waktu dan tanggal dari setiap website yang dikunjungi',
    // 'ftOnClick'=>'1 klik akses untuk semua website yang dikunjungi',

    // 'ftExplore'=>'Penjelajah file lanjutan',
    // 'ftAccessRemote'=>'Akses remot ke memori hp dan sd card',
    // 'ftViewOutput'=>'Menampilkan audio, video dan foto',
    // 'ftUnduh'=>'Download multimedia, dokumen dan file zip',
    // 'ftCheck'=>'Memeriksa semua file untuk mencari konten yang membahayakan',

    // 'ftRemote'=>'Monitor secara remot dengan fitur yang mengesankan',
    // 'ftClean'=>'Membersihkan data target perangkat',
    // 'ftTimer'=>'Membuka kunci perangkat untuk waktu yang dispesifikkan',
    // 'ftGetData'=>'Download semua data ponsel',
    // 'ftSMS'=>'Puluhan perintah SMS (Tidak menggunakan internet)',
    // 'ftRealTime'=>'Audio/Video/Foto langsung',

    // 'ftMonSMS'=>'Monitor SMS dan MMS',
    // 'ftViewSMS'=>'Menampilkan semua SMS',
    // 'ftViewMMS'=>'Menampilkan semua MMS',
    // 'ftTrackSMS'=>'Melacak detail pengirim',

    // 'ftGPS'=>'Monitor lokasi GPS',
    // 'ftLocate'=>' Menampilkan lokasi terkini',
    // 'ftHistory'=>'Riwayat lokasi dengan jalur',
    // 'ftGeofence'=>'Menentukan Geofence',
    // 'ftNotifGeofence'=>'Mendapat dapatkan lansiran email dari Geofence',

    // 'ftMonApp'=>'Monitor aplikasi',
    // 'ftView'=>'Menampilkan aplikasi yang dipasang',
    // // Menampilkan aplikasi yang dipasang ftViewOutput
    // 'ftBlokApp'=>'Blokir aplikasi',
    // 'ftActivate'=>'Aktivitas aplikasi',
    // 'ftInstallTime'=>'Menampilkan waktu dan tanggal aplikasi dipasang dan uninstall',
    // 'ftMudah'=>'Tidak memerlukan usaha',
    // 'ftUse'=>'Mudah digunakan',
    // 'ftInstall'=>'Mudah dipasang',
    // 'ftUninstall'=>'Uninstall/perbarui',
    // 'ftInfo'=>' Pesan yang penuh informasi',
    // 'ftInteract'=>'Web portal yang interactive',
    // 'ftIcognito'=>'Berkerja secara diam-diam',

    // 'ftSecurity'=>'Perintah perlindungan anti-pencurian terkuat',
    // 'ftInstLoc'=>'Lokasi Instant',
    // 'ftSendMsg'=>'Mengirim pesan',
    // 'ftHidePhone'=>'Membunyikan Handphone (walaupun mode silent)',
    // 'ftContent'=>'Periksa semua konten yang dapat membahayakan',
    // 'ftAuto'=>' Penjawab otomatis',
    // 'ftWifi'=>'Mengaktifkan WI-FI',

    // 'ftSchedule'=>'Penjadwal semua dalam satu',
    // 'ftRecAudio'=>'Menjadwalkan rekaman audio',
    // 'ftCapPhoto'=>'Menjadwalkan pengambilan foto',
    // 'ftMultSchdl'=>'Penanganan Penjadwal Berganda',

    // 'ftInstant'=>'24/7 Lansiran instan',
    // 'ftAlert'=>'Setiap hari Lansiran email dengan ringkasan',
    // 'ftAlertLoc'=>'Lansiran di lokasi spesifik',
    // 'ftAlertSIM'=>'Lansiran ketika mengganti kartu SIM',
    // 'ftAlertSys'=>'Peringatan perubahan sistem',

    // 'ftMonClip'=>'Monitor dengan clipboard',
    // 'ftViewText'=>'Lihat teks yang disalin seperti kaya, teks sederhana.',
    // 'ftUser'=>'Dapatkan User-id & kata sandi untuk Akun aman.',
    // 'ftWA'=>'Dapatkan semua pesan yang diteruskan WhatsApp.',
    // 'ftEmail'=>'Memeriksa id Email & URL Web',

    // 'ftAudio'=>'Pengawasan Audio / video yang kuat',
    // 'ftLiceAud'=>'Nikmati siaran langsung Audio & Video dari lingkungan telepon.',
    // 'ftBuffer'=>'Tonton Video Langsung tanpa Tunda.',
    // 'ftClearSound'=>'Dengarkan audio ponsel secara langsung dengan suara jernih.',
    // 'ftPhotoCapt'=>'Ambil Foto Langsung. (Menggunakan Kedua camera)',
    // 'ftTrailer'=>'Cuplikan layar langsung dari ponsel.',

    // 'ftMonSos'=>'Monitor Semua kegiatan sosial',
    // 'ftvoip'=>'Rekam panggilan VOIP',

    'testBasu'=>'Perangkat Lunak Hebat.. Harus untuk setiap orang tua yang benar-benar ingin merawat anak-anak mereka.',
    'testOlivia'=>'Sangat bagus untuk memata-matai, saya tidak pernah melihat aplikasi mata-mata ponsel seperti SPYHP.
    Hanya kekurangannya adalah saya membutuhkan akses fisik karena instalasi jarak jauh tidak mungkin
    dicapai.',
    'testMicheal'=>'Produk luar biasa. Mendapat banyak kepercayaan pada anggota keluarga. Terima kasih',

    'others'=>'Lihat lainnya',
    'header1'=>'Anda harus mencintai anak-anak Anda tanpa pamrih. Itu sulit. Tapi itu satu-satunya cara.',
    'spyhpDesc1Parag1'=>'SPYHP adalah Semua-di-satu bundel yang dibutuhkan setiap orangtua, SPYHP adalah perangkat lunak pemantauan No.1
    yang digunakan di seluruh dunia, SPYHP adalah yang tercepat di dunia & amp; perangkat lunak mata-mata ponsel
    paling ringan, SPYHP hanya memiliki ukuran 230 kb dan memiliki lebih banyak fitur daripada yang lain, bundel
    kecil ini menunjukkan keajaiban seperti yang belum pernah Anda lihat, tetap tersembunyi, lacak data secara
    diam-diam. Apakah Anda pikir itu kecil sehingga tidak berguna? Maka Anda SIR SALAH! Karena itu dikelompokkan
    dengan 50 fitur yang dikemas dalam daya dan jika Anda menemukan Aplikasi yang lebih kecil seperti SPYHP, kami
    jamin Anda bahwa itu seharusnya tidak memiliki bahkan 10 fungsi.',
    'spyhpDesc1Parag2'=>'Seperti yang kita lihat di klip film di mana orang tersebut mencoba mendengarkan percakapan seseorang dengan
    menggunakan peralatan mahal. Dalam kehidupan nyata, kita sering mengungkapkan rahasia yang membuat kita marah.
    Bagaimana jika anak saya minum alkohol? Bagaimana jika putri saya pergi ke klub porno? Bagaimana jika anak yang
    Anda cintai menjalani kehidupan rahasia melalui telepon mereka? Mengancam anak Anda untuk mengatakan yang
    sebenarnya dicegah secara hukum. Bertengkar dengan mereka demi kebenaran akan menghasilkan hubungan yang manja.
    Detektif terlalu mahal untukmu. Penggunaan perangkat lunak pemantauan orang tua hanya legal & amp; cara yang
    paling direkomendasikan untuk menyelamatkan anak Anda dari aktivitas berbahaya kapan pun mereka jauh dari Anda.',
    'header2'=>'INSTALL HANYA DALAM 2 MENIT',
    'spyhpDesc2Parag1'=>'SPYHP memberi 2 HARI masa percobaan GRATIS untuk menguji 50 fitur pemantauan utama tanpa biaya tersembunyi.
    Dengan menggunakan perangkat lunak pemantauan orang tua yang tepercaya Anda harus membuang semua kekhawatiran
    Anda & amp; rasakan orang tua terbaik di dunia ini, Dengan Menjalani anak-anak Anda dengan kebebasan MENGAPA?
    Karena: SPYHP selalu ada untuk merawat anak Anda.',
    'spyhpDesc2Parag2'=>'Cukup masuk ke dasbor SPYHP dari perangkat apa pun yang memiliki konektivitas internet & amp; pantau semua data
    perangkat anak Anda dari mana saja tanpa mengajukan satu pertanyaan kepadanya. Apakah Anda ingin tahu tentang
    apa yang akan Anda dapatkan di dasbor? Bagaimana cara memantau anak saya menggunakan Dashboard SPYHP? Jangan
    berpikir lebih jauh. Coba lihat di Dashboard',
    'bannerContext1'=>'Apakah Anda khawatir? Apa yang mereka lakukan dengan telepon mereka?',
    'bannerContext2'=>'Adalah hak Anda untuk mengetahui',
    'bannerContext3'=>'jika anak-anak yang Anda sayangi berjalan dengan cara yang salah melalui telepon mereka.',

    'benefitHead1'=>'Manfaat Untuk Anda',
    'benefitsubHead1'=>'Gunakan telepon anak anda untuk membuatnya aman. Bukankah itu Ide yang cerdas.',
    'benefitList1'=>'Tahu tentang percakapan mereka dengan orang lain.',
    'benefitList2'=>'Pastikan mereka tidak berbohong tentang lokasi mereka.',
    'benefitList3'=>'Dengarkan anak Anda dari mana saja.',
    'benefitList4'=>'Lupakan semua kekhawatiran Anda tentang keselamatan anak Anda.',
    'benefitList5'=>'Buat hubungan Orangtua-anak Anda lebih kuat dari sebelumnya!',

    'benefitHead2'=>'Kenapa Kami yang terbaik',
    'benefitsubHead2'=>'Perangkat lunak pemantauan paling tepercaya, 100% keselamatan anak.',
    'benefit2List1'=>'Kami memberi Anda 50+ fitur hebat yang tidak bisa Anda dapatkan di tempat lain.',
    'benefit2List2'=>'dukungan 24/7 melalui Live chat, email, atau tiket.',
    'benefit2List3'=>'Masa percobaan GRATIS 2 Hari untuk menguji semua fitur SPYHP.',
    'benefit2List4'=>'Sejak 2010 kami telah membantu ribuan orang tua.',
    'benefi2tList5'=>'Punya pertanyaan? Gunakan obrolan langsung sekarang!',

    //Activation
    'activateHead'=>'Kendalikan aktifitas smartphone',
    'activateParagKal1'=>'  SPYHP adalah perangkat lunak pemantauan orang tua yang digunakan untuk menjaga anak-anak Anda aman dan menjaga ketenangan pikiran di rumah Anda. SPYHP mudah digunakan, dan',
    'hrefParag1'=>'fitur lanjutannya',
    'activateParagKal2'=>', seperti panel langsung, menjadikannya aplikasi pemantauan orangtua paling kuat di dunia di pasar.',
    'activateParag2Kal1'=>'Ini menyediakan foto instan, lokasi, fasilitas menangkap yang digerakkan SMS. Selain itu, ini akan menyediakan
    pemantauan semua aplikasi komunikasi yang umum digunakan, termasuk SMS, Facebook, MMS, Viber, Line, dan lainnya.
    Yang terbaik dari semuanya, bahkan jika pesan perangkat yang dipantau dihapus setelah mengirim, Anda masih dapat
    melihatnya secara online. Cukup',
    'hrefParag2'=>'instal perangkat lunak',
    'activateParag2Kal2'=>'di ponsel anak Anda, dan Anda bisa secara rahasia
    mempelajari kebenaran tentang log panggilan, pesan teks, dan Lokasi GPS dengan masuk ke akun terdaftar Anda dari
    browser web apa pun. Anda tidak hanya dapat memantau aktivitas anak Anda, tetapi alat-alat ini juga dapat
    memastikan keselamatan anak Anda saat mereka jauh dari Anda.',

    'activateHead2'=>'Aktivitas Ponsel',
    'head2Parag'=>'Kami menyediakan perangkat lunak pemantauan keamanan terpanas dan paling kuat untuk perangkat Android Anda. Kami
    memiliki tenaga kerja yang berpengalaman, berbakat, dan inovatif yang menyediakan sistem pemantauan ponsel
    terbaik untuk ponsel android. Salah satu manfaat utama dari perangkat lunak pemantauan ponsel kami adalah
    kemudahan penggunaannya. Tim ahli kami mengkhususkan diri dalam menyediakan solusi pelacakan seluler yang
    dirancang khusus dan berorientasi pelanggan dengan menggunakan teknologi terbaru. Kami memimpin persaingan di
    pasar solusi keamanan perangkat android. Menggunakan teknologi canggih dan aplikasi pemantauan seluler paling
    canggih, kami memberikan yang terbaik dan termudah untuk menggunakan perangkat lunak pemantauan ponsel android.
    Awasi anak-anak Anda dengan SPYHP dengan mudah hanya dengan masuk ke akun Anda dari browser web apa pun di
    komputer, tablet, atau bahkan perangkat seluler Anda sendiri. Rilis kami yang akan datang akan menyediakan
    aplikasi pemantauan seluler untuk platform iPhone juga sesegera mungkin.',

    // keylogger konten
    'keylogHead'=>'SPYHP Keylogger - Langkah Depan dalam Keselamatan Anak',
    'keySpec'=>'Android 4.4 atau lebih tinggi',
    'keyLink'=>'di browser ponsel target untuk mengunduh secara manual',
    'keySubHead'=>'Kompatibel dengan semua komputer windows',
    'keyDownInst'=>'di peramban komputer Anda untuk mengunduh secara manual',
    'keySubHead2'=>'Lihat setiap penekanan tombol pada perangkat yang dipantau',
    'keySHead2Para1'=>'Fitur Keylogger SPYHP membantu Anda memantau anak-anak Anda dari mana saja. Pastikan
    anak-anak Anda aman dengan melihat apa yang mereka ketikkan ke dalam aplikasi obrolan seperti WhatsApp dan
    facebook, URL yang mereka masukkan ke dalam peramban seluler dan menangkap kata kunci yang tepat yang mereka
    ketik pada mesin pencari',
    'keySHead2Para2'=>'>Pastikan orang yang Anda cintai tidak berhubungan dengan orang yang salah menggunakan Email
    mereka. Telusuri penekanan tombol yang dimasukkan ke dalam email mereka dan ambil id email yang mereka masukkan.',
    'keySeeContent'=>'Lihat konten yang dimasukkan dalam Aplikasi pesan teks.',
    'keySeePass'=>'Lihat Kata Sandi meskipun mereka adalah karakter tersembunyi.',
    'keySeeButton'=>'Lihat penekanan tombol seperti teks yang diketik dan Kata Sandi di Facebook, Gmail, Skype, Kik, Viber,
    yahoo Mail, WeChat, Ebay, Amazon dan banyak lagi.',
    'keySeeChat'=>'Lihat obrolan yang dimasukkan di WhatsApp, Facebook, Line, Kik, Viber, Hangouts, Skype dan
    sebagainya.',
    'keyEasyDash'=>'Mudah diakses dari dasbor Anda.',
    'KeyBilng'=>'Dukungan multi bahasa.',

    'keyVersion'=>'Keyboard mata-mata android memiliki 2 versi, satu hitam dan satu putih, versi hitam berjalan
    di Android 4.0 dan lebih tinggi, sedangkan versi putih bekerja di Android 4.4 dan lebih tinggi, yaitu hingga
    versi Android terbaru',
    'keySpy'=>'Mata-mata android SPYHP mendukung semua perangkat dengan sistem operasi android.',
    'keyStep'=>'Langkah untuk memasang keylogger',
    'keyStep1'=>'Klik tombol "Unduh Keylogger" untuk mengunduh keyboard.',
    'keyStep2'=>'Buka keyboard yang Diunduh',
    'keyStep3'=>'Klik Instal',
    'keyStep4'=>'Setelah instalasi berhasil klik Open',
    'keyStep5'=>'Klik Mulai',
    'keyStep6'=>'Klik Aktifkan di Pengaturan',
    'keyStep7'=>'Aktifkan "Teclado Android (B.E) atau Keyboard Android (AOSP)"',
    'keyStep8'=>'Klik pada Beralih metode input',
    'keyStep9'=>'Pilih "Teclado B.E" atau "Keyboard Android (AOSP)" dari daftar',
    'keyStep10'=>'Klik pada Selesai',
    'keyStepHide'=>'Langkah-langkah untuk menyembunyikan keyboard dari laci aplikasi',
    'keyStepHide1'=>'Buka "Teclado Android (B.E)" atau "Keyboard Android (AOSP)".',
    'keyStepHide2'=>'Klik Tingkat Lanjut',
    'keyStepHide3'=>' Aktifkan dan nonaktifkan "Tampilkan Ikon Aplikasi".',
    'keyRemind'=>'Harap mengerti bahwa perangkat lunak pemantauan seluler, termasuk TiSPY hanya legal untuk memantau anak-anak, jangan menggunakannya dalam aktivitas ilegal apa pun.',

    'price'=>'Monitor Panggilan & Kontak',
    'price2'=>'SPYHP berjalan mulus pada : Android & Windows PC',
    'price3'=>'Hal yang Harus anda ketahui sebelum membeli SPYHP',

];
