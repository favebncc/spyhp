<?php

    return [
        'androidGuide'=>'video panduan pemasangan SPYHP',
        'androidStep1'=>'1. Pemasangan',
        'androidStep1Subhead'=>'Nonaktifkan Google play store',
        'androidStep1li1'=>'Buka aplikasi "Play Store" di Telepon Anda',
        'androidStep1li2'=>'Tekan tombol menu',
        'androidStep1li3'=>'Pilih menu "Play Protect"',
        'androidStep1li4'=>'Nonaktifkan "Scan device for security threads"',
        'androidStep1Subhead2'=>'Unduh dan Pasang "SPYHP Client"',
        'unduh'=>'Unduh APK',
        'unduhStep1'=>'Tekan file APK Setelah pengunduhan selesai untuk memulai pemasangan',
        'unduhStep2'=>'Ikuti gambar di bawah untuk menyelesaikan pemasangan aplikasi',

        'androidStep2'=>'2. Konfigurasi "SPYHP Client"',
        'androidStep2ubhead'=>'Ketentuan dan Izin Akses',
        'androidStep2li1'=>'Baca dan setuju dengan EULA (End User License Agreement) SPYHP',
        'androidStep2Subhead2'=>'Hak Administrator Perangkat',
        'androidStep2li2'=>'Selama pemasangan, SPYHP akan meminta untuk mengaktifkan hak administrator. Mengaktifkannya akan memungkinkan fitur seperti mengunci layar perangkat, Mengatur Kata sandi Lock-Screen, menonaktifkan fungsi kamera, dan perlindungan dari pencopotan aplikasi.',
        'register'=>'Buat / Daftar Akun',
        'registerStep1'=>'Sudah memiliki akun SPYHP',
        'registerStep2'=>'Jika kamu sudah memiliki akun SPYHP, tekan tombol "I am Already a SPYHP User"',
        'newAccount'=>'Buat Akun baru',
        'newAccountStep1'=>'Tekan "I am New To SPYHP" untuk membuat akun baru',
        'newAccountStep2'=>'Masukan semua informasi valid yang dibutuhkan',
        'newAccountStep3'=>'Tekan Tombol "Register For SPYHP"',
        'newAccountStep4'=>'Verifikasi alamat email yang kamu daftarkan',
        'allow'=>'izin akses',
        'accesbility'=>'Aksesibilitas',
        'HuaweiDevice'=>'Pengaturan Khusu Huawei (Cara singkat mengatur aksesibillitas)',

        'androidStep3'=>'3. Verifikasi Email',
        'androidStep3Subhead'=>'Verfikasi Email (Untuk Akun Baru)',
        'androidStep3li1'=>'Email verifikasi akan dikirimkan ke kotak masuk email',
        'androidStep3li2'=>'Verifikasi alamat email kamu dengan menekan tombol "Activate Account"',

        'androidStep4'=>'4. Tahap Setelah Instalasi',
        'androidStep4Subhead'=>'android 10 ke atas',
        'androidStep4li1'=>'Untuk melakukan konfigurasi spesial pada android 10, kamu harus menghubungkan telepon kamu ke komputer dengan sistem operasi Windows dan kemudian menjalankan "Permission Tool"',
        'androidStep4li2'=>'Lihat detail langkah-langkahnya di',

        'nonActivate'=>'Nonaktifkan Notifikasi',
        'howDeactivate'=>'Bagaimana cara menonaktifkan notifikasi yang berhubungan dengan izin di dalam telepon yang sudah root',
        'deactivateStep1'=>'Facebook dan Viber',
        'deactivateStep2'=>'Buka aplikasi "SuperSU"',
        'deactivateStep3'=>'Tekan menu "Wifi Service"',
        'deactivateStep4'=>'Nonaktifkan notifikasi untuk aplikasi "Wifi Service"',

        'trustedApp'=>'Buat aplikasi menjadi terpercaya',

        'pcStep1'=>'1. Pemasangan',
        'pcStep1Subhead1'=>'Unduh "SPYHP Installer" dan Pasang di Komputer Kamu',
        'installer'=>'Unduh Installer',
        'pcinstallstep1'=>'Setelah download selesai jalankan installer SPYHP',
        'pcinstallstep2'=>'Ikuti Instruksi cara pemasangan di bawah',
        'pcinstallstep3'=>'Hapus riwayat browser dan installer yang sudah diunduh',
        'pcinstallStepAfter'=>'Langkah-langkah setelah pemasangan',
        'pcinstallstep4'=>'Buka "Windows Defender Security Center" >> "Virus & Threat Protection" >> "Virus & Threat Protection Settings"',
        'pcinstallstep5'=>'Buka "Add or Remove Exclusions" >> "Add an Exclusion" >> "Folder" >> "C:\ProgramData\BE"',

        'pcStep2'=>'2. Verifikasi Email',

        'Verif'=>'Verifikasi Email (Untuk Akun Baru)',
        'VerifStep1'=>'Email verifikasi akan dikirimkan ke kotak masuk email',
        'VerifStep2'=>'Verifikasi alamat email kamu',

        'pcStep3'=>'3. Copot Pemasangan client SPYHP dari windows',
        'Op'=>'Opsi',
        'pcOp1Step1'=>'Tekan CTRL + ALT + B',
        'pcOp1Step2'=>'Masukkan Passwordmu',
        'pcOp1Step3'=>'Pergi ke tab "System"',
        'pcOp1Step4'=>'Copot pemasangan',

        'pcOp2Step1'=>'Pergi ke C:\ProgramData\BE',
        'pcOp2Step2'=>'Jalankan uninstall.bat',

        'pcOp3Step1'=>'Unduh installer paling baru',
        'pcOp3Step2'=>'Tingkatkan (Upgrade) Aplikasi',
        'pcOp3Step3'=>'Klik kanan MSI-nya dan pilih copot pemasangan',

        'macStep1'=>'1. Pemasangan',
        'macStep1Subhead1'=>'Unduh "SPYHP Installer" dan Pasang di MAC kamu',
        'macInstallStep1'=>'Klik "Kepp" untuk melanjutkan pengunduhan installer SPYHP',
        'macInstallStep2'=>'Jalankan perintah',
        // 'macInstallStep2Code'=>'"sudo spctl --master-disable;sudoreboot"',
        'macInstallStep2Terminal'=>'di terminal MAC kamu',
        'macInstallStep3'=>'Perintah di atas akan menghidupkan ulang PC kam',
        'macInstallStep4'=>'Jalankan installer',
        'macInstallStep5'=>'Kemudian ikuti instruksi dibawah',
        'macInstallStep6'=>'Berikan izin untuk kamera & mik (Hanya untuk versi 10.14 ke atas)',
        'macInstallStep7'=>'berikan aksesibilitas',

        'macStep2'=>'2. Verifikasi Email',
        //sama dengan di PC
        'macStep3'=>'3. Copot Pemasangan Client SPYHP dari MAC OS',
        'macOp1Step1'=>'Tekan tombol',
        'macOp1Step2'=>'Masukkan Passwordmu',
        'macOp1Step3'=>'Copot pemasangan',

        'macOp2Step1'=>'Buka Terminal',
        'macOp2Step2'=>'Jalankan',
        // 'macOp2Step2Code'=>'"sudo /user/local/.rm/postrm"',

        'macOp3Step1'=>'Unduh installer paling baru',
        'macOp3Step2'=>'Klik dua kali pada installer dan pilih copot pemasangan',

        // featureFolder
        'featureSPYHP'=>'Tertarik dengan SPYHP?',
        'featureDemo'=>'Lihat Demo',
        'featureSupport'=>'Perangkat yang didukung',

    ]
?>
