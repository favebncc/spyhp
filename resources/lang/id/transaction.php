<?php

    return [
        'profile'=>'Customer Profile',
        'pricingName'=>'Name',
        'pricingEmail'=>'Email ',
        'pricingPhone'=>'Phone',
        'pricingAddress'=>'Address',
        'pricingCity'=>'City',
        'pricingPostalCode'=>'Postal Code',

        'phName'=>' Your Name',
        'phEmail'=>'Your Email Address',
        'phPhone'=>'You current active Phone Number',
        'phAddress'=>'Your current Address',
        'phCity'=>' Your Current City',
        'phPostalCode'=>'Your Current Postal Code',

        'text'=>'Please proceed the payment. We will confirm status of the payment through your email'
    ]
?>
