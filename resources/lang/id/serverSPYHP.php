<?php
    return[
        'spyhpFunction'=>'Bagaimana SPYHP Berkerja?',
        'head1'=>'1. Klien SPYHP',
        'text1'=>'Pasang dan daftarkan aplikasi SPYHP ke  telepon anak anda',
        'head2'=>'2. Server SPYHP',
        'text2'=>'Klien SPYHP akan mengirimkan data telepon yang dipantau ke portal SPYHP',
        'head3'=>'3. Dashboard Pengguna',
        'text3'=>'Akses portal SPYHP di mana saja dan kendalikan telepon anak anda dari jarak jauh',

        'feature'=>'fitur spyhp',
        'headMonitor'=>'Monitor Panggilan & Kontak',
        'viewCall'=>'Melihat Panggilan',
        'viewTime'=>'Melihat waktu dan tanggal panggilan',
        'phonebook'=>'Akses Phonebook',
        'block'=>'Blokir kontak secara spesifik',

        'monitorWeb'=>'Monitor di Web',
        'viewTraffic'=>'Melihat semua website yang dikunjungi',
        'getVisitorData'=>'Mendapatkan waktu dan tanggal dari setiap website yang dikunjungi',
        'oneClick'=>'1 klik akses untuk semua website yang dikunjungi',

        'fileBrowse'=>'Penjelajah file lanjutan',
        'remoteAccess'=>'Akses remot ke memori hp dan sd card',
        'viewAVP'=>'Menampilkan audio, video dan foto',
        'download'=>'Download multimedia, dokumen dan file zip',
        'checkFile'=>'Memeriksa semua file untuk mencari konten yang membahayakan',

        'remoteMonitor'=>'Monitor secara remot dengan fitur yang mengesankan',
        'clearData'=>'Membersihkan data target perangkat',
        'factory'=>'Factory reset',
        'device'=>'perangkat',
        'unlockDevice'=>'Membuka kunci perangkat untuk waktu yang dispesifikkan',
        'downData'=>'Download semua data ponsel',
        'wirelessSMS'=>'Puluhan perintah SMS (Tidak menggunakan internet)',
        'avp'=>'Audio/Video/Foto langsung',

        'monSMS'=>'Monitor SMS dan MMS',
        'viewSMS'=>'Menampilkan semua SMS',
        'viewMMS'=>'Menampilkan semua MMS',
        'trackSender'=>'Melacak detail pengirim',

        'monGPS'=>'Monitor lokasi GPS',
        'viewLocation'=>'Menampilkan lokasi terkini',
        'loactionHistory'=>'Riwayat lokasi dengan jalur',
        'Geofence'=>'Menentukan Geofence',
        'notifGeofence'=>'Mendapatkan lansiran email dari Geofence',

        'monApp'=>'Monitor aplikasi',
        'viewDeviceConnected'=>'Menampilkan aplikasi yang dipasang',
        'blockApp'=>'Blokir aplikasi',
        'activateApp'=>'Aktivitas aplikasi',
        'viewConnectedTime'=>'Menampilkan waktu dan tanggal aplikasi dipasang dan uninstall',

        'convinient'=>'Tidak memerlukan usaha',
        'easyUsage'=>'Mudah digunakan',
        'easyInstall'=>'Mudah dipasang',
        'uninstall'=>'Uninstall/perbarui.',
        'usefulInfo'=>' Pesan yang penuh informasi',
        'webPort'=>'Web portal yang interactive',
        'workSecretive'=>'Berkerja secara diam-diam',

        'secureCommand'=>'Perintah perlindungan anti-pencurian terkuat',
        'instanLocation'=>'Lokasi Instant',
        'sendMessage'=>'Mengirim pesan',
        'hidePhone'=>'Membunyikan Handphone (walaupun mode silent)',
        'filterContent'=>'Periksa semua konten yang dapat membahayakan',
        'callback'=>'Telpon balik',
        'autoreply'=>'Penjawab otomatis',
        'activateWifi'=>'Mengaktifkan WI-FI',

        'scheduler'=>'Penjadwal semua dalam satu',
        'scheduleRecord'=>'Menjadwalkan rekaman audio',
        'schedulePhoto'=>'Menjadwalkan pengambilan foto',
        'scheduleMulti'=>'Penanganan Penjadwal Berganda',

        'instanAlert'=>'24/7 Lansiran instan',
        'dailyAlert'=>'Setiap hari Lansiran email dengan ringkasan',
        'alertlocation'=>'Lansiran di lokasi spesifik',
        'alertSim'=>'Lansiran ketika mengganti kartu SIM',
        'alertSystem'=>'Peringatan perubahan sistem',

        'monClipboard'=>'Monitor dengan clipboard',
        'viewText'=>'Lihat teks yang disalin seperti kaya, teks sederhana.',
        'secureAccount'=>'Dapatkan User-id & kata sandi untuk Akun aman.',
        'messageWA'=>'Dapatkan semua pesan yang diteruskan WhatsApp.',
        'checkUrl'=>'Memeriksa id Email & URL Web',


        'supervisionA/V'=>'Pengawasan Audio / video yang kuat',
        'liveAV'=>'Nikmati siaran langsung Audio & Video dari lingkungan telepon.',
        'liveVideo'=>'Tonton Video Langsung tanpa Tunda.',
        'clearAudio'=>'Dengarkan audio ponsel secara langsung dengan suara jernih.',
        'capturePhoto'=>'Ambil Foto Langsung. (Menggunakan Kedua camera)',
        'screenshoot'=>'Cuplikan layar langsung dari ponsel.',

        'monSosmed'=>'Monitor Semua kegiatan sosial',
        'voip'=>'Rekam panggilan VOIP',

        ]
?>
