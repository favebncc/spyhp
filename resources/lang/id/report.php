<?php

    return [
        'header'=>'Laporkan pelanggaran',
        'text'=>'SPYHP hanya akan digunakan dengan tujuan agar orang tua bisa mengontrol dan memantau anak-anak mereka. SPYHP tidak menganjurkan dan tidak seharusnya digunakan untuk memantau orang lain secara rahasia. Yang merupakan pelanggaran privasi. Jika anda menemukan perangkat anda sedang dipantau oleh orang lain menggunakan aplikasi SPYHP tanpa izin valid dari anda, Tolong laporkan penyalahgunaan / pelanggaran tersebut disini dan berikan informasi yang diperlukan sehingga kami dapat menghentikan layanan pemantauan dan memblokir akun pelanggar.',
        'imei'=>'IMEI Perangkat Anda',
        'email'=>'Email Anda',
    ]
?>
